////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file screening.hpp
//! \brief A file to define screening routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef NNP_SCREENER_BASE_HPP
#define NNP_SCREENER_BASE_HPP

#define S_SCREENING       "screening"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// screener_base().
//##############################################################################

class screener_base
{

  public:
    screener_base(){}
    screener_base( v_map_t& v_map )
    {
      b_screen = v_map[S_SCREENING].as<bool>();
    }

    bool useScreening()
    {
      return b_screen;
    }

  private:
    bool b_screen;

};

//##############################################################################
// screener_base_options().
//##############################################################################

class screener_base_options
{

  public:
    screener_base_options() { }

    void
    getBaseOptions( po::options_description& screener )
    {

      try
      {

        screener.add_options()

          ( S_SCREENING,
            po::value<bool>()->default_value( false, "false" ),
            "Use screening" )

        ;
 
// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
   }

};

}  // namespace detail

}  // namespace wn_user

#endif  // NNP_SCREENER_BASE_HPP
