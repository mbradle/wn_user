//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header code to compute properties of stars.
////////////////////////////////////////////////////////////////////////////////

#include <boost/any.hpp>

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef WN_STAR_BASE_HPP
#define WN_STAR_BASE_HPP

namespace wn_user
{

namespace base
{

typedef std::map<std::string, std::string> property_map_t;

class star_system
{

  public:
    star_system(){}

    template<typename T = double>
    T getProperty( std::string s_property ) const
    {
      property_map_t::const_iterator it =
         propertyMap.find( s_property );
      if( it == propertyMap.end() )
      {
	std::cerr << "Star does not have property " << s_property << ".\n";
	exit( EXIT_FAILURE );
      }
      return boost::lexical_cast<T>( it->second );
    }

    template<typename T>
    void updateProperty( const std::string s_property, T value )
    {
      propertyMap[s_property] = boost::lexical_cast<std::string>( value );
    }

    std::vector<std::string>
    getVectorOfProperties()
    {
      std::vector<std::string> result;
      BOOST_FOREACH( property_map_t::value_type& t, propertyMap )
      {
	result.push_back( t.first );
      }
      return result;
    }

  private:
    property_map_t propertyMap;

};

} // namespace detail

} // namespace wn_user

#endif // WN_STAR_BASE_HPP
