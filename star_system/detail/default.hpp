//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header code to compute properties of stars.
////////////////////////////////////////////////////////////////////////////////

#include <math.h>

#include "star_system/base/star_system.hpp"

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef WN_STAR_DETAIL_HPP
#define WN_STAR_DETAIL_HPP

#define D_YEAR    3.15e7

#include <boost/tuple/tuple.hpp>

namespace wn_user
{

namespace detail
{

#define  S_STANDARD_DEVIATION  "standard deviation"

class Star : public base::star_system
{

  public:
    Star() : base::star_system() {}

    double
    computeLifetime( double d_star_mass )
    {
      return 1.e10 * D_YEAR / pow( d_star_mass, 2 );
    }

    void
    setGridLocation( boost::tuple<int,int,int> t )
    {
      updateProperty( nnt::s_GRID_X, t.get<0>() );
      updateProperty( nnt::s_GRID_Y, t.get<1>() );
      updateProperty( nnt::s_GRID_Z, t.get<2>() );
    }

    void
    setSubGridLocation( boost::tuple<double,double,double> u )
    {
      updateProperty( nnt::s_GRID_X_FACTOR, u.get<0>() );
      updateProperty( nnt::s_GRID_Y_FACTOR, u.get<1>() );
      updateProperty( nnt::s_GRID_Z_FACTOR, u.get<2>() );
    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_STAR_DETAIL_HPP
