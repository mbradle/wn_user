//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header code to compute properties of stars.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <boost/heap/fibonacci_heap.hpp>
#include <boost/random/discrete_distribution.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/poisson_distribution.hpp>
#include <boost/random/variate_generator.hpp>

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef WN_STAR_SYSTEM_HPP
#define WN_STAR_SYSTEM_HPP

//##############################################################################
// Defines.
//##############################################################################

#define S_BLACK_HOLE       "black hole"
#define S_STAR             "star"
#define S_NEUTRON_STAR     "neutron star"
#define S_WHITE_DWARF      "white dwarf"
#define S_C_WHITE_DWARF    "chandrasekhar white dwarf"

#define S_CURRENT_MASS     "current mass"
#define S_END_TIME         "end time"
#define S_FORMATION_TIME   "formation time"
#define S_LIFE_TIME        "life time"
#define S_METALLICITY      "metallicity"
#define S_ORIGINAL_MASS    "original mass"

namespace wn_user
{

//##############################################################################
// Types.
//##############################################################################

struct StarSystemGenerator
{
    StarSystemGenerator(){} 
    size_t operator()( boost::mt19937& gen )
    {
      double p[] = {0.5,0.5};
//      double p[] = {1.,0.};
      boost::random::discrete_distribution<> dist( p );
      return dist( gen ) + 1;  // dist is zero-indexed--add 1
    }

};

class Star : public detail::Star
{

  public:
    Star() : detail::Star(){}
    Star( double d_star_mass, double d_formation_time ) : detail::Star()
    {
      updateProperty( S_FORMATION_TIME, d_formation_time );
      updateProperty( S_ORIGINAL_MASS, d_star_mass );
      updateProperty( S_LIFE_TIME, computeLifetime( d_star_mass ) );
      updateProperty( S_CURRENT_MASS, d_star_mass );
      status = S_STAR;
      updateProperty(
        S_END_TIME,
	getProperty( S_FORMATION_TIME ) + getProperty( S_LIFE_TIME )
      );
    }

    bool operator < (const Star& s) const
    {
      return( getProperty( S_END_TIME ) > s.getProperty( S_END_TIME ) );
    }

    std::string getStatus() const { return status; }
    void updateStatus( std::string s ) { status = s; }

  private:
    std::string status;
};

typedef boost::heap::fibonacci_heap<Star> star_heap_t;

class StarSystem
{

  public:
    StarSystem(){}
    void addStar( Star star ){ star_heap.push( star ); }
    Star getTopStar() const { return star_heap.top(); }
    void popTopStar() { star_heap.pop(); }
    star_heap_t getStarHeap() const { return star_heap; }
    size_t getNumberOfStars()
    {
      if( star_heap.empty() )
        return 0;
      else
        return star_heap.size();
    }
    bool operator<(const StarSystem& s) const
    {
        return(
	  getTopStar().getProperty( S_END_TIME ) >
	  s.getTopStar().getProperty( S_END_TIME )
	);
    }
    std::vector<Star>
    getStarVector() const
    {
      std::vector<Star> v;

      for(
        star_heap_t::ordered_iterator it = star_heap.ordered_begin();
        it != star_heap.ordered_end();
        it++
      )
      { v.push_back( *it ); }

      return v;
    }


  private:
    star_heap_t star_heap;
    double life_time;
    std::string type;

};

typedef boost::heap::fibonacci_heap<StarSystem> star_system_heap_t;

} // wn_user

#endif // WN_STAR_SYSTEM_HPP
