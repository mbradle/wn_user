//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to compute stellar populations.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef WN_IMF_HPP
#define WN_IMF_HPP

#define S_DEVIATE_FILE   "deviate_file"
#define S_IMF_FILE       "imf_file"

//##############################################################################
// Includes.
//##############################################################################

#include <boost/numeric/odeint.hpp>
#include <type_traits>

//##############################################################################
// Namespace.
//##############################################################################

namespace wn_user
{

//##############################################################################
// imf().
//##############################################################################

class imf : public detail::imf
{

  public:

    imf() : detail::imf() {}

    imf( v_map_t& v_map ) : detail::imf( v_map )
    {
      init( v_map, NULL );
    }

    imf( v_map_t& v_map, void * p_data ) : detail::imf( v_map )
    {
      init( v_map, p_data );
    }

    void init( v_map_t& v_map, void * p_data )
    {
      typedef boost::numeric::odeint::runge_kutta_dopri5<double> stepper_type;

      if( v_map.count(S_DEVIATE_FILE) != 0 )
      {
        s_deviate_file = v_map[S_DEVIATE_FILE].as<std::string>();
      }

      if( v_map.count(S_IMF_FILE) != 0 )
      {
        s_imf_file = v_map[S_IMF_FILE].as<std::string>();
      }

      size_t n = getNumberImfPoints();

      double d_lmin = log10( getLowerMass() );

      double d_lmax = log10( getUpperMass() );

      double delta = ( d_lmax - d_lmin ) / static_cast<double>( n );

      const double dm = delta / 10.;

      for( size_t i = 0 ; i <= n; ++i )
      {
        masses.push_back(
          pow( 10., d_lmin + delta * i )
        );
      }

      double y = 0.;

      boost::numeric::odeint::integrate_times(
        boost::numeric::odeint::make_controlled(
          1E-12,
          1E-12,
          stepper_type()
        ),
        boost::bind(
          &detail::imf::computeIntegrandValue,
          this,
          _1,
          _2,
          _3,
          p_data
        ),
        y,
        masses,
        dm,
        boost::bind( &imf::deviateObserver, this, _1, _2 )
      );

      norm = v[v.size()-1].first;

      for( size_t i = 0; i < v.size(); i++ )
      {
        v[i].first /= norm;
      }

    }

    void deviateObserver( const double &y, const double m )
    {
      v.push_back( std::make_pair( y, m ) );
    }

    double getNewStarMass( double x )
    {
      if( x > v.back().first || x < v[0].first )
      {
        std::cerr << "Invalid input to deviate routine." << std::endl;
        exit( EXIT_FAILURE );
      }
      std::vector<std::pair<double, double> >::iterator it, it2;
      it = lower_bound( v.begin(), v.end(), std::make_pair( x, -1. ) );
      if( it == v.begin() ) return it->second;
      it2 = it;
      --it2;
      return
        it2->second +
        ( it->second - it2->second ) *
        ( x - it2->first ) /( it->first - it2->first );
    }

    double getDeviate( double m )
    {
      if( m > v.back().second || m < v[0].second )
      {
        std::cerr << "m = " << m <<
          " is invalid input to deviate routine." << std::endl;
        exit( EXIT_FAILURE );
      }
      for( size_t i = 0; i < v.size() - 1; i++ )
      {
        if( m >= v[i].second && m < v[i+1].second )
        {
          return
            v[i].first +
            ( v[i+1].first - v[i].first ) * ( m - v[i].second ) /
            ( v[i+1].second - v[i].second );
        }
      }
      return v[v.size()-1].first;
    }

    double getNorm() {return norm;}

    void
    outputDeviate()
    {
      if( !s_deviate_file.empty() )
      {
        std::ofstream my_file;
        my_file.open( s_deviate_file.c_str() );

        for( size_t i = 0; i < v.size(); i++ )
        {
          my_file << v[i].second << "  " << v[i].first << std::endl;
        }

        my_file.close();
      }
    }

    void
    outputIMF()
    {
      if( !s_imf_file.empty() )
      {
        std::ofstream my_file;
        my_file.open( s_imf_file.c_str() );

        BOOST_FOREACH( double& mass, masses )
        {
          my_file <<
            mass << "  " << computeIntegrand( mass ) / norm << std::endl;
        }

        my_file.close();
      }
    }

    private:
      std::vector<std::pair<double, double> > v;
      std::vector<double> masses;
      double norm;
      std::string s_deviate_file, s_imf_file;

};

//##############################################################################
// imf_options().
//##############################################################################

class imf_options : public detail::imf_options
{

  public:
    imf_options() : detail::imf_options(){}

    std::string
    getExample()
    {
      return detail::imf_options::getExample();
    }

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description imf( "\nIMF Options" );

        imf.add_options()

        // Option for name of deviate file.
        ( S_DEVIATE_FILE, po::value<std::string>(),
          "Name of IMF deviate output file (default: no output)")

        // Option for name of imf file.
        ( S_IMF_FILE, po::value<std::string>(),
          "Name of IMF output file (default: no output)")

        ;

        detail::imf_options::get( imf );

        o_map.insert(
         std::make_pair<std::string, options_struct>(
           "imf",
           options_struct( imf, getExample() )
         )
       );

       // Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace wn_user

#endif // WN_IMF_HPP

