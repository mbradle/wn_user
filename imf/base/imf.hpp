////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file imf_base.hpp
//! \brief A file to define stellar population routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#ifndef WN_IMF_BASE_HPP
#define WN_IMF_BASE_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

#define S_IMF_ML                "imf_ml"
#define S_IMF_MU                "imf_mu"
#define S_IMF_PTS               "imf_points"

//##############################################################################
// imf().
//##############################################################################

class imf
{

  public:
    imf(){}
    imf( v_map_t& v_map )
    {
      ml = v_map[S_IMF_ML].as<double>();
      mu = v_map[S_IMF_MU].as<double>();
      n_pts = v_map[S_IMF_PTS].as<size_t>();
    }

    double getLowerMass() const { return ml; }
    double getUpperMass() const { return mu; }
    double getNumberImfPoints() const { return n_pts; }

  private:
    double ml, mu;
    size_t n_pts;
    

};
    
//##############################################################################
// imf_options().
//##############################################################################

class imf_options
{

  public:
    imf_options() {} 

    std::string
    getExample()
    {
      return "";
    }

    void
    get( po::options_description& imf )
    {

      try
      {

        imf.add_options()

        // Option for lower mass limit for imf
        ( S_IMF_ML, po::value<double>()->default_value( 0.01, "0.01" ),
          "Lower limit of star mass for IMF" )

        // Option for upper mass limit for imf
        ( S_IMF_MU, po::value<double>()->default_value( 100., "100" ),
          "Upper limit of star mass for IMF" )

        // Option for number of imf points
        ( S_IMF_PTS, po::value<size_t>()->default_value( 100 ),
          "Number of mass points for IMF" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif  // WN_IMF_BASE_HPP
