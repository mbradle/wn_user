////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file imf.hpp
//! \brief A file to define the IMF (Kroupa).
//!
////////////////////////////////////////////////////////////////////////////////

#include "imf/base/imf.hpp"

#ifndef WN_IMF_DETAIL_HPP
#define WN_IMF_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// imf_detail().
//##############################################################################

class imf : public base::imf
{

  public:
    imf() : base::imf() {}
    imf( v_map_t& v_map ) : base::imf( v_map )
    {
      C2 = pow( m1, alpha2 - alpha1 );
      C3 = pow( m2, alpha3 - alpha2 ) * C2;
    }

    double computeIntegrand( const double m )
    {
      if( m >= getLowerMass() && m < m1 )
        return pow( m, -alpha1 );
      else if( m >= m1 && m < m2 )
        return C2 * pow( m, -alpha2 );
      else if( m >= m2 && m <= getUpperMass() + 1.e-8 )
        return C3 * pow( m, -alpha3 );
      else
      {
        std::cerr << m << " is an invalid mass for IMF." << std::endl;
        exit( EXIT_FAILURE );
      }
    }

    void
    computeIntegrandValue(
      const double y , double &dydm , const double m, void * p_data
    )
    {
      if( p_data )
      {
        std::cerr << "No extra data to this function." << std::endl;
        exit( EXIT_FAILURE );
      }
      dydm = computeIntegrand( m );
    }

  private:
    double m1 = 0.08, m2 = 0.5;
    double alpha1 = 0.3, alpha2 = 1.3, alpha3 = 2.3;
    double C2, C3;

};
    
//##############################################################################
// imf_options().
//##############################################################################

class imf_options : public base::imf_options
{

  public:
    imf_options() : base::imf_options() {} 

    std::string
    getExample()
    {
      return "";
    }

    void
    get( po::options_description& imf )
    {

      try
      {

        base::imf_options::get( imf );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // detail

} // namespace wn_user

#endif  // WN_IMF_DETAIL_HPP
