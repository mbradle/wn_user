////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file 
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#ifndef WN_NETWORK_DATA_NUCNET_BASE_HPP
#define WN_NETWORK_DATA_NUCNET_BASE_HPP

namespace wn_user
{

namespace base
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class nucnet
///
/// \brief A class to handle the base structure for the network data.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// nucnet().
//##############################################################################

class nucnet
{

  public:
    nucnet( )
    {
      pNucnet = Libnucnet__new();
    }
    ~nucnet(){ Libnucnet__free( pNucnet ); }

/// Routine to retrieve the full network data structure (network plus zones).
/// \return The pointer to the Nucnet structure.

    Libnucnet * getNucnet() { return pNucnet; }

/// Routine to retrieve the network data structure.
/// \return The pointer to the network structure.

    Libnucnet__Net * getNetwork() { return Libnucnet__getNet( pNucnet ); }

  private:
    Libnucnet * pNucnet;

};
    
} // namespace base

} // namespace wn_user

#endif // WN_NETWORK_DATA_XML_BASE_HPP
