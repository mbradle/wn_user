////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"
#include "nnt/string_defs.h"
#include <boost/algorithm/string.hpp>

#include "network_data/base/nucnet.hpp"
#include "program_options/program_options.hpp"

#ifndef WN_NETWORK_DATA_XML_BASE_HPP
#define WN_NETWORK_DATA_XML_BASE_HPP

namespace wn_user
{

namespace base
{

/// \var DATA_FILE
/// \brief Option variable for the input XML file.
struct
{
  const char * name = "network_xml";
  const char * comment="Input network data XML file";
} DATA_FILE;

/// \var EXTRA_NUC_XML
/// \brief Option variable for the extra nuclide XML data files.
struct
{
  const char * name = "extra_nuc_xml";
  const char * comment="Extra input nuclide data XML file(s)";
} EXTRA_NUC_XML;

/// \var EXTRA_REAC_XML
/// \brief Option variable for the extra reaction XML data files.
struct
{
  const char * name = "extra_reac_xml";
  const char * comment="Extra input reaction data XML file(s)";
} EXTRA_REAC_XML;

/// \var VALIDATE
/// \brief Option variable to validate the XML
struct
{
  const char * name = "validate";
  bool default_value = false;
  const char * default_value_string = "false";
  const char * comment= "Validate the XML";
} VALIDATE;

////////////////////////////////////////////////////////////////////////////////
///
/// \class xml
///
/// \brief A class to handle network xml data.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// xml().
//##############################################################################

class xml : public nucnet
{

  public:
/// The constructor.
/// \param v_map The options variable map.

    xml( v_map_t& v_map ) : nucnet(),
      my_program_options()
    {
      s_file_name = v_map[DATA_FILE.name].as<std::string>();
      if( v_map.count( EXTRA_NUC_XML.name ) )
      {
        v_extra_nuc =
          my_program_options.getVectorOfStrings( v_map, EXTRA_NUC_XML.name );
      }
      if( v_map.count( EXTRA_REAC_XML.name ) )
      {
        v_extra_reac =
          my_program_options.getVectorOfStrings( v_map, EXTRA_REAC_XML.name );
      }
      if( v_map[VALIDATE.name].as<bool>() )
      {
        if( !Libnucnet__Net__is_valid_input_xml( s_file_name.c_str() ) )
        {
          std::cerr << s_file_name << " is not a valid input network xml file."
                    << std::endl;
          exit( EXIT_FAILURE );
        }
        BOOST_FOREACH( std::string s, v_extra_nuc )
        {
          if( !Libnucnet__Nuc__is_valid_input_xml( s.c_str() ) )
          {
            std::cerr << s << " is not a valid input nuclear data xml file."
                    << std::endl;
            exit( EXIT_FAILURE );
          }
          
        }
        BOOST_FOREACH( std::string s, v_extra_reac )
        {
          if( !Libnucnet__Reac__is_valid_input_xml( s.c_str() ) )
          {
            std::cerr << s << " is not a valid input reaction data xml file."
                    << std::endl;
            exit( EXIT_FAILURE );
          }
        }
      }
    }

/// Routine to return the input xml file.
/// \return A string giving the xml file name.

    std::string
    getFileName() { return s_file_name; }

/// Routine to return the xml files with extra nuclide data.
/// \return A vector giving the xml file names.

    std::vector<std::string>
    getExtraNucFiles() { return v_extra_nuc; }

/// Routine to return the xml files with extra reaction data.
/// \return A vector giving the xml file names.

    std::vector<std::string>
    getExtraReacFiles() { return v_extra_reac; }

  private:
    program_options my_program_options;
    std::string s_file_name, s_nuc_xpath, s_reac_xpath;
    std::vector<std::string> v_extra_nuc, v_extra_reac;

};
    
////////////////////////////////////////////////////////////////////////////////
///
/// \class xml_options
///
/// \brief A class to handle network xml data options.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// xml_options().
//##############################################################################

class xml_options
{

  public:
    xml_options(){}

/// Routine to return the base xml example string.
/// \return The example string.

    std::string
    getExample()
    {

      return
        "--" + std::string( DATA_FILE.name ) +
               " ../nucnet-tools-code/data_pub/my_net.xml ";

    }

/// Routine to update network data xml option descriptions.
/// \return On successful return, the option descriptions have been updated.

    void
    getXmlOptions( po::options_description& network_data_desc )
    {

      try
      {

        network_data_desc.add_options()

        // Option for input network data file
        (
          DATA_FILE.name, po::value<std::string>()->required(),
          DATA_FILE.comment
        )

        (
          EXTRA_NUC_XML.name,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          EXTRA_NUC_XML.comment
        )

        (
          EXTRA_REAC_XML.name,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          EXTRA_REAC_XML.comment
        )

        (
          VALIDATE.name,
          po::value<bool>()->default_value(
            VALIDATE.default_value,
            VALIDATE.default_value_string
          ),
          VALIDATE.comment
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // WN_NETWORK_DATA_XML_BASE_HPP
