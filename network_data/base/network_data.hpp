////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file
//!
////////////////////////////////////////////////////////////////////////////////

#include "xml.hpp"
#include "utility/isolated_species.hpp"

#ifndef WN_NETWORK_DATA_BASE_HPP
#define NW_NETWORK_DATA_BASE_HPP

#define S_NUC_XPATH       "nuc_xpath"
#define S_REAC_XPATH      "reac_xpath"
#define S_REMOVE_ISOLATED "remove_isolated_species"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

/// \var NUC_XPATH
/// \brief Option variable for value of XPath to select nuclei. 
struct
{
  const char * name="nuc_xpath";
  const char * default_value="";
  const char * comment="XPath to select nuclei (default: all nuclides)";
} NUC_XPATH;

/// \var REAC_XPATH
/// \brief Option variable for value of XPath to select reactions. 
struct
{
  const char * name="reac_xpath";
  const char * default_value="";
  const char * comment="XPath to select reactions (default: all reactions)";
} REAC_XPATH;

////////////////////////////////////////////////////////////////////////////////
///
/// \class network_data
///
/// \brief A class to handle common network data.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// network_data().
//##############################################################################

class network_data : public xml, public utility::isolated_species
{

  public:
/// The constructor.
/// \param The variable options map.

    network_data( v_map_t& v_map ) : xml( v_map ), utility::isolated_species()
    {
      s_nuc_xpath = v_map[S_NUC_XPATH].as<std::string>();
      s_reac_xpath = v_map[S_REAC_XPATH].as<std::string>();
      b_remove_isolated = v_map[S_REMOVE_ISOLATED].as<bool>();
    }

/// Routine to set the nuclear data from a data file.

    void
    setData()
    {
      Libnucnet__Net__updateFromXml(
        getNetwork(),
        getFileName().c_str(),
        s_nuc_xpath.c_str(),
        s_reac_xpath.c_str()
      );

      BOOST_FOREACH( std::string s, getExtraNucFiles() )
      {
        Libnucnet__Nuc__updateFromXml(
          Libnucnet__Net__getNuc(
            getNetwork()
          ),
          s.c_str(),
          s_nuc_xpath.c_str()
        );
      }

      BOOST_FOREACH( std::string s, getExtraReacFiles() )
      {
        Libnucnet__Reac__updateFromXml(
          Libnucnet__Net__getReac(
            getNetwork()
          ),
          s.c_str(),
          s_reac_xpath.c_str()
        );
      }

      if( b_remove_isolated )
      {
        utility::isolated_species::remove( getNetwork(), "", "" );
      }

    }

/// Routine to retrieve the XPath expression for the nuclide data.
///
/// \return The XPath string.

    std::string
    getNucXPath() { return s_nuc_xpath; }

/// Routine to retrieve the XPath expression for the reaction data.
///
/// \return The XPath string.

    std::string
    getReacXPath() { return s_reac_xpath; }

  private:
    std::string s_nuc_xpath, s_reac_xpath;
    bool b_remove_isolated;

};
    
////////////////////////////////////////////////////////////////////////////////
///
/// \class network_data_options
///
/// \brief A class to handle common network data options.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// network_data_options().
//##############################################################################

class network_data_options : public xml_options
{

  public:
    network_data_options() : xml_options(){}

/// Routine to retrieve the base example string.
/// \return The example string.

    std::string
    getExample()
    {

      return
        xml_options::getExample() + " " +
        "--" + std::string( NUC_XPATH.name ) + " \"[z <= 30]\" ";

    }

/// Routine to update the base network data option descriptions.
///
/// \param options_desc The option descriptions.
///
/// \return On successful return, the option descriptions have been updated.

    void
    get( po::options_description& options_desc )
    {

      try
      {

        getXmlOptions( options_desc );

        options_desc.add_options()

        (
          NUC_XPATH.name,
          po::value<std::string>()->default_value( NUC_XPATH.default_value ),
          NUC_XPATH.comment
        )
    
        (
          REAC_XPATH.name,
          po::value<std::string>()->default_value( REAC_XPATH.default_value ),
          REAC_XPATH.comment
        )
    
        // Option to remove isolated species
        (
          S_REMOVE_ISOLATED,
          po::value<bool>()->default_value( false, "false" ),
          "Remove isolated species."
        )
    
        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // WN_NETWORK_DATA_BASE_HPP
