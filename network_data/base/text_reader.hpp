////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file 
//!
////////////////////////////////////////////////////////////////////////////////

#include "xml.hpp"

#ifndef WN_NETWORK_DATA_TEXT_READER_BASE_HPP
#define WN_NETWORK_DATA_TEXT_READER_BASE_HPP

namespace wn_user
{

namespace base
{

/// \var NUC_XPATH_TEXT
/// \brief Option variable for value of XPath to select nuclei

struct
{
  const char * name = "nuc_xpath_text";
  const char * default_value = ".";
  const char * comment = "XPath to select nuclei (default: all nuclides)";
} NUC_XPATH_TEXT;

/// \var REAC_XPATH_TEXT
/// \brief Option variable for value of XPath to select reactions

struct
{
  const char * name = "reac_xpath_text";
  const char * default_value = ".";
  const char * comment = "XPath to select reactions (default: all reactions)";
} REAC_XPATH_TEXT;

////////////////////////////////////////////////////////////////////////////////
///
/// \class text_reader
///
/// \brief A class to handle network data from the xml text reader.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// text_reader().
//##############################################################################

class text_reader : public xml
{

  public:
/// The constructor.
/// \param v_map The options variable map.

    text_reader( v_map_t& v_map ) : xml( v_map )
    {
      s_nuc_xpath = v_map[NUC_XPATH_TEXT.name].as<std::string>();
      s_reac_xpath = v_map[REAC_XPATH_TEXT.name].as<std::string>();
    }

/// Routine to retrieve the XPath string for the nuclide data.
/// \return The XPath string.

    std::string
    getNucXPath() { return s_nuc_xpath; }

/// Routine to retrieve the XPath string for the reaction data.
/// \return The XPath string.

    std::string
    getReacXPath() { return s_reac_xpath; }

    void
    setData()
    {
      Libnucnet__Net__updateFromXmlTextReader(
        getNetwork(),
        getFileName().c_str(),
        s_nuc_xpath.c_str(),
        s_reac_xpath.c_str()
      );

      BOOST_FOREACH( std::string s, getExtraNucFiles() )
      {
        Libnucnet__Nuc__updateFromXmlTextReader(
          Libnucnet__Net__getNuc( getNetwork() ),
          s.c_str(),
          s_nuc_xpath.c_str()
        );
      }

      BOOST_FOREACH( std::string s, getExtraReacFiles() )
      {
        Libnucnet__Reac__updateFromXmlTextReader(
          Libnucnet__Net__getReac( getNetwork() ),
          s.c_str(),
          s_reac_xpath.c_str()
        );
      }
    }

  private:
    std::string s_nuc_xpath, s_reac_xpath;

};
    
////////////////////////////////////////////////////////////////////////////////
///
/// \class text_reader_options
///
/// \brief A class to handle network data options from the xml text reader.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// text_reader_options().
//##############################################################################

class text_reader_options : public xml_options
{

  public:
    text_reader_options() : xml_options(){}

/// Routine to return an example string for the network data text reader
/// options.

    std::string
    getExample()
    {

      return
        xml_options::getExample() + " " +
        "--" + std::string( S_NUC_XPATH_TEXT ) + " \".//z[. <= 30]\" ";

    }

/// Routine to update network data xml text reader option descriptions.
/// \return On successful return, the option descriptions have been updated.

    void
    get( po::options_description& options_desc )
    {

      try
      {

        getXmlOptions( options_desc );

        options_desc.add_options()

        (
          NUC_XPATH_TEXT.name,
          po::value<std::string>()->default_value(
            NUC_XPATH_TEXT.default_value
          ),
          NUC_XPATH_TEXT.comment
        )
    
        (
          REAC_XPATH_TEXT.name,
          po::value<std::string>()->default_value(
            REAC_XPATH_TEXT.default_value
          ),
          REAC_XPATH_TEXT.comment
        )
    
        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // WN_NETWORK_DATA_TEXT_READER_BASE_HPP
