////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

/// @cond _NETWORK_DATA_SIMPLE_

////////////////////////////////////////////////////////////////////////////////
//!
//! \file simple.hpp
//! \brief A file to define network_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/assign/list_of.hpp>
#include <boost/math/special_functions/binomial.hpp>

#include "my_global_types.h"
#include "nnt/iter.h"
#include "math/bernoulli.hpp"

#include "network_data/base/nucnet.hpp"

#ifndef WN_NETWORK_DATA_DETAIL_HPP
#define WN_NETWORK_DATA_DETAIL_HPP

namespace wn_user
{

namespace detail
{

#define S_SN           "Sn"
#define S_Q0           "Q0"
#define S_C1           "C1"
#define S_C2           "C2"
#define S_C3           "C3"
#define S_Z_TILDE      "z_tilde"
#define S_A_TILDE      "a_tilde"
#define S_Z1           "Z1"
#define S_Z2           "Z2"
#define S_LAMBDA_N     "lambda_n"
#define S_LAMBDA_BETA  "lambda_beta"
#define S_M_0          "m_0"
#define S_P_DELTA      "pairing_delta"
#define S_N_SHELL      "n_shell"

//##############################################################################
// network_data().
//##############################################################################

class network_data : public base::nucnet
{

  public:
    network_data( v_map_t& v_map ) : base::nucnet(),
      my_program_options(), my_bernoulli()
    {
      set_parameters( v_map );
      set_nuclides();
      set_rates();
    }

  private:
    typedef std::vector<std::pair<size_t, double> > v_shell_t;
    program_options my_program_options;
    wn_user::math::bernoulli<double> my_bernoulli;
    std::vector<double> v_sn, v_qb;
    v_shell_t v_n_shell;
    double m_0, pairingDelta;
    double Q0, C1, C2, C3;
    double lambda_n, lambda_b, delta_n, delta_p;
    size_t Z1, Z2, Ztilde, Atilde;

    void
    set_parameters( v_map_t& v_map )
    {

      delta_n = 8.071;
      delta_p = 7.279;

      if( v_map.count( S_SN ) )
      {
        BOOST_FOREACH(
          const std::vector<std::vector<std::string> >::value_type& v_m,
          my_program_options.composeOptionVectorOfVectors(
            v_map, S_SN, ",", "", 1
          )
        )
        {
          v_sn.push_back( boost::lexical_cast<double>( v_m[0] ) );
        }
      }
      else
      {
        v_sn.push_back( 20. );
	v_sn.push_back( 0.5 );
      }
      Q0 = v_map[S_Q0].as<double>();

      C1 = v_map[S_C1].as<double>();
      C2 = v_map[S_C2].as<double>();
      C3 = v_map[S_C3].as<double>();

      Z1 = v_map[S_Z1].as<size_t>();
      Z2 = v_map[S_Z2].as<size_t>();
      Ztilde = v_map[S_Z_TILDE].as<size_t>();
      Atilde = v_map[S_A_TILDE].as<size_t>();

      pairingDelta = v_map[S_P_DELTA].as<double>();

      if( v_map.count( S_N_SHELL ) )
      {
        BOOST_FOREACH(
          const std::vector<std::vector<std::string> >::value_type& v_x,
          my_program_options.composeOptionVectorOfVectors(
            v_map, S_N_SHELL, 2
          )
        )
        {
          v_n_shell.push_back(
            std::make_pair(
              boost::lexical_cast<size_t>( v_x[0] ),
              boost::lexical_cast<double>( v_x[1] )
            )
          );
        }
      }

      compute_qbeta();

      lambda_n = v_map[S_LAMBDA_N].as<double>();
      lambda_b = v_map[S_LAMBDA_BETA].as<double>();
      m_0 = v_map[S_M_0].as<double>();
    }

    double pi_function( size_t x )
    {
      if( x % 2 == 0 )
      {
        return 0;
      }
      else
      {
        return 1;
      }
    }

    double theta_function( int x )
    {
      if( x > 0 )
      {
        return 1;
      }
      else
      {
        return 0;
      }
    }

    double Z0( double A )
    {
      return ( A - C1 ) / ( C2 + 1 );
    }

    double A0( double Z )
    {
      return C1 + ( C2 + 1 ) * Z;
    }

    void
    compute_qbeta()
    {
      v_qb.resize( v_sn.size() );
      v_qb[0] = Q0;

      if( v_sn.size() == 1 ) { return; }

      size_t km = v_sn.size() - 1;
      for( size_t n = 0; n <= km - 1; n++ )
      {
        double part1 = 0;
        for( size_t k = km - n; k <= km; k++ )
        {
          part1 +=
            boost::math::binomial_coefficient<double>( k, km - n ) *
            v_sn[k] * pow( C2, k - (km - n) );
        }
    
        double part2 = 0;
        for( size_t k = km - n + 1; k <= km; k++ )
        {
          for( size_t p = km - n - 1; p <= k - 1; p++ )
          {
            double part3 = 0;
            for( size_t i = 0; i <= k - p - 1; i++ )
            {
              size_t m = p + 1 + n - km;
              part3 +=
                boost::math::binomial_coefficient<double>( k - p, i ) *
                boost::math::binomial_coefficient<double>( p + 1, m ) *
                pow( C2, i ) *
                my_bernoulli.getNumber( m ) / ( p + 1 );
            }
            part2 +=
              pow( -1., k ) *
              boost::math::binomial_coefficient<double>( k, p ) *
              v_qb[k] *
              part3 / ( pow( C2 + 1, k - p ) );
          }
        }
    
        part2 /= pow( C2 + 1, km - n );
                 
        v_qb[km - n] =
          pow( -1, km - 1 - n ) *
          pow( ( C2 + 1 ), km + 1 - n ) *
          ( part1 + part2 );
      } 
    }

    double delta( double Z, double A )
    { 
      double result = delta_n * ( A - Atilde );
      for( size_t k = 0; k < v_sn.size(); k++ )
      {
        for( size_t p = 0; p <= k; p++ )
        {
          result -=
            pow( -1., p ) *
            boost::math::binomial_coefficient<double>( k, p ) *
            (
              v_sn[k] * pow( A0( Ztilde ), k - p ) *
              (
                my_bernoulli.computePolynomial( p + 1, A + 1 )
                -
                my_bernoulli.computePolynomial( p + 1, Atilde + 1 )
              ) / ( p + 1 )
              +
              v_qb[k] * pow( Z0( A ), k - p ) *
              (
                my_bernoulli.computePolynomial( p + 1, Z )
                -
                my_bernoulli.computePolynomial( p + 1, Ztilde )
              ) / ( p + 1 )
            );
        }
      }

      result += pairingDelta * ( pi_function( (size_t) A - Z ) - 1 ); 

      BOOST_FOREACH( v_shell_t::value_type& t, v_n_shell )
      {
        double d_N = A - Z - t.first;
        result += t.second * theta_function( d_N ) * ( d_N );
      }

      return result;

    }
        
    void
    set_nuclides()
    {
      std::vector<double> v_t9 = boost::assign::list_of(0.01)(0.1)(1)(10.);
      gsl_vector * p_t9 = gsl_vector_calloc( v_t9.size() );
      for( size_t i = 0; i < v_t9.size(); i++ )
      {
        gsl_vector_set( p_t9, i, v_t9[i] );
      }

      add_species( 0, 1, "simple", 0, "", delta_n, 0., NULL, NULL );
      add_species( 1, 1, "simple", 0, "", delta_p, 0., NULL, NULL );

      for( size_t i_z = Z1; i_z <= Z2; i_z++ )
      {
        size_t i_n0 = C1 + C2 * i_z;
        for( size_t i_a = i_n0 + i_z; i_a <= i_n0 + i_z + C3; i_a++ )
        {
          double Z = i_z;
          double A = i_a;
          double mass_excess = delta( Z, A );
          double p = -1.5 * log10( ( A * WN_AMU_TO_MEV + mass_excess ) / m_0 );
          gsl_vector * p_log10_partf = gsl_vector_calloc( v_t9.size() );
          for( size_t i = 0; i < v_t9.size(); i++ )
          {
            gsl_vector_set( p_log10_partf, i, p );
          }
          add_species(
            i_z, i_a, "simple", 0, "", mass_excess, 0., p_t9, p_log10_partf
          );
          gsl_vector_free( p_log10_partf );
        }
      }
      gsl_vector_free( p_t9 );
    }

    void
    add_species(
      size_t i_z,
      size_t i_a,
      const char *s_source,
      int i_state,
      const char *s_state,
      double d_mass_excess,
      double d_spin,
      gsl_vector *p_t9,
      gsl_vector *p_log10_partf
    )
    {
      Libnucnet__Species * p_species =
        Libnucnet__Species__new(
          i_z, i_a, s_source, i_state, s_state, d_mass_excess, d_spin, p_t9,
          p_log10_partf
        );
      Libnucnet__Nuc__addSpecies(
        Libnucnet__Net__getNuc( getNetwork() ),
        p_species
      );
    } 

    void
    set_rates()
    {
      Libnucnet__Species * p_species;
      Libnucnet__Nuc * p_nuc = Libnucnet__Net__getNuc( getNetwork() );
      Libnucnet__Species * p_neutron =
        Libnucnet__Nuc__getSpeciesByZA( p_nuc, 0, 1, "" );

      BOOST_FOREACH( nnt::Species species, nnt::make_species_list( p_nuc ) )
      {
        size_t i_z = Libnucnet__Species__getZ( species.getNucnetSpecies() );
        size_t i_a = Libnucnet__Species__getA( species.getNucnetSpecies() );
        if( i_a > 1 )
        {
          p_species = Libnucnet__Nuc__getSpeciesByZA( p_nuc, i_z, i_a + 1, "" );
          if( p_species )
          {
            Libnucnet__Reaction * p_reaction = Libnucnet__Reaction__new();
            Libnucnet__Reaction__addReactant(
              p_reaction,
              Libnucnet__Species__getName( species.getNucnetSpecies() )
            );
            Libnucnet__Reaction__addReactant(
              p_reaction, Libnucnet__Species__getName( p_neutron )
            );
            Libnucnet__Reaction__addProduct(
              p_reaction, Libnucnet__Species__getName(p_species )
            );
            Libnucnet__Reaction__addProduct( p_reaction, "gamma" );
            Libnucnet__Reaction__updateSingleRate( p_reaction, lambda_n );
            Libnucnet__Reac__addReaction(
              Libnucnet__Net__getReac( getNetwork() ),
              p_reaction
            );
          }
          p_species = Libnucnet__Nuc__getSpeciesByZA( p_nuc, i_z + 1, i_a, "" );
          if( p_species )
          {
            Libnucnet__Reaction * p_reaction = Libnucnet__Reaction__new();
            Libnucnet__Reaction__addReactant(
              p_reaction,
              Libnucnet__Species__getName( species.getNucnetSpecies() )
            );
            Libnucnet__Reaction__addProduct(
              p_reaction,
              Libnucnet__Species__getName( p_species )
            );
            Libnucnet__Reaction__addProduct( p_reaction, "electron" );
            Libnucnet__Reaction__addProduct( p_reaction, "anti-neutrino_e" );
            Libnucnet__Reaction__updateSingleRate( p_reaction, lambda_b );
            Libnucnet__Reac__addReaction(
              Libnucnet__Net__getReac( getNetwork() ),
              p_reaction
            );
          }
        }
      }
    }

};
    
//##############################################################################
// network_data_options().
//##############################################################################

class network_data_options
{

  public:
    network_data_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    get( po::options_description& options_desc )
    {

      try
      {

        options_desc.add_options()

        // Option for separation energy
        (
          S_SN,
          po::value< std::vector<std::string> >()->multitoken()->composing(),
          "Comma-delimited list of neutron-separation energy coefficients S0, S1, S2 ... (default: \"20, 0.5\")"
        )

        // Option for pairing delta
        (
          S_P_DELTA, po::value<double>()->default_value( 0, "0" ),
          "Pairing delta (MeV)"
        )

        // Option for neutron shell
        (
          S_N_SHELL,
          po::value<std::vector<std::string> >()->multitoken(),
          "Neutron shell correction (enter as doublet {N_S; C_S})"
        )

        // Option for beta q
        (
          S_Q0, po::value<double>()->default_value( 0, "0" ),
          "Q_beta at left network limit (MeV)"
        )

        // Option for network limit
        (
          S_C1, po::value<double>()->default_value( 0, "0" ),
          "Constant part of network limit"
        )

        // Option for network limit
        (
          S_C2, po::value<double>()->default_value( 1., "1." ),
          "Slope part of network limit"
        )

        // Option for network limit
        (
          S_C3, po::value<double>()->default_value( 50, "50" ),
          "Right limit of network"
        )

        // Option for anchor species for mass excess
        (
          S_Z_TILDE, po::value<size_t>()->default_value( 6, "6" ),
          "Z anchor for mass excess"
        )

        // Option for anchor species for mass excess
        (
          S_A_TILDE, po::value<size_t>()->default_value( 12, "12" ),
          "A anchor for mass excess"
        )

        // Option for lower Z of network (excluding n and p)
        (
          S_Z1, po::value<size_t>()->default_value( 26, "26" ),
          "Lower Z of network (excluding n and p)"
        )

        // Option for upper Z of network (excluding n and p)
        (
          S_Z2, po::value<size_t>()->default_value( 100, "100" ),
          "Upper Z of network (excluding n and p)"
        )

        // Neutron-capture rate
        (
          S_LAMBDA_N, po::value<double>()->default_value( 1.e4, "1.e4" ),
          "Thermally-averaged neutron-capture cross section"
        )

        // Beta-decay rate
        (
          S_LAMBDA_BETA, po::value<double>()->default_value( 100., "100." ),
          "Beta-decay rate"
        )

        // Mass scale
        (
          S_M_0, po::value<double>()->default_value( 1.e5, "1.e5" ),
          "Mass scale for partition function"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_DATA_DETAIL_HPP

/// @endcond
