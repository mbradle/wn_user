////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

/// @cond _NETWORK_DATA_AA522A25_

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define network_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/assign.hpp>

#include "my_global_types.h"
#include "network_data/base/text_reader.hpp"

#ifndef WN_NETWORK_DATA_DETAIL_HPP
#define WN_NETWORK_DATA_DETAIL_HPP

#define S_AA522A25                     "aa522a25"
#define S_AA522A25_B_FACTOR            "aa522a25 B factor"

#define S_AA522A25_NUC_XPATH           "aa522a25_nuc_xpath"
#define S_AA522A25_UPDATE_NET          "aa522a25_update_net"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// aa522a25_update_reaction().
//##############################################################################

void
aa522a25_update_reaction(
  Libnucnet__Net * p_net,
  Libnucnet__Species * p_reactant,
  Libnucnet__Species * p_product,
  std::vector<std::string>& v_other_reactants,
  std::vector<std::string>& v_other_products
)
{

  Libnucnet__Reaction * p_reaction;
  std::string s_b;

  //============================================================================
  // Set aa522a25 B factor.
  //============================================================================

  if( Libnucnet__Species__getA( p_reactant ) == 1 )
    s_b = "5.76";
  else
    s_b = "4.6";

  //============================================================================
  // Reaction data.
  //============================================================================

  p_reaction = Libnucnet__Reaction__new();

  Libnucnet__Reaction__updateSource(
    p_reaction,
    "aa522a25"
  );

  Libnucnet__Reaction__addReactant(
    p_reaction,
    Libnucnet__Species__getName( p_reactant )
  );

  Libnucnet__Reaction__addProduct(
    p_reaction,
    Libnucnet__Species__getName( p_product )
  );

  BOOST_FOREACH( std::string s, v_other_reactants )
  {
    Libnucnet__Reaction__addReactant(
      p_reaction,
      s.c_str()
    );
  }

  BOOST_FOREACH( std::string s, v_other_products )
  {
    Libnucnet__Reaction__addProduct(
      p_reaction,
      s.c_str()
    );
  }

  Libnucnet__Reaction__setUserRateFunctionKey(
    p_reaction,
    S_AA522A25
  );

  Libnucnet__Reac__removeReaction(
    Libnucnet__Net__getReac( p_net ),
    p_reaction
  );

  Libnucnet__Reaction__updateUserRateFunctionProperty(
    p_reaction,
    S_AA522A25_B_FACTOR,
    NULL,
    NULL,
    s_b.c_str()
  );

  Libnucnet__Reac__addReaction(
    Libnucnet__Net__getReac( p_net ),
    p_reaction
  );

}

//##############################################################################
// aa522a25_update_reactions().
//##############################################################################

void
aa522a25_update_reactions(
  Libnucnet__Species *p_species,
  Libnucnet__Net *p_net
)
{

  Libnucnet__Species *p_product;

  //============================================================================
  // Check that electron capture or beta+ can happen.
  //============================================================================

  if(
    Libnucnet__Species__getZ( p_species ) > 1
    ||
    (
      Libnucnet__Species__getZ( p_species ) == 1 && 
      Libnucnet__Species__getA( p_species ) < 2
    )
  )
  {

    p_product =
      Libnucnet__Nuc__getSpeciesByZA(
        Libnucnet__Net__getNuc( p_net ),
        Libnucnet__Species__getZ( p_species ) - 1,
        Libnucnet__Species__getA( p_species ),
        NULL
      );

    //--------------------------------------------------------------------------
    // Electron capture.
    //--------------------------------------------------------------------------

    if( p_product )
    {

      std::vector<std::string> v_r =
        boost::assign::list_of(nnt::s_ELECTRON);
      
      std::vector<std::string> v_p =
        boost::assign::list_of(nnt::s_NEUTRINO_E);

      aa522a25_update_reaction( p_net, p_species, p_product, v_r, v_p );
      
    }

    //--------------------------------------------------------------------------
    // Beta plus.
    //--------------------------------------------------------------------------

    if( p_product )
    {
      std::vector<std::string> v_r;
      
      std::vector<std::string> v_p =
        boost::assign::list_of(nnt::s_POSITRON)(nnt::s_NEUTRINO_E);

      aa522a25_update_reaction( p_net, p_species, p_product, v_r, v_p );
    }

  }
 
  //============================================================================
  // Beta minus and positron capture.
  //============================================================================

  p_product =
    Libnucnet__Nuc__getSpeciesByZA(
      Libnucnet__Net__getNuc( p_net ),
      Libnucnet__Species__getZ( p_species ) + 1,
      Libnucnet__Species__getA( p_species ),
      NULL
    );

  if( p_product )
  {

    //--------------------------------------------------------------------------
    // Beta minus.
    //--------------------------------------------------------------------------

    {
      std::vector<std::string> v_r;
      
      std::vector<std::string> v_p =
        boost::assign::list_of(nnt::s_ELECTRON)(nnt::s_ANTI_NEUTRINO_E);

      aa522a25_update_reaction( p_net, p_species, p_product, v_r, v_p );
    }

    //--------------------------------------------------------------------------
    // Positron capture.
    //--------------------------------------------------------------------------

    {
      std::vector<std::string> v_r =
        boost::assign::list_of(nnt::s_POSITRON);
      
      std::vector<std::string> v_p =
        boost::assign::list_of(nnt::s_ANTI_NEUTRINO_E);

      aa522a25_update_reaction( p_net, p_species, p_product, v_r, v_p );
    }

  }

}

//##############################################################################
// aa522a25_update_net().
//##############################################################################

void
aa522a25_update_net(
  Libnucnet__Net *p_net,
  const char * s_nuc_xpath
)
{

  Libnucnet__NucView * p_nuc_view =
    Libnucnet__NucView__new(
      Libnucnet__Net__getNuc( p_net ),
      s_nuc_xpath
    );

  BOOST_FOREACH(
    nnt::Species species,
    nnt::make_species_list(
      Libnucnet__NucView__getNuc( p_nuc_view )
    )
  )
  {
     aa522a25_update_reactions( species.getNucnetSpecies(), p_net );
  }

  Libnucnet__NucView__free( p_nuc_view );

}

//##############################################################################
// network_data().
//##############################################################################

class network_data : public base::text_reader
{

  public:
    network_data( v_map_t& v_map ) :
      base::text_reader( v_map ),
      my_program_options()
    {
      b_update = v_map[S_AA522A25_UPDATE_NET].as<bool>();

      s_aa522a25_nuc_xpath =
        my_program_options.composeOptionStringFromVector( 
          v_map, S_AA522A25_NUC_XPATH
        );

      base::text_reader::setData();

      if( b_update )
      {
        aa522a25_update_net(
          Libnucnet__getNet( getNucnet() ),
          s_aa522a25_nuc_xpath.c_str()
        );
      }

    }

  private:
    program_options my_program_options;
    bool b_update;
    std::string s_aa522a25_nuc_xpath;

};
    
//##############################################################################
// network_data_options().
//##############################################################################

class network_data_options : base::text_reader_options
{

  public:
    network_data_options() :
      text_reader_options() {}

    std::string
    getExample()
    {
      return base::text_reader_options::getExample();
    }

    void
    get( po::options_description& network_data )
    {

      try
      {

        network_data.add_options()

        (
          S_AA522A25_UPDATE_NET,
          po::value<bool>()->default_value( false, "false" ),
          "Update net with aa522a25 data"
        )

        // Option for value of XPath to select nuclei
        (
          S_AA522A25_NUC_XPATH,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          "Nuclear XPath for aa522a25 net update (default: all species)"
        )

        ;

        base::text_reader_options::get( network_data );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_DATA_DETAIL_HPP

/// @endcond
