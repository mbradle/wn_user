////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////


////////////////////////////////////////////////////////////////////////////////
//!
//! \file
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_data/base/network_data.hpp"

#ifndef WN_NETWORK_DATA_DETAIL_HPP
#define WN_NETWORK_DATA_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class network_data
///
/// \brief The class that implements the details for the network data.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// network_data().
//##############################################################################

class network_data : public base::network_data
{

  public:
/// The constructor.
/// \param v_map The options variable map.

    network_data( v_map_t& v_map ) : base::network_data( v_map )
    {
      setData();
    }

};
    
////////////////////////////////////////////////////////////////////////////////
///
/// \class options
///
/// \detail_options
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// network_data_options().
//##############################################################################

class network_data_options : base::network_data_options
{

  public:
    network_data_options() : base::network_data_options() {}

/// Routine to return the detail example string.
/// \return The string.

    std::string
    getExample()
    {
      return base::network_data_options::getExample();
    }

/// Routine to update the network data option descriptions detail.
///
/// \param options_desc The option descriptions.
///
/// \return On successful return, the option descriptions have been updated.

    void
    get( po::options_description& options_desc )
    {

      try
      {

        base::network_data_options::get( options_desc );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_DATA_DETAIL_HPP
