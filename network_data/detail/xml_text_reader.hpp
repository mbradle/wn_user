////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

/// @cond _NETWORK_DATA_XML_TEXT_READER_

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define network_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_data/base/text_reader.hpp"

#ifndef WN_NETWORK_DATA_DETAIL_HPP
#define WN_NETWORK_DATA_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// network_data().
//##############################################################################

class network_data : public base::text_reader
{

  public:
    network_data( v_map_t& v_map ) :
      base::text_reader( v_map )
      {
        base::text_reader::setData();
      }

};
    
//##############################################################################
// network_data_options().
//##############################################################################

class network_data_options : base::text_reader_options
{

  public:
    network_data_options() : base::text_reader_options() {}

    std::string
    getExample()
    {
      return base::text_reader_options::getExample();
    }

    void
    get( po::options_description& network_data_desc )
    {

      try
      {

        base::text_reader_options::get( network_data_desc );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_DATA_DETAIL_HPP

/// @endcond
