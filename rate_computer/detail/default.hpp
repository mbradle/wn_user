////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define rate computer routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef NNP_RATE_COMPUTER_DETAIL_HPP
#define NNP_RATE_COMPUTER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define S_SMALL_RATES_THRESHOLD    "small_rates_threshold"

//##############################################################################
// rate_computer().
//##############################################################################

class rate_computer
{

  public:
    rate_computer(){}
    rate_computer( v_map_t& v_map ) :
      my_screener( v_map ), my_nse_corrector( v_map ),
      my_rate_modifier( v_map )
    { }

    void computeRates( nnt::Zone& zone )
    {

      boost::any screening_data;
      boost::any coul_corr_data;

      if(
        Libnucnet__Zone__getScreeningFunction( zone.getNucnetZone() )
      )
      {
        screening_data =
          boost::any_cast<boost::function<boost::any()> >(
            zone.getFunction( nnt::s_SCREENING_DATA_FUNCTION )
          )();
    
        Libnucnet__Zone__setScreeningFunction(
          zone.getNucnetZone(),
          (Libnucnet__Zone__screeningFunction)
            Libnucnet__Zone__getScreeningFunction( zone.getNucnetZone() ),
          &screening_data
        );
      }

      if(
        Libnucnet__Zone__getNseCorrectionFactorFunction( zone.getNucnetZone() )
      )
      {

        coul_corr_data =
          boost::any_cast<boost::function<boost::any()> >(
            zone.getFunction( nnt::s_NSE_CORRECTION_FACTOR_DATA_FUNCTION )
          )();

        Libnucnet__Zone__setNseCorrectionFactorFunction(
          zone.getNucnetZone(),
          (Libnucnet__Species__nseCorrectionFactorFunction)
            Libnucnet__Zone__getNseCorrectionFactorFunction(
              zone.getNucnetZone()
          ),
          &coul_corr_data
        );
      }

      if(
        zone.hasFunction( nnt::s_RATE_DATA_UPDATE_FUNCTION )
      )
      {
        boost::any_cast<boost::function<void( )> >(
          zone.getFunction( nnt::s_RATE_DATA_UPDATE_FUNCTION )
        )( );
      }

      Libnucnet__Zone__computeRates(
        zone.getNucnetZone(),
        zone.getProperty<double>( nnt::s_T9 ),
        zone.getProperty<double>( nnt::s_RHO )
      );

      if( zone.hasFunction( nnt::s_RATES_MODIFICATION_FUNCTION ) )
      {
        boost::any_cast<boost::function<void( )> >(
          zone.getFunction( nnt::s_RATES_MODIFICATION_FUNCTION )
        )( );
      }

// Zero out small rates.
// Weak equil.

  }

  private:
    screener my_screener;
    nse_corrector my_nse_corrector;
    rate_registerer my_rate_registerer;
    rate_modifier my_rate_modifier;

};

//##############################################################################
// rate_computer_options().
//##############################################################################

class rate_computer_options
{

  public:
    rate_computer_options(){}

    void
    getDetailOptions( po::options_description& rate_computer )
    {

      try
      {

        rate_computer.add_options()

          (
            S_SMALL_RATES_THRESHOLD,
            po::value<double>()->default_value( 0., "0." ),
            "Threshold for cut-off of small rates."
          )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

}  // namespace detail

}  // namespace wn_user

#endif // NNP_RATE_COMPUTER_DETAIL_HPP
