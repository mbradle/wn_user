////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define time step routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <Libnucnet.h>

#include "my_global_types.h"

#include <boost/optional.hpp>

#include "nnt/auxiliary.h"
#include "nnt/wrappers.hpp"

#ifndef WN_TIME_ADJUSTER_DETAIL_HPP
#define WN_TIME_ADJUSTER_DETAIL_HPP

#define S_REGT          "reg_t"
#define S_REGY          "reg_y"
#define S_YMIN          "y_min"
#define S_DTMAX         "dt_max"


namespace wn_user
{

namespace detail
{

//##############################################################################
// time_adjuster().
//##############################################################################

class time_adjuster
{

  public:
    time_adjuster( ){}
    time_adjuster( v_map_t& v_map )
    {
      dRegt = v_map[S_REGT].as<double>();
      dRegy = v_map[S_REGY].as<double>();
      dYmin = v_map[S_YMIN].as<double>();
      if( v_map.count( S_DTMAX ) )
      {
	dt_max = v_map[S_DTMAX].as<double>();
      }
    }

    void operator()( nnt::Zone& zone )
    {

      double dt, time;

      dt = zone.getProperty<double>( nnt::s_DTIME );
      time = zone.getProperty<double>( nnt::s_TIME );

      Libnucnet__Zone__updateTimeStep(
        zone.getNucnetZone(),
        &dt,
        dRegt,
        dRegy,
        dYmin
      );

      if( time + dt > zone.getProperty<double>( nnt::s_T_END ) )
      {
        dt = zone.getProperty<double>( nnt::s_T_END ) - time;
      }

      if( dt_max )
      {
	if( dt > dt_max.get() ) { dt = dt_max.get(); }
      }

      zone.updateProperty( nnt::s_DTIME, dt );

    }

    void operator()( std::vector<nnt::Zone>& zones )
    {

      double dt, time;

      std::vector<double> dtm( zones.size() );

      time = zones[0].getProperty<double>( nnt::s_TIME );

      for( size_t i = 0; i < zones.size(); i++ )
      {

        dtm[i] = zones[i].getProperty<double>( nnt::s_DTIME );

        Libnucnet__Zone__updateTimeStep(
          zones[i].getNucnetZone(),
          &dtm[i],
          dRegt,
          dRegy,
          dYmin
        );

      }

      dt = *std::min_element( dtm.begin(), dtm.end() );

      if( time + dt > zones[0].getProperty<double>( nnt::s_T_END ) )
      {
        dt = zones[0].getProperty<double>( nnt::s_T_END ) - time;
      }

      if( dt_max )
      {
	if( dt > dt_max.get() ) { dt = dt_max.get(); }
      }

      for( size_t i = 0; i < zones.size(); i++ )
      {
        zones[i].updateProperty( nnt::s_DTIME, dt );
      }

    }

  private:
    double dRegt, dRegy, dYmin;
    boost::optional<double> dt_max;

};
     
//##############################################################################
// time_adjuster_options().
//##############################################################################

class time_adjuster_options
{

  public:
    time_adjuster_options(){}

    void
    get( po::options_description& options_desc )
    {

      try
      {

        options_desc.add_options()

        // Option for time step increase
        (  S_REGT, po::value<double>()->default_value( 0.15, "0.15" ),
           "Regulator for timestep increase" )
  
        // Option for abundance increase
        (  S_REGY, po::value<double>()->default_value( 0.15, "0.15" ),
           "Regulator for timestep abundance change" )
   
        // Option for minimum abundance to affect time step
        (  S_YMIN, po::value<double>()->default_value( 1.e-10, "1.e-10" ),
           "Minimum abundance for timestep abundance change" )

        // Option for maximum timestep
        (  S_DTMAX, po::value<double>(),
           "Maximum timestep allowed" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user 

#endif // WN_TIME_ADJUSTER_DETAIL_HPP
