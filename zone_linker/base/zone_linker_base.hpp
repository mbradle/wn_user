///////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to define links between zones.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Defines.
//##############################################################################

#define   S_GRAPH_FILE          "graph_output_file"
#define   S_GRAPH_WRITE_STEP    "graph_step"

//##############################################################################
// Includes.
//##############################################################################

//#include "user/multi_zone_utilities.h"
#include "utility/zone_link_grapher.hpp"

#include <boost/graph/iteration_macros.hpp>
#include <boost/graph/graphviz.hpp>

#ifndef NNP_ZONE_LINKER_BASE_HPP
#define NNP_ZONE_LINKER_BASE_HPP

namespace wn_user
{

namespace detail
{

class zone_linker_base
{

  public:
    zone_linker_base(){}
    zone_linker_base( v_map_t& v_map )
    {
      if( v_map.count(S_GRAPH_FILE) == 1 )
      {
        b_write = true;
        s_graph_file = v_map[S_GRAPH_FILE].as<std::string>();
        i_graph_write_step = v_map[S_GRAPH_WRITE_STEP].as<size_t>();
        i_step = 0;
      }
      else
      {
        b_write = false;
      }
    } 

    template<class Graph>
    void
    outputGraph( Graph& g )
    {

      if( !b_write ) return;

      if( i_step++ == i_graph_write_step )
      {
        wn_user::utility::zone_link_grapher zlg;
        zlg( g, s_graph_file );
      }
    }

  private:
    bool b_write;
    std::string s_graph_file;
    size_t i_step, i_graph_write_step;

};

//##############################################################################
// zone_linker_base_options().
//##############################################################################

class zone_linker_base_options
{

  public:
    zone_linker_base_options(){}

    void
    getBaseOptions( po::options_description& zone_linker )
    {

      try
      {

        zone_linker.add_options()

          ( S_GRAPH_FILE, po::value<std::string>()->default_value(""),
            "Name of output graph file showing links" )

          ( S_GRAPH_WRITE_STEP,
            po::value<size_t>()->default_value( 0, "0" ),
            "Step at which to write graph" )
        ;

      // Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

    std::string
    getBaseExample()
    {
      return
        "--" + std::string( S_GRAPH_FILE ) + " links_graph.dot ";
    }

};

} // namespace detail

} // namespace wn_user

#endif  // NNP_ZONE_LINKER_BASE_HPP

