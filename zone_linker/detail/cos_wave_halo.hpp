//////////////////////////////////////////////////////////////////////////////
//  
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to compute links in multi-zone chemical evolution model.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <boost/assign.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/optional.hpp>

#include "zone_linker/base/zone_linker_base.hpp"
#include "user/multi_zone_utilities.h"
#include "utility/zone_link_grapher.hpp"

#include <boost/graph/iteration_macros.hpp>

//##############################################################################
// Check if already included.
//##############################################################################

#ifndef WN_ZONE_LINKER_DETAIL_HPP
#define WN_ZONE_LINKER_DETAIL_HPP

//##############################################################################
// Strings.
//##############################################################################

#define S_HOT_MIX  "hot_mix"
#define S_HALO_MIX  "halo_mix"
#define S_CLOUD_MIX  "cloud_mix"

#define S_P  "p"
#define S_A  "A"
#define S_T  "T"
#define S_M  "m"

#define S_P_X  "p_x"
#define S_A_X  "A_x"
#define S_T_X  "T_x"
#define S_M_X  "m_x"

#define S_P_Y  "p_y"
#define S_A_Y  "A_y"
#define S_T_Y  "T_y"
#define S_M_Y  "m_y"

#define S_P_Z  "p_z"
#define S_A_Z  "A_z"
#define S_T_Z  "T_z"
#define S_M_Z  "m_z"

//##############################################################################
// Data
//##############################################################################

#define D_YEAR  3.15e7

namespace wn_user
{

namespace detail
{

//##############################################################################
// Types.
//##############################################################################

typedef boost::tuple<int,int,int> int_tuple_t;

typedef boost::tuple<std::string,std::string,std::string> str_tuple_t;

typedef std::vector<boost::optional<double> > v_opt_t;

//##############################################################################
// check_value().
//##############################################################################

int
check_value(
  int i,
  int j
)
{

  if( i < 0 )
  {
    return j;
  }
  else if( i > j )
  {
    return 0;
  } 
  
  return i;
}

//##############################################################################
// tuple_add().
//##############################################################################

struct tuple_add
{

  std::vector<int> n_max;

  tuple_add( std::vector<int> n ) : n_max( n ) {}

  int_tuple_t operator()( int_tuple_t lhs, const int_tuple_t& rhs )
  {
    lhs.get<0>() = check_value( lhs.get<0>() + rhs.get<0>(), n_max[0] );
    lhs.get<1>() = check_value( lhs.get<1>() + rhs.get<1>(), n_max[1] );
    lhs.get<2>() = check_value( lhs.get<2>() + rhs.get<2>(), n_max[2] );

    return lhs;
  }

};

//##############################################################################
// zone_linker().
//##############################################################################

class zone_linker : public zone_linker_base
{

  public:
    zone_linker() : zone_linker_base() {}
    zone_linker( v_map_t& v_map ) : zone_linker_base( v_map )
    {

      const std::vector<std::string> p_params =
        boost::assign::list_of(S_P_X)(S_P_Y)(S_P_Z);
    
      const std::vector<std::string> a_params =
        boost::assign::list_of(S_A_X)(S_A_Y)(S_A_Z);
    
      const std::vector<std::string> t_params =
        boost::assign::list_of(S_T_X)(S_T_Y)(S_T_Z);
    
      const std::vector<std::string> m_params =
        boost::assign::list_of(S_M_X)(S_M_Y)(S_M_Z);
    
      BOOST_FOREACH( std::string s_param, p_params )
      {
        boost::optional<double> opt;
        if( v_map.count( s_param ) )
        {
          opt = v_map[s_param].as<double>();
        }
        p.push_back( opt );
      }
    
      BOOST_FOREACH( std::string s_param, a_params )
      {
        boost::optional<double> opt;
        if( v_map.count( s_param ) )
        {
          opt = v_map[s_param].as<double>();
        }
        a.push_back( opt );
      }
    
      BOOST_FOREACH( std::string s_param, t_params )
      {
        boost::optional<double> opt;
        if( v_map.count( s_param ) )
        {
          opt = v_map[s_param].as<double>();
        }
        tt.push_back( opt );
      }
    
      BOOST_FOREACH( std::string s_param, m_params )
      {
        boost::optional<double> opt;
        if( v_map.count( s_param ) )
        {
          opt = static_cast<double>( v_map[s_param].as<size_t>() );
        }
        m.push_back( opt );
      }

      cloud_mix_time = v_map[S_CLOUD_MIX].as<double>() * D_YEAR;
    
      if( v_map.count( S_HOT_MIX ) )
      {
        hot_mix_time = v_map[S_HOT_MIX].as<double>() * D_YEAR;
      }
      if( v_map.count( S_HALO_MIX ) )
      {
        halo_mix_time = v_map[S_HALO_MIX].as<double>() * D_YEAR;
      }

    }

    bool
    isHotZone( Libnucnet__Zone * p_zone )
    {
      std::string s3 = Libnucnet__Zone__getLabel( p_zone, 3 );
      return (s3.find( "h" ) != std::string::npos);
    }

    bool
    isHaloZone( Libnucnet__Zone * p_zone )
    {
      std::string s1 = Libnucnet__Zone__getLabel( p_zone, 1 );
      return (s1.find( "halo" ) != std::string::npos);
    }

    double
    computeRateRatio(
      size_t i_coord,
      size_t i_index,
      double& t,
      std::vector<int>& n_max
    )
    {  
      if( !p[i_coord] || !a[i_coord] || !tt[i_coord] || !m[i_coord])
      {
        std::cerr << "Data not available for cos wave." << std::endl;
        exit( EXIT_FAILURE );
      }
      
      return
        1.
        +
        a[i_coord].get()
        *
        pow(
          cos(
            m[i_coord].get()
            *
            M_PI
            *
            (
              ( t / tt[i_coord].get() ) -
              ( (double) i_index / (double) ( n_max[i_coord] + 1 ) )
            )
          ),
          p[i_coord].get()
        ); 
    
    }

    template<typename Graph>
    void
    setLinks( Graph& g )
    {
      typename Graph::vertex_descriptor v_to;
      typename Graph::edge_descriptor e;
      user::vertex_multi_index vm;
      user::fill_multi_zone_vertex_hash( g, vm );
    
      bool e_add;
    
      std::vector<int> labels_x, labels_y, labels_z;
      std::vector<int> n_max;
      std::vector<int_tuple_t> delta; 
    
      BGL_FORALL_VERTICES_T( v, g, Graph )
      {
        if(
          !isHotZone( g[v].getNucnetZone() ) && 
          !isHaloZone( g[v].getNucnetZone() ) 
        )
        {
          labels_x.push_back (
            boost::lexical_cast<int>(
              Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 1 )
            ) 
          );
          labels_y.push_back (
            boost::lexical_cast<int>(
              Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 2 )
            ) 
          );
          labels_z.push_back (
            boost::lexical_cast<int>(
              Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 3 )
            ) 
          );
        }
      }
    
      n_max.push_back( *std::max_element( labels_x.begin(), labels_x.end() ) );
      n_max.push_back( *std::max_element( labels_y.begin(), labels_y.end() ) );
      n_max.push_back( *std::max_element( labels_z.begin(), labels_z.end() ) );
    
      tuple_add my_tuple_add( n_max );
    
      delta.push_back( int_tuple_t( 1, 0, 0 ) );
      delta.push_back( int_tuple_t( -1, 0, 0 ) );
      delta.push_back( int_tuple_t( 0, 1, 0 ) );
      delta.push_back( int_tuple_t( 0, -1, 0 ) );
      delta.push_back( int_tuple_t( 0, 0, 1 ) );
      delta.push_back( int_tuple_t( 0, 0, -1 ) );
    
      BGL_FORALL_VERTICES_T( v, g, Graph )
      {
        nnt::Zone zone_for_data;
        zone_for_data.setNucnetZone( g[v].getNucnetZone() );
        double t = zone_for_data.getProperty<double>( nnt::s_TIME );
        str_tuple_t
          source_tuple(
            std::string( Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 1 ) ),
            std::string( Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 2 ) ),
            std::string( Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 3 ) )
          );
    
        if( 
          !isHotZone( g[v].getNucnetZone() ) && 
          !isHaloZone( g[v].getNucnetZone() ) 
        )
        {
          for( size_t i = 0; i < delta.size(); i++ )
          {
    
            int_tuple_t target_tuple;
            std::vector<int> v_tar;
    
            int_tuple_t
              source_int_tuple(
                boost::lexical_cast<int>( source_tuple.get<0>() ),
                boost::lexical_cast<int>( source_tuple.get<1>() ),
                boost::lexical_cast<int>( source_tuple.get<2>() )
              );
    
            target_tuple = my_tuple_add( source_int_tuple, delta[i] );
    
            v_tar.push_back( target_tuple.get<0>() );
            v_tar.push_back( target_tuple.get<1>() );
            v_tar.push_back( target_tuple.get<2>() );
    
            if( target_tuple != source_int_tuple )
            {
    
              v_to =
                user::get_vertex_from_multi_zone_hash(
                  vm,
                  boost::lexical_cast<std::string>( target_tuple.get<0>() ),
                  boost::lexical_cast<std::string>( target_tuple.get<1>() ),
                  boost::lexical_cast<std::string>( target_tuple.get<2>() )
                );
    
              if( v_to != Graph::null_vertex() )
              {
                boost::tie( e, e_add ) = boost::add_edge( v, v_to, g );
    
                g[e].setWeight( 
                  computeRateRatio( 
                    i/2,
                    v_tar[i/2],
                    t,
                    n_max
                  )
                  *
                  ( 1.  / cloud_mix_time ) 
                );
              }
            }
          }
        }
        else if( isHotZone( g[v].getNucnetZone() ) )
        {
          v_to =
            user::get_vertex_from_multi_zone_hash(
              vm,
              source_tuple.get<0>(),
              source_tuple.get<1>(),
              (source_tuple.get<2>()).substr( 0, 1 )
            );
    
          if( v_to != Graph::null_vertex() )
          {
            if( hot_mix_time )
            {
              boost::tie( e, e_add ) = boost::add_edge( v, v_to, g );
    
              g[e].setWeight( 1. / hot_mix_time.get() );
            }
          }
        }
        else if( isHaloZone( g[v].getNucnetZone() ) )
        {
          size_t n_to = (n_max[0]+1) * (n_max[1]+1) * (n_max[2]+1);
          BGL_FORALL_VERTICES_T( v_to, g, Graph )
          {
            if( v_to != Graph::null_vertex() )
            {
              if( 
                !isHotZone( g[v_to].getNucnetZone() ) && 
                !isHaloZone( g[v_to].getNucnetZone() ) 
              )
              {
                if( halo_mix_time )
                {
                  boost::tie( e, e_add ) = boost::add_edge( v, v_to, g );
    
                  g[e].setWeight( 
                    1. /
                    (
                      (double) n_to * halo_mix_time.get()
                    )
                  );
                }
              }
            }
          }
        }
        else
        {
          std::cerr << "Invalid zone." << std::endl;
          exit( EXIT_FAILURE );
        }
      }
    }

  private:
    double cloud_mix_time;
    boost::optional<double> hot_mix_time, halo_mix_time;
    v_opt_t p, a, tt, m;

};

//##############################################################################
// zone_linker_options().
//##############################################################################

class zone_linker_options : public zone_linker_base_options
{

  public:
    zone_linker_options() : zone_linker_base_options() {}

    std::string
    getDetailExample()
    { return ""; }

    void
    getDetailOptions( po::options_description& zone_linker )
    {
  
      try
      {

        po::options_description general( "\nGeneral options" );
    
        general.add_options()
        // Option for value of cloud-mixing timescale in years
        ( S_CLOUD_MIX,
          po::value<double>()->required()->default_value( 1.e7, "1.e7" ),
          "cloud-mixing timescale in years\n" )
      
        // Option for value of hot-zone-mixing timescale in years
        ( S_HOT_MIX, po::value<double>(),
          "hot-zone-mixing timescale in years (optional)\n" )
      
        // Option for value of halo-zone-mixing timescale in years
        ( S_HALO_MIX, po::value<double>(),
          "halo-zone-mixing timescale in years (optional)\n" )

        ;
      
        po::options_description X( "\nX options" );
    
        X.add_options()
    
        // Option for value of power of cosine wave in x-direction
        (
          S_P_X,
          po::value<double>(), "power of cosine wave in x-direction (required if"
          " neither p_y nor p_z is set)\n"
        )
      
        // Option for value of amplitude of cosine wave in x-direction
        (
          S_A_X,
          po::value<double>(), "amplitude of cosine wave in x-direction "
          "(required if neither A_y nor A_z is set)\n"
        )
      
        // Option for value of period of cosine wave in x-direction
        (
          S_T_X,
          po::value<double>(),
          "period of cosine wave in x-direction (required "
          "if neither T_y nor T_z is set)\n"
        )
      
        // Option for value of multiplier of cosine wave in x-direction
        (
          S_M_X,
          po::value<size_t>(),
          "multiplier of cosine wave in x-direction (required "
          "if neither m_y nor m_z is set)\n"
        )

        ;
      
        po::options_description Y( "\nY options" );
    
        Y.add_options()
    
        // Option for value of power of cosine wave in y-direction
        (
          S_P_Y,
          po::value<double>(),
          "power of cosine wave in y-direction (required if"
          " neither p_x nor p_z is set)\n"
        )
      
        // Option for value of amplitude of cosine wave in y-direction
        (
          S_A_Y,
          po::value<double>(),
          "amplitude of cosine wave in y-direction "
          "(required if neither A_x nor A_z is set)\n"
        )
      
        // Option for value of period of cosine wave in y-direction
        (
          S_T_Y,
          po::value<double>(),
          "period of cosine wave in y-direction (required "
          "if neither T_x nor T_z is set)\n"
        )
      
        // Option for value of multiplier of cosine wave in y-direction
        (
          S_M_Y,
          po::value<size_t>(),
          "multiplier of cosine wave in y-direction (required "
          "if neither m_x nor m_z is set)\n"
        )

        ;
      
        po::options_description Z( "\nZ options" );
    
        Z.add_options()
    
        // Option for value of power of cosine wave in z-direction
        (
          S_P_Z,
          po::value<double>(),
          "power of cosine wave in z-direction (required if"
          " neither p_x nor p_y is set)\n"
        )
      
        // Option for value of amplitude of cosine wave in z-direction
        (
          S_A_Z,
          po::value<double>(),
          "amplitude of cosine wave in z-direction "
          "(required if neither A_x nor A_y is set)\n"
        )
      
        // Option for value of period of cosine wave in z-direction
        (
          S_T_Z,
          po::value<double>(),
          "period of cosine wave in z-direction (required "
          "if neither T_x nor T_y is set)"
        )
    
        // Option for value of multiplier of cosine wave in z-direction
        (
          S_M_Z,
          po::value<size_t>(),
          "multiplier of cosine wave in z-direction (required "
          "if neither m_x nor m_y is set)\n"
        )

        ;
      
        zone_linker.add( general ).add( X ).add( Y ).add( Z );

        getBaseOptions( zone_linker );
    
    // Add checks on input.
    
      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif  // WN_ZONE_LINKER_DETAIL_HPP
