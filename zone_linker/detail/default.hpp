///////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to define links between zones.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Defines.
//##############################################################################

#define   S_PERIOD          "periodic"
#define   S_MIX_RATE        "mix_rate"

//##############################################################################
// Includes.
//##############################################################################

#include "zone_linker/base/zone_linker_base.hpp"

#include <limits>

#include <boost/graph/iteration_macros.hpp>
#include <boost/tuple/tuple.hpp>

#ifndef WN_ZONE_LINKER_DETAIL_HPP
#define WN_ZONE_LINKER_DETAIL_HPP

namespace wn_user
{

namespace detail
{

//##############################################################################
// Types.
//##############################################################################

typedef boost::tuple<int, int, int> int_tuple_t;

//##############################################################################
// tuple addition
//##############################################################################

struct tuple_add
{
  bool b_period;
  std::vector<int> v_max, v_min;
  tuple_add( bool _b_period, std::vector<int> _v_max, std::vector<int> _v_min )
    : b_period( _b_period ), v_max( _v_max ), v_min( _v_min ){};

  int_tuple_t
  operator()( const int_tuple_t& a, const int_tuple_t& b )
  {
    std::vector<int> v;
    v.push_back(a.get<0>() + b.get<0>());
    v.push_back(a.get<1>() + b.get<1>());
    v.push_back(a.get<2>() + b.get<2>());
    
    if( b_period )
    {
      for( size_t i = 0; i < v.size(); i++ )
      {
        if( v[i] < v_min[i] ) { v[i] = v_max[i]; }
        if( v[i] > v_max[i] ) { v[i] = v_min[i]; }
      }
    }

    return boost::make_tuple( v[0], v[1], v[2] );
  }

};

//##############################################################################
// make_int_tuple()
//##############################################################################

int_tuple_t
make_int_tuple( std::string s1, std::string s2, std::string s3 )
{
  return
    boost::make_tuple(
      boost::lexical_cast<int>( s1 ),
      boost::lexical_cast<int>( s2 ),
      boost::lexical_cast<int>( s3 )
    );
}

//##############################################################################
// zone_linker().
//##############################################################################

class zone_linker : public zone_linker_base
{

  public:
    zone_linker() : zone_linker_base() {}
    zone_linker( v_map_t& v_map ) : zone_linker_base( v_map )
    {
      b_period = v_map[S_PERIOD].as<bool>();
      d_mix_rate = v_map[S_MIX_RATE].as<double>();
    }

    template<class Graph>
    void
    setLinks( Graph& g )
    {

      std::vector<int_tuple_t> v_tup;
      user::vertex_multi_index vm;
      std::vector<int> v_min, v_max;

      int i_max = std::numeric_limits<int>::max();
      int i_min = std::numeric_limits<int>::min();

      for( size_t i = 0; i < 3; i++ )
      {
        v_max.push_back( i_max );
        v_min.push_back( i_min );
      }

      user::fill_multi_zone_vertex_hash( g, vm );

      BGL_FORALL_VERTICES_T( v_from, g, Graph )
      {
        int_tuple_t t =
          make_int_tuple(
            Libnucnet__Zone__getLabel( g[v_from].getNucnetZone(), 1 ),
            Libnucnet__Zone__getLabel( g[v_from].getNucnetZone(), 2 ),
            Libnucnet__Zone__getLabel( g[v_from].getNucnetZone(), 3 )
          );
        if( t.get<0>() > v_max[0] ) { v_max[0] = t.get<0>(); }
        if( t.get<1>() > v_max[1] ) { v_max[1] = t.get<1>(); }
        if( t.get<2>() > v_max[2] ) { v_max[2] = t.get<2>(); }
        if( t.get<0>() < v_min[0] ) { v_min[0] = t.get<0>(); }
        if( t.get<1>() < v_min[1] ) { v_min[1] = t.get<1>(); }
        if( t.get<2>() < v_min[2] ) { v_min[2] = t.get<2>(); }
      }

      tuple_add my_adder( b_period, v_min, v_max );

      v_tup.push_back( make_int_tuple( "1", "0", "0" ) );
      v_tup.push_back( make_int_tuple( "-1", "0", "0" ) );
      v_tup.push_back( make_int_tuple( "0", "1", "0" ) );
      v_tup.push_back( make_int_tuple( "0", "-1", "0" ) );
      v_tup.push_back( make_int_tuple( "0", "0", "1" ) );
      v_tup.push_back( make_int_tuple( "0", "0", "-1" ) );

      BGL_FORALL_VERTICES_T( v_from, g, Graph )
      {

        int_tuple_t t_tup =
          make_int_tuple(
            Libnucnet__Zone__getLabel( g[v_from].getNucnetZone(), 1 ),
            Libnucnet__Zone__getLabel( g[v_from].getNucnetZone(), 2 ),
            Libnucnet__Zone__getLabel( g[v_from].getNucnetZone(), 3 )
          );

        BOOST_FOREACH( int_tuple_t t, v_tup )
        {
          int_tuple_t t_new = my_adder( t_tup, t );
          typename Graph::vertex_descriptor v_to =
            user::get_vertex_from_multi_zone_hash(
              vm,
              boost::lexical_cast<std::string>( t_new.get<0>() ),
              boost::lexical_cast<std::string>( t_new.get<1>() ),
              boost::lexical_cast<std::string>( t_new.get<2>() )
            );
          if( v_to != Graph::null_vertex() )
          {
            typename Graph::edge_descriptor e;
            bool e_add;

            boost::tie( e, e_add ) = boost::add_edge( v_from, v_to, g );

            if( e_add )
            {
              g[e].setWeight( d_mix_rate );
            }
          }
        }
      }
    }

  private:
    bool b_period;
    double d_mix_rate;

};

//##############################################################################
// zone_linker_options().
//##############################################################################

class zone_linker_options : public zone_linker_base_options
{

  public:
    zone_linker_options() : zone_linker_base_options() {}

    void
    getDetailOptions( po::options_description& zone_linker )
    {

      try
      {

        zone_linker.add_options()

          ( S_PERIOD, po::value<bool>()->default_value( false, "false" ),
            "Periodic boundary conditions"
          )

          ( S_MIX_RATE, po::value<double>()->default_value( 0.01, "0.01" ),
            "Mixing rate between zones (s^-1)"
          )

        ;

        getBaseOptions( zone_linker );

      // Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

    std::string
    getDetailExample()
    {
      return
        getBaseExample() +
        "--" + std::string( S_MIX_RATE ) + " 10 ";
    }

};

} // namespace detail

} // namespace wn_user

#endif  // WN_ZONE_LINKER_DETAIL_HPP

