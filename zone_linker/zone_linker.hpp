////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file zone_linker.hpp
//! \brief A file to define zone linking routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_ZONE_LINKER_HPP
#define WN_ZONE_LINKER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// zone_linker().
//##############################################################################

class zone_linker : public detail::zone_linker
{

  public:
    zone_linker() : detail::zone_linker(){}
    zone_linker( v_map_t& v_map ) : detail::zone_linker( v_map ){}

};

//##############################################################################
// zone_linker_options().
//##############################################################################

class zone_linker_options : public detail::zone_linker_options
{

  public:
    zone_linker_options() : detail::zone_linker_options(){}

    std::string
    getExample()
    {
      return getDetailExample();
    }

    void
    get( options_map& o_map )
    {

      try
      {

       po::options_description zone_linker( "\nZone link Options" );

       getDetailOptions( zone_linker );

       o_map.insert(
         std::make_pair<std::string, options_struct>(
           "zone_linker",
           options_struct( zone_linker, getExample() )
         )
       );

      // Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace wn_user

#endif // WN_ZONE_LINKER_HPP
