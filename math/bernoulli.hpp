////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#include <boost/math/special_functions/bernoulli.hpp>
#include <boost/math/special_functions/binomial.hpp>

#ifndef WN_BERNOULLI_HPP
#define WN_BERNOULLI_HPP

namespace wn_user
{

namespace math
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class bernoulli
///
//! \brief A class to compute Bernoulli numbers and polynomials.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// bernoulli().
//##############################################################################

template<class T>
class bernoulli
{

  public:
    bernoulli()
    {
      std::vector<T> bn;

      boost::math::bernoulli_b2n<T>(
        0,
        getMax(),
        std::back_inserter( bn )
      );

      for(size_t n = 0; n < bn.size(); n++)
      {
        v_b.push_back( bn[n] );
        v_b.push_back( 0 );
      }
      v_b[1] = -0.5;
    }

/// \return The largest index for which a Bernoulli number can be computed
///         for the chosen type.

    size_t
    getMaxN() const { return 2 * getMax() - 1; }

/// \param n The index for the requested Bernoulli number.
/// \return The Bernoulli number \f$B_n\f$.

    T
    getNumber( size_t n )
    {
      assert( n <= getMaxN() );
      return v_b[n];
    }

/// \param n The index for the requested Bernoulli polynomial
/// \param x The value at which to compute the polynomial.
/// \return The Bernoulli polynomial value \f$B_n(x)\f$.

    T
    computePolynomial( size_t n, T x )
    {
      assert( n <= getMaxN() );
      T result = 0;
      for( size_t k = 0; k <= n; k++ )
      {
        result +=
          boost::math::binomial_coefficient<T>( n, k ) *
          getNumber( n - k ) *
          pow( x, k );
      }
      return result;
    }

  private:
    std::vector<T> v_b;

    size_t
    getMax() const { return boost::math::max_bernoulli_b2n<T>::value; }

};

}  // namespace math

}  // namespace wn_user

#endif // WN_BERNOULLI_HPP
