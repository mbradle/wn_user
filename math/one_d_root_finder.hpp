////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file 1d_root_finder.hpp
//! \brief A file to define 1d root finding.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/math/tools/roots.hpp>
#include <boost/function.hpp>

#ifndef WN_ONE_D_ROOT_FINDER_HPP
#define WN_ONE_D_ROOT_FINDER_HPP

namespace wn_user
{

namespace math
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class one_d_root_finder
///
//! \brief A class to find one-dimensional roots.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// one_d_root_finder().
//##############################################################################

template<class T = double>
class one_d_root_finder
{

  public:

/// The constructor.
/// \param func The function for which one seeks the root.

    one_d_root_finder( boost::function<T(T)> _func ) : func( _func )
    {
      max_iter = 100;
      factor = 2;
      precision = 1;
    }

/// Routine to update the factor by which to expand the guess when bracketin
/// the root.
/// \param new_factor The new factor to be applied.  The default is 2.

    void updateFactor( T new_factor ) { factor = new_factor; }

/// Routine to update the precision (relative to the number of digits for
/// the input type) to which to find the root.
/// \param new_precision The new precision to be applied.  The default is 1.

    void updatePrecision( T new_precision ) { precision = new_precision; }

/// Routine to update the maximum number of iterations allowed in seeking
/// the root.
/// \param new_max The new maximum to be applied.  The default is 100.

    void updateMaxIters( boost::uintmax_t new_max ) { max_iter = new_max; }

/// The root finder.
/// \param guess The initial guess for the root.
/// \return The root.

    T operator()( T guess )
    {

      boost::uintmax_t iter = max_iter;

      int digits = std::numeric_limits<T>::digits;
      int get_digits = static_cast<int>( digits * precision );

      boost::math::tools::eps_tolerance<T> tol( get_digits );

      std::pair<T, T> r =
        boost::math::tools::bracket_and_solve_root(
          func,
          guess,
          factor,
          isRising( guess ),
          tol,
          iter
        );

      if( iter >= max_iter )
      {
        std::cerr << "Root not found--too many iterations." << std::endl;
        exit( EXIT_FAILURE );
      }
  
      return (r.first + r.second) / 2.;
    }

  private:
    boost::function<T(T)> func;
    boost::uintmax_t max_iter;
    boost::uintmax_t iter;
    T factor;
    T precision;

    bool
    isRising( T x )
    {
      bool result;
      if( x > 0 )
      {
        if( func( x * factor ) > func( x / factor ) )
          result = true;
        else
          result = false;
      }
      else
      {
        if( func( x * factor ) > func( x / factor ) )
          result = false;
        else
          result = true;
      }
      return result;
    }

};

} // namespace math

} // namespace wn_user

#endif // WN_ONE_D_ROOT_FINDER_HPP
