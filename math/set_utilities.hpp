////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer and Norberto J. Davila.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#include <numeric>
#include <boost/math/special_functions/binomial.hpp>

#ifndef WN_SET_SUM_HPP
#define WN_SET_SUM_HPP

namespace wn_user
{

namespace math
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class set
///
//! \brief A class to compute set sums.  The mth set sum of the set of N
//!        elements is the elementary symmetric polynomial of degree m of
//!        the set elements.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// set().
//##############################################################################

template <class T = double>
class set
{
  public:

    set( ){}

/// \param n The highest order of set sums required.  This number must be less
///          than the cardinality of the set.
/// \param x The vector of x values from which to compute the set sums.
/// \return A vector of n + 1 set sums.

    std::vector<T>
    compute_sums( size_t n, std::vector<T>& x )
    {
       assert( n <= x.size() );
       std::vector<T> result;
       result.push_back(1);
       std::vector<T> y = x;

       for( size_t i = 1; i <= n; i++ )
       {
         result.push_back( std::accumulate( y.begin(), y.end(), 0 ) );
         for( size_t j = 0; j < y.size(); j++ )
         {
           y[j] = 0;
           for( size_t k = j+1; k < y.size(); k++ )
           {
             y[j] += x[j] * y[k];
           }
         }
       }

       return result;
     }


/// \param n The highest order of set sums required.  This number must be less
///          than the cardinality of the set.
/// \param x A vector of set values
/// \return A vector of n + 1 normalized set sums.

    std::vector<T>
    compute_normalized_sums( size_t n, std::vector<T>& x )
    {
       assert( n <= x.size() );
       std::vector<T> result = compute_sums( n, x );
       size_t m = x.size();

       for( size_t i = 0; i < result.size(); i++ )
       {
         result[i] /= boost::math::binomial_coefficient<T>( m, i );
       }

       return result;
     }

};

////////////////////////////////////////////////////////////////////////////////
///
/// \class multi_set
///
//! \brief A class to compute multi set sums.  The mth multi set sum of a
//!        set of N elements is the complete homogeneous symmetric polynomial
//!        of degree m of the set elements.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// multi_set().
//##############################################################################

template <class T = double>
class multi_set
{
  public:

    multi_set( ){}

/// \param n The highest order of multi set sums required.
/// \param x The vector of set values.
/// \return A vector of n + 1 multi set sums.

    std::vector<T>
    compute_sums( size_t n, std::vector<T>& x )
    {
       std::vector<T> result;
       result.push_back(1);
       std::vector<T> y = x;
       result.push_back( std::accumulate( y.begin(), y.end(), 0. ) );

       for( size_t i = 2; i <= n; i++ )
       {
         for( size_t j = 0; j < y.size(); j++ )
         {
           T sum = 0;
           for( size_t k = j; k < y.size(); k++ )
           {
             sum += x[j] * y[k];
           }
           y[j] = sum;
         }
         T tot = std::accumulate( y.begin(), y.end(), 0. );
         result.push_back( tot );
       }

       return result;
     }

/// \param n The highest order of multi set sums required.
/// \return A vector of n + 1 normalized set sums.

    std::vector<T>
    compute_normalized_sums( size_t n, std::vector<T>& x )
    {
       std::vector<T> result = compute_sums( n, x );
       size_t m = x.size();

       for( size_t i = 0; i < result.size(); i++ )
       {
         result[i] /= boost::math::binomial_coefficient<T>( i + m - 1, i );
       }

       return result;
     }

};

////////////////////////////////////////////////////////////////////////////////
///
/// \class power_set
///
//! \brief A class to compute power set sums.  The power set sum (or power sum)
//!        of degree m is the sum of mth powers of the set elements.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// power_set().
//##############################################################################

template <class T = double>
class power_set
{
  public:

    power_set( ){}

/// \param n The highest order of power set sums required.
/// \param x The vector of set values.
/// \return A vector of n + 1 power set sums.

    std::vector<T>
    compute_sums( size_t n, std::vector<T>& x )
    {
       std::vector<T> result;
       result.push_back(1);
       std::vector<T> y = x;
       result.push_back( std::accumulate( y.begin(), y.end(), 0. ) );

       for( size_t i = 2; i <= n; i++ )
       {
         for( size_t j = 0; j < y.size(); j++ )
         {
           y[j] *= x[j];
         }
         T tot = std::accumulate( y.begin(), y.end(), 0. );
         result.push_back( tot );
       }

       return result;
     }

/// \param n The highest order of power set sums required.
/// \return A vector of n + 1 normalized set sums.

    std::vector<T>
    compute_normalized_sums( size_t n, std::vector<T>& x )
    {
       std::vector<T> result = compute_sums( n, x );
       size_t m = x.size();

       for( size_t i = 0; i < result.size(); i++ )
       {
         result[i] /= x.size();
       }

       return result;
     }

};

}  // namespace math

}  // namespace wn_user

#endif // WN_SET_SUM_HPP
