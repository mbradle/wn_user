////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer and Norberto J. Davila.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#include <boost/functional/hash.hpp>
#include <boost/math/special_functions/binomial.hpp>
#include <unordered_map>
#include <utility>

#ifndef WN_BELL_HPP
#define WN_BELL_HPP

namespace wn_user
{

namespace math
{

typedef std::pair<size_t, size_t> my_pair;

////////////////////////////////////////////////////////////////////////////////
///
/// \class bell
///
//! \brief A class to compute Bell polynomials.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// bell().
//##############################################################################

template <class T = double>
class bell
{
  public:


/// \param x A vector of x values for the polynomials.  The value x[0]
///          must be zero.
/// \param n The index for the requested Bell polynomial.
/// \return The Bell polynomial evaluated at the input x values
///         \f$B_n(x)\f$.

    T
    compute( std::vector<T>& x, size_t n )
    {
      assert( x[0] == 0 );
      assert( x.size() >= n );
      b.clear();

      if( b.find(n) != b.end() )
      {
        return b[n];
      }

      if( n == 0 )
      {
        return 1;
      }

      double result = 0;
      for( size_t i = 0; i <= n - 1; i++ )
      {
        result +=
          boost::math::binomial_coefficient<T>( n - 1, i ) *
          x[i+1] * compute( x, n - i - 1 );
      }
      b[n] = result;
      return result;
    }
  
/// \param v_b A vector of values $\fB_n\f$ for $\fn = 0...p\f$.  $\fB_0\f$
///            must be unity in keeping with the definition of Bell
///            polynomials.
/// \return x A vector of length $\fp+1\f$ containing the x values obtained
///           by inverting the Bell polynomials. x[0] will be zero.

    std::vector<T>
    invert( std::vector<T>& v_b )
    {
      std::vector<T> x;
      assert( v_b[0] == 1 );
      x.clear();
      x.push_back( 0 );
      for( size_t n = 0; n < v_b.size()-1; n++ )
      {
        T result = v_b[n+1];
        for( size_t i = 0; i < n; i++ )
        {
          result -=
            boost::math::binomial_coefficient<T>( n, i ) * x[i+1] * v_b[n-i];
        }
        x.push_back( result );
      }
      return x;
    }
  
/// \param v_b A vector of values $\fB_n(x)\f$ for $\fn = 0...p\f$.  $\fB_0\f$
///            must be unity in keeping with the definition of Bell
///            polynomials.
/// \return v_n_b A vector of length $\fp\f$ containing the Bell polynomials
///            $\fB_n(-x)\f$ corresponding to the input values. 

    std::vector<T>
    negate( std::vector<T>& v_b )
    {
      std::vector<T> v_n_b;
      assert( v_b[0] == 1 );
      v_n_b.push_back( 1 );
      for( size_t n = 1; n < v_b.size(); n++ )
      {
        v_n_b.push_back( 0 );
        for( size_t i = 0; i < n; i++ )
        {
          v_n_b[n] -=
            boost::math::binomial_coefficient<T>( n, i ) *
            v_n_b[i] * v_b[n - i];
        }
      }
        
      return v_n_b;
    }
  
  private:
    std::unordered_map<size_t, T> b;
};

////////////////////////////////////////////////////////////////////////////////
///
/// \class partial_bell
///
//! \brief A class to compute partial Bell polynomials.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// partial_bell().
//##############################################################################

template <class T = double>
class partial_bell
{
  public:

/// \param x A vector of x values for the polynomials.  The value x[0]
///          must be zero.

    partial_bell( std::vector<T>& x ) : m_x( x )
    {
      assert( m_x[0] == 0 );
      b.clear();
    }

/// \param n The first index for the requested partial Bell polynomial.
/// \param k The second index for the requested partial Bell polynomial.
/// \return The partial Bell polynomial evaluated at the input x values
///         \f$B_{n,k}(x)\f$.

    T
    operator()( size_t n, size_t k )
    {
       assert( m_x.size() + k > n + 1 );

       if( b.find({n, k}) != b.end() )
       {
         return b[{n, k}];
       }

       if( n == 0 && k == 0 )
       {
         return 1;
       }
       else if( n == 0 || k == 0 )
       {
         return 0;
       }

       double result = 0;
       for( size_t i = 1; i <= n - k  + 1; i++ )
       {
         result +=
           boost::math::binomial_coefficient<T>( n - 1, i - 1 ) *
           m_x[i] * (*this)( n - i, k - 1 );
       }
       b[{n, k}] = result;
       return result;
    }
  
  private:
    std::vector<T> m_x;
    std::unordered_map<my_pair, T, boost::hash<my_pair>> b;
};

////////////////////////////////////////////////////////////////////////////////
///
/// \class partial_bell_inverter
///
/// \brief A class to compute the x values for an input set of
///        partial Bell polynomials $\fB_{k+m, k}\f$.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// partial_bell_inverter().
//##############################################################################

template <class T = double>
class partial_bell_inverter
{
  public:

/// \param k The second index for the polynomials.

    partial_bell_inverter( size_t k ) : m_k( k ){}

/// \param v_b A vector of values $\fB_{k+m,k}\f$ for $\fm = 0...p\f$
/// \return x A vector of length $\fp+1\f$ containing the x values obtained
///           by inverting the partial Bell polynomials.

    std::vector<T>
    operator()( std::vector<T>& v_b )
    {
      x.clear();
      b.clear();
      x.push_back( 0 );
      x.push_back( pow( v_b[0], 1. / m_k ) );
      for( size_t n = 0; n <= m_k; n++ )
      {
        b[{n,n}] = pow( x[1], n );
      }
      for( size_t m = 1; m < v_b.size(); m++ )
      {
        T result = v_b[m];
        for( size_t i = 1; i <= m; i++ )
        {
          result -=
            boost::math::binomial_coefficient<T>( m_k + m - 1, i - 1 ) *
            x[i] * reverse_b( m_k + m - i, m_k - 1 );
        }
        x.push_back(
          result / 
            (
              boost::math::binomial_coefficient<T>( m_k + m, m + 1 ) *
              pow( x[1], m_k - 1 )
            )
        );
        partial_bell<T> pb( x );
        for( size_t n = 1; n <= m_k; n++ )
        {
          b[{n+m, n}] = pb( n+m, n );
        }
      }
      return x;
    }
  
  private:
    size_t m_k;
    std::vector<T> x;
    std::unordered_map<my_pair, T, boost::hash<my_pair>> b;

    T
    reverse_b( size_t n, size_t k )
    {
      if( n == 0 && k == 0 )
      {
        return 1;
      }
      else if( n == 0 || k == 0 || n < k )
      {
        return 0;
      }
      else if( b.find({n, k}) != b.end() )
      {
        return b[{n, k}];
      }
      else
      {
        T result = 0;
        for( size_t i = 1; i <= n - k; i++ )
        {
          result +=
             boost::math::binomial_coefficient<T>( n - 1, i - 1 ) *
             x[i] * reverse_b( n - i, k - 1 );
        }
        return result;
      }
    }

};

}  // namespace math

}  // namespace wn_user

#endif // WN_BELL_HPP
