////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#include <algorithm>
#include <limits>

#ifndef WN_LINEAR_INTERPOLATOR_HPP
#define WN_LINEAR_INTERPOLATOR_HPP

namespace wn_user
{

namespace math
{

////////////////////////////////////////////////////////////////////////////////
//!
//! \class linear_interpolator
//!
//! \brief A class to perform linear interpolation on arrays.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// linear_interpolator().
//##############################################################################

template<class T = double>
class linear_interpolator
{

  public:
    linear_interpolator(){}

/// Routine to set the interpolator.
/// \param x The independent variable array.  The array must be sorted
///          in ascending order.
/// \param y The dependent variable array.
    void set( std::vector<T> x, std::vector<T> y )
    {
      assert( x.size() == y.size() );
      for( size_t i = 0; i < x.size(); i++ )
      {
        table.push_back( std::make_pair( x[i], y[i] ) );
      }
    }

/// The interpolation routine.
/// \param x The value at which to perform the interpolation.
/// \return The interpolated value.  If the independent variable is less
///         than the first value in the input indendent variable array,
///         the routine returns the dependent variable value corresponding to
///         that first value.  If the independent variable is greater
///         than the largest value in the array, the routine returns the
///         dependent variable value corresponding to that largest value.

    T operator()( T x ) const
    {
      if( x <= table[0].first )
      {
        return table[0].second;
      }
      else if( x >= table.back().first )
      {
        return table.back().second;
      }

      typename std::vector<std::pair<T,T> >::const_iterator it, it2;

      it =
         std::lower_bound(
           table.begin(),
           table.end(),
           std::make_pair( x, -std::numeric_limits<T>::infinity() )
         );

      it2 = it;
      --it2;

      if(
        it->second == std::numeric_limits<T>::infinity() &&
        it2->second == std::numeric_limits<T>::infinity()
      )
      {
        return std::numeric_limits<T>::infinity();
      }
      else if(
        it->second == -std::numeric_limits<T>::infinity() &&
        it2->second == -std::numeric_limits<T>::infinity()
      )
      {
        return -std::numeric_limits<T>::infinity();
      }
      else
      {
        return
          it2->second +
          ( it->second - it2->second ) *
          ( x - it2->first ) / ( it->first - it2->first );
      }

    }

/// Routine to return the interpolation table.
/// \return The interpolation table as a vector of (x, y) pairs.

    std::vector<std::pair<T,T> >
    getTable( ) const
    {
      return table;
    }

  private:
    std::vector<std::pair<T,T> > table;

};

}  // namespace math

}  // namespace wn_user

#endif // WN_LINEAR_INTERPOLATOR_HPP
