////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file bracketer_by_expansion.hpp
//! \brief A file to bracket 1d roots.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/function.hpp>

#ifndef WN_BRACKETER_BY_EXPANSION_HPP
#define WN_BRACKETER_BY_EXPANSION_HPP

namespace wn_user
{

namespace math
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class bracketer_by_expansion
///
//! \brief A class to bracket a root by expansion.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// bracketer_by_expansion().
//##############################################################################

template<class T = double>
class bracketer_by_expansion
{

  public:
/// The constructor.
/// \param func The function whose root should be bracketed.

    bracketer_by_expansion( boost::function<T(T)> _func ) : func( _func )
    {
      max_iter = 100;
      factor = 2;
    }

/// Routine to update the expansion factor.
/// \param new_factor The new factor to be applied.

    void updateFactor( T new_factor ) { factor = new_factor; }

/// Routine to update the maximum number of iterations in the bracketing.
/// \param new_max The new maximum number of iterations to be applied.

    void updateMaxIters( boost::uintmax_t new_max ) { max_iter = new_max; }

/// The bracketer.
/// \param x1 The lower guess.
/// \param x2 The upper guess.  x2 must be greater than x1.
/// \return The bracket as a pair (x1, x2).

    std::pair<T, T>
    operator()( T x1, T x2 )
    {

      assert( x1 < x2 );

      boost::uintmax_t iter = 0;

      while( iter++ < max_iter )
      {
        if( func( x1 ) * func( x2 ) < 0 )
        {
          return std::make_pair( x1, x2 );
        }
        x1 -= fabs( x1 ) * factor;
        x2 += fabs( x2 ) * factor;
      }

      std::cerr << "Unable to bracket root." << std::endl;
      exit( EXIT_FAILURE );

    }

  private:
    boost::function<T(T)> func;
    boost::uintmax_t max_iter;
    boost::uintmax_t iter;
    T factor;

};

} // namespace math

} // namespace wn_user

#endif // WN_BRACKETER_BY_EXPANSION_HPP
