////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

/// @cond _MATRIX_MODIFIER_SPECIFIC_SPECIES_

////////////////////////////////////////////////////////////////////////////////
//!
//! \file 
//!
////////////////////////////////////////////////////////////////////////////////

#include "matrix_modifier/base/specific_species.hpp"
#include "nnt/iter.h"

#ifndef NNP_MATRIX_MODIFIER_DETAIL_HPP
#define NNP_MATRIX_MODIFIER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

typedef std::map<std::string, double> specific_map_t;

//##############################################################################
// matrix_modifier().
//##############################################################################

class matrix_modifier : public base::specific_species
{

  public:
    matrix_modifier( v_map_t& v_map ) :
      base::specific_species(), my_program_options()
    {

      if( v_map.count( S_SPECIFIC_SPECIES ) )
      {

        BOOST_FOREACH(
          const std::vector<std::vector<std::string> >::value_type& v_y,
          my_program_options.composeOptionVectorOfVectors(
            v_map, S_SPECIFIC_SPECIES, 2
          )
        )
        {
          my_map[v_y[0]] = boost::lexical_cast<double>( v_y[1] );
        }
      }

    }

    void
    modifySpecificSpecies(
      WnMatrix * p_matrix,
      gsl_vector * p_rhs,
      nnt::Zone& zone
    )
    {

      BOOST_FOREACH( specific_map_t::value_type& t, my_map )
      {

        modifyMatrixForSpecificSpecies(
          p_matrix,
          p_rhs,
          zone,
          t.first,
          t.second
        );

      }
   
    }

    void operator()( nnt::Zone& zone )
    {
      if( !my_map.empty() )
      {
        zone.updateFunction(
          nnt::s_MATRIX_MODIFICATION_FUNCTION,
          static_cast<boost::function<void( WnMatrix *, gsl_vector * )> >(
            boost::bind(
              &matrix_modifier::modifySpecificSpecies,
              this,
              _1,
              _2,
              boost::ref( zone )
            )
          )
        );
      }
    }

  private:
    program_options my_program_options;
    specific_map_t my_map;

};

//##############################################################################
// matrix_modifier_options().
//##############################################################################

class matrix_modifier_options
{

  public:
    matrix_modifier_options(){}

    std::string
    getExample()
    {
      return " --specific_species \"{n; 1.e-15}\"";
    }

    void
    getOptions( po::options_description& matrix_modifier )
    {

      try
      {

        matrix_modifier.add_options()

        (
          S_SPECIFIC_SPECIES,
          po::value<std::vector<std::string> >()->multitoken(),
          "Species to keep at specified abundance (enter as doublet {name; abundance})"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

}  // namespace detail

}  // namespace wn_user

#endif // NNP_MATRIX_MODIFIER_DETAIL_HPP
