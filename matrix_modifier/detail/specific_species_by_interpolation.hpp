////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file matrix_modifier.hpp
//! \brief A file to define matrix modification routines.  Specific
//!        species have their abundances set to a value interpolated
//!        from data in a file.
//!
////////////////////////////////////////////////////////////////////////////////

#include <algorithm>

#include "math/linear_interpolator.hpp"
#include "matrix_modifier/base/specific_species.hpp"

#ifndef NNP_MATRIX_MODIFIER_DETAIL_HPP
#define NNP_MATRIX_MODIFIER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

typedef
std::map<std::string, math::linear_interpolator<> > interp_map_t;

namespace detail
{


//##############################################################################
// matrix_modifier().
//##############################################################################

class matrix_modifier : public base::specific_species
{

  public:
    matrix_modifier( v_map_t& v_map ) :
      base::specific_species(), my_program_options()
    {

      if( v_map.count( S_SPECIFIC_SPECIES ) )
      {

        BOOST_FOREACH(
          const std::vector<std::vector<std::string> >::value_type& v_y,
          my_program_options.composeOptionVectorOfVectors(
            v_map, S_SPECIFIC_SPECIES, 2
          )
        )
        {

          std::ifstream my_file;
          double d_x1, d_x2;
          std::vector<double> v_x1, v_x2;

          my_file.open( v_y[1].c_str() );

          if( !my_file.is_open() || my_file.bad() )
          {
            std::cerr << "Couldn't open file " << v_y[1] << "!" << std::endl;
            exit( EXIT_FAILURE );
          }

          while( my_file >> d_x1 >> d_x2 )
          {
            v_x1.push_back( d_x1 );
            v_x2.push_back( d_x2 );
          }

          my_file.close();

          interp_map.insert(
            std::make_pair(
              v_y[0],
              math::linear_interpolator<>()
            )
          );

          interp_map[v_y[0]].set( v_x1, v_x2 );

        }
      }
    }

    void
    modifySpecificSpecies(
      WnMatrix * p_matrix,
      gsl_vector * p_rhs,
      nnt::Zone& zone
    )
    {

      BOOST_FOREACH( interp_map_t::value_type& t, interp_map )
      {
        modifyMatrixForSpecificSpecies(
          p_matrix,
          p_rhs,
          zone,
          t.first,
          t.second( zone.getProperty<double>( nnt::s_TIME ) )
        );
      }

    }

    void operator()( nnt::Zone& zone )
    {
      if( !interp_map.empty() )
      {

        zone.updateFunction(
          nnt::s_MATRIX_MODIFICATION_FUNCTION,
          static_cast<boost::function<void( WnMatrix *, gsl_vector * )> >(
            boost::bind(
              &matrix_modifier::modifySpecificSpecies,
              this,
              _1,
              _2,
              boost::ref( zone )
            )
          )
        );
      }

    }

  private:
    program_options my_program_options;
    interp_map_t interp_map;


};

//##############################################################################
// matrix_modifier_options().
//##############################################################################

class matrix_modifier_options
{

  public:
    matrix_modifier_options(){}

    std::string
    getExample()
    {
      return " --specific_species \"{n; n_abundance.txt}\"";
    }

    void
    getOptions( po::options_description& matrix_modifier )
    {

      try
      {

        matrix_modifier.add_options()

        (
          S_SPECIFIC_SPECIES,
          po::value<std::vector<std::string> >()->multitoken(),
          "Species to keep at specified abundance from file (enter as doublet {name; file})"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

}  // namespace detail

}  // namespace wn_user

#endif // NNP_MATRIX_MODIFIER_DETAIL_HPP
