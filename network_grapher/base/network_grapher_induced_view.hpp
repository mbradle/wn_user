////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file induced_view.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_NETWORK_GRAPHER_INDUCED_VIEW__HPP
#define WN_NETWORK_GRAPHER_INDUCED_VIEW__HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

typedef std::pair<Libnucnet__NucView *, Libnucnet__ReacView *> views_t;

#define S_INDUCED_NUC_XPATH     "induced_nuc_xpath"
#define S_INDUCED_REAC_XPATH    "induced_reac_xpath"

class network_grapher_induced_view
{

  public:
    network_grapher_induced_view(){}
    network_grapher_induced_view( v_map_t& v_map )
    {
      sInducedNucXPath = v_map[S_INDUCED_NUC_XPATH].as<std::string>();
      sInducedReacXPath = v_map[S_INDUCED_REAC_XPATH].as<std::string>();
    }

    std::string
    getInducedNucXPath( ) { return sInducedNucXPath; }

    std::string
    getInducedReacXPath( ) { return sInducedReacXPath; }

    views_t
    getInducedViews( Libnucnet__Net * p_net )
    {
      return
        std::make_pair(
          Libnucnet__NucView__new( 
            Libnucnet__Net__getNuc( p_net ),
            getInducedNucXPath().c_str()
          ),
          Libnucnet__ReacView__new( 
            Libnucnet__Net__getReac( p_net ),
            getInducedReacXPath().c_str()
          )
        );
    }

    void
    clearViews( views_t& views )
    {
      Libnucnet__NucView__free( views.first );
      Libnucnet__ReacView__free( views.second );
    }

  private:
    std::string sInducedNucXPath, sInducedReacXPath;

};
    
//##############################################################################
// network_grapher_induced_view_options().
//##############################################################################

class network_grapher_induced_view_options
{

  public:
    network_grapher_induced_view_options(){}

    std::string
    getExampleString()
    { return ""; }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Induced nuclear XPath.
        (
          S_INDUCED_NUC_XPATH,
          po::value<std::string>()->default_value( "" ),
          "Nuclear XPath to get induced subgraph (default: all nuclides)."
        )
    
        // Induced reaction XPath.
        (
          S_INDUCED_REAC_XPATH,
          po::value<std::string>()->default_value( "" ),
          "Reaction XPath to get induced subgraph (default: all reactions)."
        )
    
        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_INDUCED_VIEW__HPP

