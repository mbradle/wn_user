////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file output_file.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_NETWORK_GRAPHER_OUTPUT_FILE__HPP
#define WN_NETWORK_GRAPHER_OUTPUT_FILE__HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

#define S_GRAPH_OUTPUT_FILE     "graph_output_file"

class network_grapher_output_file
{

  public:
    network_grapher_output_file(){}
    network_grapher_output_file( v_map_t& v_map )
    {
      sOutputFile = v_map[S_GRAPH_OUTPUT_FILE].as<std::string>();
    }

    std::string
    getOutputFile()
    {
      return sOutputFile;
    }

  private:
    std::string sOutputFile;

};
    
//##############################################################################
// network_grapher_output_file_options().
//##############################################################################

class network_grapher_output_file_options
{

  public:
    network_grapher_output_file_options(){}

    std::string
    getExampleString()
    { return ""; }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Output file.
        (
          S_GRAPH_OUTPUT_FILE,
          po::value<std::string>()->required(),
          "Graph output file."
        )
    
        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_OUTPUT_FILE__HPP
