////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file graph.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_NETWORK_GRAPHER_BASE_HPP
#define WN_NETWORK_GRAPHER_BASE_HPP

#include <boost/graph/copy.hpp>

#include <boost/graph/iteration_macros.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/graphviz.hpp>
#include <boost/scoped_ptr.hpp>

#include "my_graph.h"
#include "utility/stable_species.hpp"
#include "network_grapher/base/vertex_mapper.hpp"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

#define S_ALLOW_ISOLATED        "allow_isolated_vertices"
#define S_BGCOLOR               "background_color"
#define S_COLOR                 "color"
#define S_GRAPH_REACTION_COLOR  "graph_reaction_color"
#define S_NODE_LABEL_STYLE      "node_label_style"
#define S_NODE_SHAPE            "node_shape"
#define S_NODE_OUTLINE_COLOR    "node_outline_color"
#define S_NON_STABLE_COLOR      "non_stable_color"
#define S_SCALING               "scaling"
#define S_STABLE_COLOR          "stable_color"
#define S_STABLE_VERTEX         "stable vertex"
#define S_STATE_SCALING         "state_scaling"
#define S_STYLE                 "style"
#define S_USER_SPECIES_COLOR    "user_species_color"
#define S_USER_VERTEX           "user vertex"

typedef std::map<std::string, std::string> my_map_t;

typedef std::vector<std::pair<std::string, std::string> > my_vector_t;

//##############################################################################
// vertexFilter().
//##############################################################################

template<
  typename Graph,
  typename Vertex = typename boost::graph_traits<Graph>::vertex_descriptor
>
struct vertexFilter
{
  vertexFilter(){}
  vertexFilter(
    Graph& g, Libnucnet__NucView * p_view, bool b_allow_isolated
  ) : bAllowIsolated( b_allow_isolated )
  {
    pNuc = Libnucnet__NucView__getNuc( p_view );
    vertexNuclideMap = boost::get( vertex_nuclide, g );
    vertexPropsMap = boost::get( vertex_properties, g );
    BGL_FORALL_VERTICES_T( v, g, Graph )
    {
      if( boost::in_degree( v, g ) == 0 && boost::out_degree( v, g ) == 0 )
      {
        isolatedSet.insert( v );
      }
    }
    iz_max = Libnucnet__Nuc__getLargestNucleonNumber( pNuc, "z" ); 
    in_max = Libnucnet__Nuc__getLargestNucleonNumber( pNuc, "a" ) - iz_max; 
    iz_min = iz_max;
    in_min = in_max;
    BOOST_FOREACH( nnt::Species species, nnt::make_species_list( pNuc ) )
    {
      unsigned int i_z = Libnucnet__Species__getZ( species.getNucnetSpecies() );
      unsigned int i_n =
        Libnucnet__Species__getA( species.getNucnetSpecies() ) - i_z;
      if( i_z < iz_min )
      {
        iz_min = i_z;
      }
      if( i_n < in_min )
      {
        in_min = i_n;
      }
    }
  }

  bool operator()( const Vertex& v ) const
  {
    Libnucnet__Species * p_species_graph = vertexNuclideMap[v];

    // Include anchor vertices.

    if( isAnchor( v ) )
    {
      return true;
    }

    // Check for isolated vertex.

    if(
      !bAllowIsolated
      &&
      !boost::any_cast<bool>( vertexPropsMap[v][S_STABLE_VERTEX] )
      &&
      !boost::any_cast<bool>( vertexPropsMap[v][S_USER_VERTEX] )
    )
    {
      if( isolatedSet.find( v ) != isolatedSet.end() )
      {
        return false;
      }
    }

    // Check that vertex is in view.

    Libnucnet__Species * p_species_view =
      Libnucnet__Nuc__getSpeciesByName(
        pNuc,
        Libnucnet__Species__getName( p_species_graph )
      );
    if( p_species_view )
    {
      return true;
    }
    else
    {
      return false;
    } 
  }

  bool isAnchor( const Vertex& v ) const
  {
    Libnucnet__Species * p_species_graph = vertexNuclideMap[v];

    // Include anchor vertices.

    unsigned int i_z = Libnucnet__Species__getZ( p_species_graph );
    unsigned int i_n = Libnucnet__Species__getA( p_species_graph ) - i_z;

    if( (i_z == iz_max && i_n == in_max) || (i_z == iz_min && i_n == in_min) )
    {
      return true;
    }
    else
    {
      return false;
    }
  }

  Libnucnet__Nuc * pNuc;
  typename boost::property_map<Graph, vertex_nuclide_t>::type vertexNuclideMap;
  typename boost::property_map<Graph, vertex_properties_t>::type vertexPropsMap;
  std::set<Vertex> isolatedSet;
  unsigned int iz_min, iz_max, in_min, in_max;
  bool bAllowIsolated;
};
  
//##############################################################################
// edgeFilter().
//##############################################################################

template<typename Graph>
struct edgeFilter
{
  edgeFilter(){}
  edgeFilter(
    Graph& g,
    Libnucnet__NucView * p_nuc_view,
    Libnucnet__ReacView * p_reac_view
  )
  {
    pNuc = Libnucnet__NucView__getNuc( p_nuc_view );
    pReac = Libnucnet__ReacView__getReac( p_reac_view );
    vertexNuclideMap = boost::get( vertex_nuclide, g );
    edgeReactionMap = boost::get( edge_reaction, g );
    BGL_FORALL_EDGES_T( e, g, Graph )
    {
      sourceMap[e] = boost::source( e, g );
    }
  }

  template<typename Edge>
  bool operator()( const Edge& e ) const
  {
    Libnucnet__Reaction * p_reaction_view =
      Libnucnet__Reac__getReactionByString(
        pReac,
        Libnucnet__Reaction__getString( edgeReactionMap[e] )
      );
    if( !p_reaction_view )
    {
      return false;
    }
    vertex_t v = sourceMap.find( e )->second;
    if(
      !Libnucnet__Nuc__getSpeciesByName(
        pNuc,
        Libnucnet__Species__getName( vertexNuclideMap[v] )
      )
    ) { return false; }

    return true;
  }

  Libnucnet__Nuc * pNuc;
  Libnucnet__Reac * pReac;
  typename boost::property_map<Graph, edge_reaction_t>::type edgeReactionMap;
  typename boost::property_map<Graph, vertex_nuclide_t>::type
    vertexNuclideMap;
  typedef typename boost::graph_traits<Graph>::vertex_descriptor vertex_t;
  typedef typename boost::graph_traits<graph_t>::edge_descriptor edge_t;
  std::map<edge_t, vertex_t> sourceMap;
};

//##############################################################################
// vertexWriter().
//##############################################################################

template<typename Graph>
class vertexWriter
{
  public:
    vertexWriter( Graph& g )
    {
      vertexPropsMap = boost::get( vertex_properties, g );
      vertexNuclideMap = boost::get( vertex_nuclide, g );
    };

    template <class Vertex>
    void operator()( std::ostream& out, Vertex v )
    {
      Libnucnet__Species * p_species = vertexNuclideMap[v];
      int i_z = Libnucnet__Species__getZ( p_species );
      int i_n = Libnucnet__Species__getA( p_species ) - i_z;
      double d_scale = boost::any_cast<double>( vertexPropsMap[v][S_SCALING] );
      double d_state_scale = 
        boost::any_cast<double>( vertexPropsMap[v][S_STATE_SCALING] );
      char * s_latex_string =
        Libnucnet__Species__createLatexString( p_species );
      double d_n = d_scale * i_n, d_z;
      std::string state = "";
      if( Libnucnet__Species__getStateString( p_species ) )
      {
        state = std::string( Libnucnet__Species__getStateString( p_species ) );
      }
      if( state == "g" )
      {
        d_z = d_scale * (i_z - d_state_scale);
      }
      else if( state == "m" )
      {
        d_z = d_scale * (i_z + d_state_scale);
      }
      else
      {
        d_z = d_scale * i_z;
      }
      out <<
        boost::format(
          " [texlbl=\"\\huge{$%s$}\" \
          pos=\"%d,%d!\", \
          style=filled, fillcolor=\"%s\" "
        ) % 
          s_latex_string %
          d_n %
          d_z %
          boost::any_cast<std::string>( (vertexPropsMap[v])[S_COLOR] );
      if( vertexPropsMap[v].find( S_NODE_OUTLINE_COLOR ) !=
          vertexPropsMap[v].end()
      )
      {
        out << ", color=" << 
          boost::any_cast<std::string>(
            (vertexPropsMap[v])[S_NODE_OUTLINE_COLOR]
          );
      }
      if( vertexPropsMap[v].find( S_NODE_SHAPE ) != vertexPropsMap[v].end() )
      {
        out << ", shape=" << 
          boost::any_cast<std::string>( (vertexPropsMap[v])[S_NODE_SHAPE] );
      }
      if( vertexPropsMap[v].find( S_NODE_LABEL_STYLE ) !=
          vertexPropsMap[v].end()
      )
      {
        out << ", lblstyle=" << 
          boost::any_cast<std::string>(
            (vertexPropsMap[v])[S_NODE_LABEL_STYLE]
          );
      }
      out << "]" << std::endl;
      free( s_latex_string );
    }

  private:
    typename boost::property_map<Graph, vertex_properties_t>::type
      vertexPropsMap;
    typename boost::property_map<Graph, vertex_nuclide_t>::type
      vertexNuclideMap;

};

//##############################################################################
// edgeWriter().
//##############################################################################

template<typename Graph>
class edgeWriter
{
  public:
    edgeWriter(){}
    edgeWriter( Graph& g )
    {
      edgePropsMap = boost::get( edge_properties, g );
      edgeWeightMap = boost::get( boost::edge_weight, g );
    };

    template <class Edge>
    void operator()( std::ostream& out, Edge e )
    {
      out.precision(6);
      out <<
          boost::format(
              "[style=\"line width = %.4fpt, %s\" color = \"%s\"]"
          ) %
              edgeWeightMap[e] %
              boost::any_cast<std::string>( edgePropsMap[e][S_STYLE] ) %
              boost::any_cast<std::string>( edgePropsMap[e][S_COLOR] )
          <<
          std::endl;
    } 

  private:
    typename boost::property_map<Graph, edge_properties_t>::type
        edgePropsMap;
    typename boost::property_map<Graph, boost::edge_weight_t>::type
        edgeWeightMap;

};

//##############################################################################
// graphWriter().
//##############################################################################

template<typename Graph>
class graphWriter
{ 
  public:
    graphWriter(){}
    graphWriter( Graph& g )
    {
      typename boost::graph_property<Graph, graph_properties_t>::type
        graph_props_map = boost::get_property( g, graph_properties );
      bgcolor = graph_props_map[S_BGCOLOR];
      shape = graph_props_map[S_NODE_SHAPE];
      color = graph_props_map[S_NODE_OUTLINE_COLOR];
    }

    void operator()( std::ostream& out ) const
    { 
      out << "graph [bgcolor=" + bgcolor + "]" << std::endl;
      out << "node [shape=" + shape + "]" << std::endl;
    };

  private:
    std::string bgcolor, shape, color;
};

//##############################################################################
// network_grapher().
//##############################################################################

class network_grapher : public base::vertex_mapper
{

  public:
    network_grapher(){}
    network_grapher( v_map_t& v_map ) :
      base::vertex_mapper(), my_stable_species(), my_program_options()
    {

      sBackGroundColor = v_map[S_BGCOLOR].as<std::string>();
      sNodeShape = v_map[S_NODE_SHAPE].as<std::string>();
      sNodeOutlineColor = v_map[S_NODE_OUTLINE_COLOR].as<std::string>();
      sNonStableColor = v_map[S_NON_STABLE_COLOR].as<std::string>();
      sStableColor = v_map[S_STABLE_COLOR].as<std::string>();
      dScaling = v_map[S_SCALING].as<double>();
      dStateScaling = v_map[S_STATE_SCALING].as<double>();

      bAllowIsolatedVertices = v_map[S_ALLOW_ISOLATED].as<bool>();

      // Set user species map.

      BOOST_FOREACH(
        const std::vector<std::vector<std::string> >::value_type& v_m,
        my_program_options.composeOptionVectorOfVectors(
          v_map, S_USER_SPECIES_COLOR, 2
        )
      )
      {
        user_species_map[v_m[0]] = v_m[1];
      }

      // Set view color vector.

      BOOST_FOREACH(
        const std::vector<std::vector<std::string> >::value_type& v_m,
        my_program_options.composeOptionVectorOfVectors(
          v_map, S_GRAPH_REACTION_COLOR, 2
        )
      )
      {
        vViewColor.push_back( std::make_pair(v_m[0], v_m[1]) );
      }
    }

    template<typename Graph>
    void
    setGraphReactionColorMap( Graph& g )
    {

      Libnucnet__Net * p_net = boost::get_property( g, graph_network );

      BOOST_FOREACH(
        nnt::Reaction reaction,
        nnt::make_reaction_list( Libnucnet__Net__getReac( p_net ) )
      )
      {
        boost::get_property( g, graph_reaction_color_map )[
          Libnucnet__Reaction__getString( reaction.getNucnetReaction() )
        ] = "black";
      }

      BOOST_FOREACH( const my_vector_t::value_type& p, vViewColor )
      {
        Libnucnet__ReacView * p_reac_view =
          Libnucnet__ReacView__new(
            Libnucnet__Net__getReac( p_net ), p.first.c_str()
          );

        BOOST_FOREACH(
          nnt::Reaction reaction,
          nnt::make_reaction_list(
            Libnucnet__ReacView__getReac( p_reac_view )
          )
        )
        {
          boost::get_property( g, graph_reaction_color_map )[
            Libnucnet__Reaction__getString( reaction.getNucnetReaction() )
          ] = p.second;
        }
        Libnucnet__ReacView__free( p_reac_view );
      }

    }

    template<typename Graph>
    Graph
    createGraph( Libnucnet__Net * p_net )
    {

      typedef typename boost::graph_traits<Graph>::vertex_descriptor vertex_t;

      Graph g;

      boost::get_property( g, graph_network ) = p_net;
      (boost::get_property( g, graph_properties ) )[S_BGCOLOR] =
        sBackGroundColor;
      (boost::get_property( g, graph_properties ) )[S_NODE_SHAPE] =
        sNodeShape;
      (boost::get_property( g, graph_properties ) )[S_NODE_OUTLINE_COLOR] =
        sNodeOutlineColor;

      // Set the vertices (nuclides).

      typename boost::property_map<Graph, vertex_nuclide_t>::type
        vertex_nuclide_map = boost::get( vertex_nuclide, g );

      typename boost::property_map<Graph, vertex_properties_t>::type
        vertex_props_map = boost::get( vertex_properties, g );

      std::set<std::string> my_stable_set = my_stable_species.getSet();

      BOOST_FOREACH(
        nnt::Species species,
        nnt::make_species_list( Libnucnet__Net__getNuc( p_net ) )
      )
      {
        Libnucnet__Species * p_species = species.getNucnetSpecies();
        std::string s_name = Libnucnet__Species__getName( p_species );
        vertex_t v = boost::add_vertex( g );
        vertex_nuclide_map[v] = p_species;
        vertex_props_map[v][S_SCALING] = dScaling;
        vertex_props_map[v][S_STATE_SCALING] = dStateScaling;
        vertex_props_map[v][S_USER_VERTEX] = false;
        if( my_stable_set.find( s_name ) != my_stable_set.end() )
        {
          vertex_props_map[v][S_STABLE_VERTEX] = true;
          vertex_props_map[v][S_COLOR] = sStableColor;
        }
        else
        {
          vertex_props_map[v][S_STABLE_VERTEX] = false;
          vertex_props_map[v][S_COLOR] = sNonStableColor;
        }

        if( user_species_map.find( s_name ) != user_species_map.end() )
        {
          vertex_props_map[v][S_COLOR] = user_species_map[s_name];
          vertex_props_map[v][S_USER_VERTEX] = true;
        }
      }

      // Set the reaction color map.

      setGraphReactionColorMap( g );

      // Done.

      return g;

    }

    template<typename Graph>
    void
    addReactionEdges(
      Graph& g,
      Libnucnet__Reaction * p_reaction,
      nnt::reaction_element_list_t& list1,
      nnt::reaction_element_list_t& list2,
      my_map_t& props,
      double weight
    )
    {
      typedef typename boost::graph_traits<Graph>::vertex_descriptor vertex_t;
      typename boost::graph_traits<Graph>::edge_descriptor e;
      bool b_result;

      typedef typename std::map<std::string, vertex_t> vertex_map_t;
      vertex_map_t vertexMap = createSpeciesVertexMap<Graph, vertex_t>( g );

      std::string s_color =
        boost::get_property( g, graph_reaction_color_map )[
          Libnucnet__Reaction__getString( p_reaction )
        ];

      BOOST_FOREACH( nnt::ReactionElement& r_from, list1 )
      {
        typename vertex_map_t::iterator it_from =
          vertexMap.find(
            Libnucnet__Reaction__Element__getName(
              r_from.getNucnetReactionElement()
            )
          );
        if( it_from != vertexMap.end() )
        {
          BOOST_FOREACH( nnt::ReactionElement& r_to, list2 )
          {
            if(
              r_from.getNucnetReactionElement() !=
              r_to.getNucnetReactionElement()
            )
            {
              typename vertex_map_t::iterator it_to =
                vertexMap.find(
                  Libnucnet__Reaction__Element__getName(
                    r_to.getNucnetReactionElement()
                  )
                );
              boost::tie( e, b_result ) =
                boost::add_edge( it_from->second, it_to->second, g );
              if( !b_result )
              {
                std::cerr << "Couldn't add edge." << std::endl;
                exit( EXIT_FAILURE );
              }
              boost::get( edge_reaction, g )[e] = p_reaction;
              boost::get( boost::edge_weight, g )[e] = weight;
              boost::get( edge_properties, g )[e][S_COLOR] = s_color;
              BOOST_FOREACH( my_map_t::value_type& t, props )
              {
                boost::get( edge_properties, g )[e][t.first] = t.second;
              }
            }
          }
        }
      }
    }
        
    template<typename Graph>
    Libnucnet__Net *
    getGraphNetwork( Graph& g )
    {
      return boost::get_property( g, graph_network );
    }
        
    template<typename Graph>
    void
    clearVertices( Graph& g )
    {
      BGL_FORALL_VERTICES_T( v, g, Graph )
      {
        boost::clear_vertex( v, g );
      }
    }

    bool
    allowIsolatedVertices()
    { return bAllowIsolatedVertices; }

    template<typename Graph>
    void
    copyGraph( Graph& g_original, Graph& g_destination )
    {
      boost::copy_graph( g_original, g_destination );
      boost::get_property( g_destination, graph_network ) =
        boost::get_property( g_original, graph_network );
      (boost::get_property( g_destination, graph_properties ) )[S_BGCOLOR] =
        (boost::get_property( g_original, graph_properties ) )[S_BGCOLOR];
      (boost::get_property( g_destination, graph_properties ) )[S_NODE_SHAPE] =
        (boost::get_property( g_original, graph_properties ) )
        [S_NODE_SHAPE];
      (boost::get_property( g_destination, graph_properties ) )
      [S_NODE_OUTLINE_COLOR] =
        (boost::get_property( g_original, graph_properties ) )
        [S_NODE_OUTLINE_COLOR];
      boost::get_property( g_destination, graph_reaction_color_map ) =
        boost::get_property( g_original, graph_reaction_color_map );

    }

  private:
    utility::stable_species my_stable_species;
    program_options my_program_options;
    std::string sBackGroundColor, sNodeShape, sNodeOutlineColor;
    std::string sNonStableColor, sStableColor;
    double dScaling, dStateScaling;
    std::map<std::string, std::string> user_species_map;
    my_vector_t vViewColor;
    bool bAllowIsolatedVertices;

};
    
//##############################################################################
// network_grapher_options().
//##############################################################################

class network_grapher_options
{

  public:
    network_grapher_options(){}

    std::string
    getExampleString()
    { return ""; }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Isolated vertices.
        (
          S_ALLOW_ISOLATED,
          po::value<bool>()->default_value( false, "false" ),
          "Allow isolated non-stable vertices."
        )
    
        // Background color.
        (
          S_BGCOLOR,
          po::value<std::string>()->default_value( "white" ),
          "Background color for graph."
        )
    
        // Node outline color.
        (
          S_NODE_OUTLINE_COLOR,
          po::value<std::string>()->default_value( "blue" ),
          "Outline color for nodes."
        )
    
        // Node shape.
        (
          S_NODE_SHAPE,
          po::value<std::string>()->default_value( "box" ),
          "Shape for nodes."
        )
    
        // Option for non-stable nuclides color
        (
          S_NON_STABLE_COLOR,
          po::value<std::string>()->default_value( "white" ),
          "Color for non-stable nuclides."
        )
    
        // Nuclide scaling
        (
          S_SCALING,
          po::value<double>()->default_value( 75, "75" ),
          "Scaling for nuclide vertices."
        )
    
        // State scaling
        (
          S_STATE_SCALING,
          po::value<double>()->default_value( 0.325, "0.325" ),
          "Vertical scaling for states."
        )
    
        // Option for user species color
        (
          S_USER_SPECIES_COLOR,
          po::value<std::vector<std::string> >()->multitoken(),
          "Color pair for user species (enter as {species; color})."
        )

        // Option for stable nuclides color
        (
          S_STABLE_COLOR,
          po::value<std::string>()->default_value( "yellow" ),
          "Color for stable nuclides."
        )

        // Reaction color
        (
          S_GRAPH_REACTION_COLOR,
          po::value<std::vector<std::string> >()->multitoken(),
          "Color pair for reaction (enter as {reac_xpath; color})."
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_BASE_HPP
