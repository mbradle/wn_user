#ifndef WN_MY_GRAPH_H
#define WN_MY_GRAPH_H

#include <boost/config.hpp>
#include <boost/utility.hpp>
#include <boost/any.hpp>
#include <boost/property_map/property_map.hpp>
#include <boost/graph/adjacency_list.hpp>

enum vertex_nuclide_t { vertex_nuclide };
enum vertex_properties_t { vertex_properties };
enum edge_reaction_t { edge_reaction };
enum edge_properties_t { edge_properties };
enum graph_reaction_color_map_t { graph_reaction_color_map };
enum graph_network_t { graph_network };
enum graph_properties_t { graph_properties };

namespace boost {
  BOOST_INSTALL_PROPERTY( vertex, nuclide );
  BOOST_INSTALL_PROPERTY( vertex, properties );
  BOOST_INSTALL_PROPERTY( edge, reaction );
  BOOST_INSTALL_PROPERTY( edge, properties );
  BOOST_INSTALL_PROPERTY( graph, reaction_color_map );
  BOOST_INSTALL_PROPERTY( graph, network );
  BOOST_INSTALL_PROPERTY( graph, properties );
}

typedef
  boost::property<
    vertex_properties_t, std::map<std::string, boost::any>
  > vertex_props;
typedef
  boost::property<
   vertex_nuclide_t, Libnucnet__Species *, vertex_props
  > VertexProperty;

typedef
  boost::property<
    edge_properties_t, std::map<std::string, boost::any>
  > edge_props;
typedef
  boost::property<
    edge_reaction_t, Libnucnet__Reaction *, edge_props
  > edge_props_and_reaction;
typedef
  boost::property<
    boost::edge_weight_t, double, edge_props_and_reaction
  > EdgeProperty;
typedef
  boost::property<
    boost::edge_weight_t, std::pair<double, int>, edge_props_and_reaction
  > BranchingEdgeProperty;

typedef
  boost::property<
    graph_reaction_color_map_t, std::map<std::string, std::string>
  > graph_reaction_colors;
typedef
  boost::property<
    graph_properties_t, std::map<std::string, std::string>,
    graph_reaction_colors
  > graph_props;
typedef
  boost::property<
    graph_network_t, Libnucnet__Net *, graph_props
  > GraphProperty;

typedef
  boost::adjacency_list<
    boost::listS,
    boost::vecS,
    boost::bidirectionalS,
    VertexProperty,
    EdgeProperty,
    GraphProperty
  > graph_t;

typedef
  boost::adjacency_list<
    boost::listS,
    boost::vecS,
    boost::bidirectionalS,
    VertexProperty,
    BranchingEdgeProperty,
    GraphProperty
  > branching_graph_t;

#endif // WN_MY_GRAPH_H
