////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file output_file.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_NETWORK_GRAPHER_OUTPUT_BASE__HPP
#define WN_NETWORK_GRAPHER_OUTPUT_BASE__HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

#define S_GRAPH_OUTPUT_BASE     "graph_output_base"

class network_grapher_output_base
{

  public:
    network_grapher_output_base(){}
    network_grapher_output_base( v_map_t& v_map )
    {
      sOutputBase = v_map[S_GRAPH_OUTPUT_BASE].as<std::string>();
    }

    std::string
    getOutputBase()
    {
      return sOutputBase;
    }

  private:
    std::string sOutputBase;

};
    
//##############################################################################
// network_grapher_output_base_options().
//##############################################################################

class network_grapher_output_base_options
{

  public:
    network_grapher_output_base_options(){}

    std::string
    getExampleString()
    { return ""; }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Output base.
        (
          S_GRAPH_OUTPUT_BASE,
          po::value<std::string>()->default_value(""),
          "Graph output base."
        )
    
        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_OUTPUT_BASE__HPP
