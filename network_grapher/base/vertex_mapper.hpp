////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file graph.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_VERTEX_MAPPER_HPP
#define WN_VERTEX_MAPPER_HPP

#include <boost/graph/iteration_macros.hpp>

#include "my_graph.h"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

class vertex_mapper
{

  public:
    vertex_mapper(){}

    template<typename Graph, typename Vertex>
    std::map<std::string, Vertex>
    createSpeciesVertexMap( Graph& g )
    {
      std::map<std::string, Vertex> result;
      typename boost::property_map<Graph, vertex_nuclide_t>::type
        vertex_nuclide_map = boost::get( vertex_nuclide, g );

      BGL_FORALL_VERTICES_T( v, g, Graph )
      {
        result[Libnucnet__Species__getName( vertex_nuclide_map[v] )] = v;
      }
      return result;
    }

    template<typename Graph, typename Vertex>
    std::map<Vertex, std::string>
    createReverseSpeciesVertexMap( Graph& g )
    {
      std::map<Vertex, std::string> result;
      typename boost::property_map<Graph, vertex_nuclide_t>::type
        vertex_nuclide_map = boost::get( vertex_nuclide, g );

      BGL_FORALL_VERTICES_T( v, g, Graph )
      {
        result[v] = Libnucnet__Species__getName( vertex_nuclide_map[v] );
      }
      return result;
    }

};

} // namespace base

} // namespace wn_user

#endif // WN_VERTEX_MAPPER_HPP
