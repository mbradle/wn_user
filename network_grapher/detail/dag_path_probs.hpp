///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file integrated_currents.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_grapher/base/network_grapher.hpp"
#include "network_grapher/base/network_grapher_induced_view.hpp"

#include "utility/zone_copier.hpp"
#include "yen/yen_ksp.hpp"

#ifndef WN_NETWORK_GRAPHER_DETAIL_HPP
#define WN_NETWORK_GRAPHER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define   S_DIRECTION                       "direction"
#define   S_END_VERTEX                      "end_vertex"
#define   S_NUMBER_PATHS                    "number_paths"
#define   S_PATH_PROBABILITY                "path probability"
#define   S_PATH_RANK                       "path rank"
#define   S_START_VERTEX                    "start_vertex"
#define   S_THRESHOLD_ALPHA                 "threshold_alpha"

typedef std::map<std::string, double> currents_map_t;

typedef std::pair<Libnucnet__NucView *, Libnucnet__ReacView *> views_t;

typedef typename boost::property_map<graph_t, boost::edge_weight_t>::type
  weight_map_t;

typedef typename boost::property_traits<weight_map_t>::value_type weight_t;

//##############################################################################
// edgeFilter().
//##############################################################################

template<typename Graph>
class
edgeFilter : base::edgeFilter<Graph>
{
  public:
    edgeFilter() : base::edgeFilter<Graph>() {}
    edgeFilter(
      Graph& g,
      Libnucnet__NucView * p_nuc_view,
      Libnucnet__ReacView * p_reac_view,
      weight_t d_thresh_hold
    ) : base::edgeFilter<Graph>( g, p_nuc_view, p_reac_view ),
      dThreshold( d_thresh_hold )
    {
      wm = boost::get( boost::edge_weight, g );
    }

    template<typename Edge>
    bool operator()( const Edge& e ) const
    {
      return ( wm[e] >= dThreshold && base::edgeFilter<Graph>::operator()( e ) );
    }

  private:
    weight_map_t wm;
    weight_t dThreshold;

};

typedef
  boost::filtered_graph<
    graph_t,
    edgeFilter<graph_t>,
    base::vertexFilter<graph_t>
  > filtered_graph_t;

typedef
  std::list<
    std::pair<
      weight_t,
      std::list<boost::graph_traits<filtered_graph_t>::edge_descriptor>
    >
  > path_t;

//##############################################################################
// network_grapher().
//##############################################################################

class network_grapher : public base::network_grapher,
                        public base::network_grapher_induced_view
{

  public:
    network_grapher() :
      base::network_grapher(),
      base::network_grapher_induced_view(),
      my_flow_computer(){}
    network_grapher( v_map_t& v_map ) :
      base::network_grapher( v_map ),
      base::network_grapher_induced_view( v_map ),
      my_flow_computer( v_map )
      {
        dThresholdAlpha = v_map[S_THRESHOLD_ALPHA].as<double>();
        sStart = v_map[S_START_VERTEX].as<std::string>();
        sEnd = v_map[S_END_VERTEX].as<std::string>();
        M = v_map[S_NUMBER_PATHS].as<size_t>();
        sDirection = v_map[S_DIRECTION].as<std::string>();
        if( sDirection != "forward" && sDirection != "reverse" )
        {
          std::cerr << "Graph reaction direction is invalid." << std::endl;
          exit( EXIT_FAILURE );
        }
        dT9 = v_map[nnt::s_T9].as<double>();
        dRho = v_map[nnt::s_RHO].as<double>();
      }

    graph_t
    create_graph( nnt::Zone& zone )
    {
      Libnucnet__Net * p_net = Libnucnet__Zone__getNet( zone.getNucnetZone() );

      Libnucnet__Reac * p_reac = Libnucnet__Net__getReac( p_net );

      zone.updateProperty( nnt::s_T9, dT9 );
      zone.updateProperty( nnt::s_RHO, dRho );

      graph_t g = createGraph<graph_t>( p_net );

      update_abundances( zone );

      wn_user::flow_map_t flow_map = my_flow_computer( zone );

      BOOST_FOREACH( wn_user::flow_map_t::value_type& t, flow_map )
      {
        Libnucnet__Reaction * p_reaction =
          Libnucnet__Reac__getReactionByString( p_reac, t.first.c_str() );

        double forward, reverse;
        boost::tie( forward, reverse ) = t.second;
        std::map<std::string, std::string> props;
        props["style"] = "solid";
        nnt::reaction_element_list_t reactants =
          nnt::make_reaction_nuclide_reactant_list( p_reaction );
        nnt::reaction_element_list_t products =
          nnt::make_reaction_nuclide_product_list( p_reaction );
        if( sDirection == "forward" )
        {
          addReactionEdges(
            g, p_reaction, reactants, products, props, forward
          );
        }
        else
        {
          addReactionEdges(
            g, p_reaction, products, reactants, props, reverse
          );
        }
      }

      return g;

    }

    std::vector<nnt::Zone>
    operator()( nnt::Zone& zone )
    {
      graph_t g = create_graph( zone );

      update_abundances( zone );

      views_t views = getInducedViews( getGraphNetwork( g ) );
      filtered_graph_t
        fg(
          g,
          edgeFilter<graph_t>( g, views.first, views.second, dThresholdAlpha ),
          base::vertexFilter<graph_t>( g, views.first, allowIsolatedVertices() )
        );

      std::map<std::string, vertex_t> forwardVertexMap =
        createSpeciesVertexMap<filtered_graph_t, vertex_t>( fg );

      std::map<vertex_t, std::string> reverseVertexMap =
        createReverseSpeciesVertexMap<filtered_graph_t, vertex_t>( fg );

      scaleGraphEdgeWeights<filtered_graph_t, weight_t>( fg );

      path_t r =
        boost::yen_ksp(
          fg, forwardVertexMap[sStart], forwardVertexMap[sEnd], M
        );

      reset_abundances( zone );

      std::vector<nnt::Zone> zones;

      utility::zone_copier
        zc( Libnucnet__Zone__getNet( zone.getNucnetZone() ) );

      for( const auto& r_element : r )
      {
        std::vector<std::string> s_labels;
        s_labels.push_back( boost::lexical_cast<std::string>( zones.size() ) );

        nnt::Zone new_zone;
        new_zone.setNucnetZone( zc( zone.getNucnetZone(), s_labels ) );
        new_zone.updateProperty( S_PATH_PROBABILITY, exp( -r_element.first ) );
        new_zone.updateProperty( S_PATH_RANK, zones.size() );
        new_zone.updateProperty( S_DIRECTION, sDirection );

        size_t count = 0;
        for( const auto& e : r_element.second )
        {
          new_zone.updateProperty(
            "step", boost::lexical_cast<std::string>( count++ ),
            Libnucnet__Reaction__getString( boost::get( edge_reaction, fg )[e] )
          );
        }
        zones.push_back( new_zone );
      }

      return zones;

    }

  private:
    wn_user::flow_computer my_flow_computer;
    std::string sStart, sEnd;
    size_t M;
    std::string sDirection;
    typedef boost::graph_traits<graph_t>::vertex_descriptor vertex_t;
    std::set<Libnucnet__Species *> updated_abundances;
    weight_t dThresholdAlpha;
    double dT9, dRho;

    template<typename Graph, typename Weight>
    void
    scaleGraphEdgeWeights( Graph& g )
    {
      std::map<vertex_t, Weight> v_map;
      BGL_FORALL_VERTICES_T( v, g, Graph )
      {
        v_map[v] = 0;
      }
      BGL_FORALL_EDGES_T( e, g, Graph )
      {
        Weight weight = boost::get( boost::edge_weight, g )[e];
        v_map[boost::source( e, g )] += weight;
      }
      BGL_FORALL_EDGES_T( e, g, Graph )
      {
        boost::get( boost::edge_weight, g )[e] =
          -log( boost::get( boost::edge_weight, g )[e] /
                v_map[boost::source( e, g )] ) + 1.e-100;
      }
    }

    void
    update_abundances( nnt::Zone& zone )
    {
      BOOST_FOREACH(
        nnt::Species species,
        nnt::make_species_list( 
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          )
        )
      )
      {
        if(
          Libnucnet__Zone__getSpeciesAbundance(
            zone.getNucnetZone(),
            species.getNucnetSpecies()
          ) == 0
        )
        {
          Libnucnet__Zone__updateSpeciesAbundance(
            zone.getNucnetZone(),
            species.getNucnetSpecies(),
            1.
          );
          updated_abundances.insert( species.getNucnetSpecies() );
        }
      }
    }

    void
    reset_abundances( nnt::Zone& zone )
    {
      BOOST_FOREACH( Libnucnet__Species * p_species, updated_abundances )
      {
        Libnucnet__Zone__updateSpeciesAbundance(
          zone.getNucnetZone(),
          p_species,
          0.
        );
      }
    }

};
    
//##############################################################################
// network_grapher_options().
//##############################################################################

class network_grapher_options :
  public base::network_grapher_options,
  public base::network_grapher_induced_view_options
{

  public:
    network_grapher_options() :
      base::network_grapher_options(),
      base::network_grapher_induced_view_options() {}

    std::string
    getExampleString()
    {
      return
        base::network_grapher_options::getExampleString();
    }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Threshold flow alpha
        (
          S_THRESHOLD_ALPHA,
          po::value<double>()->default_value( 0, "0" ),
          "Threshold alpha."
        )

        // T9
        (
          nnt::s_T9,
          po::value<double>()->default_value( 1., "1." ),
          "T in 10^9 K."
        )

        // rho
        (
          nnt::s_RHO,
          po::value<double>()->default_value( 1000., "1000." ),
          "mass density in g/cc."
        )

        // Start vertex
        (
          S_START_VERTEX,
          po::value<std::string>()->required(),
          "The name of the start species."
        )

        // End vertex
        (
          S_END_VERTEX,
          po::value<std::string>()->required(),
          "The name of the end species."
        )

        // Number of paths
        (
          S_NUMBER_PATHS,
          po::value<size_t>()->default_value( 100, "100" ),
          "The number of paths."
        )

        // Graph reaction direction
        (
          S_DIRECTION,
          po::value<std::string>()->default_value( "forward" ),
          "The graph reaction direction."
        )

        ;

        base::network_grapher_options::getOptions( network_grapher );
        base::network_grapher_induced_view_options::getOptions(
          network_grapher
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_DETAIL_HPP
