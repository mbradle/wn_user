///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_grapher/base/network_grapher.hpp"
#include "network_grapher/base/network_grapher_induced_view.hpp"

#ifndef WN_NETWORK_GRAPHER_DETAIL_HPP
#define WN_NETWORK_GRAPHER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define   S_GRAPH_OUTPUT_BASE      "graph_output_base"
#define   S_FLOW_CUTOFF            "flow_cutoff"
#define   S_MAX_FLOW               "maximum flow"
#define   S_MAX_FLOW_SCALE         "max_flow_scale"
#define   S_NET_FLOW               "net_flow"
#define   S_WRITE_TITLE            "write_title"

typedef
  boost::filtered_graph<
    graph_t,
    base::edgeFilter<graph_t>,
    base::vertexFilter<graph_t>
  > filtered_graph_t;

typedef std::pair<Libnucnet__NucView *, Libnucnet__ReacView *> views_t;

//##############################################################################
// vertexWriter().
//##############################################################################

template<typename Graph>
class
vertexWriter : public base::vertexWriter<Graph>
{
  public:
    vertexWriter() : base::vertexWriter<Graph>(){}
    vertexWriter( Graph& g ) : base::vertexWriter<Graph>( g ){}

    template <class Vertex>
    void operator()( std::ostream& out, Vertex v )
    {
      base::vertexWriter<Graph>::operator()( out, v );
    }

};

//##############################################################################
// edgeWriter().
//##############################################################################

template<typename Graph>
class
edgeWriter : public base::edgeWriter<Graph>
{
  public:
    edgeWriter() : base::edgeWriter<Graph>(){}
    edgeWriter( Graph& g ) : base::edgeWriter<Graph>( g )
    {
      edgeWeightMap = boost::get( boost::edge_weight, g );
    }


    template <class Edge>
    void operator()( std::ostream& out, Edge e )
    {
      base::edgeWriter<Graph>::operator()( out, e );
    }

  private:
    typename boost::property_map<Graph, boost::edge_weight_t>::type
        edgeWeightMap;

};

//##############################################################################
// graphWriter().
//##############################################################################

template<typename Graph>
class
graphWriter : public base::graphWriter<Graph>
{
  public:
    graphWriter() : base::graphWriter<Graph>(){}
    graphWriter( Graph& g, nnt::Zone& zone_, bool b_write_title ) :
      base::graphWriter<Graph>( g ), zone( zone_ ),
      bWriteTitle( b_write_title ){};

    void operator()( std::ostream& out ) const
    {
      base::graphWriter<Graph>::operator()( out );

      if( bWriteTitle )
      {
        out << "label = \"latex\";" << std::endl;
        out <<
          boost::format(
            "texlbl = \"\\huge{$time(s) = %g \
             \\ \\ \\ \\ T_9 = %g \
             \\ \\ \\ \\ \\rho(g/cc) = %g \
             \\ \\ \\ \\ {\\mathrm{flow}_{max}} = %g$}\";"
          ) %
          zone.getProperty<double>( nnt::s_TIME ) %
          zone.getProperty<double>( nnt::s_T9 ) %
          zone.getProperty<double>( nnt::s_RHO ) %
          zone.getProperty<double>( S_MAX_FLOW ) << std::endl;
       }
     }

  private:
    nnt::Zone& zone;
    bool bWriteTitle;

};

//##############################################################################
// network_grapher().
//##############################################################################

class network_grapher :
  public base::network_grapher,
    public base::network_grapher_induced_view
{

  public:
    network_grapher() :
      base::network_grapher(),
      base::network_grapher_induced_view(),
      my_flow_computer(){}
    network_grapher( v_map_t& v_map ) :
      base::network_grapher( v_map ),
      base::network_grapher_induced_view( v_map ),
      my_flow_computer( v_map )
      {
        sGraphOutputBase = v_map[S_GRAPH_OUTPUT_BASE].as<std::string>();
        dMaxScale = v_map[S_MAX_FLOW_SCALE].as<double>();
        dCut = v_map[S_FLOW_CUTOFF].as<double>();
        bNet = v_map[S_NET_FLOW].as<bool>();
        bWriteTitle = v_map[S_WRITE_TITLE].as<bool>();
      }

    void
    write( graph_t& g, nnt::Zone& zone, std::ostream& my_out, views_t& views )
    {
      graph_t gc;
      copyGraph( g, gc );
      filtered_graph_t
        fg1(
          g,
          base::edgeFilter<graph_t>( g, views.first, views.second ),
          base::vertexFilter<graph_t>( g, views.first, allowIsolatedVertices() )
        );
      zone.updateProperty( S_MAX_FLOW, findMaxEdgeWeight( fg1 ) );
      scaleGraphEdgeWeights( gc, zone.getProperty<double>( S_MAX_FLOW ) );
      pruneGraph( gc );
      filtered_graph_t
        fg2(
          gc,
          base::edgeFilter<graph_t>( gc, views.first, views.second ),
          base::vertexFilter<graph_t>( gc, views.first, allowIsolatedVertices() )
        );
      boost::write_graphviz(
        my_out,
        fg2,
        vertexWriter<filtered_graph_t>( fg2 ),
        edgeWriter<filtered_graph_t>( fg2 ),
        graphWriter<graph_t>( g, zone, bWriteTitle )
      );
    }

    void
    write( graph_t& g, nnt::Zone& zone, std::ostream& my_out )
    {
      views_t views = getInducedViews( getGraphNetwork( g ) );
      write( g, zone, my_out, views );
      clearViews( views );
    }

    void
    write( std::vector<nnt::Zone>& zones )
    {
      views_t views = getInducedViews( getGraphNetwork( v_g[0] ) );
      #pragma omp parallel for schedule( dynamic, 1 )
      for( size_t i = 0; i < zones.size(); i++ )
      {
        std::string s_i = boost::lexical_cast<std::string>( i );
        boost::trim( s_i );
        std::ofstream my_out;
        std::string s_file = sGraphOutputBase + "_" + s_i + ".dot"; 
        my_out.open( s_file.c_str() ),
        write( v_g[i], zones[i], my_out, views );
        my_out.close();
      }
      clearViews( views );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      Libnucnet__Net * p_net =
        Libnucnet__Zone__getNet( zones[0].getNucnetZone() );

      Libnucnet__Reac * p_reac = Libnucnet__Net__getReac( p_net );

      graph_t g = createGraph<graph_t>( p_net );

      v_g.resize( zones.size() );

      #pragma omp parallel for schedule( dynamic, 1 )
      for( size_t i = 0; i < zones.size(); i++ )
      {
        copyGraph( g, v_g[i] );
      }

      wn_user::flow_map_vector_t v_flow_map = my_flow_computer( zones );

      #pragma omp parallel for schedule( dynamic, 1 )
      for( size_t i = 0; i < v_flow_map.size(); i++ )
      {

        BOOST_FOREACH( wn_user::flow_map_t::value_type& t, v_flow_map[i] )
        {
          Libnucnet__Reaction * p_reaction =
            Libnucnet__Reac__getReactionByString( p_reac, t.first.c_str() );

          double forward, reverse, flow;
          boost::tie( forward, reverse ) = t.second;
          flow = forward - reverse;
          std::map<std::string, std::string> props;
          props["style"] = "solid";
          nnt::reaction_element_list_t reactants =
            nnt::make_reaction_nuclide_reactant_list( p_reaction );
          nnt::reaction_element_list_t products =
            nnt::make_reaction_nuclide_product_list( p_reaction );
          if( bNet )
          {
            if( flow > 0 )
            {
              addReactionEdges(
                v_g[i], p_reaction, reactants, products, props, flow
              );
            }
            else if( flow < 0 )
            {
              addReactionEdges(
                v_g[i], p_reaction, products, reactants, props, -flow
              );
            }
          }
          else
          {
            addReactionEdges(
              v_g[i], p_reaction, reactants, products, props, forward
            );
            addReactionEdges(
              v_g[i], p_reaction, products, reactants, props, reverse
            );
          }
        }
      }
    }
        
  private:
    wn_user::flow_computer my_flow_computer;
    std::vector<graph_t> v_g;
    std::string sGraphOutputBase;
    double dMaxScale, dCut;
    bool bNet, bWriteTitle;

    template<class Graph>
    double
    findMaxEdgeWeight( Graph& g )
    {
      double d_max = 0;
      BGL_FORALL_EDGES_T( e, g, Graph )
      {
        double d_weight = boost::get( boost::edge_weight, g )[e];
        if( fabs( d_weight ) > d_max ) { d_max = d_weight; }
      }
      return d_max;
    }        
        
    template<class Graph>
    void
    scaleGraphEdgeWeights( Graph& g, double d_max )
    {
      BGL_FORALL_EDGES_T( e, g, Graph )
      {
        boost::get( boost::edge_weight, g )[e] *= dMaxScale / d_max;
      }
    }

    template<class Graph>
    void
    pruneGraph( Graph& g )
    {
      typename boost::graph_traits<Graph>::edge_iterator ei, ei_end, next;
      boost::tie( ei, ei_end ) = boost::edges( g );
      for( next = ei; ei != ei_end; ei = next )
      {
        ++next;
        if( boost::get( boost::edge_weight, g )[*ei] < dCut )
        {
          boost::remove_edge( *ei, g );
        }
      }
    }        

};
    
//##############################################################################
// network_grapher_options().
//##############################################################################

class network_grapher_options :
  public base::network_grapher_options,
  public base::network_grapher_induced_view_options
{

  public:
    network_grapher_options() :
      base::network_grapher_options(),
      base::network_grapher_induced_view_options(){}


    std::string
    getExampleString()
    {
      return
        base::network_grapher_options::getExampleString() +
        base::network_grapher_induced_view_options::getExampleString();
    }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Maximum flow scale.
        (
          S_NET_FLOW,
          po::value<bool>()->default_value( true, "true" ),
          "Use net flow (otherwise both forward and reverse flows)"
        )

        // Maximum flow scale.
        (
          S_MAX_FLOW_SCALE,
          po::value<double>()->default_value( 10., "10" ),
          "Maximum flow scaling."
        )

        // Flow cutoff.
        (
          S_FLOW_CUTOFF,
          po::value<double>()->default_value( 1.e-2, "1.e-2" ),
          "Flow cutoff (relative to maximum)."
        )

        // Output file.
        (
          S_GRAPH_OUTPUT_BASE,
          po::value<std::string>()->required(),
          "Graph output base."
        )

        // Write title.
        (
          S_WRITE_TITLE,
          po::value<bool>()->default_value( true, "true" ),
          "Write title for graph."
        )

        ;

        base::network_grapher_options::getOptions( network_grapher );

        base::network_grapher_induced_view_options::getOptions(
          network_grapher
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_DETAIL_HPP
