///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022 Clemson University.
//
// This file was originally written by Bradley S. Meyer and Norberto J. Davila.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file dag_paths_evolve.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_grapher/base/network_grapher.hpp"
#include "network_grapher/base/network_grapher_induced_view.hpp"

#include "utility/dag_paths.hpp"

#ifndef WN_NETWORK_GRAPHER_DETAIL_HPP
#define WN_NETWORK_GRAPHER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define   S_BELL_TERMS                      "bell_terms"
#define   S_DIRECTION                       "direction"
#define   S_STEADY_CUT                      "steady_cut"
#define   S_THRESHOLD_ALPHA                 "threshold_alpha"

typedef std::pair<Libnucnet__NucView *, Libnucnet__ReacView *> views_t;

typedef typename boost::property_map<graph_t, boost::edge_weight_t>::type
  weight_map_t;

typedef typename boost::property_traits<weight_map_t>::value_type weight_t;

//##############################################################################
// edgeFilter().
//##############################################################################

template<typename Graph>
class
edgeFilter : base::edgeFilter<Graph>
{
  public:
    edgeFilter() : base::edgeFilter<Graph>() {}
    edgeFilter(
      Graph& g,
      Libnucnet__NucView * p_nuc_view,
      Libnucnet__ReacView * p_reac_view,
      weight_t d_thresh_hold
    ) : base::edgeFilter<Graph>( g, p_nuc_view, p_reac_view ),
      dThreshold( d_thresh_hold )
    {
      wm = boost::get( boost::edge_weight, g );
    }

    template<typename Edge>
    bool operator()( const Edge& e ) const
    {
      return ( wm[e] >= dThreshold && base::edgeFilter<Graph>::operator()( e ) );
    }

  private:
    weight_map_t wm;
    weight_t dThreshold;

};

typedef
  boost::filtered_graph<
    graph_t,
    edgeFilter<graph_t>,
    base::vertexFilter<graph_t>
  > filtered_graph_t;

typedef boost::graph_traits<filtered_graph_t>::edge_descriptor edge_t;

typedef
  wn_user::utility::dag_path_expansion_evolver<filtered_graph_t, edge_t>
  evolver_t;
/*
typedef
  wn_user::utility::dag_path_evolver<filtered_graph_t, edge_t>
  evolver_t;
*/

typedef std::map<std::string, edge_t> edge_map_t;

//##############################################################################
// count_edge_reactions().
//##############################################################################

void
count_edge_reactions(
  const char * s_name,
  const char * s_tag1,
  const char * s_tag2,
  const char * s_value,
  size_t * p_count
)
{ 
  if( s_tag2 )
  { 
    std::cerr << "Tag not allowed." << std::endl;
  }
  
  (*p_count)++;
}

//##############################################################################
// get_edge_reactions().
//##############################################################################

void
get_edge_reactions(
  const char * s_name,
  const char * s_tag1,
  const char * s_tag2,
  const char * s_value,
  std::vector<std::string> * p_r
)
{
  if( s_tag2 )
  {
    std::cerr << "Tag not allowed." << std::endl;
  }

  size_t i = boost::lexical_cast<size_t>( s_tag1 );
  (*p_r)[i] = std::string( s_value );
}

//##############################################################################
// network_grapher().
//##############################################################################

class network_grapher :
  public base::network_grapher,
  public base::network_grapher_induced_view
{

  public:
    network_grapher() :
      base::network_grapher(),
      base::network_grapher_induced_view(),
      my_flow_computer(){}
    network_grapher( v_map_t& v_map ) :
      base::network_grapher( v_map ),
      base::network_grapher_induced_view( v_map ),
      my_flow_computer( v_map )
      {
        bell_terms = v_map[S_BELL_TERMS].as<size_t>();
        dThresholdAlpha = v_map[S_THRESHOLD_ALPHA].as<double>();
        if( v_map.count( S_STEADY_CUT ) )
        {
          steady_cut = v_map[S_STEADY_CUT].as<weight_t>();
        }
        else
        {
          steady_cut = std::numeric_limits<weight_t>::infinity();
        }
      }

    evolver_t
    operator()( nnt::Zone& zone )
    {
      return create_evolver( zone );
    }

    std::vector<evolver_t>
    operator()( std::vector<nnt::Zone>& zones )
    {
      std::vector<evolver_t> result;
      graph_t g = create_graph( zones[0] );

      views_t views = getInducedViews( getGraphNetwork( g ) );

      filtered_graph_t
        fg(
          g,
          edgeFilter<graph_t>( g, views.first, views.second, dThresholdAlpha ),
          base::vertexFilter<graph_t>( g, views.first, allowIsolatedVertices() )
        );

      edge_map_t edge_map = getEdgeMap<filtered_graph_t>( fg );

      result.reserve( zones.size() );

      #pragma omp parallel for schedule( dynamic, 1 )
      for( size_t i = 0; i < zones.size(); i++ )
      {
        result.push_back( create_evolver( fg, edge_map, zones[i] ) );
      }
      return result;
    }

  private:
    wn_user::flow_computer my_flow_computer;
    size_t bell_terms;
    std::string sDirection;
    weight_t steady_cut,dThresholdAlpha;

    graph_t
    create_graph( nnt::Zone& zone )
    {
      Libnucnet__Net * p_net = Libnucnet__Zone__getNet( zone.getNucnetZone() );

      Libnucnet__Reac * p_reac = Libnucnet__Net__getReac( p_net );

      graph_t g = createGraph<graph_t>( p_net );

      update_abundances( zone );

      wn_user::flow_map_t flow_map = my_flow_computer( zone );

      BOOST_FOREACH( wn_user::flow_map_t::value_type& t, flow_map )
      {
        Libnucnet__Reaction * p_reaction =
          Libnucnet__Reac__getReactionByString( p_reac, t.first.c_str() );

        double forward, reverse;
        boost::tie( forward, reverse ) = t.second;
        std::map<std::string, std::string> props;
        props["style"] = "solid";
        nnt::reaction_element_list_t reactants =
          nnt::make_reaction_nuclide_reactant_list( p_reaction );
        nnt::reaction_element_list_t products =
          nnt::make_reaction_nuclide_product_list( p_reaction );
        if( zone.getProperty<std::string>( S_DIRECTION ) == "forward" )
        {
          addReactionEdges(
            g, p_reaction, reactants, products, props, forward
          );
        }
        else
        {
          addReactionEdges(
            g, p_reaction, products, reactants, props, reverse
          );
        }
      }

      return g;

    }

    evolver_t
    create_evolver( filtered_graph_t& fg, edge_map_t& edge_map, nnt::Zone& zone )
    {
      size_t i_count = 0;
      Libnucnet__Zone__iterateOptionalProperties(
        zone.getNucnetZone(),
        "step",
        NULL,
        NULL,
        (Libnucnet__Zone__optional_property_iterate_function)
           count_edge_reactions,
        &i_count
      );

      std::vector<std::string> v_r( i_count );
      Libnucnet__Zone__iterateOptionalProperties(
        zone.getNucnetZone(),
        "step",
        NULL,
        NULL,
        (Libnucnet__Zone__optional_property_iterate_function)
          get_edge_reactions,
        &v_r
      );

      std::vector<edge_t> v_e;
      BOOST_FOREACH( std::string& s, v_r )
      {
        v_e.push_back( edge_map[s] );
      }

      evolver_t pb( fg, v_e, bell_terms, steady_cut );
      //evolver_t pb( fg, v_e );
      return pb;
    }

    evolver_t
    create_evolver( nnt::Zone& zone )
    {
      graph_t g = create_graph( zone );

      views_t views = getInducedViews( getGraphNetwork( g ) );

      filtered_graph_t
        fg(
          g,
          edgeFilter<graph_t>( g, views.first, views.second, dThresholdAlpha ),
          base::vertexFilter<graph_t>( g, views.first, allowIsolatedVertices() )
        );

      edge_map_t edge_map = getEdgeMap<filtered_graph_t>( fg );

      return create_evolver( fg, edge_map, zone );
    }

    template<typename Graph>
    edge_map_t
    getEdgeMap( Graph& g )
    {
      edge_map_t edge_map;
      BGL_FORALL_EDGES_T( e, g, Graph )
      {
        Libnucnet__Reaction * p_reaction = boost::get( edge_reaction, g )[e];
        edge_map[Libnucnet__Reaction__getString( p_reaction )] = e;
      }
      return edge_map;
    }

    void
    update_abundances( nnt::Zone& zone )
    {
      BOOST_FOREACH(
        nnt::Species species,
        nnt::make_species_list( 
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          )
        )
      )
      {
        if(
          Libnucnet__Zone__getSpeciesAbundance(
            zone.getNucnetZone(),
            species.getNucnetSpecies()
          ) == 0
        )
        {
          Libnucnet__Zone__updateSpeciesAbundance(
            zone.getNucnetZone(),
            species.getNucnetSpecies(),
            1.
          );
        }
      }
    }

};
    
//##############################################################################
// network_grapher_options().
//##############################################################################

class network_grapher_options :
   public base::network_grapher_options,
   public base::network_grapher_induced_view_options
{

  public:
    network_grapher_options() :
      base::network_grapher_options(),
      base::network_grapher_induced_view_options() {}

    std::string
    getExampleString()
    {
      return
        base::network_grapher_options::getExampleString();
    }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Expansion terms
        (
          S_BELL_TERMS,
          po::value<size_t>()->default_value( 10000, "10000" ),
          "The number of terms in the bell expansion."
        )

        // Steady-state cutoff
        (
          S_STEADY_CUT,
          po::value<weight_t>(),
          "The steady-state factor."
        )

        // Threshold flow alpha
        (
          S_THRESHOLD_ALPHA,
          po::value<double>()->default_value( 0, "0" ),
          "Threshold alpha."
        )

        ;

        base::network_grapher_options::getOptions( network_grapher );
        base::network_grapher_induced_view_options::getOptions(
          network_grapher
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_DETAIL_HPP
