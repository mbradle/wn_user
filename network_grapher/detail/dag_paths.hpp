///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/graph/directed_graph.hpp>
#include <boost/heap/fibonacci_heap.hpp>

#include "my_global_types.h"
#include "network_grapher/base/network_grapher.hpp"
#include "network_grapher/base/network_grapher_output_file.hpp"
#include "network_grapher/base/network_grapher_induced_view.hpp"

#include "utility/dag_paths.hpp"
#include "utility/all_paths_finder.hpp"
#include "utility/paths_counter.hpp"
#include "utility/directed_graph_populator.hpp"
#include "yen/yen_ksp.hpp"

#ifndef WN_NETWORK_GRAPHER_DETAIL_HPP
#define WN_NETWORK_GRAPHER_DETAIL_HPP

#define S_START_VERTEX   "start_vertex"
#define S_END_VERTEX     "end_vertex"
#define S_NUMBER_PATHS   "number_paths"
#define S_DIRECTION      "direction"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

typedef std::pair<Libnucnet__NucView *, Libnucnet__ReacView *> views_t;

template<class T, class U>
struct graph_pair
{
  T t;
  U u;
  graph_pair( T& _t, U& _u ) : t( _t ), u( _u ){}

  bool operator<( const graph_pair& rhs ) const
  {
    return this->t < rhs.t;
  }
};

//##############################################################################
// vertexWriter().
//##############################################################################

template<typename Graph>
class
vertexWriter : public base::vertexWriter<Graph>
{
  public:
    vertexWriter() : base::vertexWriter<Graph>(){}
    vertexWriter( Graph& g ) : base::vertexWriter<Graph>( g ){}

    template <class Vertex>
    void operator()( std::ostream& out, Vertex v )
    {
      base::vertexWriter<Graph>::write( out, v );
    }

};

//##############################################################################
// edgeWriter().
//##############################################################################

template<typename Graph>
class
edgeWriter : public base::edgeWriter<Graph>
{
  public:
    edgeWriter() : base::edgeWriter<Graph>(){}
    edgeWriter( Graph& g ) : base::edgeWriter<Graph>( g ){}

    template <class Edge>
    void operator()( std::ostream& out, Edge e )
    {
      base::edgeWriter<Graph>::write( out, e );
    }

};

//##############################################################################
// graphWriter().
//##############################################################################

template<typename Graph>
class
graphWriter : public base::graphWriter<Graph>
{
  public:
    graphWriter() : base::graphWriter<Graph>(){}
    graphWriter( Graph& g ) : base::graphWriter<Graph>( g ){};

    void operator()( std::ostream& out ) const
    {
      base::graphWriter<Graph>::write( out );
    }
};

//##############################################################################
// network_grapher().
//##############################################################################

class network_grapher :
  public base::network_grapher,
  public base::network_grapher_output_file,
    public base::network_grapher_induced_view
{

  public:
    network_grapher() :
      base::network_grapher(),
      base::network_grapher_output_file(),
      base::network_grapher_induced_view(),
      my_flow_computer(){}
    network_grapher( v_map_t& v_map ) :
      base::network_grapher( v_map ),
      base::network_grapher_output_file( v_map ),
      base::network_grapher_induced_view( v_map ),
      my_flow_computer( v_map )
    {
      sStart = v_map[S_START_VERTEX].as<std::string>();
      sEnd = v_map[S_END_VERTEX].as<std::string>();
      sDirection = v_map[S_DIRECTION].as<std::string>();
      if( sDirection != "forward" && sDirection != "reverse" )
      {
        std::cerr << "Graph reaction direction is invalid." << std::endl;
        exit( EXIT_FAILURE );
      }
      M = v_map[S_NUMBER_PATHS].as<size_t>();
    }

    void
    write( std::ostream& my_out )
    {
      views_t views = getInducedViews( getGraphNetwork( v_g[0] ) );
      filtered_graph_t
        fg(
          v_g[0],
          base::edgeFilter( v_g[0], views.first, views.second ),
          base::vertexFilter( v_g[0], views.first, allowIsolatedVertices() )
        );

      boost::write_graphviz(
        my_out,
        fg,
        vertexWriter<filtered_graph_t>( fg ),
        edgeWriter<filtered_graph_t>( fg ),
        graphWriter<graph_t>( v_g[0] )
      );
      clearViews( views );
    }

    void
    write()
    {
      std::ofstream my_out;
      my_out.open( getOutputFile().c_str() ),
      write( my_out );
      my_out.close();
    }

    void operator()(  nnt::Zone& zone )
    {
      Libnucnet__Net * p_net = Libnucnet__Zone__getNet( zone.getNucnetZone() );

      Libnucnet__Reac * p_reac = Libnucnet__Net__getReac( p_net );

zone.updateProperty( "t9", 1. );
zone.updateProperty( "rho", 1.e3 );

      graph_t g = createGraph( p_net );

      update_abundances( zone );

      wn_user::flow_map_t flow_map = my_flow_computer( zone );

      BOOST_FOREACH( wn_user::flow_map_t::value_type& t, flow_map )
      {
        Libnucnet__Reaction * p_reaction =
          Libnucnet__Reac__getReactionByString( p_reac, t.first.c_str() );

        double forward, reverse;
        boost::tie( forward, reverse ) = t.second;
        std::map<std::string, std::string> props;
        props["style"] = "solid";
        nnt::reaction_element_list_t reactants =
          nnt::make_reaction_nuclide_reactant_list( p_reaction );
        nnt::reaction_element_list_t products =
          nnt::make_reaction_nuclide_product_list( p_reaction );
        if( sDirection == "forward" )
        {
          addReactionEdges(
            g, p_reaction, reactants, products, props, forward
          );
        }
        else
        {
          addReactionEdges(
            g, p_reaction, products, reactants, props, reverse
          );
        }
      }

      views_t views = getInducedViews( getGraphNetwork( g ) );
      filtered_graph_t
        fg(
          g,
          base::edgeFilter( g, views.first, views.second ),
          base::vertexFilter( g, views.first, allowIsolatedVertices() )
        );

      std::map<std::string, vertex_t> forwardVertexMap =
        createSpeciesVertexMap<filtered_graph_t, vertex_t>( fg );

      std::map<vertex_t, std::string> reverseVertexMap =
        createReverseSpeciesVertexMap<filtered_graph_t, vertex_t>( fg );

      invert_weights( fg );

      auto r =
        boost::yen_ksp(
          fg, forwardVertexMap[sStart], forwardVertexMap[sEnd], M
        );

      typedef std::vector<vertex_t> v_p_t;
      std::vector<v_p_t> v_ps;

      for( const auto& r_element : r )
      {
std::cout << r_element.first << std::endl;
        v_p_t v;
        v.push_back( forwardVertexMap[sStart] );

        for( const auto& e : r_element.second )
        {
          v.push_back( boost::target( e, fg ) );
        }

        v_ps.push_back( v );
      }

      for( size_t i = 0; i < v_ps.size(); i++ )
      {
        for( size_t j = 0; j < v_ps[i].size(); j++ )
        {
          std::cout << i << "  " << reverseVertexMap[v_ps[i][j]] << std::endl;
        }
      }

/*
utility::paths_counter<filtered_graph2_t> pc;
std::vector<double> v_n_paths = pc( forwardVertexMap[sStart], forwardVertexMap[sEnd], fg2, 200, true );
std::cout << "Here" << std::endl;
for( size_t i = 0; i < v_n_paths.size(); i++ )
{
  if( v_n_paths[i] >= 0) std::cout << i << "  " << v_n_paths[i] << std::endl;
}

      std::vector<std::vector<vertex_t> > v_p =
        my_finder( forwardVertexMap[sStart], forwardVertexMap[sEnd], fg2 );

      for( size_t i = 0; i < v_p.size(); i++ )
      {
        std::cout << "Path " << i << ": ";;
        BOOST_FOREACH( vertex_t& p_v, v_p[i] )
        {
          std::cout << reverseVertexMap[p_v] << " ";
        }
        std::cout << std::endl;
      }
*/

/*
      boost::heap::fibonacci_heap<graph_pair<double, std::vector<vertex_t> > > h;
      for( size_t i = 0; i < v_p.size(); i++ )
      {
        std::vector<vertex_t> v_q;
        for( size_t j = 0; j < v_p[i].size(); j++ )
        {
          v_q.push_back( map2[v_p[i][j]] );
        }
          
        wn_user::utility::dag_path_expansion_evolver<graph_t, vertex_t>
          p( g, v_q );
        double tau  = 1.e10;
        double x = p( tau );
        h.push( graph_pair<double, std::vector<vertex_t> >( x, v_q ) );
        
      }

      boost::graph_traits<graph_t>::edge_descriptor e;
      bool found;
      while( h.size() > 0 )
      {
        graph_t gc;
        copyGraph( g, gc ); 
        BGL_FORALL_EDGES( e, gc, graph_t )
        {
          boost::get( boost::edge_weight, gc )[e] = 1;
        }
        for( size_t i = 0; i < h.top().u.size() - 1; i++ )
        {
          boost::tie( e, found ) =
            boost::edge( h.top().u[i], h.top().u[i+1], gc ); 
          std::string s_color = "red";
          boost::get( edge_properties, gc )[e]["color"] = s_color;
          boost::get( boost::edge_weight, gc )[e] = 10;
        }
        v_g.push_back( gc );
        h.pop();
      }
std::cout << h.size() << std::endl;
        std::vector<vertex_t> v_q1 = h.top().u;
        wn_user::utility::dag_path_expansion_evolver<graph_t, vertex_t>
          p1( g, v_q1, 10000 );
        h.pop();
        std::vector<vertex_t> v_q2 = h.top().u;
        wn_user::utility::dag_path_expansion_evolver<graph_t, vertex_t>
          p2( g, v_q2, 10000 );
        h.pop();
        wn_user::utility::dag_path_expansion_evolver<graph_t, vertex_t>
          p3( g, v_q2, 10000 );
        for( size_t i = 0; i < 1001; i++ )
        {
          double tau = i * 0.01;
          std::cout << tau << "  " << p1( tau ) << "  " << p2( tau ) << "  " << p3( tau ) << std::endl;
        }
*/

    }
        
  private:
    graph_t g;
    wn_user::flow_computer my_flow_computer;
    std::vector<graph_t> v_g;
    std::string sStart, sEnd, sDirection;
    size_t M;
    typedef
      boost::filtered_graph<graph_t, base::edgeFilter, base::vertexFilter>
      filtered_graph_t;
    typedef boost::graph_traits<graph_t>::vertex_descriptor vertex_t;
    typedef boost::directed_graph<boost::vecS, boost::vecS> directed_graph_t;
    typedef boost::graph_traits<directed_graph_t>::vertex_descriptor
      directed_vertex_t;

    void
    update_abundances( nnt::Zone& zone )
    {
      BOOST_FOREACH(
        nnt::Species species,
        nnt::make_species_list( 
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          )
        )
      )
      {
        if(
          Libnucnet__Zone__getSpeciesAbundance(
            zone.getNucnetZone(),
            species.getNucnetSpecies()
          ) == 0
        )
        {
          Libnucnet__Zone__updateSpeciesAbundance(
            zone.getNucnetZone(),
            species.getNucnetSpecies(),
            1.
          );
        }
      }
    }

    template<typename Graph>
    void
    invert_weights( Graph& g )
    {
      typename boost::property_map<Graph, boost::edge_weight_t>::type
        wm = boost::get( boost::edge_weight, g );
      BGL_FORALL_EDGES_T( e, g, Graph )
      {
        wm[e] = 1. / wm[e];
      }
    }

};
    
//##############################################################################
// network_grapher_options().
//##############################################################################

class network_grapher_options :
  public base::network_grapher_options,
  public base::network_grapher_output_file_options,
  public base::network_grapher_induced_view_options
{

  public:
    network_grapher_options() :
      base::network_grapher_options(),
      base::network_grapher_output_file_options(),
      base::network_grapher_induced_view_options(){}


    std::string
    getExampleString()
    {
      return
        base::network_grapher_options::getExampleString() +
        base::network_grapher_output_file_options::getExampleString() +
        base::network_grapher_induced_view_options::getExampleString();
    }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Start vertex
        (
          S_START_VERTEX,
          po::value<std::string>()->required(),
          "The name of the start species."
        )

        // End vertex
        (
          S_END_VERTEX,
          po::value<std::string>()->required(),
          "The name of the end species."
        )

        // Graph reaction direction
        (
          S_DIRECTION,
          po::value<std::string>()->default_value( "forward" ),
          "The graph reaction direction."
        )

        // Number of paths
        (
          S_NUMBER_PATHS,
          po::value<size_t>()->default_value( 100, "100" ),
          "The number of paths."

        )

        ;

        base::network_grapher_options::getOptions( network_grapher );

        base::network_grapher_output_file_options::getOptions(
          network_grapher
        );

        base::network_grapher_induced_view_options::getOptions(
          network_grapher
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_DETAIL_HPP
