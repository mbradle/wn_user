///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_grapher/base/network_grapher.hpp"
#include "network_grapher/base/network_grapher_output_file.hpp"
#include "network_grapher/base/network_grapher_induced_view.hpp"

#ifndef WN_NETWORK_GRAPHER_DETAIL_HPP
#define WN_NETWORK_GRAPHER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

typedef std::pair<Libnucnet__NucView *, Libnucnet__ReacView *> views_t;

//##############################################################################
// vertexWriter().
//##############################################################################

template<typename Graph>
class
vertexWriter : public base::vertexWriter<Graph>
{
  public:
    vertexWriter() : base::vertexWriter<Graph>(){}
    vertexWriter( Graph& g ) : base::vertexWriter<Graph>( g ){}

    template <class Vertex>
    void operator()( std::ostream& out, Vertex v )
    {
      base::vertexWriter<Graph>::write( out, v );
    }

};

//##############################################################################
// edgeWriter().
//##############################################################################

template<typename Graph>
class
edgeWriter : public base::edgeWriter<Graph>
{
  public:
    edgeWriter() : base::edgeWriter<Graph>(){}
    edgeWriter( Graph& g ) : base::edgeWriter<Graph>( g ){}

    template <class Edge>
    void operator()( std::ostream& out, Edge e )
    {
      base::edgeWriter<Graph>::write( out, e );
    }

};

//##############################################################################
// graphWriter().
//##############################################################################

template<typename Graph>
class
graphWriter : public base::graphWriter<Graph>
{
  public:
    graphWriter() : base::graphWriter<Graph>(){}
    graphWriter( Graph& g ) : base::graphWriter<Graph>( g ){};

    void operator()( std::ostream& out ) const
    {
      base::graphWriter<Graph>::write( out );
    }
};

//##############################################################################
// network_grapher().
//##############################################################################

class network_grapher :
  public base::network_grapher,
    public base::network_grapher_output_file,
    public base::network_grapher_induced_view
{

  public:
    network_grapher() :
      base::network_grapher(),
      base::network_grapher_output_file(),
      base::network_grapher_induced_view(){}
    network_grapher( v_map_t& v_map ) :
      base::network_grapher( v_map ),
      base::network_grapher_output_file( v_map ),
      base::network_grapher_induced_view( v_map ){}

    void
    write( std::ostream& my_out )
    {
      views_t views = getInducedViews( getGraphNetwork( g ) );
      boost::filtered_graph<graph_t, base::edgeFilter, base::vertexFilter>
        fg(
          g,
          base::edgeFilter( g, views.first, views.second ),
          base::vertexFilter( g, views.first, allowIsolatedVertices() )
        );

      boost::write_graphviz(
        my_out,
        fg,
        vertexWriter<filtered_graph_t>( fg ),
        edgeWriter<filtered_graph_t>( fg ),
        graphWriter<graph_t>( g )
      );
      clearViews( views );
    }

    void
    write()
    {
      std::ofstream my_out;
      my_out.open( getOutputFile().c_str() ),
      write( my_out );
      my_out.close();
    }

    void operator()( Libnucnet__Net * p_net )
    {
      g = createGraph( p_net );
      BOOST_FOREACH(
        nnt::Reaction reaction,
        nnt::make_reaction_list( Libnucnet__Net__getReac( p_net ) )
      )
      {
        Libnucnet__Reaction * p_reaction = reaction.getNucnetReaction();
        if( Libnucnet__Net__isValidReaction( p_net, p_reaction ) )
        {
          std::map<std::string, std::string> props;
          props["style"] = "solid";
          nnt::reaction_element_list_t reactants =
            nnt::make_reaction_nuclide_reactant_list( p_reaction );
          nnt::reaction_element_list_t products =
            nnt::make_reaction_nuclide_product_list( p_reaction );
          addReactionEdges( g, p_reaction, reactants, products, props, 1. );
          addReactionEdges( g, p_reaction, products, reactants, props, 1. );
        }
      }
    }
        
  private:
    graph_t g;
    typedef
      boost::filtered_graph<graph_t, base::edgeFilter, base::vertexFilter>
      filtered_graph_t;


};
    
//##############################################################################
// network_grapher_options().
//##############################################################################

class network_grapher_options :
  public base::network_grapher_options,
  public base::network_grapher_output_file_options,
  public base::network_grapher_induced_view_options
{

  public:
    network_grapher_options() :
      base::network_grapher_options(),
      base::network_grapher_output_file_options(),
      base::network_grapher_induced_view_options(){}


    std::string
    getExampleString()
    {
      return
        base::network_grapher_options::getExampleString() +
        base::network_grapher_output_file_options::getExampleString() +
        base::network_grapher_induced_view_options::getExampleString();
    }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        base::network_grapher_options::getOptions( network_grapher );

        base::network_grapher_output_file_options::getOptions(
          network_grapher
        );

        base::network_grapher_induced_view_options::getOptions(
          network_grapher
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_DETAIL_HPP
