///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file integrated_currents.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_grapher/base/network_grapher.hpp"
#include "network_grapher/base/network_grapher_output_base.hpp"
#include "network_grapher/base/network_grapher_induced_view.hpp"

#ifndef WN_NETWORK_GRAPHER_DETAIL_HPP
#define WN_NETWORK_GRAPHER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define   S_DIRECTION                       "direction"
#define   S_PATH_COLOR                      "path_color"
#define   S_WRITE_TITLE                     "write_title"


typedef std::pair<Libnucnet__NucView *, Libnucnet__ReacView *> views_t;

typedef
  boost::filtered_graph<
    graph_t,
    base::edgeFilter<graph_t>,
    base::vertexFilter<graph_t>
  > filtered_graph_t;

//##############################################################################
// count_edge_reactions().
//##############################################################################

void
count_edge_reactions(
  const char * s_name,
  const char * s_tag1,
  const char * s_tag2,
  const char * s_value,
  size_t * p_count
)
{
  if( s_tag2 )
  {
    std::cerr << "Tag not allowed." << std::endl;
  }

  (*p_count)++;
}

//##############################################################################
// get_edge_reactions().
//##############################################################################

void
get_edge_reactions(
  const char * s_name,
  const char * s_tag1,
  const char * s_tag2,
  const char * s_value,
  std::vector<std::string> * p_r
)
{
  if( s_tag2 )
  {
    std::cerr << "Tag not allowed." << std::endl;
  }

  size_t i = boost::lexical_cast<size_t>( s_tag1 );
  (*p_r)[i] = std::string( s_value );
}

//##############################################################################
// graphWriter().
//##############################################################################

template<typename Graph>
class
graphWriter : public base::graphWriter<Graph>
{
  public:
    graphWriter() : base::graphWriter<Graph>(){}
    graphWriter(
      Graph& g, std::string s_rank_, std::string s_prob_, bool b_write_title
    ) :
      base::graphWriter<Graph>( g ), s_rank( s_rank_ ), s_prob( s_prob_ ),
      bWriteTitle( b_write_title ){};

    void operator()( std::ostream& out ) const
    {
      base::graphWriter<Graph>::operator()( out );

      if( bWriteTitle )
      {
        out << "label = \"latex\";" << std::endl;
        out <<
          boost::format(
            "texlbl = \"\\huge{$Path = %s \
             \\ \\ \\ \\ Probability = %s$}\";"
          ) %
          s_rank %
          s_prob;
       }
     }

  private:
    std::string s_rank, s_prob;
    bool bWriteTitle;

};

//##############################################################################
// network_grapher().
//##############################################################################

class network_grapher :
  public base::network_grapher,
    public base::network_grapher_output_base,
    public base::network_grapher_induced_view
{

  public:
    network_grapher() :
      base::network_grapher(),
      base::network_grapher_output_base(),
      base::network_grapher_induced_view(){}
    network_grapher( v_map_t& v_map ) :
      base::network_grapher( v_map ),
      base::network_grapher_output_base( v_map ),
      base::network_grapher_induced_view( v_map )
      {
        bWriteTitle = v_map[S_WRITE_TITLE].as<bool>();
        sPathColor = v_map[S_PATH_COLOR].as<std::string>();
      }

    graph_t
    create_graph( nnt::Zone& zone )
    {
      Libnucnet__Net * p_net = Libnucnet__Zone__getNet( zone.getNucnetZone() );

      Libnucnet__Reac * p_reac = Libnucnet__Net__getReac( p_net );

      graph_t g = createGraph<graph_t>( p_net );

      BOOST_FOREACH(
        nnt::Reaction reaction,
        nnt::make_reaction_list( p_reac )
      )
      {
        Libnucnet__Reaction * p_reaction = reaction.getNucnetReaction();
        std::map<std::string, std::string> props;
        props["style"] = "solid";
        nnt::reaction_element_list_t reactants =
          nnt::make_reaction_nuclide_reactant_list( p_reaction );
        nnt::reaction_element_list_t products =
          nnt::make_reaction_nuclide_product_list( p_reaction );
        if( zone.hasProperty( S_DIRECTION ) )
        {
          if( zone.getProperty<std::string>( S_DIRECTION ) == "forward" )
          {
            addReactionEdges( g, p_reaction, reactants, products, props, 1. );
          }
          else if( zone.getProperty<std::string>( S_DIRECTION ) == "reverse" )
          {
            addReactionEdges( g, p_reaction, products, reactants, props, 1. );
          }
        }
        else
        {
          addReactionEdges( g, p_reaction, reactants, products, props, 1. );
          addReactionEdges( g, p_reaction, products, reactants, props, 1. );
        }
      }

      return g;

    }

    void
    write( graph_t& g, filtered_graph_t& fg, nnt::Zone& zone )
    {

      if( !getOutputBase().empty() )
      {
        writePaths( g, fg, zone );
      }

    }

    void
    write( nnt::Zone& zone )
    {
      graph_t g = create_graph( zone );
      views_t views = getInducedViews( getGraphNetwork( g ) );
      filtered_graph_t
        fg(
          g,
          base::edgeFilter<graph_t>( g, views.first, views.second ),
          base::vertexFilter<graph_t>( g, views.first, allowIsolatedVertices() )
        );
      setEdgeMap( fg );
      write( g, fg, zone );
    }

    void
    write( std::vector<nnt::Zone>& zones )
    {
      graph_t g = create_graph( zones[0] );;
      views_t views = getInducedViews( getGraphNetwork( g ) );
      filtered_graph_t
        fg(
          g,
          base::edgeFilter<graph_t>( g, views.first, views.second ),
          base::vertexFilter<graph_t>( g, views.first, allowIsolatedVertices() )
        );
      setEdgeMap( fg );

      BOOST_FOREACH( nnt::Zone& zone, zones )
      {
        write( g, fg, zone );
      }
    }

  private:
    bool bWriteTitle;
    std::string sPathColor;
    typedef typename boost::graph_traits<filtered_graph_t>::edge_descriptor
      edge_t;
    std::map<std::string, edge_t> edgeMap;

    void
    setEdgeMap( filtered_graph_t& fg )
    {
      BGL_FORALL_EDGES_T( e, fg, filtered_graph_t )
      {
        Libnucnet__Reaction * p_reaction = boost::get( edge_reaction, fg )[e];
        edgeMap[Libnucnet__Reaction__getString( p_reaction )] = e;
      } 
    }

    void
    writePaths( graph_t& g, filtered_graph_t& fg, nnt::Zone& zone )
    {
      
      BGL_FORALL_EDGES_T( e, fg, filtered_graph_t )
      {
        boost::get( edge_properties, fg )[e]["color"] = std::string( "black" );
        boost::get( boost::edge_weight, fg )[e] = 1;
      } 

      size_t i_count = 0;
      Libnucnet__Zone__iterateOptionalProperties(
        zone.getNucnetZone(),
        "step",
        NULL,
        NULL,
        (Libnucnet__Zone__optional_property_iterate_function)
          count_edge_reactions,
        &i_count
      );

      std::vector<std::string> v_r( i_count );
      Libnucnet__Zone__iterateOptionalProperties(
        zone.getNucnetZone(),
        "step",
        NULL,
        NULL,
        (Libnucnet__Zone__optional_property_iterate_function)
          get_edge_reactions,
        &v_r
      );

      BOOST_FOREACH( std::string s, v_r )
      {
         edge_t e = edgeMap[s];
         boost::get( edge_properties, fg )[e]["color"] = sPathColor;
         boost::get( boost::edge_weight, fg )[e] = 10;
      }

      std::string s_rank = zone.getProperty<std::string>( "path rank" );
      std::ofstream my_out;
      std::string s_file = getOutputBase() + "_" + s_rank + ".dot";
      my_out.open( s_file.c_str() );
      std::string s_prob = zone.getProperty<std::string>( "path probability" );

      boost::write_graphviz(
        my_out,
        fg,
        base::vertexWriter<filtered_graph_t>( fg ),
        base::edgeWriter<filtered_graph_t>( fg ),
        graphWriter<graph_t>( g, s_rank, s_prob, bWriteTitle )
      );
      my_out.close();
    }

};
    
//##############################################################################
// network_grapher_options().
//##############################################################################

class network_grapher_options :
  public base::network_grapher_options,
  public base::network_grapher_output_base_options,
  public base::network_grapher_induced_view_options
{

  public:
    network_grapher_options() :
      base::network_grapher_options(),
      base::network_grapher_output_base_options(),
      base::network_grapher_induced_view_options(){}


    std::string
    getExampleString()
    {
      return
        base::network_grapher_options::getExampleString() +
        base::network_grapher_output_base_options::getExampleString() +
        base::network_grapher_induced_view_options::getExampleString();
    }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Write title.
        (
          S_WRITE_TITLE,
          po::value<bool>()->default_value( true, "true" ),
          "Write title for graph."
        )

        // Path color
        (
          S_PATH_COLOR,
          po::value<std::string>()->default_value( "red" ),
          "Color for path arcs."
        )

        ;

        base::network_grapher_options::getOptions( network_grapher );

        base::network_grapher_output_base_options::getOptions(
          network_grapher
        );

        base::network_grapher_induced_view_options::getOptions(
          network_grapher
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_DETAIL_HPP
