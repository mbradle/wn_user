///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file integrated_currents.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_grapher/base/network_grapher.hpp"
#include "network_grapher/base/network_grapher_output_base.hpp"
#include "network_grapher/base/network_grapher_induced_view.hpp"

#include "utility/equilibrium_computer.hpp"
#include "utility/cluster_abundance_moment_computer.hpp"

#ifndef WN_NETWORK_GRAPHER_DETAIL_HPP
#define WN_NETWORK_GRAPHER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define   S_WRITE_TITLE                     "write_title"
#define   S_ABUND                           "abundance"
#define   S_BW                              "b/w"
#define   S_BW_LABEL_MU                     "b/w_shift"
#define   S_MAX_NODE_SHAPE                  "max_node_shape"
#define   S_MAX_Z_NODE_SHAPE                "max_z_node_shape"
#define   S_MIN_NODE_ABUND                  "min_node_abund"
#define   S_MU_MIN                          "mu_min"
#define   S_MU_MAX                          "mu_max"
#define   S_MUH_KT                          "muh_kT"

typedef std::pair<Libnucnet__NucView *, Libnucnet__ReacView *> views_t;

//##############################################################################
// vertexFilter().
//##############################################################################

template<typename Graph>
struct vertexFilter
{
  vertexFilter() {}
  vertexFilter(
    Graph& g, Libnucnet__NucView * p_view, double d_y_min
  )
  {
    vertexPropsMap = boost::get( vertex_properties, g );
    dYMin = d_y_min;
    struct base::vertexFilter<Graph> _my_base_filter( g, p_view, true );
    my_base_filter = _my_base_filter;
  }

  template<typename Vertex>
  bool operator()( const Vertex& v ) const
  {
    if( !my_base_filter( v ) ) {return false;}

    if( my_base_filter.isAnchor( v ) ) {return true;}

    if(
      boost::any_cast<double>( vertexPropsMap[v][S_ABUND] ) > dYMin
    )
    {
      return true;
    }
    else
    {
      return false;
    }

  }

  Libnucnet__Nuc * pNuc;
  typename boost::property_map<Graph, vertex_properties_t>::type vertexPropsMap;
  double dYMin;
  struct base::vertexFilter<Graph> my_base_filter;

};

typedef
  boost::filtered_graph<
    graph_t,
    base::edgeFilter<graph_t>,
    vertexFilter<graph_t>
  > filtered_graph_t;

//##############################################################################
// vertexWriter().
//##############################################################################

template<typename Graph>
class
vertexWriter : public base::vertexWriter<Graph>
{
  public:
    vertexWriter() : base::vertexWriter<Graph>(){}
    vertexWriter( Graph& g ) : base::vertexWriter<Graph>( g ){}

    template <class Vertex>
    void operator()( std::ostream& out, Vertex v )
    {
      base::vertexWriter<Graph>::operator()( out, v );
    }

};

//##############################################################################
// edgeWriter().
//##############################################################################

template<typename Graph>
class
edgeWriter : public base::edgeWriter<Graph>
{
  public:
    edgeWriter() : base::edgeWriter<Graph>(){}
    edgeWriter( Graph& g ) : base::edgeWriter<Graph>( g ){}

    template <class Edge>
    void operator()( std::ostream& out, Edge e ) { }

};

//##############################################################################
// graphWriter().
//##############################################################################

template<typename Graph>
class
graphWriter : public base::graphWriter<Graph>
{
  public:
    graphWriter() : base::graphWriter<Graph>(){}
    graphWriter( Graph& g, nnt::Zone& zone_, bool b_write_title ) :
      base::graphWriter<Graph>( g ), zone( zone_ ),
      bWriteTitle( b_write_title ){};

    void operator()( std::ostream& out ) const
    {
      base::graphWriter<Graph>::operator()( out );

      if( bWriteTitle )
      {
        out << "label = \"latex\";" << std::endl;
        out <<
          boost::format(
            "texlbl = \"\\huge{$time(s) = %g \
             \\ \\ \\ \\ T_9 = %g \
             \\ \\ \\ \\ \\rho(g/cc) = %g$}\";"
          ) %
          zone.getProperty<double>( nnt::s_TIME ) %
          zone.getProperty<double>( nnt::s_T9 ) %
          zone.getProperty<double>( nnt::s_RHO );
       }
     }

  private:
    nnt::Zone& zone;
    bool bWriteTitle;

};

//##############################################################################
// network_grapher().
//##############################################################################

class network_grapher :
  public base::network_grapher,
    public base::network_grapher_output_base,
    public base::network_grapher_induced_view
{

  public:
    network_grapher() :
      base::network_grapher(),
      base::network_grapher_output_base(),
      base::network_grapher_induced_view(){}
    network_grapher( v_map_t& v_map ) :
      base::network_grapher( v_map ),
      base::network_grapher_output_base( v_map ),
      base::network_grapher_induced_view( v_map )
      {
        bWriteTitle = v_map[S_WRITE_TITLE].as<bool>();
        bVertexBW = v_map[S_BW].as<bool>();
        dVertexBWLabelMu = v_map[S_BW_LABEL_MU].as<double>();
        sMaxNodeShape = v_map[S_MAX_NODE_SHAPE].as<std::string>();
        sMaxZNodeShape = v_map[S_MAX_Z_NODE_SHAPE].as<std::string>();
        dYMin = v_map[S_MIN_NODE_ABUND].as<double>();
        dMuMin = v_map[S_MU_MIN].as<double>();
        dMuMax = v_map[S_MU_MAX].as<double>();
      }

    void
    write( graph_t& g, nnt::Zone& zone, std::ofstream& my_out, views_t& views )
    {
      filtered_graph_t
        fg1(
          g,
          base::edgeFilter<graph_t>( g, views.first, views.second ),
          vertexFilter<graph_t>( g, views.first, dYMin )
        );
      colorVertices( fg1 );

      boost::write_graphviz(
        my_out,
        fg1,
        vertexWriter<filtered_graph_t>( fg1 ),
        edgeWriter<filtered_graph_t>( fg1 ),
        graphWriter<graph_t>( g, zone, bWriteTitle )
      );

    }

    void
    write( std::vector<nnt::Zone>& zones )
    {
      views_t views = getInducedViews( getGraphNetwork( v_g[0] ) );
      for( size_t i = 0; i < zones.size(); i++ )
      {
        std::string s_i = boost::lexical_cast<std::string>( i );
        boost::trim( s_i );
        std::ofstream my_out;
        std::string s_file = getOutputBase() + "_" + s_i + ".dot";
        my_out.open( s_file.c_str() );
        write( v_g[i], zones[i], my_out, views );
        my_out.close();
      }
      clearViews( views );
    }

    graph_t
    create_graph( nnt::Zone& zone )
    {
      graph_t g =
        createGraph<graph_t>( Libnucnet__Zone__getNet( zone.getNucnetZone() ) );

      wn_user::utility::equilibrium_computer my_equil( zone );
      wn_user::utility::cluster_abundance_moment_computer my_cluster_computer;

      my_equil.setYe( my_cluster_computer( zone, "", "z", 1. ) );

      my_equil.compute(
        zone.getProperty<double>( nnt::s_T9 ),
        zone.getProperty<double>( nnt::s_RHO )
      );

      Libnuceq * p_equil = my_equil.getEquilibrium();

      boost::property_map<graph_t, vertex_nuclide_t>::type
        vertex_nuclide_map = boost::get( vertex_nuclide, g );

      boost::property_map<graph_t, vertex_properties_t>::type
        vertex_props_map = boost::get( vertex_properties, g );

      sBGColor = (boost::get_property( g, graph_properties ) )[S_BGCOLOR];

      double d_rn = compute_species_r( zone, p_equil, "n" );
      double d_rp = compute_species_r( zone, p_equil, "h1" );

      BGL_FORALL_VERTICES( v, g, graph_t )
      {
        Libnucnet__Species * p_species = vertex_nuclide_map[v];

        std::string s_species = Libnucnet__Species__getName( p_species );

        vertex_props_map[v][S_ABUND] =
          Libnucnet__Zone__getSpeciesAbundance(
            zone.getNucnetZone(), p_species
          );

        vertex_props_map[v][S_MUH_KT] =
          log( compute_species_r( zone, p_equil, s_species ) ) -
          Libnucnet__Species__getZ( p_species ) * log( d_rp ) -
          (
            Libnucnet__Species__getA( p_species ) -
            Libnucnet__Species__getZ( p_species ) 
          ) * log( d_rn );

      }
      return g;
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      v_g.clear();
      for( size_t i = 0; i < zones.size(); i++ )
      {
        v_g.push_back( create_graph( zones[i] ) );
      }
    }

  private:
    std::vector<graph_t> v_g;
    bool bWriteTitle, bVertexBW;
    std::string sMaxNodeShape, sMaxZNodeShape, sBGColor;
    double dYMin, dMuMin, dMuMax, dVertexBWLabelMu;

    double
    compute_species_r( nnt::Zone& zone, Libnuceq * p_equil, std::string s )
    {
      return
        Libnucnet__Zone__getSpeciesAbundance(
          zone.getNucnetZone(),
          Libnucnet__Nuc__getSpeciesByName(
            Libnucnet__Net__getNuc(
              Libnucnet__Zone__getNet( zone.getNucnetZone() )
            ),
            s.c_str()
          ) 
       ) /
       (
         Libnuceq__Species__getAbundance(
            Libnuceq__getSpeciesByName( p_equil, s.c_str() )
         )
         + 1.e-300
       ) + 1.e-300;
   }

   template<class Graph>
   void
   colorVertices( Graph& g )
   {

     boost::property_map<graph_t, vertex_nuclide_t>::type
       vertex_nuclide_map = boost::get( vertex_nuclide, g );

     boost::property_map<graph_t, vertex_properties_t>::type
       vertex_props_map = boost::get( vertex_properties, g );

     double d_y_max = 0, d_muh_kT_max;
     Libnucnet__Species * p_max_species;
     std::map<size_t, std::pair<Libnucnet__Species *, double> > max_z_map;

     BGL_FORALL_VERTICES_T( v, g, Graph )
     {
       if(
           boost::any_cast<double>( (vertex_props_map[v])[S_ABUND] )
           >
           d_y_max
       )
       {
         d_y_max = boost::any_cast<double>( (vertex_props_map[v])[S_ABUND] );
         p_max_species = vertex_nuclide_map[v];
         d_muh_kT_max =
           boost::any_cast<double>( (vertex_props_map[v])[S_MUH_KT] );
       }
       size_t i_z = Libnucnet__Species__getZ( vertex_nuclide_map[v] );
       if( max_z_map.find( i_z ) == max_z_map.end() )
       {
         max_z_map[i_z] =
           std::make_pair(
             vertex_nuclide_map[v],
             boost::any_cast<double>( (vertex_props_map[v])[S_ABUND] )
           );
       }
       else
       {
         if(
           boost::any_cast<double>( (vertex_props_map[v])[S_ABUND] )
           >
           max_z_map[i_z].second
         )
         {
           max_z_map[i_z] =
             std::make_pair(
               vertex_nuclide_map[v],
               boost::any_cast<double>( (vertex_props_map[v])[S_ABUND] )
             );
         }
       }
     }

     BGL_FORALL_VERTICES_T( v, g, Graph )
     {
       Libnucnet__Species * p_species = vertex_nuclide_map[v];
       size_t i_z = Libnucnet__Species__getZ( p_species );
       double d_offset =
         boost::any_cast<double>( (vertex_props_map[v])[S_MUH_KT] )
         -
         d_muh_kT_max;
       if( d_offset >= dMuMin && d_offset <= dMuMax )
       {
         if( !bVertexBW )
         {
           vertex_props_map[v][S_COLOR] = get_color_from_value( d_offset );
         }
         else
         {
           std::string s1, s2;
           boost::tie( s1, s2 ) = get_bw_from_value( d_offset );
           vertex_props_map[v][S_COLOR] = s1;
           vertex_props_map[v][S_NODE_LABEL_STYLE] = s2;
         }
       }
       else
       {
         vertex_props_map[v][S_COLOR] = sBGColor;
         vertex_props_map[v][S_NODE_OUTLINE_COLOR] = sBGColor;
         vertex_props_map[v][S_NODE_LABEL_STYLE] = sBGColor;
       }
       if( p_species == max_z_map[i_z].first && !sMaxZNodeShape.empty() )
       {
         vertex_props_map[v][S_NODE_SHAPE] = sMaxZNodeShape;
       }
       if( p_species == p_max_species && !sMaxNodeShape.empty() )
       {
         vertex_props_map[v][S_NODE_SHAPE] = sMaxNodeShape;
       }
     }
    }

    std::string
    get_color_from_value( double x )
    {

      int i;
      std::stringstream s_color;

      if( x >= 0 )
      {
        i = int( ( 511. / dMuMax ) * x );
      }
      else
      {
        i = int( ( -767. / dMuMin ) * x );
      }

      if( i >= -256 && i <= 0 )    // Pivot magenta to red.
      {
        s_color.str("");
        s_color << "#FF00";
        s_color << boost::format( "%02x" ) % (256 + i - 1);
      }
      else if( i >= -511 && i < -255 )  // Pivot red to yellow.
      {
        s_color.str("");
        s_color << "#FF";
        s_color << boost::format( "%02x" ) % (-i - 255);
        s_color << "00";
      }
      else if( i >= -767 && i < -511 )  // Pivot yellow to green.
      {
        s_color.str("");
        s_color << "#";
        s_color << boost::format( "%02x" ) % (i + 767);
        s_color << "FF00";
      }
      else if( i <= 255 && i > 0 )  // Pivot light purple to blue.
      {
        s_color.str("");
        s_color << "#";
        s_color << boost::format( "%02x" ) % (255 - i);
        s_color << "00";
        s_color << "FF";
      }
      else if( i < 511 && i > 255 )  // Pivot blue to cyan.
      {
        s_color.str("");
        s_color << "#00";
        s_color << boost::format( "%02x" ) % (i - 255);
        s_color << "FF";
      }
      else
      {
        s_color.str( "#808080" );  // Outside range = grey.
      }
    
      return s_color.str();
    
    }

    std::pair<std::string, std::string>
    get_bw_from_value( double x )
    {

      std::stringstream s_color;
      std::string s_str_color;

      int i = int( 240. * (x - dMuMin) / (dMuMax - dMuMin) );

      s_color.str("");
      s_color << "#";
      s_color << boost::format( "%02x" ) % i;
      s_color << boost::format( "%02x" ) % i;
      s_color << boost::format( "%02x" ) % i;

      if( x > dVertexBWLabelMu )
      {
        s_str_color = std::string( "black" );
      }
      else
      {
        s_str_color = std::string( "white" );
      }
    
      return std::make_pair( s_color.str(), s_str_color );
    
    }

  };
    
//##############################################################################
// network_grapher_options().
//##############################################################################

class network_grapher_options :
  public base::network_grapher_options,
  public base::network_grapher_output_base_options,
  public base::network_grapher_induced_view_options
{

  public:
    network_grapher_options() :
      base::network_grapher_options(),
      base::network_grapher_output_base_options(),
      base::network_grapher_induced_view_options(){}


    std::string
    getExampleString()
    {
      return
        base::network_grapher_options::getExampleString() +
        base::network_grapher_output_base_options::getExampleString() +
        base::network_grapher_induced_view_options::getExampleString();
    }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Write title.
        (
          S_WRITE_TITLE,
          po::value<bool>()->default_value( true, "true" ),
          "Write title for graph."
        )

        // B/W
        (
          S_BW,
          po::value<bool>()->default_value( false, "false" ),
          "Vertices in greyscale."
        )

        // B/W
        (
          S_BW_LABEL_MU,
          po::value<double>()->default_value( 100, "100" ),
          "Offset for shift from black to white label (bw version only)"
        )

        // Max abundance shape.
        (
          S_MAX_NODE_SHAPE, po::value<std::string>()->default_value(""),
          "Shape for max abundance species (default: none)."
        )

        // Max Z abundance shape.
        (
          S_MAX_Z_NODE_SHAPE, po::value<std::string>()->default_value(""),
          "Shape for max isotope abundance (default: none)."
        )

        // Min abundance
        (
          S_MIN_NODE_ABUND, po::value<double>()->default_value(1.e-25),
          "Minimum abundance for species to include in diagrams."
        )

        // Mu range
        (
          S_MU_MIN, po::value<double>()->default_value(-1000, "-1000" ),
          "Minimum for chemical potential offset."
        )

        (
          S_MU_MAX, po::value<double>()->default_value(500, "500" ),
          "Maximum for chemical potential offset."
        )
        ;

        base::network_grapher_options::getOptions( network_grapher );

        base::network_grapher_output_base_options::getOptions(
          network_grapher
        );

        base::network_grapher_induced_view_options::getOptions(
          network_grapher
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_DETAIL_HPP
