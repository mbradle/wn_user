///////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file integrated_currents.hpp
//! \brief A file to define network graph routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_grapher/base/network_grapher.hpp"
#include "network_grapher/base/network_grapher_output_base.hpp"
#include "network_grapher/base/network_grapher_induced_view.hpp"

#ifndef WN_NETWORK_GRAPHER_DETAIL_HPP
#define WN_NETWORK_GRAPHER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define   S_MAX_CURRENT_SCALE               "max_current_scale"
#define   S_THRESHOLD_CURRENT_SCALE         "threshold_current_scale"
#define   S_WRITE_TITLE                     "write_title"

typedef std::map<std::string, double> currents_map_t;

typedef std::pair<Libnucnet__NucView *, Libnucnet__ReacView *> views_t;

typedef typename boost::property_map<graph_t, boost::edge_weight_t>::type
  weight_map_t;

typedef typename boost::property_traits<weight_map_t>::value_type weight_t;

//##############################################################################
// edgeFilter().
//##############################################################################

template<typename Graph>
class
edgeFilter : public base::edgeFilter<Graph>
{
  public:
    edgeFilter() : base::edgeFilter<Graph>(){}
    edgeFilter(
      Graph& g,
      Libnucnet__NucView * p_nuc_view,
      Libnucnet__ReacView * p_reac_view,
      weight_t d_thresh_hold
    ) :
    base::edgeFilter<Graph>( g, p_nuc_view, p_reac_view ),
    dThreshold( d_thresh_hold )
    {
      wm = boost::get( boost::edge_weight, g );
    }

    template<typename Edge>
    bool operator()( const Edge& e ) const
    {
      if( base::edgeFilter<Graph>::operator()( e ) && wm[e] >= dThreshold )
      {
        return true;
      }

      return false;
    }

  private:
    weight_map_t wm;
    weight_t dThreshold;

};

typedef
  boost::filtered_graph<
    graph_t,
    edgeFilter<graph_t>,
    base::vertexFilter<graph_t>
  > filtered_graph_t;

//##############################################################################
// graphWriter().
//##############################################################################

template<typename Graph>
class
graphWriter : public base::graphWriter<Graph>
{
  public:
    graphWriter() : base::graphWriter<Graph>(){}
    graphWriter( Graph& g, nnt::Zone& zone_, bool b_write_title ) :
      base::graphWriter<Graph>( g ), zone( zone_ ),
      bWriteTitle( b_write_title ){};

    void operator()( std::ostream& out ) const
    {
      base::graphWriter<Graph>::operator()( out );

      if( bWriteTitle )
      {
        out << "label = \"latex\";" << std::endl;
        out <<
          boost::format(
            "texlbl = \"\\huge{$time(s) = %g \
             \\ \\ \\ \\ T_9 = %g \
             \\ \\ \\ \\ \\rho(g/cc) = %g$}\";"
          ) %
          zone.getProperty<double>( nnt::s_TIME ) %
          zone.getProperty<double>( nnt::s_T9 ) %
          zone.getProperty<double>( nnt::s_RHO );
       }
     }

  private:
    nnt::Zone& zone;
    bool bWriteTitle;

};

//##############################################################################
// get_currents().
//##############################################################################

void
get_currents(
  const char * s_name,
  const char * s_tag1,
  const char * s_tag2,
  const char * s_value,
  currents_map_t * p_currents
)
{
  (*p_currents)[s_tag1] = boost::lexical_cast<double>( s_value );
}

//##############################################################################
// network_grapher().
//##############################################################################

class network_grapher :
  public base::network_grapher,
    public base::network_grapher_output_base,
    public base::network_grapher_induced_view
{

  public:
    network_grapher() :
      base::network_grapher(),
      base::network_grapher_output_base(),
      base::network_grapher_induced_view(){}
    network_grapher( v_map_t& v_map ) :
      base::network_grapher( v_map ),
      base::network_grapher_output_base( v_map ),
      base::network_grapher_induced_view( v_map )
      {
        dMaxScale = v_map[S_MAX_CURRENT_SCALE].as<double>();
        dThresholdScale = v_map[S_THRESHOLD_CURRENT_SCALE].as<double>();
        bWriteTitle = v_map[S_WRITE_TITLE].as<bool>();
      }

    void
    write( graph_t& g, nnt::Zone& zone, std::ofstream& my_out, views_t& views )
    {
      filtered_graph_t
        fg(
          g,
          edgeFilter<graph_t>( g, views.first, views.second, dThresholdScale ),
          base::vertexFilter<graph_t>( g, views.first, allowIsolatedVertices() )
        );
      scaleGraphEdgeWeights<filtered_graph_t, weight_t>( fg );

      boost::write_graphviz(
        my_out,
        fg,
        base::vertexWriter<filtered_graph_t>( fg ),
        base::edgeWriter<filtered_graph_t>( fg ),
        graphWriter<graph_t>( g, zone, bWriteTitle )
      );

    }

    void
    write( std::vector<nnt::Zone>& zones )
    {
      if( getOutputBase().empty() )
      {
        std::cerr << "Output base not set." << std::endl;
        exit( EXIT_FAILURE );
      }
      views_t views = getInducedViews( getGraphNetwork( v_g[0] ) );
      for( size_t i = 0; i < zones.size(); i++ )
      {
        std::string s_i = boost::lexical_cast<std::string>( i );
        boost::trim( s_i );
        std::ofstream my_out;
        std::string s_file = getOutputBase() + "_" + s_i + ".dot";
        my_out.open( s_file.c_str() );
        write( v_g[i], zones[i], my_out, views );
        my_out.close();
      }
      clearViews( views );
    }

    graph_t
    create_graph( nnt::Zone& zone )
    {
      currents_map_t my_currents;
      Libnucnet__Zone * p_zone = zone.getNucnetZone();
      Libnucnet__Reac * p_reac =
        Libnucnet__Net__getReac( Libnucnet__Zone__getNet( p_zone ) );

      graph_t g = createGraph<graph_t>( Libnucnet__Zone__getNet( p_zone ) );

      Libnucnet__Zone__iterateOptionalProperties(
        p_zone,
        nnt::s_FLOW_CURRENT,
        NULL,
        NULL,
        (Libnucnet__Zone__optional_property_iterate_function) get_currents,
        &my_currents
      );

      BOOST_FOREACH( currents_map_t::value_type& t, my_currents )
      {
        Libnucnet__Reaction * p_reaction =
          Libnucnet__Reac__getReactionByString( p_reac, t.first.c_str() );
        std::map<std::string, std::string> props;
        props["style"] = "solid";
        nnt::reaction_element_list_t reactants =
          nnt::make_reaction_nuclide_reactant_list( p_reaction );
        nnt::reaction_element_list_t products =
          nnt::make_reaction_nuclide_product_list( p_reaction );
        if( t.second >= 0 )
        {
          addReactionEdges(
            g, p_reaction, reactants, products, props, t.second
          );
        }
        else
        {
          addReactionEdges(
            g, p_reaction, products, reactants, props, -t.second
          );
        }
      }
      return g;
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      for( size_t i = 0; i < zones.size(); i++ )
      {
        v_g.push_back( create_graph( zones[i] ) );
      }
    }

  private:
    std::vector<graph_t> v_g;
    weight_t dThresholdScale;
    double dMaxScale;
    bool bWriteTitle;

    template<typename Graph, typename Weight>
    void
    scaleGraphEdgeWeights( Graph& g )
    {
      Weight w_max = 0;
      BGL_FORALL_EDGES_T( e, g, Graph )
      {
        Weight weight = boost::get( boost::edge_weight, g )[e];
        if( fabs( weight ) > w_max ) { w_max = weight; }
      }
      BGL_FORALL_EDGES_T( e, g, Graph )
      {
        boost::get( boost::edge_weight, g )[e] *= dMaxScale / w_max;
      }
    }

};
    
//##############################################################################
// network_grapher_options().
//##############################################################################

class network_grapher_options :
  public base::network_grapher_options,
  public base::network_grapher_output_base_options,
  public base::network_grapher_induced_view_options
{

  public:
    network_grapher_options() :
      base::network_grapher_options(),
      base::network_grapher_output_base_options(),
      base::network_grapher_induced_view_options(){}


    std::string
    getExampleString()
    {
      return
        base::network_grapher_options::getExampleString() +
        base::network_grapher_output_base_options::getExampleString() +
        base::network_grapher_induced_view_options::getExampleString();
    }

    void
    getOptions( po::options_description& network_grapher )
    {

      try
      {

        network_grapher.add_options()

        // Maximum current scale.
        (
          S_MAX_CURRENT_SCALE,
          po::value<double>()->default_value( 10., "10" ),
          "Maximum current scaling."
        )

        // Threshold current scale.
        (
          S_THRESHOLD_CURRENT_SCALE,
          po::value<double>()->default_value( 1.e-5, "1.e-5" ),
          "Threshold current scaling."
        )

        // Write title.
        (
          S_WRITE_TITLE,
          po::value<bool>()->default_value( true, "true" ),
          "Write title for graph."
        )

        ;

        base::network_grapher_options::getOptions( network_grapher );

        base::network_grapher_output_base_options::getOptions(
          network_grapher
        );

        base::network_grapher_induced_view_options::getOptions(
          network_grapher
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_GRAPHER_DETAIL_HPP
