////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017-2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file global_types.h
//! \brief A file to define global types.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/any.hpp>
#include <boost/bind/bind.hpp>
#include <boost/program_options.hpp>
#include <boost/tuple/tuple.hpp>

#include "nnt/string_defs.h"
#include "program_options/program_options.hpp"

namespace po = boost::program_options;

#ifndef MY_GLOBAL_TYPES_H
#define MY_GLOBAL_TYPES_H

/**
 * @brief A namespace for user-defined types.
 */
namespace wn_user
{

typedef po::variables_map v_map_t;

typedef std::vector< double > state_type;

struct options_struct
{

  po::options_description desc;
  std::string example;
  int iNumber;

  options_struct(){}
  options_struct( po::options_description _desc) : desc( _desc ){}
  options_struct(
    po::options_description _desc,
    std::string _example
  ) : desc( _desc ), example( _example ){}
  options_struct(
    po::options_description _desc,
    std::string _example,
    int _iNumber
  ) : desc( _desc ), example( _example ), iNumber( _iNumber ){}
    
};

typedef std::map<std::string, options_struct> options_map;

}  // namespace wn_user

#endif // MY_GLOBAL_TYPES_H
