////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file evolver.hpp
//! \brief A file to define evolver routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "user/multi_zone_utilities.h"

#ifndef WN_NETWORK_EVOLVER_DETAIL_HPP
#define WN_NETWORK_EVOLVER_DETAIL_HPP

#define S_CHECK            "check"
#define S_ITERATIVE_DEBUG  "iterative_debug"
#define S_ITMAX            "max_it"
#define S_REL_TOL          "rel_tol"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// network_evolver().
//##############################################################################

class network_evolver
{

  public:
    network_evolver( v_map_t& v_map )
    {
      d_check = v_map[S_CHECK].as<double>();
      itmax = v_map[S_ITMAX].as<int>();
      b_debug = v_map[S_ITERATIVE_DEBUG].as<bool>();
      d_rel_tol = v_map[S_REL_TOL].as<double>();
    }

    void
    solverProperties( WnSparseSolve__Phi * p_solver )
    {

      WnSparseSolve__Phi__updateMaximumIterations(
        p_solver,
        itmax
      );

      WnSparseSolve__Phi__updateTolerance(
        p_solver,
        d_rel_tol
      );

      if( b_debug ) { WnSparseSolve__Phi__setDebug( p_solver ); }

    }

    std::vector<WnMatrix *>
    get_matrices(
      std::vector<nnt::Zone>& zones,
      zone_linker& my_linker
    )
    {

      std::vector<WnMatrix *> mix_matrices;
      typedef user::zone_link_graph_t Graph;
      size_t i_offset, i_total_size;
      bool is_multi_mass = user::is_multi_mass_calculation( zones );

      Graph g = user::make_zone_link_graph( zones );

      my_linker.setLinks( g );

      my_linker.outputGraph( g );

      size_t i_species =
        Libnucnet__Nuc__getNumberOfSpecies(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zones[0].getNucnetZone() )
          )
        );

      if( is_multi_mass )
      {
        i_offset = i_species + 1;
      }
      else
      {
        i_offset = i_species;
      }
      i_total_size = zones.size() * i_offset;

      for( size_t i = 0; i < zones.size(); i++ )
        mix_matrices.push_back( WnMatrix__new( i_total_size, i_total_size ) );

      nnt::species_list_t species_list =
        nnt::make_species_list(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zones[0].getNucnetZone() )
          )
        );

      BGL_FORALL_VERTICES( v_from, g, Graph )
      {

        size_t i_from = v_from;

        BGL_FORALL_OUTEDGES( v_from, e, g, Graph )
        {
          Graph::vertex_descriptor v_to = boost::target( e, g );

          size_t i_to = v_to;

          if( is_multi_mass )
          {

            WnMatrix__assignElement(
	      mix_matrices[i_from],
	      i_offset * i_from + 1,
	      i_offset * i_from + 1,
	      -g[e].getWeight()
            );

            WnMatrix__assignElement(
              mix_matrices[i_from],
	      i_offset * i_to + 1,
	      i_offset * i_from + 1,
	      g[e].getWeight()
            );

          }

          BOOST_FOREACH( nnt::Species species, species_list )
          {

	    size_t i_species_offset =
	      Libnucnet__Species__getIndex( species.getNucnetSpecies() );

            if( is_multi_mass )
            {
              i_species_offset += 1;
            }

	    WnMatrix__assignElement(
	      mix_matrices[i_from],
	      ( i_offset * i_from ) + i_species_offset + 1,
	      ( i_offset * i_from ) + i_species_offset + 1,
	      -g[e].getWeight()
	    );

	    WnMatrix__assignElement(
	      mix_matrices[i_from],
	      ( i_offset * i_to ) + i_species_offset + 1,
	      ( i_offset * i_from ) + i_species_offset + 1,
	      g[e].getWeight()
	    );

          }
        
        }

      }

      return mix_matrices;

    }

    int
    operator()(
      std::vector<nnt::Zone>& zones,
      zone_linker& my_linker,
      double d_dt
    )
    {
      user::exp_multi_zone(
        zones,
        boost::bind(
          &network_evolver::get_matrices,
          this,
          boost::ref( zones ),
          my_linker
        ),
        boost::bind( &network_evolver::solverProperties, this, _1 ),
        boost::bind(
          user::check_multi_zone_mass_fractions,
          boost::ref( zones ),
          d_check
        ),
        d_dt
      );

      return 1;
    }

  private:
    int itmax;
    double d_rel_tol, d_check;
    bool b_debug;

};

//##############################################################################
// network_evolver_options().
//##############################################################################

class network_evolver_options
{

  public:
    network_evolver_options(){}

    std::string
    getExample()
    {

      return
        "--" + std::string( S_ITMAX ) + " 200 ";

    }

    void
    getOptions( po::options_description& network_evolver )
    {

      try
      {

        network_evolver.add_options()

        // Option for iterative solver
        (  S_ITERATIVE_DEBUG,
           po::value<bool>()->default_value( false, "false" ),
           "Iterative solver debug" )

        // Option for value of max iterations
        (  S_ITMAX,
           po::value<int>()->default_value( 50 ),
           "Maximum number of iterations in solver\n" )
    
        // Option for value of relative tolerance
        (  S_REL_TOL,
           po::value<double>()->default_value( 1.e-12, "1.e-12" ),
           "Relative tolerance for solutions\n" )
    
        // Option for value of ilu drop tolerance
        (  S_CHECK,
           po::value<double>()->default_value( 1.e-4, "1.e-4" ),
           "Tolerance for mass fraction check\n" )
    
        ;
    
// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_EVOLVER_DETAIL_HPP

