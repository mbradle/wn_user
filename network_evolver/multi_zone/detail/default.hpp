////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file evolver.hpp
//! \brief A file to define evolver routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "network_evolver/base/network_evolver.hpp"

#ifndef WN_NETWORK_EVOLVER_DETAIL_HPP
#define WN_NETWORK_EVOLVER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// network_evolver().
//##############################################################################

class network_evolver : public base::network_evolver
{

  public:
    network_evolver( v_map_t& v_map ) : base::network_evolver( v_map ){}

    std::pair<WnMatrix *, gsl_vector *>
    matrix_and_vector(
      std::vector<nnt::Zone>& zones,
      zone_linker& my_linker,
      gsl_vector * p_current
    )
    {

      std::pair<WnMatrix *, gsl_vector *> my_pair;
      typedef user::zone_link_graph_t Graph;
      size_t i_species, i_offset, i_total_size;
      double d_mass_from;

      bool is_multi_mass = user::is_multi_mass_calculation( zones );

      Graph g = user::make_zone_link_graph( zones );

      my_linker.setLinks( g );

      my_linker.outputGraph( g );

      //========================================================================
      // Get species list.
      //========================================================================
  
      nnt::species_list_t species_list =
        nnt::make_species_list(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zones[0].getNucnetZone() )
          )
        );

      i_species = species_list.size();

      if( is_multi_mass )
      {
        i_offset = i_species + 1;
      }
      else
      {
        i_offset = i_species;
      }

      i_total_size = zones.size() * i_offset;

      //========================================================================
      // Create matrix and vector.
      //========================================================================
  
      my_pair.first = WnMatrix__new( i_total_size, i_total_size );

      my_pair.second = gsl_vector_calloc( i_total_size );

      //========================================================================
      // Iterate links.
      //========================================================================
  
      BGL_FORALL_VERTICES( v_from, g, Graph )
      {
        size_t i_from = v_from;

        if( is_multi_mass )
        {
          d_mass_from = zones[i_from].getProperty<double>( nnt::s_ZONE_MASS );
        }

        BGL_FORALL_OUTEDGES( v_from, e, g, Graph )
        {
          Graph::vertex_descriptor v_to = boost::target( e, g );

          size_t i_to = v_to;

          if( is_multi_mass )
          {

            WnMatrix__assignElement(
              my_pair.first,
              i_offset * i_from + 1,
              i_offset * i_from + 1,
              g[e].getWeight()
            );

            WnMatrix__assignElement(
              my_pair.first,
              i_offset * i_to + 1,
              i_offset * i_from + 1,
              -g[e].getWeight()
            );

            gsl_vector_set(
              my_pair.second,
              i_offset * i_from,
              gsl_vector_get(
                my_pair.second,
                i_offset * i_from
              ) -
              d_mass_from * g[e].getWeight()
            );

            gsl_vector_set(
              my_pair.second,
              i_offset * i_to,
              gsl_vector_get(
                my_pair.second,
                i_offset * i_to
              ) +
              d_mass_from * g[e].getWeight()
            );

          }

          BOOST_FOREACH( nnt::Species species, species_list )
          {

	    size_t i_species_offset =
	      Libnucnet__Species__getIndex( species.getNucnetSpecies() );

            if( is_multi_mass )
            {
              i_species_offset += 1;
            }

	    WnMatrix__assignElement(
	      my_pair.first,
	      ( i_offset * i_from ) + i_species_offset + 1,
	      ( i_offset * i_from ) + i_species_offset + 1,
              g[e].getWeight()
	    );

	    gsl_vector_set(
	      my_pair.second,
	      ( i_from * i_offset ) + i_species_offset,
	      gsl_vector_get(
	        my_pair.second,
	        ( i_from * i_offset ) + i_species_offset
	      )
	      -
	      gsl_vector_get(
	        p_current,
	        ( i_from * i_offset ) + i_species_offset
	      ) * g[e].getWeight()
	    );
	  
	    WnMatrix__assignElement(
	      my_pair.first,
	      ( i_offset * i_from ) + i_species_offset + 1,
	      ( i_offset * i_to ) + i_species_offset + 1,
	      -g[e].getWeight()
	    );

	    gsl_vector_set(
	      my_pair.second,
	      ( i_from * i_offset ) + i_species_offset,
	      gsl_vector_get(
	        my_pair.second,
	        ( i_from * i_offset ) + i_species_offset
	      )
	      +
	      gsl_vector_get(
	        p_current,
	        ( i_to * i_offset ) + i_species_offset
	      ) * g[e].getWeight()
	    );
	  
          }
        }

      }

      return my_pair;

    }

    std::pair<size_t, double>
    setSolverProperties( WnSparseSolve__Mat * p_solver )
    {

      WnSparseSolve__Mat__updateSolverMethod(
        p_solver,
        sIterativeSolver.c_str()
      );

      WnSparseSolve__Mat__updateMaximumIterations(
        p_solver,
        iIterativeMaxIter
      );

      WnSparseSolve__Mat__updateRelativeTolerance(
        p_solver,
        dIterativeRelTol
      );

      WnSparseSolve__Mat__updateAbsoluteTolerance(
        p_solver,
        dIterativeAbsTol
      );

      WnSparseSolve__Mat__updateConvergenceMethod(
        p_solver,
        sConverge.c_str()
      );

      if( bIterativeDebug ) { WnSparseSolve__Mat__setDebug( p_solver ); }

      //========================================================================
      // Return the preconditioner data.
      //========================================================================

      return std::make_pair( iILUDelta, dILUDropTol );

    }

    int operator()(
      std::vector<nnt::Zone>& zones,
      zone_linker& my_linker,
      double d_dt
    )
    {
      user::safe_evolve_multi_zone(
        zones,
        boost::bind(
          &network_evolver::matrix_and_vector,
          this,
          boost::ref( zones ),
          boost::ref( my_linker ),
          _1
        ),
        boost::bind(
          &network_evolver::setSolverProperties, this, _1
        ),
        boost::bind(
          user::check_multi_zone_mass_fractions,
          boost::ref( zones ),
          dAbundCheck
        ),
        d_dt
      );

      return 1;
    }

};

//##############################################################################
// network_evolver_options().
//##############################################################################

class network_evolver_options : public base::network_evolver_options
{

  public:
    network_evolver_options() : base::network_evolver_options(){}

    std::string
    getExample()
    {
      return base::network_evolver_options::getExample();
    }

    void
    getOptions( po::options_description& network_evolver )
    {

       base::network_evolver_options::getOptions( network_evolver );
    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_EVOLVER_DETAIL_HPP
