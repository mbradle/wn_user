////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file evolver.hpp
//! \brief A file to define evolver routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "network_evolver/base/network_evolver.hpp"
#include "utility/zone_abundance_normalizer.hpp"

#ifndef WN_NETWORK_EVOLVER_DETAIL_HPP
#define WN_NETWORK_EVOLVER_DETAIL_HPP

#define S_ARROW_WIDTH       "arrow_width"
#define S_DETAILED_BALANCE  "detailed_balance"
#define S_NEG_MAX_ABUND     "max_neg_abund"
#define S_NORM_ABUNDS_STEP  "norm_abunds_step"
#define S_SAFE_FACTOR       "safe_factor"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// network_evolver().
//##############################################################################

class network_evolver : public base::network_evolver
{

  public:
    network_evolver() : base::network_evolver(){}
    network_evolver( v_map_t& v_map ) : base::network_evolver( v_map )
    {

      sEvolver = v_map[nnt::s_SOLVER].as<std::string>();

      dFactor = v_map[S_SAFE_FACTOR].as<double>();
      bNormAbundsStep = v_map[S_NORM_ABUNDS_STEP].as<bool>();
      bDetailedBalance = v_map[S_DETAILED_BALANCE].as<bool>();
      dNegMaxAbund = v_map[S_NEG_MAX_ABUND].as<double>();

      if( sEvolver == nnt::s_ARROW )
      {
        iArrowWidth = v_map[S_ARROW_WIDTH].as<size_t>();

      }

      if( !sIterativeSolver.empty() )
      {
        dIterativeT9 = v_map[S_ITERATIVE_T9].as<double>();
      }

      bInitialized = false;

    }

    static bool
    check_function( nnt::Zone& zone, double d_x_min, double d_neg_max_abund )
    {
      double d_xsum =
        1. - Libnucnet__Zone__computeAMoment( zone.getNucnetZone(), 1 );

      gsl_vector * p_abunds =
        Libnucnet__Zone__getAbundances( zone.getNucnetZone() );

      int is_non_neg = gsl_vector_isnonneg( p_abunds );

      int is_small_neg = 1;

      if( !is_non_neg )
      {
        for( size_t i = 0; i < p_abunds->size; i++ )
        {
          if( gsl_vector_get( p_abunds, i ) < d_neg_max_abund )
          {
            is_small_neg = 0;
          }
        }
      }

      if( is_small_neg ) { is_non_neg = 1; }

      gsl_vector_free( p_abunds );

      return ( fabs( d_xsum ) < d_x_min && is_non_neg );
    }

    void initializeForZone( nnt::Zone& zone )
    {
      if( !bInitialized && sEvolver == nnt::s_ARROW )
      {
        Libnucnet__Nuc * p_nuc =
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          );

        Libnucnet__Nuc__setSpeciesCompareFunction(
          p_nuc,
          (Libnucnet__Species__compare_function) nnt::species_sort_function
        );

        Libnucnet__Nuc__sortSpecies( p_nuc );
      }

      bInitialized = true;
    }

    void operator()( nnt::Zone& zone )
    {
      if( !bInitialized )
      {
        initializeForZone( zone );
      }
      double d_dt = zone.getProperty<double>( nnt::s_DTIME );
      zone.updateFunction(
        nnt::s_SAFE_EVOLVE_CHECK_FUNCTION,
        static_cast<boost::function<bool( nnt::Zone& )> >(
          boost::bind( check_function, _1, dAbundCheck, dNegMaxAbund )
        )
      );
      if( !bDetailedBalance )
      {
        Libnucnet__Zone__toggleReverseRateDetailedBalance(
          zone.getNucnetZone(), "off"
        );
      }
      if( d_dt > 0 )
      {
        zone.updateProperty( nnt::s_SOLVER, sEvolver );
        if( sEvolver == nnt::s_ARROW )
        {
          zone.updateProperty( nnt::s_ARROW_WIDTH, iArrowWidth );
        }
        if( !sIterativeSolver.empty() )
        {
          zone.updateProperty( nnt::s_ITER_SOLVER, sIterativeSolver );
          zone.updateProperty( nnt::s_ITER_SOLVER_T9, dIterativeT9 );
          zone.updateProperty(
            nnt::s_ITER_SOLVER_MAX_ITERATIONS, iIterativeMaxIter
          );
          zone.updateProperty( nnt::s_ITER_SOLVER_ABS_TOL, dIterativeAbsTol );
          zone.updateProperty( nnt::s_ITER_SOLVER_REL_TOL, dIterativeRelTol );
          if( bIterativeDebug )
          {
            zone.updateProperty( nnt::s_ITER_SOLVER_DEBUG, "yes" );
          }
          zone.updateProperty( nnt::s_ILU_DROP_TOL, dILUDropTol );
          zone.updateProperty( nnt::s_ILU_DELTA, iILUDelta );
        }
        user::safe_evolve( zone, dFactor * d_dt, d_dt );
        if( bNormAbundsStep )
        {
          utility::zone_abundance_normalizer zm;
          zm( zone );
        }
      }
    }

  private:
    std::string sEvolver;
    double dFactor, dIterativeT9;
    double dNegMaxAbund;
    size_t iArrowWidth;
    bool bInitialized, bNormAbundsStep, bDetailedBalance;

};

//##############################################################################
// network_evolver_options().
//##############################################################################

class network_evolver_options :
  public base::network_evolver_options
{

  public:
    network_evolver_options() : base::network_evolver_options(){}

    std::string
    getExample()
    {

      return
        base::network_evolver_options::getExample()
        +
        "--" + std::string( S_ITERATIVE_T9 ) + " 2 ";

    }

    void
    getOptions( po::options_description& network_evolver )
    {

      try
      {

        network_evolver.add_options()

        // Option for solver type
        (  nnt::s_SOLVER,
           po::value<std::string>()->default_value( nnt::s_ARROW ),
           "Solver type" )
   
        // Option for arrow width
        (  S_ARROW_WIDTH,
           po::value<size_t>()->default_value( 3 ),
           "Arrow solver width" )

        // Option for safe evolver factor
        (  S_SAFE_FACTOR,
           po::value<double>()->default_value( 1 ),
           "Safe evolver factor" )

        // Option for iterative solver
        (  S_ITERATIVE_T9,
           po::value<double>()->default_value( 2 ),
           "Iterative solver t9" )

        // Option for normalizing abundances
        (  S_NORM_ABUNDS_STEP,
           po::value<bool>()->default_value( false, "false" ),
           "Normalize each step" )

        // Option for normalizing abundances
        (  S_DETAILED_BALANCE,
           po::value<bool>()->default_value( true, "true" ),
           "Use detailed balance" )

        // Largest allowed negative abundance (absolute)
        (  S_NEG_MAX_ABUND,
           po::value<double>()->default_value( -1.e-25 ),
           "Largest absolute negative abundance" )

        ;

        base::network_evolver_options::getOptions( network_evolver );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_EVOLVER_DETAIL_HPP
