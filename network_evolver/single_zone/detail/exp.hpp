////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file evolver.hpp
//! \brief A file to define evolver routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_NETWORK_EVOLVER_DETAIL_HPP
#define WN_NETWORK_EVOLVER_DETAIL_HPP

#include <WnSparseSolve.h>

#define S_ITER_DEBUG        "iter_debug"
#define S_MAX_ITER          "max_iter"
#define S_TOLERANCE         "tolerance"
#define S_WORKSPACE         "workspace"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// network_evolver().
//##############################################################################

class network_evolver
{

  public:
    network_evolver(){}
    network_evolver( v_map_t& v_map )
    {

      p_phi = WnSparseSolve__Exp__new();

      WnSparseSolve__Exp__updateMaximumIterations(
        p_phi, 
        v_map[S_MAX_ITER].as<size_t>()
      );

      WnSparseSolve__Exp__updateWorkSpace(
        p_phi, 
        v_map[S_WORKSPACE].as<size_t>()
      );

      WnSparseSolve__Exp__updateTolerance(
        p_phi, 
        v_map[S_TOLERANCE].as<double>()
      );

      if( v_map[S_ITER_DEBUG].as<bool>() )
      {
        WnSparseSolve__Exp__setDebug( p_phi );
      }

    }
    ~network_evolver(){
       WnMatrix__free( p_matrix );
       WnSparseSolve__Exp__free( p_phi );
    }

    void initialize( nnt::Zone& zone )
    {
      Libnucnet__Zone__toggleReverseRateDetailedBalance(
        zone.getNucnetZone(), "off"
      );
      Libnucnet__Zone__computeRates( zone.getNucnetZone(), 0.1, 1000. );

      p_matrix =
        Libnucnet__Zone__computeJacobian(
          zone.getNucnetZone()
        );
    }

    void operator()( nnt::Zone& zone, double& d_t )
    {
      initialize( zone );

      gsl_vector * p_solution_vector = NULL;
      double d_dt = d_t;

      gsl_vector * p_initial_vector =
          Libnucnet__Zone__getAbundances( zone.getNucnetZone() );

      p_solution_vector = 
        WnSparseSolve__Exp__solve( p_phi, p_matrix, p_initial_vector, d_dt );

      Libnucnet__Zone__updateAbundances(
        zone.getNucnetZone(),
        p_solution_vector
      );

      if( !p_solution_vector )
      {
        std::cerr << "Solution failed.  Run with debug to diagnose." <<
           std::endl;
        exit( EXIT_FAILURE );
      }

      gsl_vector_free( p_initial_vector );
      gsl_vector_free( p_solution_vector );

      d_t = d_dt;

    }

  private:
    WnSparseSolve__Exp * p_phi;
    WnMatrix * p_matrix;

};

//##############################################################################
// network_evolver_options().
//##############################################################################

class network_evolver_options
{

  public:
    network_evolver_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    getOptions( po::options_description& network_evolver )
    {

      try
      {

        network_evolver.add_options()

        // Option for solver type
        (  S_MAX_ITER,
           po::value<size_t>()->default_value( 100, "100" ),
           "Maximum iterations" )
   
        // Option for arrow width
        (  S_WORKSPACE,
           po::value<size_t>()->default_value( 20, "20" ),
           "Workspace (> 0 and <= 50)" )

        // Option for safe evolver factor
        (  S_TOLERANCE,
           po::value<double>()->default_value( 1.e-8, "1.e-8" ),
           "Tolerance" )

        // Option for iterative solver
        (  S_ITER_DEBUG,
           po::value<bool>()->default_value( false, "false" ),
           "Iterative debug." )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NETWORK_EVOLVER_DETAIL_HPP
