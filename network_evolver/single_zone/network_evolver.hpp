////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file evolver.hpp
//! \brief A file to define evolver routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_NETWORK_EVOLVER_HPP
#define WN_NETWORK_EVOLVER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// network_evolver().
//##############################################################################

class network_evolver : public detail::network_evolver
{

  public:
    network_evolver(){}
    network_evolver( v_map_t& v_map ) : detail::network_evolver( v_map ){}

};

//##############################################################################
// network_evolver_options().
//##############################################################################

class network_evolver_options : detail::network_evolver_options
{

  public:
    network_evolver_options() : detail::network_evolver_options() {}

    std::string
    getExample()
    {
      return detail::network_evolver_options::getExample();
    }

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description network_evolver( "\nEvolver Options" );

        detail::network_evolver_options::getOptions( network_evolver );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "network_evolver",
            options_struct( network_evolver, getExample() )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace wn_user

#endif // WN_NETWORK_EVOLVER_HPP
