////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file evolver.hpp
//! \brief A file to define evolver routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "user/evolve.h"

#ifndef WN_NETWORK_EVOLVER_BASE_HPP
#define WN_NETWORK_EVOLVER_BASE_HPP

#define S_ABUND_CHECK         "abundance_check"
#define S_CONVERGE      "converge"
#define S_ITERATIVE_SOLVER   "iterative_solver"
#define S_ITERATIVE_T9  "iterative_t9"
#define S_ITERATIVE_DEBUG  "iterative_debug"
#define S_ITERATIVE_ABS_TOL  "iterative_abs_tol"
#define S_ITERATIVE_REL_TOL  "iterative_rel_tol"
#define S_ITERATIVE_MAX_IT  "iterative_max_it"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// check_solver().
//##############################################################################

void
check_solver( const std::string& s )
{
  std::set<std::string> solvers;
  std::map<std::string, std::string> solvers_description;

  solvers.insert( "cg" );
  solvers_description["cg"] = "conjugate gradient";

  solvers.insert( "cgnr" );
  solvers_description["cgnr"] = "conjugate gradient with normalized residue";

  solvers.insert( "bcg" );
  solvers_description["bcg"] = "bi-conjugate gradient";

  solvers.insert( "dbcg" );
  solvers_description["dbcg"] = "bi-conjugate gradient with partial pivoting";

  solvers.insert( "bcgstab" );
  solvers_description["bcgstab"] =  "stabilized bi-conjugate gradient";

  solvers.insert( "tfqmr" );
  solvers_description["tfqmr"] = "transpose-free quasi-minimum residual";

  solvers.insert( "fom" );
  solvers_description["fom"] = "full orthogonalization";

  solvers.insert( "gmres" );
  solvers_description["gmres"] = "generalized minimum residual (default)";

  if( !s.empty() && solvers.find( s ) == solvers.end() )
  {
    std::cerr << "solver type " << s << " is not valid." << std::endl;
    std::cerr << "\nValid solvers are:\n";
    BOOST_FOREACH( std::string st, solvers )
    {
      std::cerr << "  " << st << ": " << solvers_description[st] << std::endl;
    }
    std::cerr << "\n";
    std::cerr <<
      "To not use iterative solver, set iterative_solver to " << "\"\"" << "\n";
    std::cerr << "\n";
    exit( EXIT_FAILURE );
  }
}

//##############################################################################
// network_evolver().
//##############################################################################

class network_evolver
{

  public:
    network_evolver(){}
    network_evolver( v_map_t& v_map )
    {
   
      dAbundCheck = v_map[S_ABUND_CHECK].as<double>();

      if( v_map.count( S_ITERATIVE_SOLVER ) )
      {
        sIterativeSolver = v_map[S_ITERATIVE_SOLVER].as<std::string>();
        sConverge = v_map[S_CONVERGE].as<std::string>();
        bIterativeDebug = v_map[S_ITERATIVE_DEBUG].as<bool>();
        dIterativeAbsTol = v_map[S_ITERATIVE_ABS_TOL].as<double>();
        dIterativeRelTol = v_map[S_ITERATIVE_REL_TOL].as<double>();
        iIterativeMaxIter = v_map[S_ITERATIVE_MAX_IT].as<size_t>();
        dILUDropTol = v_map[nnt::s_ILU_DROP_TOL].as<double>();
        iILUDelta = v_map[nnt::s_ILU_DELTA].as<size_t>();
      }

    }

  protected:
    std::string sConverge;
    std::string sIterativeSolver;
    double dAbundCheck, dILUDropTol;
    double dIterativeRelTol, dIterativeAbsTol;
    bool bIterativeDebug;
    size_t iIterativeMaxIter, iILUDelta;

};

//##############################################################################
// network_evolver_options().
//##############################################################################

class network_evolver_options
{

  public:
    network_evolver_options(){}

    std::string
    getExample()
    {

      return
        "--" + std::string( S_ITERATIVE_SOLVER ) + " gmres ";

    }

    void
    getOptions( po::options_description& network_evolver )
    {

      try
      {

        network_evolver.add_options()

        // Option for iterative solver
        (  S_ITERATIVE_SOLVER,
           po::value<std::string>()->default_value( "gmres" )->notifier(
              boost::bind( &check_solver, _1 )
           ),
           "Iterative solver type (supply \"\" to not use iterative solver)" )

        // Option for mass fraction check
        (  S_ABUND_CHECK,
           po::value<double>()->default_value( 1.e-4, "1.e-4" ),
           "Tolerance for mass fraction check\n" )
    
        // Option for value of max iterations
        (  S_CONVERGE,
           po::value<std::string>()->default_value( "initial residual" ),
           "Convergence method (\"initial residual\" or \"rhs\").\n" )

        // Option for iterative solver
        (  S_ITERATIVE_DEBUG,
           po::value<bool>()->default_value( false, "false" ),
           "Iterative solver debug" )

        // Option for iterative solver
        (  S_ITERATIVE_ABS_TOL,
           po::value<double>()->default_value( 0., "0." ),
           "Iterative solver absolute tolerance" )

        // Option for iterative solver
        (  S_ITERATIVE_REL_TOL,
           po::value<double>()->default_value( 1.e-16, "1.e-16" ),
           "Iterative solver relative tolerance" )

        // Option for iterative solver
        (  S_ITERATIVE_MAX_IT,
           po::value<size_t>()->default_value( 100, "100" ),
           "Iterative solver maximum iterations" )

        // Option for value of ilu delta
        (  nnt::s_ILU_DELTA,
           po::value<size_t>()->default_value( 50, "50" ),
           "Delta for ilu preconditioner\n" )

        // Option for value of ilu drop tolerance
        (  nnt::s_ILU_DROP_TOL,
           po::value<double>()->default_value( 0, "0." ),
           "Drop tolerance for ilu preconditioner\n" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace base

} // namespace wn_user

#endif // WN_NETWORK_EVOLVER_BASE_HPP
