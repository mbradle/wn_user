///////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#ifndef WN_INPUTTER_HPP
#define WN_INPUTTER_HPP

namespace wn_user
{


////////////////////////////////////////////////////////////////////////////////
///
/// \class inputter
///
/// \brief A class to handle input including program options.
///
////////////////////////////////////////////////////////////////////////////////


//##############################################################################
// inputter().
//##############################################################################

class inputter
{

  public:
    
    inputter() : pos_params( "Positional parameters" ){}

/// The input retriever.
/// \param argc The command line argument count.
/// \param argv The command line argument list.
/// \return The resulting options variable map.

    v_map_t getInput( int argc, char **argv )
    {
      getOptions( o_map );
      return parseData( argc, argv, o_map );
    }

    template<typename T>
    void
    addPositionalParameter(
      std::string s_param,
      int i_number,
      std::string s_usage,
      std::string s_example
    )
    {
      pos_params.add_options()
      ( s_param.c_str(), po::value<T>(), s_usage.c_str() )

      ;

      pMap[s_param] = std::make_pair( i_number, s_example );
    }

    template<typename T>
    T getPositionalParameter( v_map_t& v_map, std::string s_param )
    {
      return v_map[s_param.c_str()].as<T>();
    }

  private:
    typedef std::map<std::string, std::pair<int, std::string> > positional_map;
    options_map o_map;
    po::options_description pos_params;
    positional_map pMap;
    program_options my_program_options;
    std::vector<std::string> vExample;


    void getOptions( options_map& o_map )
    {
#ifdef WN_EQUILIBRIUM_HPP
      equilibrium_options my_equilibrium_options;
      my_equilibrium_options.get( o_map );
#endif
#ifdef NNP_FLOW_COMPUTER_HPP
      flow_computer_options my_flow_computer_options;
      my_flow_computer_options.get( o_map );
#endif
#ifdef WN_GCE_HPP
      gce_options my_gce_options;
      my_gce_options.get( o_map );
#endif
#ifdef NNP_HYDRO_HPP
      hydro_options my_hydro_options;
      my_hydro_options.get( o_map );
#endif
#ifdef WN_IMF_HPP
      imf_options my_imf_options;
      my_imf_options.get( o_map );
#endif
#ifdef NNP_LIBNUCNET_DATA_HPP
      libnucnet_data_options my_libnucnet_data_options;
      my_libnucnet_data_options.get( o_map );
#endif
#ifdef WN_MASS_TRACKER_HPP
      mass_tracker_options my_mass_tracker_options;
      my_mass_tracker_options.get( o_map );
#endif
#ifdef NNP_MATRIX_MODIFIER_HPP
      matrix_modifier_options my_matrix_modifier_options;
      my_matrix_modifier_options.get( o_map );
#endif
#ifdef WN_NETWORK_DATA_HPP
      network_data_options my_network_data_options;
      my_network_data_options.get( o_map );
#endif
#ifdef WN_NETWORK_EVOLVER_HPP
      network_evolver_options my_network_evolver_options;
      my_network_evolver_options.get( o_map );
#endif
#ifdef NNP_NETWORK_GRAPHER_HPP
      network_grapher_options my_network_grapher_options;
      my_network_grapher_options.get( o_map );
#endif
#ifdef WN_NETWORK_TIME_HPP
      network_time_options my_network_time_options;
      my_network_time_options.get( o_map );
#endif
#ifdef WN_OUTPUTTER_HPP
      outputter_options my_outputter_options;
      my_outputter_options.get( o_map );
#endif
#ifdef NNP_NETWORK_LIMITER_HPP
      network_limiter_options my_network_limiter_options;
      my_network_limiter_options.get( o_map );
#endif
#ifdef NNP_NEUTRINO_COLLECTION_HPP
      neutrino_collection_options my_neutrino_collection_options;
      my_neutrino_collection_options.get( o_map );
#endif
#ifdef NNP_NSE_CORRECTOR_HPP
      nse_corrector_options my_nse_corrector_options;
      my_nse_corrector_options.get( o_map );
#endif
#ifdef WN_PROPERTIES_UPDATER_HPP
      options my_properties_updater_options;
      my_properties_updater_options.get( o_map );
#endif
#ifdef NNP_RATE_COMPUTER_HPP
      rate_computer_options my_rate_computer_options;
      my_rate_computer_options.get( o_map );
#endif
#ifdef NNP_RATE_MODIFIER_HPP
      rate_modifier_options my_rate_modifier_options;
      my_rate_modifier_options.get( o_map );
#endif
#ifdef WN_RATE_REGISTERER_HPP
      rate_registerer_options my_rate_registerer_options;
      my_rate_registerer_options.get( o_map );
#endif
#ifdef WN_REMNANTS_HPP
      remnants_options my_remnants_options;
      my_remnants_options.get( o_map );
#endif
#ifdef NNP_SCREENER_HPP
      screener_options my_screener_options;
      my_screener_options.get( o_map );
#endif
#ifdef WN_STAR_TRACKER_HPP
      star_tracker_options my_star_tracker_options;
      my_star_tracker_options.get( o_map );
#endif
#ifdef WN_STEP_PRINTER_HPP
      step_printer_options my_step_printer_options;
      my_step_printer_options.get( o_map );
#endif
#ifdef WN_TIME_ADJUSTER_HPP
      time_adjuster_options my_time_adjuster_options;
      my_time_adjuster_options.get( o_map );
#endif
#ifdef WN_YIELDS_HPP
      yields_options my_yields_options;
      my_yields_options.get( o_map );
#endif
#ifdef WN_ZONE_DATA_HPP
      zone_data_options my_zone_data_options;
      my_zone_data_options.get( o_map );
#endif
#ifdef WN_ZONE_LINKER_HPP
      zone_linker_options my_zone_linker_options;
      my_zone_linker_options.get( o_map );
#endif
    }

    void 
    programOptions( 
      v_map_t& v_map, 
      options_map& o_map
   ) 
   {
      std::string s = v_map["program_options"].as<std::string>();

      if( o_map.find( s ) == o_map.end() )
      {
        std::cerr << s << " is an invalid program option." << std::endl;
        exit( EXIT_FAILURE );
      }

      std::cout << o_map[s].desc << std::endl;

      exit( EXIT_SUCCESS );
    }

    void 
    paramDescriptions( v_map_t& v_map ) 
   {
      if( v_map.count( "positional_params" ) )
      {
        std::cout << pos_params << std::endl;
      }
      exit( EXIT_SUCCESS );
    }

    void addHelp( options_map& o_map )
    {
      std::string s_prog = "print out list of program options (help, ";
      BOOST_FOREACH(
        const options_map::value_type& t,
        o_map
      )
      {
        s_prog += t.first + ", ";
      }
      s_prog += "or all) and exit";
        
      po::options_description help( "\nHelp Options" );
      help.add_options()
      // Option to print help message for usage statement
      ( "help", "print out usage statement and exit\n" )

      // Option to print example usage
      ( "example", "print out example usage and exit\n" )

      // Option to specify response file
      ( "response-file", po::value<std::string>(), 
        "can be specified with '@name', too\n" )

      // Option to print help message for specific options
      ( "program_options", po::value<std::string>(), s_prog.c_str() )

      ;

      // Option to print help message for positional parameters
      if( pMap.size() > 0 )
      {
        help.add_options()
        (
          "positional_params", 
          "print out positional parameters and exit\n" 
        )
        ;
      }

      ;

      o_map.insert(
        std::make_pair<std::string, options_struct>(
          "help", options_struct( help, "" )
        )
      );

    }

    void addAll( options_map& o_map )
    {

      po::options_description all( "\nAll Allowed Options" );

      BOOST_FOREACH(
        const options_map::value_type& t,
        o_map
      )
      {
        all.add( t.second.desc );
      }

      o_map.insert(
        std::make_pair<std::string, options_struct>(
          "all", options_struct( all, "" )
        )
      );
 
    }

    void
    addPositional( po::positional_options_description& pd )
    {
      
      BOOST_FOREACH(
        const positional_map::value_type& t,
        pMap
      )
      {
        pd.add( t.first.c_str(), t.second.first );
      }
    }

    void
    addExtraExampleString( std::string s )
    {
      vExample.push_back( s );
    }

    v_map_t
    parseData( int argc, char ** argv, options_map& o_map )
    {

      try
      {

        v_map_t v_map;

        addHelp( o_map );
        addAll( o_map );

        po::options_description cmdline_options;
        cmdline_options.add(o_map["all"].desc).add(pos_params);

        po::positional_options_description pd;
        addPositional( pd );
    
        store( 
          po::command_line_parser( argc, argv ).
          options( cmdline_options ).
          positional( pd ).
          extra_parser( my_program_options.atOptionParser ).
          run(), 
          v_map 
        );

        if( argc == 1 || v_map.count( "help" ) == 1 )
        {
          std::cout << std::endl << "Usage: " << argv[0]
                    << " [program_options]";
          if( pMap.size() > 0 )
          {
            std::cout << " positional_params" << std::endl;
          }
          std::cout << o_map["help"].desc << std::endl;
          exit( EXIT_FAILURE );
        }

        if( v_map.count( "example" ) == 1 )
        {
          std::string s_example = argv[0];
          BOOST_FOREACH(
            const options_map::value_type& t,
            o_map
          )
          {
            if( !(t.second.example).empty() )
            {
              s_example += " " + t.second.example;
            }
          }
          BOOST_FOREACH( const std::string s, vExample )
          {
            s_example += " " + s;
          }
          BOOST_FOREACH(
            const positional_map::value_type& t,
            pMap
          )
          {
            s_example += " " + t.second.second;
          }
          std::cout << std::endl << s_example << std::endl << std::endl;
          exit( EXIT_FAILURE );
        }
                              
        if( v_map.count( "program_options" ) )
        {
          programOptions(
            v_map,
            o_map
          );
        }

        if( v_map.count( "positional_params" ) )
        {
          paramDescriptions( v_map );
        }

        if( v_map.count( "response-file" ) ) 
          v_map = my_program_options.responseFile( v_map, o_map["all"].desc );

        po::notify( v_map );

        BOOST_FOREACH( positional_map::value_type& t, pMap )
        {
          if( v_map.count(t.first) == 0 )
          {
            std::cerr << "Required positional parameter \"" << t.first
                      << "\" not supplied." << std::endl;
            exit( EXIT_FAILURE );
          }
        }
           
        return v_map;

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }


};

} // wn_user

#endif // WN_INPUTTER_HPP
