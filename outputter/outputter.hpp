////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////


#ifndef WN_OUTPUTTER_HPP
#define WN_OUTPUTTER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// outputter().
//##############################################################################

class outputter : public detail::outputter
{
  public:
    outputter( v_map_t& v_map, bool b_zone_data_only = false ) :
      detail::outputter( v_map, b_zone_data_only ){}
};

//##############################################################################
// outputter_options()
//##############################################################################

class outputter_options : public detail::outputter_options
{

  public:
    outputter_options() : detail::outputter_options() {}

    std::string
    getExample()
    {
      return detail::outputter_options::getExample();
    }

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description outputter("\nOutputter options");

        detail::outputter_options::getOptions( outputter );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "outputter", 
            options_struct( outputter, getExample() )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace wn_user

#endif // WN_OUTPUTTER_HPP
