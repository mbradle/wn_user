////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"
#include "nnt/auxiliary.h"

#include "user/hdf5_routines.h"

#include "outputter/base/outputter.hpp"
#include "utility/nucnet_copier.hpp"

#ifndef WN_OUTPUTTER_HDF5_BASE_HPP
#define WN_OUTPUTTER_HDF5_BASE_HPP

#define S_HDF5_FILE        "output_hdf5"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// outputter_hdf5().
//##############################################################################

class outputter_hdf5
{

  public:
    outputter_hdf5( v_map_t& v_map )
    {

      if( v_map.count( S_HDF5_FILE ) )
      {
        sHdf5File = v_map[S_HDF5_FILE].as<std::string>();
      }

    }

    ~outputter_hdf5(){
      Libnucnet__free( pOutput );
    }

    void
    set( Libnucnet * p_nucnet )
    {
      wn_user::utility::nucnet_copier nc;
      pOutput = nc( p_nucnet );
      user::hdf5::create_output( sHdf5File.c_str(), pOutput );
    }

    Libnucnet *
    getOutputNucnet()
    { return pOutput; }

    std::string getHdf5File(){ return sHdf5File; }

    void updateZone( nnt::Zone& zone )
    {
      Libnucnet__Zone * p_zone =
        Libnucnet__getZoneByLabels(
          pOutput,
          Libnucnet__Zone__getLabel( zone.getNucnetZone(), 1 ),
          Libnucnet__Zone__getLabel( zone.getNucnetZone(), 2 ),
          Libnucnet__Zone__getLabel( zone.getNucnetZone(), 3 )
        );

      Libnucnet__Zone * p_copy = Libnucnet__Zone__copy( zone.getNucnetZone() );

      if( p_zone ) { Libnucnet__removeZone( pOutput, p_zone ); }

      Libnucnet__addZone( pOutput, p_copy );
    }

  private:
    Libnucnet * pOutput;
    std::string sHdf5File;

};

//##############################################################################
// outputter_hdf5_options()
//##############################################################################

class outputter_hdf5_options
{

  public:
    outputter_hdf5_options(){}

    std::string
    getExample()
    {

      return
        "--" + std::string( S_HDF5_FILE ) + " out.h5 ";
    }

    void getOptions( po::options_description& outputter )
    {

      try
      {

        outputter.add_options()

          ( S_HDF5_FILE, po::value<std::string>(),
            "Name of output hdf5 file" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace base

} // namespace wn_user

#endif // WN_OUTPUTTER_HDF5_BASE_HPP

