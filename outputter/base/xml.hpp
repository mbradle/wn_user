////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"
#include "nnt/auxiliary.h"

#include "utility/invalid_reaction_remover.hpp"
#include "utility/nucnet_copier.hpp"
#include "utility/zone_copier.hpp"
#include "outputter/base/outputter.hpp"

#ifndef WN_OUTPUTTER_XML_BASE_HPP
#define WN_OUTPUTTER_XML_BASE_HPP

#define S_XML_FILE        "output_xml"
#define S_XML_FORMAT      "xml_format"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// outputter_xml().
//##############################################################################

class outputter_xml
{

  public:
    outputter_xml( v_map_t& v_map )
    {
      sXmlFormat = v_map[S_XML_FORMAT].as<std::string>();
      sXmlFile = v_map[S_XML_FILE].as<std::string>();
    }

    ~outputter_xml()
    {
      for( size_t i = 0; i < outputZones.size(); i++ )
      {
        Libnucnet__Zone__free( outputZones[i] );
      }
    }

    void
    set( Libnucnet * p_nucnet )
    {
      pOriginal = p_nucnet;
      outputZones.clear();
    }

    std::string
    getXmlFile() const
    { return sXmlFile; }

    std::string
    getXmlFormat() const
    { return sXmlFormat; } 

    void operator()( std::vector<nnt::Zone>& zones )
    {
      BOOST_FOREACH( nnt::Zone zone, zones )
      {
        outputZones.push_back( Libnucnet__Zone__copy( zone.getNucnetZone() ) );
      }
    }

    void operator()( nnt::Zone& zone )
    {
      size_t i_zones = outputZones.size();

      utility::zone_copier zc( Libnucnet__getNet( pOriginal ) );

      std::vector<std::string> s_labels;
      s_labels.push_back( boost::lexical_cast<std::string>( i_zones ) );
      outputZones.push_back(
        zc( zone.getNucnetZone(), s_labels )
      );
    }

    Libnucnet * createOutputNucnet( Libnucnet * p_nucnet )
    {

      utility::nucnet_copier nc;
      Libnucnet * p_output = nc( p_nucnet );

      Libnucnet__setZoneCompareFunction(
        p_output,
        (Libnucnet__Zone__compare_function) nnt::zone_compare_by_first_label
      );

      utility::invalid_reaction_remover irr;
      irr( Libnucnet__getNet( p_output ) );
      utility::zone_copier zc( Libnucnet__getNet( p_output ) );

      for( size_t i = 0; i < outputZones.size(); i++ )
      {
        std::vector<std::string> s_labels;
        for( size_t j = 1; j <= 3; j++ )
        {
          s_labels.push_back(
            Libnucnet__Zone__getLabel( outputZones[i], j )
          );
        }
        Libnucnet__addZone( p_output, zc( outputZones[i], s_labels ) );
      }

      return p_output;
    }

    Libnucnet *
    createOutputNucnet()
    {
      return createOutputNucnet( pOriginal );
    }

  private:
    Libnucnet * pOriginal = NULL;
    std::vector<Libnucnet__Zone *> outputZones;
    std::string sXmlFile;
    std::string sXmlFormat;
    
};

//##############################################################################
// outputter_xml_options()
//##############################################################################

class outputter_xml_options
{

  public:
    outputter_xml_options(){}

    std::string
    getExample()
    {

      return
        "--" + std::string( S_XML_FILE ) + " out.xml ";
    }

    void getOptions( po::options_description& outputter )
    {

      try
      {

        outputter.add_options()

          ( S_XML_FILE, po::value<std::string>()->default_value( "" ),
            "Name of output xml file" )

          ( S_XML_FORMAT, po::value<std::string>()->default_value( "%.15e" ),
            "Format for xml mass fractions" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace base

} // namespace wn_user

#endif // WN_OUTPUTTER_XML_BASE_HPP

