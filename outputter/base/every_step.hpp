////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef WN_OUTPUTTER_EVERY_STEP_BASE_HPP
#define WN_OUTPUTTER_EVERY_STEP_BASE_HPP

#define S_EVERY_STEP      "every_step"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// outputter_every_step().
//##############################################################################

class outputter_every_step
{

  public:
    outputter_every_step( v_map_t& v_map )
    {
      bEveryStep = v_map[S_EVERY_STEP].as<bool>();
    }

    bool
    outputEveryStep()
    { return bEveryStep; }

  private:
    bool bEveryStep; 

};

//##############################################################################
// outputter_every_step_options()
//##############################################################################

class outputter_every_step_options
{

  public:
    outputter_every_step_options() {}

    std::string
    getExample()
    {
      return "";
    }

    void getOptions( po::options_description& outputter )
    {

      try
      {

        outputter.add_options()

        ( S_EVERY_STEP,
          po::value<bool>()->default_value( false, "false" ),
          "Set to write xml file every time dump" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace base

} // namespace wn_user

#endif // WN_OUTPUTTER_EVERY_STEP_BASE_HPP
