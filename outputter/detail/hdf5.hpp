////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "outputter/base/hdf5.hpp"
#include "outputter/base/outputter.hpp"

#ifndef WN_OUTPUTTER_HDF5_HPP
#define WN_OUTPUTTER_HDF5_HPP

#define S_HDF5_STEPS    "hdf5_steps"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// outputter().
//##############################################################################

class outputter : public base::outputter,
                  public base::outputter_hdf5
{

  public:
    outputter( v_map_t& v_map, bool b_zone_data_only=false ) :
      base::outputter( v_map ),
      base::outputter_hdf5( v_map )
    {
      iCount = 0;
      iHdf5Steps = v_map[S_HDF5_STEPS].as<size_t>();
    }

    void set( Libnucnet * p_nucnet )
    {
      base::outputter_hdf5::set( p_nucnet );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      if( !base::outputter_hdf5::getHdf5File().empty() )
      {
        if(
           (
             iCount++ % iHdf5Steps == 0 ||
             zones[0].getProperty<double>( nnt::s_TIME ) >=
               zones[0].getProperty<double>( nnt::s_T_END )
           )
        )
        {
          BOOST_FOREACH( nnt::Zone zone, zones )
          {
            base::outputter_hdf5::updateZone( zone );
          }
          user::hdf5::append_zones(
            base::outputter_hdf5::getHdf5File().c_str(),
            base::outputter_hdf5::getOutputNucnet()
          );
        }
      }
    }

    void operator()( nnt::Zone& zone )
    {
      if( !base::outputter_hdf5::getHdf5File().empty() )
      {
        if(
           (
             iCount++ % iHdf5Steps == 0 ||
             zone.getProperty<double>( nnt::s_TIME ) >=
               zone.getProperty<double>( nnt::s_T_END )
           )
        )
        {
          base::outputter_hdf5::updateZone( zone );
          user::hdf5::append_zones(
            base::outputter_hdf5::getHdf5File().c_str(),
            base::outputter_hdf5::getOutputNucnet()
          );
        }
      }
    }

  private:
    size_t iCount, iHdf5Steps;

};

//##############################################################################
// outputter_options()
//##############################################################################

class outputter_options : public base::outputter_options,
                          public base::outputter_hdf5_options
{

  public:
    outputter_options() : base::outputter_options(),
                          base::outputter_hdf5_options() {}

    std::string
    getExample()
    {
      return base::outputter_options::getExample() +
        base::outputter_hdf5_options::getExample();
    }

    void getOptions( po::options_description& outputter )
    {

      try
      {

        po::options_description hdf5("  Hdf5 options");

        hdf5.add_options()

        ( S_HDF5_STEPS, po::value<size_t>()->default_value( 20, "20" ),
          "Frequency of output to hdf5 file." )

        ;

        base::outputter_hdf5_options::getOptions( hdf5 );

        outputter.add( hdf5 );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_OUTPUTTER_HDF5_HPP
