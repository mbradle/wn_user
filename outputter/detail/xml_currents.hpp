////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef WN_OUTPUTTER_HPP
#define WN_OUTPUTTER_HPP

#define S_XML_FILE           "output_xml"
#define S_XML_CURRENTS_FILE  "output_currents_xml"
#define S_XML_FORMAT         "xml_format"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

typedef std::map<std::string, std::string> map_t;

//##############################################################################
// Helper function.
//##############################################################################

void create_map(
  const char * s_name,
  const char * s_tag1,
  const char * s_tag2,
  const char * s_value,
  map_t * p_map
)
{
  if( !s_name || s_tag2 )
  {
    std::cerr << "Invalid input." << std::endl;
    exit( EXIT_FAILURE );
  }
  (*p_map)[s_tag1] = s_value;
} 

//##############################################################################
// outputter_xml().
//##############################################################################

class outputter
{

  public:
    outputterl(){}
 
    void set( v_map_t& v_map, bool b_zone_data_only = false )
    {
      pOutput = nnt::create_network_copy( p_nucnet );
      pCurrentsOutput = nnt::create_network_copy( p_nucnet );
      sXmlFormat = v_map[S_XML_FORMAT].as<std::string>();
      Libnucnet__setZoneCompareFunction(
        pOutput,
        (Libnucnet__Zone__compare_function) nnt::zone_compare_by_first_label
      );

      if( v_map.count( S_XML_FILE ) )
      {
        sXmlFile = v_map[S_XML_FILE].as<std::string>();
      }

      if( v_map.count( S_XML_CURRENTS_FILE ) )
      {
        sXmlCurrentsFile = v_map[S_XML_CURRENTS_FILE].as<std::string>();
      }

      bZoneDataOnly = b_zone_data_only;

    }

    ~outputter_xml(){
      Libnucnet__free( pOutput );
      Libnucnet__free( pCurrentsOutput );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      BOOST_FOREACH( nnt::Zone zone, zones )
      {
        nnt::write_xml( pOutput, zone.getNucnetZone() );
      }
    }

    void writeXmlToOutput( nnt::Zone& zone )
    {
      Libnucnet__Zone * p_copy =
        Libnucnet__Zone__copy( zone.getNucnetZone() );

      m_currents.clear();
      m_initial_abundances.clear();
      m_final_abundances.clear();

      Libnucnet__Zone__iterateOptionalProperties(
        p_copy,
        nnt::s_FLOW_CURRENT,
        NULL,
        NULL,
        (Libnucnet__Zone__optional_property_iterate_function) create_map,
        &m_currents
      );

      Libnucnet__Zone__iterateOptionalProperties(
        p_copy,
        nnt::s_INITIAL_ABUNDANCE,
        NULL,
        NULL,
        (Libnucnet__Zone__optional_property_iterate_function) create_map,
        &m_initial_abundances
      );

      Libnucnet__Zone__iterateOptionalProperties(
        p_copy,
        nnt::s_FINAL_ABUNDANCE,
        NULL,
        NULL,
        (Libnucnet__Zone__optional_property_iterate_function) create_map,
        &m_final_abundances
      );

      sCurrentsTimeBegin =
        zone.getProperty<std::string>( nnt::s_CURRENTS_T_BEGIN );
      sCurrentsTimeEnd =
        zone.getProperty<std::string>( nnt::s_CURRENTS_T_END );
      
      if( !sXmlCurrentsFile.empty() )
      {
        BOOST_FOREACH( map_t::value_type& t, m_currents )
        {
          Libnucnet__Zone__removeProperty(
            p_copy,
            nnt::s_FLOW_CURRENT,
            t.first.c_str(),
            NULL
          );
        }
        BOOST_FOREACH( map_t::value_type& t, m_initial_abundances )
        {
          Libnucnet__Zone__removeProperty(
            p_copy,
            nnt::s_INITIAL_ABUNDANCE,
            t.first.c_str(),
            NULL
          );
        }
        BOOST_FOREACH( map_t::value_type& t, m_final_abundances )
        {
          Libnucnet__Zone__removeProperty(
            p_copy,
            nnt::s_FINAL_ABUNDANCE,
            t.first.c_str(),
            NULL
          );
        }
        Libnucnet__Zone__removeProperty(
          p_copy,
          nnt::s_CURRENTS_T_BEGIN,
          NULL,
          NULL
        );
        Libnucnet__Zone__removeProperty(
          p_copy,
          nnt::s_CURRENTS_T_END,
          NULL,
          NULL
        );
      }

      if( !sXmlFile.empty() )
      {
        nnt::write_xml( pOutput, p_copy );
      }

      Libnucnet__Zone__free( p_copy );

    }

    void write()
    {
      if( !sXmlFile.empty() )
      {
        Libnucnet__updateZoneXmlMassFractionFormat(
          pOutput,
          sXmlFormat.c_str()
        );

        Libnucnet__writeToXmlFile(
          pOutput,
          sXmlFile.c_str()
        );
      }

      if( !sXmlCurrentsFile.empty() )
      {

        Libnucnet__Zone * p_zone =
          Libnucnet__Zone__new(
            Libnucnet__getNet( pCurrentsOutput ),
            "integrated currents",
            NULL,
            NULL
          );

        BOOST_FOREACH( map_t::value_type& t, m_currents )
        {
          Libnucnet__Zone__updateProperty(
            p_zone,
            nnt::s_FLOW_CURRENT,
            t.first.c_str(),
            NULL,
            t.second.c_str()
          );
        }

        BOOST_FOREACH( map_t::value_type& t, m_initial_abundances )
        {
          Libnucnet__Zone__updateProperty(
            p_zone,
            nnt::s_INITIAL_ABUNDANCE,
            t.first.c_str(),
            NULL,
            t.second.c_str()
          );
        }

        BOOST_FOREACH( map_t::value_type& t, m_final_abundances )
        {
          Libnucnet__Zone__updateProperty(
            p_zone,
            nnt::s_FINAL_ABUNDANCE,
            t.first.c_str(),
            NULL,
            t.second.c_str()
          );
        }

        Libnucnet__Zone__updateProperty(
          p_zone,
          nnt::s_CURRENTS_T_BEGIN,
          NULL,
          NULL,
          sCurrentsTimeBegin.c_str()
        );

        Libnucnet__Zone__updateProperty(
          p_zone,
          nnt::s_CURRENTS_T_END,
          NULL,
          NULL,
          sCurrentsTimeEnd.c_str()
        );

        Libnucnet__addZone( pCurrentsOutput, p_zone );
          
        Libnucnet__updateZoneXmlMassFractionFormat(
          pCurrentsOutput,
          sXmlFormat.c_str()
        );

        Libnucnet__writeToXmlFile(
          pCurrentsOutput,
          sXmlCurrentsFile.c_str()
        );
      }
    }

    std::string getXmlFile() { return sXmlFile; }

  private:
    Libnucnet * pOutput;
    Libnucnet * pCurrentsOutput;
    std::string sXmlFile;
    std::string sXmlCurrentsFile;
    std::string sXmlFormat;
    map_t m_currents, m_initial_abundances, m_final_abundances;
    std::string sCurrentsTimeBegin, sCurrentsTimeEnd;
    bool bZoneDataOnly;   // Placeholder for now.

};

//##############################################################################
// outputter_options()
//##############################################################################

class outputter_options
{

  public:
    outputter_options() {}

    std::string
    getExample()
    {

      return
        "--" + std::string( S_XML_FILE ) + " out.xml";
    }

    void operator()( po::options_description& outputter )
    {

      try
      {

        outputter.add_options()

          ( S_XML_FILE, po::value<std::string>(),
            "Name of output xml file" )

          ( S_XML_FORMAT, po::value<std::string>()->default_value( "%.15e" ),
            "Format for xml mass fractions" )

          ( S_XML_CURRENTS_FILE, po::value<std::string>(),
            "Name of output current xml file" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_OUTPUTTER_HPP
