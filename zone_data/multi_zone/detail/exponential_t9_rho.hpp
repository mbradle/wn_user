////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define zone_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "zone_data/base/zone_neutrino_base.hpp"
#include "zone_data/base/zone_init_x_base.hpp"
#include "user/user_rate_functions.h"
#include "user/multi_zone_utilities.h"

#define S_NUMBER           "number_zones"
#define S_LAMBDA_T9        "lambda_t9"
#define S_LAMBDA_RHO       "lambda_rho"
#define S_INIT_X           "init_mass_frac"

#ifndef WN_ZONE_DATA_DETAIL_HPP
#define WN_ZONE_DATA_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// zone_data().
//##############################################################################

class zone_data :
  public zone_neutrino_base, public zone_init_x_base
{

  public:
    zone_data() : zone_neutrino_base(), zone_init_x_base() {}
    zone_data( v_map_t& v_map, Libnucnet * p_nucnet ) :
      zone_neutrino_base( v_map ), zone_init_x_base( v_map )
    {
      if( !p_nucnet )
      {
        std::cerr << "No nucnet defined for zone data." << std::endl;
        exit( EXIT_FAILURE );
      }
      iZones = v_map[S_NUMBER].as<size_t>();
      dT9_0 = v_map[nnt::s_T9_0].as<double>();
      dRho_0 = v_map[nnt::s_RHO_0].as<double>();
      dLambda_T9 = v_map[S_LAMBDA_T9].as<double>();
      dLambda_Rho = v_map[S_LAMBDA_RHO].as<double>();
      pNucnet = p_nucnet;
    }

    std::vector<nnt::Zone>
    createNewZones()
    {
       
      std::vector<nnt::Zone> zones;

      for( size_t i = 0; i < iZones; i++ )
      {

        nnt::Zone zone;
       
        zone.setNucnetZone(
          Libnucnet__Zone__new(
            Libnucnet__getNet( pNucnet ),
            boost::lexical_cast<std::string>( i ).c_str(),
            "0",
            "0"
          )
        );

        zone.updateProperty(
          nnt::s_T9,
          dT9_0 * exp( -static_cast<double>( i ) / dLambda_T9 )
        );

        zone.updateProperty(
          nnt::s_RHO,
          dRho_0 * exp( -static_cast<double>( i ) / dLambda_Rho )
        );

        setZoneMassFractionsFromInitX( zone );

        zone.updateProperty( nnt::s_MU_NUE_KT, getMuNuekT() );

        Libnucnet__addZone( pNucnet, zone.getNucnetZone() );

        zones.push_back( zone );

      }

      return zones;

    }

    void
    setZoneRateDataUpdateFunction( nnt::Zone& zone )
    {
      user::set_rate_data_update_function( zone );
    }

  private:
    Libnucnet * pNucnet;
    size_t iZones;
    double dT9_0, dRho_0, dLambda_T9, dLambda_Rho;
    std::vector<Libnucnet__Species *> f_sp;
    std::vector<double> f_x;
    
};
    
//##############################################################################
// zone_data_options().
//##############################################################################

class zone_data_options :
  zone_neutrino_base_options, zone_init_x_base_options
{

  public:
    zone_data_options() :
      zone_neutrino_base_options(), zone_init_x_base_options() {}

    std::string
    getDetailExampleString()
    {
      return getInitXBaseExampleString();
    }

    void
    getDetailOptions( po::options_description& zone_data )
    {

      try
      {

        zone_data.add_options()

        // Option for input network data file
        (
          S_NUMBER, po::value<size_t>()->default_value( 100, "100" ),
          "Number of zones"
        )

        // Option for input network data file
        (
          nnt::s_T9_0, po::value<double>()->default_value( 0.3, "0.3" ),
          "T9 of innermost zone"
        )

        // Option for input network data file
        (
          nnt::s_RHO_0, po::value<double>()->default_value( 1.e3, "1.e3" ),
          "Mass density (g/cc) of innermost zone"
        )

        // Option for input network data file
        (
          S_LAMBDA_T9, po::value<double>()->default_value( 10, "10." ),
          "e-folding of t9"
        )

        // Option for input network data file
        (
          S_LAMBDA_RHO, po::value<double>()->default_value( 10, "10." ),
          "e-folding of rho"
        )

        ;

        getNeutrinoBaseOptions( zone_data );
        getInitXBaseOptions( zone_data );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_ZONE_DATA_DETAIL_HPP
