////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define network_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "zone_data/base/zone_xml.hpp"
#include "zone_data/base/zone_neutrino.hpp"
#include "zone_data/base/zone_init_x.hpp"

#include "user/user_rate_functions.h"

#include "nnt/iter.h"

#ifndef WN_ZONE_DATA_DETAIL_HPP
#define WN_ZONE_DATA_DETAIL_HPP

#define S_N_X_ZONES   "number_x_zones"
#define S_N_Y_ZONES   "number_y_zones"
#define S_N_Z_ZONES   "number_z_zones"

#define S_HALO        "halo"
#define S_HALO_ZONE   "halo_zone"
#define S_HOT_ZONES   "hot_zones"

#define S_INITIAL_TOTAL_MASS  "initial_total_mass"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

typedef boost::tuple<std::string, std::string, std::string> zone_tuple_t;

typedef std::vector<zone_tuple_t> zone_tuple_vector_t;

//##############################################################################
// zone_data().
//##############################################################################

class zone_data : public base::zone_xml, public base::zone_neutrino
{

  public:
    zone_data() : base::zone_xml(), base::zone_neutrino(){}
    zone_data( v_map_t& v_map, Libnucnet * p_nucnet ) :
      base::zone_xml( v_map ), base::zone_neutrino( v_map )
    {

      pNucnet = p_nucnet;

      b_halo = v_map[S_HALO_ZONE].as<bool>();

      b_hot = v_map[S_HOT_ZONES].as<bool>();

      d_total_mass = v_map[S_INITIAL_TOTAL_MASS].as<double>();

      n_grid = v_map[S_N_X_ZONES].as<size_t>() *
               v_map[S_N_Y_ZONES].as<size_t>() *
               v_map[S_N_Z_ZONES].as<size_t>();

      for( size_t i = 0; i < v_map[S_N_X_ZONES].as<size_t>(); i++ )
      {
        for( size_t j = 0; j < v_map[S_N_Y_ZONES].as<size_t>(); j++ )
        {
          for( size_t k = 0; k < v_map[S_N_Z_ZONES].as<size_t>(); k++ )
          {
            zone_tuple_vector.push_back
            (
              boost::make_tuple(
                boost::lexical_cast<std::string>( i ).c_str(),
                boost::lexical_cast<std::string>( j ).c_str(),
                boost::lexical_cast<std::string>( k ).c_str()
              )
            );
            if( b_hot )
            {
              zone_tuple_vector.push_back
              (
                boost::make_tuple(
                  boost::lexical_cast<std::string>( i ).c_str(),
                  boost::lexical_cast<std::string>( j ).c_str(),
                  (boost::lexical_cast<std::string>( k ) + "h").c_str()
                )
              );
            }
           }
         }
      }

      if( b_halo )
      {
        zone_tuple_vector.push_back
        (
          boost::make_tuple( S_HALO, "0", "0" )
        );
      }
    }

    nnt::Zone
    createZone(
      zone_tuple_t& t,
      gsl_vector * p_abunds
    )
    {
      nnt::Zone zone;
      Libnucnet__Zone * p_zone =
        Libnucnet__Zone__new(
          Libnucnet__getNet( pNucnet ),
          t.get<0>().c_str(),
          t.get<1>().c_str(),
          t.get<2>().c_str()
        );
      Libnucnet__Zone__updateAbundances( p_zone, p_abunds );
      Libnucnet__addZone( pNucnet, p_zone );
      zone.setNucnetZone( p_zone );
      zone.updateProperty( nnt::s_MU_NUE_KT, getMuNuekT() );
      zone.updateProperty( nnt::s_ZONE_MASS, 0. );
      zone.updateProperty( nnt::s_RHO, 1. );
      zone.updateProperty( nnt::s_T9, 0.1 );
      return zone;
    }

    bool
    isHotZone( Libnucnet__Zone * p_zone )
    {
      std::string s3 = Libnucnet__Zone__getLabel( p_zone, 3 );
      return ( s3.find( "h" ) != std::string::npos );
    }

    bool
    isHaloZone( Libnucnet__Zone * p_zone )
    {
      std::string s1 = Libnucnet__Zone__getLabel( p_zone, 1 );
      return( s1.find( "halo" ) != std::string::npos );
    }

    std::vector<nnt::Zone>
    createNewZones( )
    {
      std::vector<nnt::Zone> zones;

      Libnucnet__assignZoneDataFromXml(
        pNucnet,
        getZoneXmlFile().c_str(),
        getZoneXPath().c_str()
      );

      Libnucnet__Zone * p_zone =
        Libnucnet__getZoneByLabels( pNucnet, "0", "0", "0" );

      gsl_vector * p_abunds = Libnucnet__Zone__getAbundances( p_zone );

      Libnucnet__removeZone( pNucnet, p_zone );
  
      BOOST_FOREACH( zone_tuple_t& t, zone_tuple_vector )
      {
        zones.push_back( createZone( t, p_abunds ) );
      }
    
      gsl_vector_free( p_abunds );
  
      if( b_halo )
      {
        BOOST_FOREACH( nnt::Zone& zone, zones )
        {
          if( isHaloZone( zone.getNucnetZone() ) )
          {
            zone.updateProperty( nnt::s_ZONE_MASS, d_total_mass );
          }
        }
      }
      else
      {
        BOOST_FOREACH( nnt::Zone& zone, zones )
        {
          if( !isHotZone( zone.getNucnetZone() ) )
          {
            zone.updateProperty( nnt::s_ZONE_MASS, d_total_mass / n_grid );
          }
        }
      }
      return zones;

    }

    void
    setZoneRateDataUpdateFunction( nnt::Zone& zone )
    {
      user::set_rate_data_update_function( zone );
    }

    Libnucnet *
    getNucnet()
    { return pNucnet; }

  private:
    Libnucnet * pNucnet;
    bool b_halo, b_hot;
    size_t n_grid;
    double d_total_mass;
    zone_tuple_vector_t zone_tuple_vector;

};
    
//##############################################################################
// zone_data_options().
//##############################################################################

class zone_data_options :
  public base::zone_xml_options, public base::zone_neutrino_options
{

  public:
    zone_data_options() :
      base::zone_xml_options(), base::zone_neutrino_options() {}

    std::string
    getExample()
    {
      return
        base::zone_xml_options::getExample() +
        base::zone_neutrino_options::getExample();
    }

    void
    getOptions( po::options_description& zone_data )
    {

      try
      {

        zone_data.add_options()

        // Option for number of x zones
        (  S_N_X_ZONES, po::value<size_t>()->default_value( 10 ),
           "Number of x zones" )

        // Option for number of y zones
        (  S_N_Y_ZONES, po::value<size_t>()->default_value( 1 ),
           "Number of y zones" )

        // Option for number of z zones
        (  S_N_Z_ZONES, po::value<size_t>()->default_value( 1 ),
           "Number of z zones" )

        // Option for halo zone
        (  S_HALO_ZONE, po::value<bool>()->default_value( true, "true" ),
           "Include halo zone" )

        // Option for hot zones
        (  S_HOT_ZONES, po::value<bool>()->default_value( false, "false" ),
           "Include hot zones" )

        // Option for total mass
        (  S_INITIAL_TOTAL_MASS,
	   po::value<double>()->default_value( 1.e7, "1.e7" ),
           "Initial total mass" )

        ;

        base::zone_xml_options::getOptions( zone_data );
        base::zone_neutrino_options::getOptions( zone_data );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_ZONE_DATA_DETAIL_HPP
