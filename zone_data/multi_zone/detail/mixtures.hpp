////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file mixtures.hpp
//! \brief A file to define zone input routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"
#include "user/multi_zone_utilities.h"
#include "zone_data/base/zone_data_base.hpp"

#ifndef WN_ZONE_DATA_DETAIL_HPP
#define WN_ZONE_DATA_DETAIL_HPP

#define S_F1            "f1"
#define S_F2            "f2"
#define S_MISSING       "missing"
#define S_MIXTURE       "mixture"
#define S_MIXTURE_XML   "mixture_xml"
#define S_RESULTANT     "resultant"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

typedef boost::tuple<std::string, std::string, std::string> zone_tuple_t;
typedef std::map<nnt::Zone, double> f_map_t;

//##############################################################################
// zone_data().
//##############################################################################

class zone_data : public zone_data_base
{

  public:
    zone_data( v_map_t& v_map ) : zone_data_base( v_map ), my_program_options()
    {

      v_s1 =
        my_program_options.composeOptionVectorOfVectors(
          v_map, S_F1, "{}", ":", 2 )
        );

      v_s2 =
        my_program_options.composeOptionVectorOfVectors(
          v_map, S_F2, "{}", ":", 2 )
        );

      v_mixture =
        my_program_options.composeOptionVectorOfStrings(
          v_map, S_MIXTURE, "," );

      while( v_mixture.size() < 3 )
      {
        v_mixture.push_back( "0" );
      }

      if( v_map.count( S_MISSING ) )
      {
        v_missing = v_map[S_MISSING].as<std::string>();
      }

      if( v_map.count( S_RESULTANT ) )
      {
        v_resultant =
          my_program_options.composeOptionVectorOfStrings(
            v_map, S_RESULTANT, ","
          );
        while( v_resultant.size() < 3 )
        { 
          v_resultant.push_back( "0" );
        }
      }

      if( v_map.count( S_MIXTURE_XML ) )
      {
        s_mixture_xml = v_map[S_MIXTURE_XML].as<std::string>();
      }

    }

    std::vector<std::string>
    get_zone_string_vector(
      std::string s,
      const char * s_sep
    )
    {
      std::vector<std::string> v_s, v_r( 3, "0" );
      typedef boost::tokenizer<boost::char_separator<char> > tokenizer;
      boost::char_separator<char> sep( s_sep );
      tokenizer tokens( s, sep );
      std::copy( tokens.begin(), tokens.end(), std::back_inserter( v_s ) );
      for( size_t i = 0; i < v_s.size(); i++ )
      { 
        boost::trim( v_s[i] );
        v_r[i] = v_s[i];
      }
      return v_r;
    }

    f_map_t
    assignFMap( std::vector<std::vector<std::string> >& s_v )
    {
      f_map_t f;
      BOOST_FOREACH( std::vector<std::string> v, s_v )
      {
        nnt::Zone zone;
        std::vector<std::string> v_r = get_zone_string_vector( v[0], "," );
        zone.setNucnetZone(
          Libnucnet__getZoneByLabels(
            getNucnet(), v_r[0].c_str(), v_r[1].c_str(), v_r[2].c_str()
          )
        );
        f[zone] = boost::lexical_cast<double>( v[1] );
      }
      return f;
    }

    void
    setZoneRateDataUpdateFunction( nnt::Zone& zone )
    {
    }

    std::vector<nnt::Zone>
    createNewZones( )
    {
      std::vector<nnt::Zone> zones = createNewZonesBase();
      BOOST_FOREACH( nnt::Zone& zone, zones )
      {
        nnt::normalize_zone_abundances( zone );
      } 
      f1 = assignFMap( v_s1 );
      f2 = assignFMap( v_s2 );
      if( !v_resultant.empty() )
      {
        resultant_zone.setNucnetZone(
          Libnucnet__getZoneByLabels(
            getNucnet(),
            v_resultant[0].c_str(),
            v_resultant[1].c_str(),
            v_resultant[2].c_str()
          )
        );
      }
      return zones;
    }

    void
    addMixtureZone( std::vector<nnt::Zone>& zones )
    {
      Libnucnet__Zone * p_zone =
        Libnucnet__Zone__new(
          Libnucnet__getNet( getNucnet() ),
          v_mixture[0].c_str(), v_mixture[1].c_str(), v_mixture[2].c_str()
        );
      gsl_vector * p_mixture =
        gsl_vector_calloc(
          Libnucnet__Nuc__getNumberOfSpecies(
            Libnucnet__Net__getNuc( Libnucnet__getNet( getNucnet() ) )
          )
        );
      double f_norm = 0;
      BOOST_FOREACH( f_map_t::value_type& t, f1 )
      {
        nnt::Zone zone = t.first;
        f_norm += t.second;
        gsl_vector * p_masses =
          Libnucnet__Zone__getMassFractions( zone.getNucnetZone() );
        gsl_vector_scale( p_masses, t.second );
        gsl_vector_add( p_mixture, p_masses );
        gsl_vector_free( p_masses );
      }
      gsl_vector_scale( p_mixture, 1. / f_norm );
      Libnucnet__Zone__updateMassFractions( p_zone, p_mixture );
      Libnucnet__addZone( getNucnet(), p_zone );
      gsl_vector_free( p_mixture );
    }

    void
    addMissingZone( std::vector<nnt::Zone>& zones )
    {
      if( !v_missing.empty() )
      {
        std::vector<std::string> v_m =
          get_zone_string_vector( (v_missing[0])[0], "," ); 
        Libnucnet__Zone * p_zone =
          Libnucnet__Zone__new(
            Libnucnet__getNet( getNucnet() ),
            v_m[0].c_str(), v_m[1].c_str(), v_m[2].c_str()
          );
        gsl_vector * p_missing =
          Libnucnet__Zone__getMassFractions( resultant_zone.getNucnetZone() );
        BOOST_FOREACH( f_map_t::value_type& t, f1 )
        {
          nnt::Zone zone = t.first;
          gsl_vector * p_masses =
            Libnucnet__Zone__getMassFractions( zone.getNucnetZone() );
          gsl_vector_scale( p_masses, t.second );
          gsl_vector_sub( p_missing, p_masses );
          gsl_vector_free( p_masses );
        }
        gsl_vector_scale(
          p_missing,
          1. / boost::lexical_cast<double>( (v_missing[0])[1] )
        );
        if( !gsl_vector_isnonneg( p_missing ) )
        {
          std::cerr << "Missing component has negative mass fractions:\n\n";
          BOOST_FOREACH(
            nnt::Species species,
            nnt::make_species_list(
              Libnucnet__Net__getNuc(
                Libnucnet__Zone__getNet( zones[0].getNucnetZone() )
              )
            )
          )
          {
            double d_abund =
              gsl_vector_get(
                p_missing,
                Libnucnet__Species__getIndex( species.getNucnetSpecies() )
              );
            if( d_abund < 0 )
            {
              std::cout <<
                Libnucnet__Species__getName( species.getNucnetSpecies() )
                << "  " << d_abund << std::endl;
            }
          }
          exit( EXIT_FAILURE );
        }
        Libnucnet__Zone__updateMassFractions( p_zone, p_missing );
        Libnucnet__addZone( getNucnet(), p_zone );
        gsl_vector_free( p_missing );
      }
    }

    void
    outputMixtureXml()
    {
      if( !s_mixture_xml.empty() )
      {
        Libnucnet * p_output = nnt::create_network_copy( getNucnet() );
        Libnucnet__Zone * p_mixture_zone =
          Libnucnet__getZoneByLabels(
            getNucnet(),
            v_mixture[0].c_str(),
            v_mixture[1].c_str(),
            v_mixture[2].c_str()
          );
        Libnucnet__updateZoneXmlMassFractionFormat( p_output, "%.15e" );
        nnt::write_xml( p_output, p_mixture_zone );
        Libnucnet__relabelZone(
          p_output,
          Libnucnet__getZoneByLabels( p_output, "1", "0", "0" ),
          v_mixture[0].c_str(),
          v_mixture[1].c_str(),
          v_mixture[2].c_str()
        );
        Libnucnet__writeZoneDataToXmlFile( p_output, s_mixture_xml.c_str() );
        Libnucnet__free( p_output );
      }
    }

  private:
    program_options my_program_options;
    nnt::Zone resultant_zone;
    std::vector<std::vector<std::string> > v_s1, v_s2, v_missing;
    std::vector<std::string> v_mixture, v_resultant;
    std::string s_mixture_xml;
    f_map_t f1, f2;

};
    
//##############################################################################
// zone_data_options().
//##############################################################################

class zone_data_options : public zone_data_base_options
{

  public:
    zone_data_options() : zone_data_base_options() {}

    std::string
    getDetailExampleString()
    {
      return getBaseExampleString();
    }

    void
    getDetailOptions( po::options_description& zone_data )
    {

      try
      {

        zone_data.add_options()

        // Option for original mixtures
        (
          S_F1,
          po::value<
            std::vector<std::string>
          >()->multitoken()->composing()->required(),
          "String to select zone contributions (enter as {zone label1, label2, label3: f})"
        )

        // Option for output mixtures
        (
          S_F2,
          po::value<
            std::vector<std::string>
          >()->multitoken()->composing()->required(),
          "String to select zone contributions (enter as {zone label1, label2, label3: f})"
        )

        // Option for missing component
        (
          S_MISSING,
          po::value<
            std::vector<std::string> >()->composing(),
          "String to select missing contributions (enter as {zone label1, label2, label3: f})"
        )

        // Option for mixture component
        (
          S_MIXTURE,
          po::value<std::string>()->required()->default_value( "mixture, 0, 0" ),
          "String to select mixture contributions (enter comma-delimited list of up to 3 labels)"
        )

        // Option for output mixture xml
        (
          S_MIXTURE_XML,
          po::value<std::string>(),
          "String for output mixture xml"
        )

        // Option for resultant component
        (
          S_RESULTANT,
          po::value<std::string>(),
          "String to select resulting contributions (enter comma-delimited list of up to 3 labels)"
        )

        ;

        getBaseOptions( zone_data );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace detail

} // namespace wn_user

#endif // WN_ZONE_DATA_DETAIL_HPP
