////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file zone_data.hpp
//! \brief A file to define zone_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#ifndef WN_ZONE_DATA_HPP
#define WN_ZONE_DATA_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// zone_data().
//##############################################################################

class zone_data : public detail::zone_data
{

  public: zone_data( v_map_t& v_map, Libnucnet * p_nucnet ) :
    detail::zone_data( v_map, p_nucnet ){}

    nnt::Zone
    createZone( )
    {

      nnt::Zone zone = createNewZone();

      //========================================================================
      // Set zone data.
      //========================================================================

      setZoneRateDataUpdateFunction( zone );

      return zone;

    }

  private:
    std::vector<std::vector<std::string> > v_init_x;

};
    
//##############################################################################
// zone_data_options().
//##############################################################################

class zone_data_options : detail::zone_data_options
{

  public:
    zone_data_options() : detail::zone_data_options() {}

    std::string
    getExample()
    {

      return
        detail::zone_data_options::getExample();

    }

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description zone_data( "\nZone data Options" );

        detail::zone_data_options::getOptions( zone_data );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "zone_data",
            options_struct( zone_data, getExample() )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace wn_user

#endif // WN_ZONE_DATA_HPP
