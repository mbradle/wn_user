////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define zone_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "zone_data/base/zone_xml.hpp"
#include "zone_data/base/zone_neutrino.hpp"
#include "zone_data/base/zone_network.hpp"
#include "zone_data/base/zone_init_x.hpp"
#include "zone_data/base/zone_normalize_abundances.hpp"
#include "user/user_rate_functions.h"

#ifndef WN_ZONE_DATA_DETAIL_HPP
#define WN_ZONE_DATA_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// zone_data().
//##############################################################################

class zone_data : public base::zone_xml, public base::zone_init_x,
                         public base::zone_neutrino,
                         public base::zone_normalize_abundances,
                         public base::zone_network
{

  public:
    zone_data( v_map_t& v_map, Libnucnet * p_nucnet ) :
      base::zone_xml( v_map ), base::zone_init_x( v_map ),
      base::zone_neutrino( v_map ), base::zone_normalize_abundances( v_map ),
      base::zone_network( v_map )
    {
      pNucnet = p_nucnet;
    }

    nnt::Zone
    createNewZone()
    {

      nnt::Zone zone;

      if( !getZoneXmlFile().empty() )
      {

        Libnucnet__assignZoneDataFromXml(
          pNucnet,
          getZoneXmlFile().c_str(),
          getZoneXPath().c_str()
        );

        nnt::zone_list_t zone_list = nnt::make_zone_list( pNucnet );

        if( zone_list.size() != 1 )
        {
          std::cerr << "Incorrect number of zones in input." << std::endl;
          exit( EXIT_FAILURE );
        }

        zone = *(zone_list.begin());

      }
      else
      {
        zone.setNucnetZone(
          Libnucnet__Zone__new( Libnucnet__getNet( pNucnet ), "0", "0", "0" )
        );

        Libnucnet__addZone( pNucnet, zone.getNucnetZone() );
      }

      setZoneMassFractionsFromInitX( zone );

      normalizeAbundances( zone );

      zone.updateProperty( nnt::s_MU_NUE_KT, getMuNuekT() );

      setEvolutionNetwork( zone );

      return zone;
    }

    void
    setZoneRateDataUpdateFunction( nnt::Zone& zone )
    {
      user::set_rate_data_update_function( zone );
    }

  private:
    Libnucnet * pNucnet;

};
    
//##############################################################################
// zone_data_options().
//##############################################################################

class zone_data_options : public base::zone_xml_options, 
                          public base::zone_init_x_options,
                          public base::zone_neutrino_options,
                          public base::zone_normalize_abundances_options,
                          public base::zone_network_options
{

  public:
    zone_data_options() : base::zone_xml_options(),
                          base::zone_init_x_options(),
                          base::zone_neutrino_options(),
                          base::zone_normalize_abundances_options(),
                          base::zone_network_options(){}

    std::string
    getExample()
    {
      return
        base::zone_xml_options::getExample() + 
        base::zone_init_x_options::getExample() +
        base::zone_normalize_abundances_options::getExample() +
        base::zone_neutrino_options::getExample();
    }

    void
    getOptions( po::options_description& zone_data )
    {

      try
      {

        base::zone_xml_options::getOptions( zone_data );
        base::zone_init_x_options::getOptions( zone_data );
        base::zone_neutrino_options::getOptions( zone_data );
        base::zone_normalize_abundances_options::getOptions( zone_data );
        base::zone_network_options::getOptions( zone_data );


// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_ZONE_DATA_DETAIL_HPP

