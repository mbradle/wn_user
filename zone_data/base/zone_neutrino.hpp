////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file zone_neutrino.hpp
//! \brief A file to define zone input routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef WN_ZONE_NEUTRINO_BASE_HPP
#define WN_ZONE_NEUTRINO_BASE_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// zone_neutrino().
//##############################################################################

class zone_neutrino
{

  public:
    zone_neutrino(){}
    zone_neutrino( v_map_t& v_map )
    {
      s_mu_nue_kT = v_map[nnt::s_MU_NUE_KT].as<std::string>();
    }

  std::string
  getMuNuekT()
  {
    return s_mu_nue_kT;
  }

  private:
    std::string s_mu_nue_kT;

};
    
//##############################################################################
// zone_neutrino_options().
//##############################################################################

class zone_neutrino_options
{

  public:
    zone_neutrino_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    getOptions( po::options_description& zone_data )
    {

      try
      {

        zone_data.add_options()

        // Option for value of neutrino chemical potential
        (  nnt::s_MU_NUE_KT, po::value<std::string>()->default_value( "-inf" ),
           "Electron neutrino chemical potential / kT" )
       
        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // WN_ZONE_NEUTRINO_BASE_HPP
