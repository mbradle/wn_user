////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file zone_data_base.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"
#include "utility/zone_abundance_normalizer.hpp"

#ifndef WN_ZONE_ABUNDANCE_NORMALIZER_BASE_HPP
#define WN_ZONE_ABUNDANCE_NORMALIZER_BASE_HPP

#define S_NORMALIZE_ABUNDANCES   "norm_abunds"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// zone_normalize_abundances().
//##############################################################################

class zone_normalize_abundances
{

  public:
    zone_normalize_abundances( v_map_t& v_map )
    {
      b_normalize = v_map[S_NORMALIZE_ABUNDANCES].as<bool>();
    }

    void
    normalizeAbundances( nnt::Zone& zone )
    {
      if( b_normalize )
      {
        utility::zone_abundance_normalizer zm;
        zm( zone );
      }
    }

  private:
    bool b_normalize;

};
    
//##############################################################################
// zone_normalize_abundances_options().
//##############################################################################

class zone_normalize_abundances_options
{

  public:
    zone_normalize_abundances_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    getOptions( po::options_description& zone_data )
    {

      try
      {

        zone_data.add_options()

        // Option to normalize zone abundances.
        (
          S_NORMALIZE_ABUNDANCES,
          po::value<bool>()->default_value( false, "false" ),
          "Normalize input zone abundances"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // WN_ZONE_ABUNDANCE_NORMALIZER_BASE_HPP
