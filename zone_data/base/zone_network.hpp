////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file zone_network.hpp
//! \brief A file to define zone_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#ifndef WN_ZONE_NETWORK_BASE_HPP
#define WN_ZONE_NETWORK_BASE_HPP

#define S_EVOLUTION_NETWORK_NUC_XPATH   "evolution_network_nuc_xpath" 
#define S_EVOLUTION_NETWORK_REAC_XPATH  "evolution_network_reac_xpath" 

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// zone_network().
//##############################################################################

class zone_network
{

  public:
    zone_network(){}
    zone_network( v_map_t& v_map )
    {
      s_base_evolution_nuc_xpath =
        v_map[S_EVOLUTION_NETWORK_NUC_XPATH].as<std::string>();
      s_base_evolution_reac_xpath =
        v_map[S_EVOLUTION_NETWORK_REAC_XPATH].as<std::string>();
    }

    void
    setEvolutionNetwork( nnt::Zone& zone )
    {
      zone.updateProperty(
        nnt::s_BASE_EVOLUTION_NUC_XPATH,
        s_base_evolution_nuc_xpath
      );
      zone.updateProperty(
        nnt::s_BASE_EVOLUTION_REAC_XPATH,
        s_base_evolution_reac_xpath
      );
    }

    void
    updateEvolutionNetwork(
      nnt::Zone& zone,
      std::string& s_nuc_xpath,
      std::string& s_reac_xpath
    )
    {
      zone.updateProperty( nnt::s_BASE_EVOLUTION_NUC_XPATH, s_nuc_xpath );
      zone.updateProperty( nnt::s_BASE_EVOLUTION_REAC_XPATH, s_reac_xpath );
    }

  private:
    std::string s_base_evolution_nuc_xpath, s_base_evolution_reac_xpath;
    
};
    
//##############################################################################
// zone_network_options().
//##############################################################################

class zone_network_options
{

  public:
    zone_network_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    getOptions( po::options_description& zone_data )
    {

      try
      {

        zone_data.add_options()

        // Option for value of base evolution nuclear xpath
        (
          S_EVOLUTION_NETWORK_NUC_XPATH,
          po::value<std::string>()->default_value( "" ),
          "Nuclear XPath for evolution network (default: all nuclides)"
        )

        // Option for value of base evolution reaction xpath
        (
          S_EVOLUTION_NETWORK_REAC_XPATH,
          po::value<std::string>()->default_value( "" ),
          "Reaction XPath for evolution network (default: all reaction)"
        )

        ;

// Add checks on input.

      }

      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace base

} // namespace wn_user

#endif // WN_ZONE_NETWORK_BASE_HPP
