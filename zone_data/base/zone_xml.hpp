////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file zone_data_base.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef WN_ZONE_XML_BASE_HPP
#define WN_ZONE_XML_BASE_HPP

#define S_ZONE_FILE     "zone_xml"
#define S_ZONE_XPATH    "zone_xpath"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// zone_xml().
//##############################################################################

class zone_xml
{

  public:
    zone_xml(){}
    zone_xml( v_map_t& v_map )
    {
      if( v_map.count( S_ZONE_FILE ) == 1 )
      {
        s_zone_file = v_map[S_ZONE_FILE].as<std::string>();
        s_zone_xpath = v_map[S_ZONE_XPATH].as<std::string>();
      }
    }

    void
    assignDataFromXml( Libnucnet * p_nucnet )
    {
      if( !s_zone_file.empty() )
      {
        Libnucnet__assignZoneDataFromXml(  
          p_nucnet,
          s_zone_file.c_str(),
          s_zone_xpath.c_str()
        );
      }
    }

    std::string getZoneXmlFile() { return s_zone_file; }
    
    std::string getZoneXPath() { return s_zone_xpath; }
    
  private:
    std::string s_zone_file, s_zone_xpath;

};
    
//##############################################################################
// zone_xml_options().
//##############################################################################

class zone_xml_options
{

  public:
    zone_xml_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    getOptions( po::options_description& zone_data )
    {

      try
      {

        zone_data.add_options()

        // Option for value of XPath to select zones
        (
          S_ZONE_FILE, po::value<std::string>(),
          "Input zone xml file"
        )

        // Option for value of XPath to select zones
        (
          S_ZONE_XPATH,
          po::value<std::string>()->default_value( "" ),
          "XPath to select zones (default: all zones)"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // WN_ZONE_XML_BASE_HPP
