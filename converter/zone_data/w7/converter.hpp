////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file converter.hpp
//! \brief A file to output w7 data to xml.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#include <fstream>
#include <string>
#include <map>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <Libnucnet.h>

#ifndef NNP_CONVERTER_HPP
#define NNP_CONVERTER_HPP

#define S_NETWORK_FILE    "network_xml"
#define S_TEXT_FILE       "input_text"
#define S_XML_FILE        "output_xml"
#define S_XML_FORMAT      "xml_format"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// converter().
//##############################################################################

class converter
{

  public:
    converter( v_map_t& v_map )
    {
      pNucnet = Libnucnet__new();
      Libnucnet__Net__updateFromXml(
        Libnucnet__getNet( pNucnet ),
        v_map[S_NETWORK_FILE].as<std::string>().c_str(),
        "",
        NULL
      );

      sXmlFile = v_map[S_XML_FILE].as<std::string>();
      sTextFile = v_map[S_TEXT_FILE].as<std::string>();
      sXmlFormat = v_map[S_XML_FORMAT].as<std::string>();
    }

    ~converter(){
      Libnucnet__free( pNucnet );
    }

    void operator()()
    {
      typedef std::map<std::string, double> my_map_t;
      std::map<std::string, std::string> replacements;
      std::string line, str, str1, str2;
      my_map_t m_map;

      std::ifstream file( sTextFile );

      replacements["p"] = "h1";
      replacements["d"] = "h2";
      replacements["t"] = "h3";

      for( std::string line; getline( file, line ); )
      {
        for( size_t i = 0; i < 5; i++ )
        {
          str = line.substr( 2 + i*14, 4 );
          boost::algorithm::trim( str );
          if( str.empty() ) { break; }
          str1 = boost::algorithm::to_lower_copy( str );
          if( replacements.find( str1 ) != replacements.end() )
          {
            str1 = replacements[str1];
          }
          str2 = line.substr( 6 + i*14, 10 );
          boost::algorithm::trim( str2 );
          m_map[str1] = boost::lexical_cast<double>( str2 );
        }
      }

      Libnucnet__Zone * p_zone =
        Libnucnet__Zone__new(
          Libnucnet__getNet( pNucnet ),
          "white dwarf",
          "0",
          "0"
        );

      Libnucnet__addZone( pNucnet, p_zone );

      BOOST_FOREACH( const my_map_t::value_type& t, m_map )
      {
        Libnucnet__Species * p_species =
          Libnucnet__Nuc__getSpeciesByName(
            Libnucnet__Net__getNuc( Libnucnet__getNet( pNucnet ) ),
            t.first.c_str()
          );
        Libnucnet__Zone__updateSpeciesMassFraction(
          p_zone, p_species, t.second
        );
      }

      Libnucnet__Zone__updateProperty(
        p_zone, "remnant mass", NULL, NULL, "0"
      );

    }

    void write( )
    {
      Libnucnet__updateZoneXmlMassFractionFormat(
        pNucnet,
        sXmlFormat.c_str()
      );

      Libnucnet__writeZoneDataToXmlFile(
        pNucnet,
        sXmlFile.c_str()
      );
    }

  private:
    Libnucnet * pNucnet;
    std::string sTextFile;
    std::string sXmlFile;
    std::string sXmlFormat;

};

//##############################################################################
// converter_options()
//##############################################################################

class converter_options
{

  public:
    converter_options(){}

    std::string
    getExample()
    {

      return
        "--" + std::string( S_NETWORK_FILE ) + 
               " ../nucnet-tools-code/data_pub/my_net.xml " +
        "--" + std::string( S_TEXT_FILE ) + " w7.txt " +
        "--" + std::string( S_XML_FILE ) + " out.xml ";
    }

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description converter( "\nConverter Options" );
        converter.add_options()

          ( S_XML_FILE, po::value<std::string>()->required(),
            "Name of output xml file" )

          ( S_XML_FORMAT, po::value<std::string>()->default_value( "%.15e" ),
            "Format for xml mass fractions" )

          ( S_TEXT_FILE, po::value<std::string>()->required(),
            "Name of text file with data" )

          ( S_NETWORK_FILE, po::value<std::string>()->required(),
            "Name of file with network data" )

        ;

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "converter",
            options_struct( converter, getExample() )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace wn_user

#endif // NNP_CONVERTER_HPP

