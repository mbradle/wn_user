////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define libnucnet_data routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "libnucnet_data/base/libnucnet_data.hpp"
#include "zone_data/base/zone_neutrino.hpp"

#ifndef NNP_LIBNUCNET_DATA_DETAIL_HPP
#define NNP_LIBNUCNET_DATA_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

typedef std::vector<std::pair<std::string, std::string> > v_views_t;

//##############################################################################
// libnucnet_data().
//##############################################################################

class libnucnet_data :
  public base::libnucnet_data, public base::zone_neutrino
{

  public:
    libnucnet_data( v_map_t& v_map ) :
      base::libnucnet_data( v_map ), base::zone_neutrino( v_map ) {}

    void
    setViewsInZones( std::vector<nnt::Zone>& zones, v_views_t& v_views )
    {
      BOOST_FOREACH( v_views_t::value_type& t, v_views )
      {
        zones[0].getNetView( t.first.c_str(), t.second.c_str() );
      }

#ifndef NO_OPENMP
      #pragma omp parallel for schedule( dynamic, 1 )
#endif
      for( size_t i = 1; i < zones.size(); i++ )
      {
        Libnucnet__Zone__copy_net_views(
          zones[i].getNucnetZone(),
          zones[0].getNucnetZone()
        );
      }
    }

    void
    setViewsInZones( std::vector<nnt::Zone>& zones )
    {
      v_views_t v_views;
      v_views.push_back( std::make_pair( "", "" ) );
      setViewsInZones( zones, v_views );
    }
      
    std::vector<nnt::Zone>
    getVectorOfZones( )
    {
      std::vector<nnt::Zone> zones;

      nnt::zone_list_t zone_list = nnt::make_zone_list( getNucnet() );

      if( zone_list.size() == 0 )
      {
        std::cerr << "No zones in input file.\n";
        exit( EXIT_FAILURE );
      }

      BOOST_FOREACH( nnt::Zone zone, zone_list )
      {
        zones.push_back( zone );
        zone.updateProperty( nnt::s_MU_NUE_KT, getMuNuekT() );
      }

      setViewsInZones( zones );

      return zones;
    }

    std::vector<nnt::Zone>
    getZonesFromVectorByXPath( std::vector<nnt::Zone>& zones, std::string s )
    {
      Libnucnet * p_nucnet =
        Libnucnet__new_from_xml(
          getDataFile().c_str(),
          getNucXPath().c_str(),
          "",
          s.c_str()
        );
      nnt::zone_list_t zone_list = nnt::make_zone_list( p_nucnet );
      std::vector<nnt::Zone> zone_vector;

      BOOST_FOREACH( nnt::Zone& zone, zone_list )
      {
        std::vector<nnt::Zone>::iterator it =
          std::find( zones.begin(), zones.end(), zone );
        if( it != zones.end() )
        {
          zone_vector.push_back( *it );
        }
        else
        {
          std::cerr << "Zone not found." << std::endl;
          exit( EXIT_FAILURE );
        }
      }

      Libnucnet__free( p_nucnet );
      return zone_vector;
    }
          
};
    
//##############################################################################
// libnucnet_data_options().
//##############################################################################

class libnucnet_data_options :
  public base::libnucnet_data_options, public base::zone_neutrino_options
{

  public:
    libnucnet_data_options() :
      base::libnucnet_data_options(), base::zone_neutrino_options() {}

    std::string
    getExample()
    {
      return
        base::libnucnet_data_options::getExample() +
          base::zone_neutrino_options::getExample();

    }

    void
    getOptions( po::options_description& libnucnet_data )
    {

      try
      {

        base::libnucnet_data_options::getOptions( libnucnet_data );
        base::zone_neutrino_options::getOptions( libnucnet_data );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_LIBNUCNET_DATA_DETAIL_HPP
