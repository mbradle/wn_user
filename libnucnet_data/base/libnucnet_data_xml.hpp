////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"
#include "nnt/string_defs.h"

#ifndef NNP_LIBNUCNET_DATA_XML_BASE_HPP
#define NNP_LIBNUCNET_DATA_XML_BASE_HPP

#define S_DATA_FILE      "libnucnet_xml"
#define S_EXTRA_REAC_XML "extra_reac_xml"
#define S_VALIDATE       "validate"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// libnucnet_data_xml().
//##############################################################################

class libnucnet_data_xml
{

  public:
    libnucnet_data_xml( v_map_t& v_map ) : my_program_options()
    {
      sDataFile = v_map[S_DATA_FILE].as<std::string>();
      if( v_map.count( S_EXTRA_REAC_XML ) )
      {
        v_extra_reac =
          my_program_options.getVectorOfStrings( v_map, S_EXTRA_REAC_XML );
      }
      if( v_map[S_VALIDATE].as<bool>() )
      {
        if( !Libnucnet__is_valid_input_xml( sDataFile.c_str() ) )
        {
          std::cerr << sDataFile << " is not a valid input network xml file."
                    << std::endl;
          exit( EXIT_FAILURE );
        }
        BOOST_FOREACH( std::string s, v_extra_reac )
        {
          if( !Libnucnet__Reac__is_valid_input_xml( s.c_str() ) )
          {
            std::cerr << s << " is not a valid input reaction data xml file."
                    << std::endl;
            exit( EXIT_FAILURE );
          }
        }
      }
    }

    ~libnucnet_data_xml(){ Libnucnet__free( pNucnet ); }

    void
    setNucnet( Libnucnet * p_nucnet )
    {
       pNucnet = p_nucnet;
    }

    Libnucnet * getNucnet(){ return pNucnet; }

    Libnucnet__Net *
    getNetwork()
    { return Libnucnet__getNet( pNucnet ); }

    std::string getDataFile(){ return sDataFile; }

    std::vector<std::string> getExtraReac(){ return v_extra_reac; }

  private:
    Libnucnet * pNucnet;
    program_options my_program_options;
    std::string sDataFile;
    std::vector<std::string> v_extra_reac;

};
    
//##############################################################################
// libnucnet_data_xml_options().
//##############################################################################

class libnucnet_data_xml_options
{

  public:
    libnucnet_data_xml_options(){}

    std::string
    getExample()
    {

      return
        "--" + std::string( S_DATA_FILE ) + " libnucnet_input.xml ";

    }

    void
    getOptions( po::options_description& libnucnet_data )
    {

      try
      {

        libnucnet_data.add_options()

        // Option for input libnucnet data file
        (
          S_DATA_FILE, po::value<std::string>()->required(),
          "Input libnucnet data xml file"
        )

        // Option for extra reaction data
        (
          S_EXTRA_REAC_XML,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          "Extra input reaction data xml file(s)"
        )
    
        // Option to validate xml
        (
          S_VALIDATE,
          po::value<bool>()->default_value( false, "false" ),
          "Validate the XML"
        )
    
        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // NNP_LIBNUCNET_DATA_XML_BASE_HPP
