////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file outputter.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "libnucnet_data/base/libnucnet_data_xml.hpp"

#ifndef NNP_LIBNUCNET_DATA_TEXT_READER_BASE_HPP
#define NNP_LIBNUCNET_DATA_TEXT_READER_BASE_HPP

#define S_REAC_XPATH_TEXT     "reac_xpath_text"
#define S_ZONE_XPATH_TEXT     "zone_xpath_text"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// libnucnet_data().
//##############################################################################

class libnucnet_data_text_reader : public libnucnet_data_xml
{

  public:
    libnucnet_data_text_reader( v_map_t& v_map ) :
      libnucnet_data_xml( v_map )
    {
      sReacXPath = v_map[S_REAC_XPATH_TEXT].as<std::string>();
      sZoneXPath = v_map[S_ZONE_XPATH_TEXT].as<std::string>();

      setNucnet(
        Libnucnet__new_from_xml_text_reader(
          getDataFile().c_str(),
          ".",
          sReacXPath.c_str(),
          sZoneXPath.c_str()
        )
      );

      BOOST_FOREACH( std::string s, getExtraReac() )
      {
        Libnucnet__Reac__updateFromXmlTextReader(
          Libnucnet__Net__getReac(
            getNetwork()
          ),
          s.c_str(),
          sReacXPath.c_str()
        );
      }
    }

    std::string getNucXPath(){ return "."; }

    std::string getReacXPath(){ return sReacXPath; }

    std::string getZoneXPath(){ return sZoneXPath; }

  private:
    std::string sReacXPath, sZoneXPath;

};
    
//##############################################################################
// libnucnet_data_text_reader_options().
//##############################################################################

class libnucnet_data_text_reader_options :
  public libnucnet_data_xml_options
{

  public:
    libnucnet_data_text_reader_options() :
      libnucnet_data_xml_options() {}

    std::string
    getExample()
    {
      return
        libnucnet_data_xml_options::getExample() +
        "--" + S_REAC_XPATH_TEXT + " \".//reactant[. = \'h1\']\" ";
    }

    void
    getOptions( po::options_description& libnucnet_data )
    {

      try
      {

        libnucnet_data_xml_options::getOptions( libnucnet_data );

        libnucnet_data.add_options()

        // Option for value of XPath to select reactions
        (
          S_REAC_XPATH_TEXT,
          po::value<std::string>()->default_value( "." ),
          "XPath to select reactions (default: all reactions)"
        )
    
        // Option for value of XPath to select zones
        (
          S_ZONE_XPATH_TEXT,
          po::value<std::string>()->default_value( "." ),
          "XPath to select zones (default: all zones)"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // NNP_LIBNUCNET_DATA_TEXT_READER_BASE_HPP
