////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define network time routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#ifndef WN_NETWORK_TIME_BASE_HPP
#define WN_NETWORK_TIME_BASE_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// network_time().
//##############################################################################

class network_time
{

  public:
    network_time(){}
    network_time( v_map_t& v_map )
    {
      d_dt = v_map[nnt::s_DTIME].as<double>();
      d_t = v_map[nnt::s_TIME].as<double>();
      d_t_end = v_map[nnt::s_T_END].as<double>();
    }

    bool isLessThanEndTime( nnt::Zone& zone )
    {
      return
        zone.getProperty<double>( nnt::s_TIME ) < d_t_end;
    }

    bool isLessThanEndTime( std::vector<nnt::Zone>& zones )
    {
      return
        zones[0].getProperty<double>( nnt::s_TIME ) < d_t_end;
    }

    void initializeTimes( nnt::Zone& zone )
    {
      initializeZoneTimes( zone );
    }

    void initializeTimes( std::vector<nnt::Zone>& zones )
    {
#ifndef NO_OPENMP
      #pragma omp parallel for schedule( dynamic, 1 )
#endif
      for( size_t i = 0; i < zones.size(); i++ )
      {
        initializeZoneTimes( zones[i] );
      }
    }

  private:
    double d_dt, d_t, d_t_end;

    void initializeZoneTimes( nnt::Zone& zone )
    {
      zone.updateProperty( nnt::s_DTIME, d_dt );
      zone.updateProperty( nnt::s_TIME, d_t );
      zone.updateProperty( nnt::s_T_END, d_t_end );
    }

};

//##############################################################################
// network_time_options().
//##############################################################################

class network_time_options
{

  public:
    network_time_options(){}

    void
    getOptions( po::options_description& network_time )
    {

      try
      {
    
        network_time.add_options()

        // Option for value of initial time
        (  nnt::s_TIME, po::value<double>()->default_value( 0., "0." ),
           "Initial time (seconds)" )
    
        // Option for value of initial time step
        (  nnt::s_DTIME, po::value<double>()->default_value( 1.e-15, "1.e-15" ),
           "Initial time step (seconds)" )
    
        // Option for value of duration of calculation
        (  nnt::s_T_END, po::value<double>()->default_value( 1.e6, "1.e6" ),
           "Duration of calculation (seconds)" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

}  // namespace base

}  // namespace wn_user

#endif  // WN_NETWORK_TIME_BASE_HPP
