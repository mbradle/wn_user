////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define network time routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "network_time/base/network_time.hpp"

#ifndef WN_NETWORK_TIME_DETAIL_HPP
#define WN_NETWORK_TIME_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// network_time().
//##############################################################################

class network_time : public base::network_time
{

  public:
    network_time() : base::network_time(){}
    network_time( v_map_t& v_map ) : base::network_time( v_map ){}

};

//##############################################################################
// network_time_options().
//##############################################################################

class network_time_options : public base::network_time_options
{

  public:
    network_time_options() : base::network_time_options(){}

    void
    getOptions( po::options_description& network_time )
    {

      try{

        base::network_time_options::getOptions( network_time );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

}  // namespace detail

}  // namespace wn_user

#endif  // WN_NETWORK_TIME_DETAIL_HPP
