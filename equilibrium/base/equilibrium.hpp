////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \brief A file to define equilibrium routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#ifndef WN_EQUILIBRIUM_BASE_HPP
#define WN_EQUILIBRIUM_BASE_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// equilibrium().
//##############################################################################

class equilibrium
{

  public:
    equilibrium() {}

    ~equilibrium(){ Libnuceq__free( pEquil ); }

    void
    set( Libnucnet__Nuc * p_nuc )
    {
      if( pEquil )
      {
        Libnuceq__free( pEquil );
      }
      pEquil = Libnuceq__new( p_nuc );
    }

    void
    setYe( double d_ye )
    {
      Libnuceq__setYe( pEquil, d_ye );
    }

    void
    updateCluster( std::string s_xpath, double d_y )
    {
      Libnuceq__Cluster * p_cluster =
        Libnuceq__getCluster( pEquil, s_xpath.c_str() );
      if( !p_cluster )
      {
        Libnuceq__newCluster( pEquil, s_xpath.c_str() );
      }
      Libnuceq__Cluster__updateConstraint( p_cluster, d_y );
    }

    void
    compute( double d_t9, double d_rho )
    {
      Libnuceq__computeEquilibrium( pEquil, d_t9, d_rho );
    }

  private:
    Libnuceq * pEquil = NULL;

};
     
//##############################################################################
// equilibrium_options().
//##############################################################################

class equilibrium_options
{

  public:
    equilibrium_options(){}

    void
    get( po::options_description& equilibrium ) {}

};

} // namespace base

} // namespace wn_user 

#endif // WN_EQUILIBRIUM_DETAIL_HPP
