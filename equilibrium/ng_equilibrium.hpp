////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/auxiliary.h"
#include "nnt/math.h"
#include "math/bracketer_by_expansion.hpp"

#ifndef WN_NG_EQUILIBRIUM_HPP
#define WN_NG_EQUILIBRIUM_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

typedef std::map<std::string, double> ng_map_t;

//##############################################################################
// z_root().
//##############################################################################

class z_root
{
  public:
    z_root(
      double _dMunkT, size_t _iZ, double _yz,
      std::vector<Libnucnet__Species *>& _v, ng_map_t&_F
    ) : dMunkT( _dMunkT ), iZ( _iZ ), yz( _yz ), v( _v ), F( _F ){}

    double
    operator()( double x )
    {
      double d_result = yz;
      BOOST_FOREACH( Libnucnet__Species * p_species, v )
      {
        double ep =
          x
          +
          Libnucnet__Species__getA( p_species ) * dMunkT
          +
          F[Libnucnet__Species__getName( p_species )];
        if( ep > 300. ) { ep = 300.; }
        d_result -= exp( ep );
      }
      return d_result;
    }

    std::pair<double, double>
    guess_root( )
    {
      std::pair<double, double> p;
      p.first = -1;
      p.second = 1;
      while( (*this)( p.first ) * (*this)( p.second ) > 0 )
      {
        p.first *= 10;
        p.second *= 10;
      }
      return p;
    }

  private:
    double dMunkT;
    size_t iZ;
    double yz;
    std::vector<Libnucnet__Species *>& v;
    ng_map_t& F;

};

//##############################################################################
// ng_equilibrium().
//##############################################################################

class ng_equilibrium
{

  public:
    ng_equilibrium( nnt::Zone& zone )
    {
      gsl_vector * p_summed =
        Libnucnet__Zone__getSummedAbundances( zone.getNucnetZone(), "z" );
      for( size_t i = 0; i < p_summed->size; i++ )
      {
        yz.push_back( gsl_vector_get( p_summed, i ) );
        lambda.push_back( 0. );
      }
      gsl_vector_free( p_summed );

      vz_species.resize( yz.size() );

      pNuc =
        Libnucnet__Net__getNuc(
          Libnucnet__Zone__getNet( zone.getNucnetZone() )
        );
      double delta_n =
        Libnucnet__Species__getMassExcess(
          Libnucnet__Nuc__getSpeciesByName( pNuc, "n" )
        );
      BOOST_FOREACH( nnt::Species species, nnt::make_species_list( pNuc ) )
      {
        Libnucnet__Species * p_species = species.getNucnetSpecies();
        F[Libnucnet__Species__getName( p_species )] =
          log(
            Libnucnet__Species__computeQuantumAbundance(
              p_species,
              zone.getProperty<double>( nnt::s_T9 ),
              zone.getProperty<double>( nnt::s_RHO )
            ) 
          )
          +
          (
            Libnucnet__Species__getA( p_species ) * delta_n
            -
            Libnucnet__Species__getMassExcess( p_species )
          )
          /
          nnt::compute_kT_in_MeV( zone.getProperty<double>( nnt::s_T9 ) );
        vz_species[Libnucnet__Species__getZ( p_species )].push_back( p_species );
      }
    }

    double
    computeNeutronChemicalPotentialKT( nnt::Zone& zone )
    {

      double d_result;

      Libnucnet__Species * p_species =
        Libnucnet__Nuc__getSpeciesByName(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          ),
          "n"
        );

      Libstatmech__Fermion * p_neutron =
        Libstatmech__Fermion__new(
          "n",
          WN_AMU_TO_MEV * Libnucnet__Species__getA( p_species ) +
            Libnucnet__Species__getMassExcess( p_species ),
          2.,
          0.
        );

      double n_n =
        GSL_CONST_NUM_AVOGADRO *
        zone.getProperty<double>( nnt::s_RHO ) *
        Libnucnet__Zone__getSpeciesAbundance( zone.getNucnetZone(), p_species );

      d_result =
        Libstatmech__Fermion__computeChemicalPotential(
          p_neutron,
          zone.getProperty<double>( nnt::s_T9 ) * GSL_CONST_NUM_GIGA,
          n_n,
          NULL,
          NULL
        );

      Libstatmech__Fermion__free( p_neutron );

      return d_result;

    }
          
    double
    operator()( double x )
    {
      double d_result = 1.;
      for( size_t i_z = 1; i_z < yz.size(); i_z++ )
      {
        if( yz[i_z] == 0 )
        {
          lambda[i_z] = GSL_NEGINF;
        }
        else
        {
          z_root zr( x, i_z, yz[i_z], vz_species[i_z], F );
          wn_user::math::bracketer_by_expansion<double> br( zr );
          br.updateFactor( 10. );
          std::pair<double, double> p = br( -1., 1. );
          std::pair<double, double> result =
            boost::math::tools::bisect(
              zr,
              p.first,
              p.second,
              boost::math::tools::eps_tolerance<double>( )
            );
          lambda[i_z] = ( result.first + result.second ) / 2;
        }
      }
      lambda[0] = 0;
      BOOST_FOREACH( nnt::Species species, nnt::make_species_list( pNuc ) )
      {
        d_result -=
          Libnucnet__Species__getA( species.getNucnetSpecies() )
          *
          computeSpeciesAbundance( species, x );
      }
      return d_result;
    }

    double
    computeSpeciesAbundance( nnt::Species& species, double x )
    {
      Libnucnet__Species * p_species = species.getNucnetSpecies();
      double cc = lambda[Libnucnet__Species__getZ( p_species )];
      if( cc == GSL_NEGINF )
      {
        return 0;
      }
      else
      {
        return
          exp(
            cc
            +
            (x * Libnucnet__Species__getA( p_species ))
            + 
            F[Libnucnet__Species__getName( p_species )]
          );
      }
    }

    std::vector<double>
    getLambdas()
    {
      return lambda;
    }

  private:
    Libnucnet__Nuc * pNuc;
    ng_map_t F;
    double munkT;
    size_t iZ;
    std::vector<double> yz, lambda;
    std::vector<std::vector<Libnucnet__Species *> > vz_species;

};

} // namespace wn_user

#endif // WN_NG_EQUILIBRIUM_HPP
