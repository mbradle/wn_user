////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \brief A file to define equilibrium routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include <boost/optional.hpp>
#include "utility/zone_copier.hpp"
#include "utility/equilibrium_computer.hpp"
#include "utility/cluster_abundance_moment_computer.hpp"

#ifndef WN_EQUILIBRIUM_DETAIL_HPP
#define WN_EQUILIBRIUM_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define S_CONSTRAINT       "constraint"
#define S_MUKT             "mukT"
#define S_Y                "Y_c"
#define S_USE_YE           "use_Ye"

//##############################################################################
// equilibrium().
//##############################################################################

class equilibrium
{

  public:
    equilibrium( v_map_t& v_map )
    {
      if( v_map.count( S_CONSTRAINT ) )
      {
        sConstraint = v_map[S_CONSTRAINT].as<std::string>();
      }
      bUseYe = v_map[S_USE_YE].as<bool>();
    }

    nnt::Zone
    operator()( nnt::Zone& zone )
    {
      Libnucnet__Nuc__sortSpecies(
        Libnucnet__Net__getNuc( Libnucnet__Zone__getNet( zone.getNucnetZone() ) )
      );
      utility::zone_copier zc( Libnucnet__Zone__getNet( zone.getNucnetZone() ) );
      utility::cluster_abundance_moment_computer mc;
      utility::equilibrium_computer ec( zone );
      nnt::Zone new_zone;
      Libnucnet__Zone * p_old_zone = zone.getNucnetZone();
      std::vector<std::string> s_labels;
      s_labels.push_back( Libnucnet__Zone__getLabel( p_old_zone, 1 ) );
      s_labels.push_back( Libnucnet__Zone__getLabel( p_old_zone, 2 ) );
      s_labels.push_back( Libnucnet__Zone__getLabel( p_old_zone, 3 ) );
      Libnucnet__Zone * p_new_zone = zc( p_old_zone, s_labels );
      new_zone.setNucnetZone( p_new_zone );
      if( bUseYe )
      {
        double d_Ye = mc( zone, "", "z", 1. );
        ec.setYe( d_Ye );
        new_zone.updateProperty( nnt::s_YE, d_Ye );
      }
      if( sConstraint )
      {
        double d_y = mc( zone, sConstraint.get(), "a", 0. );
        ec.updateCluster( sConstraint.get(), d_y );
        new_zone.updateProperty(
          S_CONSTRAINT,
          sConstraint.get(),
          S_Y,
          d_y
        );
      }
      ec.compute(
        zone.getProperty<double>( nnt::s_T9 ),
        zone.getProperty<double>( nnt::s_RHO )
      );
      ec.setZoneAbundancesToEquilibrium( new_zone );
      if( sConstraint )
      {
        new_zone.updateProperty(
          S_CONSTRAINT,
          sConstraint.get(),
          S_MUKT,
          Libnuceq__Cluster__getMukT(
            Libnuceq__getCluster(
              ec.getEquilibrium(),
              sConstraint.get().c_str()
            )
          )
        );
      }
      return new_zone;
    }
      
  private:
    boost::optional<std::string> sConstraint = boost::none;
    bool bUseYe;

};
     
//##############################################################################
// equilibrium_options().
//##############################################################################

class equilibrium_options
{

  public:
    equilibrium_options() {}

    void
    get( po::options_description& equilibrium )
    {

      try
      {

        equilibrium.add_options()

        (
          S_CONSTRAINT,
          po::value<std::string>(),
          "Cluster constraint"
        )

        (
          S_USE_YE,
          po::value<bool>()->default_value( true, "true" ),
          "Use Ye"
        )

        ;

        //base::equilibrium_options::get( equilibrium );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user 

#endif // WN_EQUILIBRIUM_DETAIL_HPP
