//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header code to track star data.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef WN_STAR_TRACKER_HPP
#define WN_STAR_TRACKER_HPP

namespace wn_user
{

class star_tracker : public detail::star_tracker
{

  public:
    star_tracker() : detail::star_tracker(){}
    star_tracker( v_map_t& v_map, Libnucnet * p_nucnet ) :
      detail::star_tracker( v_map, p_nucnet ){}

  private:
};

//##############################################################################
// star_tracker_options().
//##############################################################################

class star_tracker_options : detail::star_tracker_options
{

  public:
    star_tracker_options() : detail::star_tracker_options() {}

    void
    get( options_map& o_map )
    {

      try
      {
    
        po::options_description star_tracker( "\nStar tracker Options" );

        getDetailOptions( star_tracker );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "star_tracker",
            options_struct( star_tracker )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // wn_user

#endif // WN_STAR_TRACKER_HPP
