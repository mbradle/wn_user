//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header code to track star data.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Check if already defined.
//##############################################################################

#include "user/hdf5_routines.h"

#ifndef WN_STAR_TRACKER_DETAIL_HPP
#define WN_STAR_TRACKER_DETAIL_HPP

namespace wn_user
{

namespace detail
{

#define  S_STARS_HDF5   "stars_hdf5"

typedef
std::map<std::string, std::pair<double,double> > file_map_t;

class star_tracker
{

  public:
    star_tracker(){}
    star_tracker( v_map_t& v_map, Libnucnet * p_nucnet ) : my_program_options()
    {
      pStarsNucnet = nnt::create_network_copy( p_nucnet );

      if( v_map.count( S_STARS_HDF5 ) )
      {

        BOOST_FOREACH(
          const std::vector<std::vector<std::string> >::value_type& v_y,
          my_program_options.composeOptionVectorOfVectors(
            v_map, S_STARS_HDF5, 3
          )
        )
        {
          user::hdf5::create_output( v_y[0].c_str(), pStarsNucnet );

          file_map[v_y[0]] =
            std::make_pair(
	      boost::lexical_cast<double>( v_y[1] ),
	      boost::lexical_cast<double>( v_y[2] )
	    );
	}
      }
    }


    void
    addStar( 
      Star star,
      gsl_vector * p_abunds
    )
    {
      if( file_map.size() != 0 )
      {
	BOOST_FOREACH( file_map_t::value_type& t, file_map )
        {
          if(
            star.getProperty( S_CURRENT_MASS ) >= t.second.first &&
            star.getProperty( S_CURRENT_MASS )  <= t.second.second
          )
          {
            nnt::Zone zone;

	    zone.setNucnetZone(
              Libnucnet__Zone__new(
		Libnucnet__getNet( pStarsNucnet ), "0", "0", "0"
              )
	    );

            Libnucnet__Zone__updateAbundances( zone.getNucnetZone(), p_abunds );

	    BOOST_FOREACH( std::string s, star.getVectorOfProperties() )
	    {
	      zone.updateProperty( s, star.getProperty( s ) );
	    }  

            Libnucnet__addZone( pStarsNucnet, zone.getNucnetZone() );

            boost::format fmt( "%015d" );
            fmt % star.getProperty<size_t>( nnt::s_STAR_ID );

            user::hdf5::append_zones(
              t.first.c_str(),
              pStarsNucnet,
              ( "Star " + fmt.str() ).c_str()
            );

            Libnucnet__removeZone( pStarsNucnet, zone.getNucnetZone() );
	    
          }

        }

      }

    }

  private:
    program_options my_program_options;
    Libnucnet * pStarsNucnet;
    file_map_t file_map;

};

//##############################################################################
// star_tracker_options().
//##############################################################################

class star_tracker_options
{

  public:
    star_tracker_options() {}

    void
    getDetailOptions( po::options_description& star_tracker )
    {

      try
      {
        star_tracker.add_options()

        // Option for output files
	(
          S_STARS_HDF5,
          po::value<std::vector<std::string> >()->multitoken(),
          "Stars output {file; mass_low; mass_high})"
        )

	;

    
// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_STAR_TRACKER_HPP
