////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file yields.hpp
//! \brief A file to define yields routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_YIELDS_BASE_HPP
#define WN_YIELDS_BASE_HPP

#include "my_global_types.h"
#include "user/nuclear_decay_utilities.h"

#define   S_YIELDS_XML            "yields_xml"
#define   S_YIELDS_ZONE_XPATH     "yields_xpath"
#define   S_YIELDS_PUSH_TIME      "yields_push_time"
#define   S_SOLAR_METALLICITY     "solar_metallicity"
#define   S_ZERO_YIELDS           "zero_yields"

#define   S_INITIAL_MASS          "initial mass"
#define   S_INITIAL_METALLICITY   "initial metallicity"
#define   S_REMNANT_MASS          "remnant mass"
#define   S_TOTAL_EJECTED_MASS    "total ejected mass"

#define   D_YEAR 3.15e7

//##############################################################################
// Types.
//##############################################################################

typedef std::pair<double,double> yield_key_t;

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// yields().
//##############################################################################

class yields
{

  public:
    yields(){}
    yields( v_map_t& v_map )
    {
      sYieldsXml = v_map[S_YIELDS_XML].as<std::string>();
      sYieldsZoneXPath = v_map[S_YIELDS_ZONE_XPATH].as<std::string>();
      dYieldsPushTime = v_map[S_YIELDS_PUSH_TIME].as<double>() * D_YEAR;
      dSolarMetallicity = v_map[S_SOLAR_METALLICITY].as<double>();
      bZeroYields = v_map[S_ZERO_YIELDS].as<bool>();
    }
    ~yields() { Libnucnet__free( pYieldsNucnet ); }

    void
    createYieldsNucnet( Libnucnet * p_nucnet )
    {
      pYieldsNucnet = nnt::create_network_copy( p_nucnet );

      Libnucnet__assignZoneDataFromXml(
        pYieldsNucnet, sYieldsXml.c_str(), sYieldsZoneXPath.c_str()
      );
    }

    Libnucnet *
    getYieldsNucnet()
    {
      return pYieldsNucnet;
    }

    void
    setYieldsData()
    {
      BOOST_FOREACH( nnt::Zone zone, nnt::make_zone_list( pYieldsNucnet ) )
      {
        std::string s_type =
          Libnucnet__Zone__getLabel( zone.getNucnetZone(), 1 );

//	std::cout << s_type << std::endl;

        if( s_type == S_STAR )
        {
          double d_star_mass = zone.getProperty<double>( S_INITIAL_MASS );
          double d_metallicity =
            zone.getProperty<double>( S_INITIAL_METALLICITY );
           double d_remnant_mass = zone.getProperty<double>( S_REMNANT_MASS );
          addYieldAndRemnant(
            zone, d_star_mass, d_metallicity, d_remnant_mass
          );
        }
        else
        {
          addOtherYield( s_type, zone );
        }
        nnt::normalize_zone_abundances( zone );
      }
    }

    bool shouldZeroYields() const
    {
      return bZeroYields;
    }

    void zeroYields()
    {
      BOOST_FOREACH( nnt::Zone zone, nnt::make_zone_list( pYieldsNucnet ) )
      {
        gsl_vector * p_abunds =
          Libnucnet__Zone__getAbundances( zone.getNucnetZone() );
        gsl_vector_set_zero( p_abunds );
        Libnucnet__Zone__updateAbundances( zone.getNucnetZone(), p_abunds );
        gsl_vector_free( p_abunds );
      }
    }

    void pushYields()
    {
      if( dYieldsPushTime != 0 )
      {
        user::push_abundances_to_daughters(
          pYieldsNucnet,
          1. / ( dYieldsPushTime * D_YEAR )
        );
      }
    }

    double getSolarMetallicity() const
    {
      return dSolarMetallicity;
    }

    void
    addYieldAndRemnant(
      nnt::Zone& zone,
      double d_star_mass,
      double d_metallicity,
      double d_remnant_mass
    )
    {

      mass.insert( d_star_mass );
      metallicity.insert( d_metallicity );

      yield_key_t p( d_metallicity, d_star_mass );

      if( remnantMassMap.find( p ) != remnantMassMap.end() )
      {
         std::cerr <<
           "Duplicate yield entry for mass " << d_star_mass << 
           " and metallicity " << d_metallicity << "!" << std::endl;
         exit( EXIT_FAILURE );
      }

      remnantMassMap[p] = d_remnant_mass;
      yieldMap[p] = zone.getNucnetZone();

      // Update vectors.

      vMass.clear();
      BOOST_FOREACH( double m, mass )
      {
        vMass.push_back( m );
      }

      vMetallicity.clear();
      BOOST_FOREACH( double x, metallicity )
      {
        vMetallicity.push_back( x );
      }
    }

    bool
    yieldMapIsValid()
    {
      bool b_result = true;

      BOOST_FOREACH( double x, metallicity )
      { 
        BOOST_FOREACH( double m, mass )
        { 
          if( yieldMap.find( std::make_pair( x, m ) ) == yieldMap.end() )
          { 
            std::cerr <<
              "Entry (" << x << "," << m << ") not present" << std::endl;
            b_result = false;
          }
        }
      }
      return b_result;
    }

    void
    addOtherYield(
      std::string s_type,
      nnt::Zone& zone
    )
    {
      if( zone.hasProperty( S_REMNANT_MASS ) )
      {
        double d_remnant_mass = zone.getProperty<double>( S_REMNANT_MASS );
        
        if(
          !( 
             otherRemnantMassMap.insert(
               std::make_pair(s_type, d_remnant_mass )
             )
          ).second
        )
        {  
           std::cerr <<
             "Duplicate yield entry for " << s_type << "!" << std::endl;
           exit( EXIT_FAILURE );
        }
      }
      if( zone.hasProperty( S_TOTAL_EJECTED_MASS ) )
      {
        double d_ejected_mass =
          zone.getProperty<double>( S_TOTAL_EJECTED_MASS );

        if(
          !(
             otherEjectedMassMap.insert(
               std::make_pair(s_type, d_ejected_mass )
             )
          ).second
        )
        {
           std::cerr <<
             "Duplicate yield entry for " << s_type << "!" << std::endl;
           exit( EXIT_FAILURE );
        }

      }
      otherYieldMap[s_type] = zone.getNucnetZone();
    }

    boost::tuple<gsl_vector *, double, double>
    getYieldDataFromTable( double scaled_metallicity, double star_mass )
    {

      std::vector<double>::iterator low;
  
      low = std::lower_bound(
        vMetallicity.begin(), vMetallicity.end(), scaled_metallicity
      );
  
      size_t i_x = low - vMetallicity.begin();
  
      if( i_x >= vMetallicity.size() )
      {
        std::cerr << "Metallicity exceeds highest table value." << std::endl;
        exit( EXIT_FAILURE );
      }
      else if(
        i_x == vMetallicity.size() - 1 ||
        scaled_metallicity != vMetallicity[i_x]
      )
      {
        i_x -= 1;
      }
  
      low = std::lower_bound( vMass.begin(), vMass.end(), star_mass );
  
      size_t i_m = low - vMass.begin();
  
      if( i_m >= vMass.size() )
      {
        std::cerr << "Stellar mass exceeds highest table value." << std::endl;
        exit( EXIT_FAILURE );
      }
      else if(
        i_m == vMass.size() - 1 ||
        star_mass != vMass[i_m]
      )
      {
        i_m -= 1;
      }
  
      double x = scaled_metallicity;
      double y = star_mass;
  
      double x1 = vMetallicity[i_x];
      double x2 = vMetallicity[i_x+1];
  
      double y1 = vMass[i_m];
      double y2 = vMass[i_m+1];
  
      double d = ( x2 - x1 ) * ( y2 - y1 );
  
      double f11 = ( y2 - y ) * ( x2 - x ) / d;
      double f21 = ( y2 - y ) * ( x - x1 ) / d;
      double f12 = ( y - y1 ) * ( x2 - x ) / d;
      double f22 = ( y - y1 ) * ( x - x1 ) / d;
  
      gsl_vector * p_11 =
        Libnucnet__Zone__getMassFractions(
          yieldMap[std::make_pair( x1, y1 )]
        );
      gsl_vector * p_21 =
        Libnucnet__Zone__getMassFractions(
          yieldMap[std::make_pair( x2, y1 )]
        );
      gsl_vector * p_12 =
        Libnucnet__Zone__getMassFractions(
          yieldMap[std::make_pair( x1, y2 )]
        );
      gsl_vector * p_22 =
        Libnucnet__Zone__getMassFractions(
          yieldMap[std::make_pair( x2, y2 )]
        );
  
      gsl_vector_scale( p_11, f11 ); 
      gsl_vector_scale( p_21, f21 ); 
      gsl_vector_scale( p_12, f12 ); 
      gsl_vector_scale( p_22, f22 ); 
  
      size_t i_size = p_11->size;
      gsl_vector * p_result = gsl_vector_calloc( i_size );
  
      gsl_vector_add( p_result, p_11 );
      gsl_vector_add( p_result, p_21 );
      gsl_vector_add( p_result, p_12 );
      gsl_vector_add( p_result, p_22 );
  
      gsl_vector_free( p_11 );
      gsl_vector_free( p_21 );
      gsl_vector_free( p_12 );
      gsl_vector_free( p_22 );
  
      double remnant_mass =
        remnantMassMap[std::make_pair( x1, y1 )] * f11
        +
        remnantMassMap[std::make_pair( x2, y1 )] * f21
        +
        remnantMassMap[std::make_pair( x1, y2 )] * f12
        +
        remnantMassMap[std::make_pair( x2, y2 )] * f22;
  
      return
        boost::make_tuple(
          p_result, star_mass - remnant_mass, remnant_mass
        );

    }

    boost::tuple<gsl_vector *, double, double>
    getOtherYieldData(
      double star_mass,
      std::string s_status
    )
    {
      if( otherRemnantMassMap.find( s_status ) != otherRemnantMassMap.end() )
      {
        double remnant_mass = otherRemnantMassMap[s_status];

        return
          boost::make_tuple(
            Libnucnet__Zone__getMassFractions( otherYieldMap[s_status] ),
            star_mass - remnant_mass,
            remnant_mass
          );
      }
      else if(
        otherEjectedMassMap.find( s_status ) != otherEjectedMassMap.end()
      )
      {
        double ejecta_mass = otherEjectedMassMap[s_status];

        return
          boost::make_tuple(
            Libnucnet__Zone__getMassFractions( otherYieldMap[s_status] ),
            ejecta_mass,
            star_mass - ejecta_mass
          );
      }
      else
      {
        std::cout << "Improper data for " << s_status << std::endl;
        exit( EXIT_FAILURE );
      }
    }

    std::vector<double>
    getYieldMasses() { return vMass; }


  private:
    std::string sYieldsXml, sYieldsZoneXPath;
    Libnucnet * pYieldsNucnet = NULL;
    double dYieldsPushTime, dSolarMetallicity;
    bool bZeroYields;
    std::vector<double> vMass, vMetallicity;
    std::map<yield_key_t, double> remnantMassMap;
    std::map<std::string, double> otherRemnantMassMap;
    std::map<std::string, double> otherEjectedMassMap;
    std::map<yield_key_t, Libnucnet__Zone *> yieldMap;
    std::map<std::string, Libnucnet__Zone *> otherYieldMap;
    std::set<double> mass, metallicity;

};


//##############################################################################
// yields_options().
//##############################################################################

class yields_options
{

  public:
    yields_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    get( po::options_description& yields )
    {

      try
      {

        yields.add_options()

        // Option for yields xml
          ( S_YIELDS_XML,
            po::value<std::string>()->required(),
            "Name of XML file with yields"
          )

        // Option for yields zone xpath
          ( S_YIELDS_ZONE_XPATH,
            po::value<std::string>()->default_value( "", "\"\"" ),
            "Zone XPath for yields"
          )

        // Option for push time of yields
          ( S_YIELDS_PUSH_TIME, po::value<double>()->default_value( 0, "0" ),
            "Push time for yields in years"
          )

        // Option for solar metallicity
          ( S_SOLAR_METALLICITY,
            po::value<double>()->default_value( 0.02, "0.02" ),
            "Solar metallicity"
          )

        // Option to zero out yields
          ( S_ZERO_YIELDS, po::value<bool>()->default_value( false, "false" ),
            "Zero the yields"
          )

        ;

      // Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }
};

} // namespace base

} // namespace wn_user

#endif // WN_YIELDS_BASE_HPP
