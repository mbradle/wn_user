////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file yields_history_base.hpp
//! \brief A file to define yields routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_YIELDS_HISTORY_BASE_HPP
#define WN_YIELDS_HISTORY_BASE_HPP

#include "my_global_types.h"
#include "user/nuclear_decay_utilities.h"

#define   S_HISTORY_STEPS         "history_steps"

//##############################################################################
// Types.
//##############################################################################

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// yields().
//##############################################################################

class yields_history
{

  public:
    yields_history(){}
    yields_history( v_map_t& v_map )
    {
      i_history_steps = v_map[S_HISTORY_STEPS].as<size_t>();
      d_history_time = 0;
    }
    ~yields_history(){ Libnucnet__free( pHistoryNucnet ); }

    void
    createHistoryNucnet( Libnucnet * p_nucnet )
    {
      pHistoryNucnet = nnt::create_network_copy( p_nucnet );
    }

    void
    setHistoryZones( std::vector<nnt::Zone>& zones )
    {

      historyMap.clear();

      BOOST_FOREACH( nnt::Zone zone, zones )
      {
        nnt::write_xml( pHistoryNucnet, zone.getNucnetZone() );
        historyMap.insert(
          std::make_pair( zone, Libnucnet__getNumberOfZones( pHistoryNucnet ) )
        );
      }
    }

    void
    updateHistoryZones(
      std::vector<nnt::Zone>& zones,
      double d_dt
    )
    {    
      d_history_time +=  d_dt;

      if(
        Libnucnet__getNumberOfZones( pHistoryNucnet ) > 0 &&
        i_step % i_history_steps == 0
      )
      {
        user::decay_abundances( pHistoryNucnet, d_history_time );
        setHistoryZones( zones );
        d_history_time = 0;
      }
    }

    Libnucnet * getHistoryNucnet() { return pHistoryNucnet; }

    size_t
    getHistoryZoneId( nnt::Zone& zone )
    {
      std::map<nnt::Zone, size_t>::iterator it = historyMap.find( zone );

      if( it == historyMap.end() )
      {
        std::cerr << "History zone not found." << std::endl;
        exit( EXIT_FAILURE );
      }

      return it->second;;

   }

  private:
    Libnucnet * pHistoryNucnet = NULL;
    std::map<nnt::Zone, size_t> historyMap;
    double d_history_time;
    size_t i_history_steps, i_step = 0;

};

//##############################################################################
// yields_history_options().
//##############################################################################

class yields_history_options
{

  public:
    yields_history_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    get( po::options_description& yields )
    {

      try
      {

        yields.add_options()

        // Option for frequency of zone history saving
        ( S_HISTORY_STEPS, po::value<size_t>()->default_value( 100 ),
          "History steps." )

        ;

      // Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace base

} // namespace wn_user

#endif // WN_YIELDS_HISTORY_BASE_HPP
