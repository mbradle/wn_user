////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file yields.hpp
//! \brief A file to define yields routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_YIELDS_DETAIL_HPP
#define WN_YIELDS_DETAIL_HPP

#include "yields/base/yields.hpp"
#include "yields/base/yields_history.hpp"

#define  S_R_PROCESS          "r process"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// yields().
//##############################################################################

class yields : public base::yields, public base::yields_history
{

  public:
    yields() : base::yields(), base::yields_history() {}
    yields( v_map_t& v_map ) :
      base::yields( v_map ), base::yields_history( v_map ) {}

    void
    setYields( Libnucnet * p_nucnet )
    {
      createYieldsNucnet( p_nucnet );
      setYieldsData();
      pushYields();
      if( shouldZeroYields() )
      {
        zeroYields();
      }
    } 

    boost::tuple<gsl_vector *, double, double>
    getYieldData( const StarSystem& star_system )
    {
    
      std::vector<wn_user::Star> v = star_system.getStarVector();

      if( v[0].getStatus() == S_STAR )
      {
    
        if(
	    getYieldMasses().empty() ||
	    v[0].getProperty( S_CURRENT_MASS ) < getYieldMasses()[0] )
        {

          Libnucnet__Zone * p_zone =
            Libnucnet__getZoneByLabels(
              getHistoryNucnet(),
              v[0].getProperty<std::string>( nnt::s_ORIGINAL_ZONE_ID ).c_str(),
              "0",
              "0"
            );
    
          double remnant_mass =
            GSL_MIN( v[0].getProperty( S_CURRENT_MASS ), 0.8 );
    
          // Add s-process here.
    
          return
            boost::make_tuple(
              Libnucnet__Zone__getMassFractions( p_zone ),
              v[0].getProperty( S_CURRENT_MASS ) - remnant_mass,
              remnant_mass
            );
        }
    
        double scaled_metallicity =
          v[0].getProperty( S_METALLICITY ) / getSolarMetallicity();
    
        return
          getYieldDataFromTable(
	    scaled_metallicity, v[0].getProperty( S_CURRENT_MASS )
	  );
    
      }
      else if( v[0].getStatus() == S_WHITE_DWARF && v[1].getStatus() == S_STAR )
      {
    std::cout << "Ia!" << std::endl;
        return
          getOtherYieldData(
	    v[0].getProperty( S_CURRENT_MASS ), v[0].getStatus()
	  );
      }
      else if(
        v[0].getStatus() == S_C_WHITE_DWARF && v[1].getStatus() == S_STAR
      )
      {
    std::cout << "IaC!" << std::endl;
        return
          getOtherYieldData(
	    v[0].getProperty( S_CURRENT_MASS ), v[0].getStatus()
	  );
      }
      else if(
        v[0].getStatus() == S_NEUTRON_STAR &&
        v[1].getStatus() == S_NEUTRON_STAR
      )
      {
    std::cout << "NS-NS merger!" << std::endl;
        return
        getOtherYieldData(
	  v[0].getProperty( S_CURRENT_MASS ) +
	    v[1].getProperty( S_CURRENT_MASS ),
	  S_R_PROCESS
	);
      }
      else
      {
        BOOST_FOREACH( wn_user::Star s, v )
        {
          std::cerr << s.getStatus() << "  ";
        }
        std::cerr << "\nNo such yield\n";
        exit( EXIT_FAILURE );
      }
    
    }
  
};

//##############################################################################
// yields_options().
//##############################################################################

class yields_options :
  public base::yields_options, public base::yields_history_options
{

  public:
    yields_options() :
      base::yields_options(), base::yields_history_options() {}

    std::string
    getExample()
    {
      return base::yields_options::getExample();
    }

    void
    get( po::options_description& yields )
    {

      try
      {

        base::yields_options::get( yields );
        base::yields_history_options::get( yields );

      // Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_YIELDS_DETAIL_HPP
