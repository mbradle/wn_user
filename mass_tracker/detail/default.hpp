//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header code to track mass data.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef WN_MASS_TRACKER_DETAIL_HPP
#define WN_MASS_TRACKER_DETAIL_HPP

namespace wn_user
{

namespace detail
{

#define  S_ACTIVE_STAR     "active star"
#define  S_GAS             "gas"
#define  S_HALO            "halo"
#define  S_INACTIVE_STAR   "inactive star"
#define  S_MASS_FILE       "mass_file"
#define  S_TOTAL_MASS      "total mass"

typedef std::map<std::string, double> mass_map_t;

class mass_tracker
{

  public:
    mass_tracker(){}
    mass_tracker( v_map_t& v_map, Libnucnet * p_nucnet )
    {
      if( v_map.count( S_MASS_FILE ) )
      {
	sMassFile = v_map[S_MASS_FILE].as<std::string>();
	std::ofstream my_file;
        my_file.open( sMassFile.c_str() );
	my_file.close();
      }
      pNucnet = p_nucnet;
    }

    void addStarSystem( const StarSystem& ss, double d_tend )
    {

      BOOST_FOREACH( wn_user::Star s, ss.getStarHeap() )
      {
        if( s.getStatus() == S_STAR )
        {
          if( s.getProperty( S_END_TIME ) <= d_tend )
          {
            massMap[S_ACTIVE_STAR] +=
	      s.getProperty( nnt::s_NUMBER ) * s.getProperty( S_CURRENT_MASS );
          }
          else
          {
            massMap[S_INACTIVE_STAR] +=
	      s.getProperty( nnt::s_NUMBER ) * s.getProperty( S_CURRENT_MASS );
          }
        }
        else
        {
          massMap[s.getStatus()] +=
	    s.getProperty( nnt::s_NUMBER ) * s.getProperty( S_CURRENT_MASS );
        }
      }
    }

    void subtractStarSystem( const StarSystem& ss, double d_tend )
    {

      BOOST_FOREACH( wn_user::Star s, ss.getStarHeap() )
      {
        if( s.getStatus() == S_STAR )
        {
          if( s.getProperty( S_END_TIME ) <= d_tend )
          {
            massMap[S_ACTIVE_STAR] -=
	      s.getProperty( nnt::s_NUMBER ) * s.getProperty( S_CURRENT_MASS );
          }
          else
          {
            massMap[S_INACTIVE_STAR] -=
	      s.getProperty( nnt::s_NUMBER ) * s.getProperty( S_CURRENT_MASS );
          }
        }
        else
        {
          massMap[s.getStatus()] -=
	    s.getProperty( nnt::s_NUMBER ) * s.getProperty( S_CURRENT_MASS );
        }
      }
    }

    void computeZoneMasses( )
    {
      massMap[S_HALO] = 0;
      massMap[S_GAS] = 0;

      BOOST_FOREACH( nnt::Zone zone, nnt::make_zone_list( pNucnet ) )
      {
        if(
          std::string( Libnucnet__Zone__getLabel( zone.getNucnetZone(), 1 ) )
          ==
          S_HALO
        )
        {
          massMap[S_HALO] += zone.getProperty<double>( nnt::s_ZONE_MASS );
        }
        else
        {
          massMap[S_GAS] += zone.getProperty<double>( nnt::s_ZONE_MASS );
        }
      }
    }

    void addDataToFile( double d_time )
    {

      computeZoneMasses( );

      massMap[S_TOTAL_MASS] = 0;

      BOOST_FOREACH( mass_map_t::value_type& m, massMap )
      {
	if( m.first != S_TOTAL_MASS )
        {
          massMap[S_TOTAL_MASS] += m.second;
	}
      }

      if( !sMassFile.empty() )
      {
         std::ofstream my_file;

         my_file.open( sMassFile.c_str(), std::ios::app );

         my_file << d_time << "  " <<
                    massMap[S_HALO] << "  " <<
                    massMap[S_GAS] << "  " <<
                    massMap[S_ACTIVE_STAR] << "  " <<
                    massMap[S_INACTIVE_STAR] << "  " <<
                    massMap[S_WHITE_DWARF] << "  " <<
                    massMap[S_NEUTRON_STAR] << "  " <<
                    massMap[S_BLACK_HOLE] << "  " <<
                    massMap[S_TOTAL_MASS] << std::endl;

         my_file.close();
       }
    }


  private:
    std::string sMassFile;
    mass_map_t massMap;
    Libnucnet * pNucnet;

};

//##############################################################################
// mass_tracker_options().
//##############################################################################

class mass_tracker_options
{

  public:
    mass_tracker_options() {}

    void
    get( po::options_description& mass_tracker )
    {

      try
      {
        mass_tracker.add_options()

        // Option for output files
	(
          S_MASS_FILE,
          po::value<std::string>(),
          "Mass output file"
        )

	;

    
// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // detail

} // wn_user

#endif // WN_MASS_TRACKER_HPP
