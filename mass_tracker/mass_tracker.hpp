//////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header code to track zone mass data.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef WN_MASS_TRACKER_HPP
#define WN_MASS_TRACKER_HPP

namespace wn_user
{

class mass_tracker : public detail::mass_tracker
{

  public:
    mass_tracker() : detail::mass_tracker(){}
    mass_tracker( v_map_t& v_map, Libnucnet * p_nucnet ) :
      detail::mass_tracker( v_map, p_nucnet ){}

  private:
};

//##############################################################################
// mass_tracker_options().
//##############################################################################

class mass_tracker_options : detail::mass_tracker_options
{

  public:
    mass_tracker_options() : detail::mass_tracker_options() {}

    void
    get( options_map& o_map )
    {

      try
      {
    
        po::options_description mass_tracker( "\nMass tracker Options" );

        detail::mass_tracker_options::get( mass_tracker );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "mass_tracker",
            options_struct( mass_tracker )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // wn_user

#endif // WN_MASS_TRACKER_HPP
