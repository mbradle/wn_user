////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file step_printer.hpp
//! \brief A file to define output routines.
//!
////////////////////////////////////////////////////////////////////////////////


#ifndef WN_STEP_PRINTER_BASE_HPP
#define WN_STEP_PRINTER_BASE_HPP

#include "my_global_types.h"

#include "boost/format.hpp"

#define S_Y_MIN_PRINT    "y_min_print"

namespace wn_user
{

namespace base
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class step_printer
///
/// \brief A class to handle the common step printout implementation.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// step_printer().
//##############################################################################

class step_printer
{
  public:
    step_printer(){}

/// The constructor.
///
/// \param v_map The options variable map.

    step_printer( v_map_t& v_map )
    {
      d_y_min_print = v_map[S_Y_MIN_PRINT].as<double>();
    }

/// Routine to print out zone data for a time step.
///
/// \param zone The zone.
/// \return On successful return, the data for the zone has been printed out.

    void print( nnt::Zone& zone )
    {
      double d_t, d_dt, d_t9, d_rho;
    
      d_t = zone.getProperty<double>( nnt::s_TIME );
    
      d_dt = zone.getProperty<double>( nnt::s_DTIME );
    
      d_t9 = zone.getProperty<double>( nnt::s_T9 );
    
      d_rho = zone.getProperty<double>( nnt::s_RHO );
    
      std::cout <<
        boost::format(
          "t = %10.4e, dt = %10.4e, t9 = %10.4e, rho (g/cc) = %10.4e\n\n"
        ) % d_t % d_dt % d_t9 % d_rho;
    
      BOOST_FOREACH(
        nnt::Species species,
        nnt::make_species_list(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          )
        )
      )
      {
        printAbundance( species.getNucnetSpecies(), zone.getNucnetZone() );
      }
    
      std::cout <<
        boost::format( "1 - xsum = %e\n\n" ) %
        ( 1. - Libnucnet__Zone__computeAMoment( zone.getNucnetZone(), 1 ) );
    
      std::cout <<
        boost::format( "Ye = %f\n\n" ) %
        ( Libnucnet__Zone__computeZMoment( zone.getNucnetZone(), 1 ) );
    
    }

/// Routine to print out data for a collection of zones for a time step.
///
/// \param zones The zones.
/// \return On successful return, the data for the zones has been printed out.

    void print( std::vector<nnt::Zone>& zones )
    {
      double d_abund, d_change;

      for( size_t i = 0; i < zones.size(); i++ )
      {
    
        std::cout << "Zone: (" <<
          Libnucnet__Zone__getLabel( zones[i].getNucnetZone(), 1 ) << 
          ", " <<
          Libnucnet__Zone__getLabel( zones[i].getNucnetZone(), 2 ) << 
          ", " <<
          Libnucnet__Zone__getLabel( zones[i].getNucnetZone(), 3 ) << 
          ")" << std::endl;
    
        nnt::species_list_t species_list =
          nnt::make_species_list(
            Libnucnet__Net__getNuc(
              Libnucnet__Zone__getNet( zones[i].getNucnetZone() )
            )
          );
    
        BOOST_FOREACH( nnt::Species species, species_list )
        {
    
          d_abund =
            Libnucnet__Zone__getSpeciesAbundance(
              zones[i].getNucnetZone(),
              species.getNucnetSpecies()
            );
    
          if( d_abund > d_y_min_print )
          {
    
    	d_change =
    	  Libnucnet__Zone__getSpeciesAbundanceChange(
    	    zones[i].getNucnetZone(),
    	    species.getNucnetSpecies()
    	  );
    
            std::cout <<
              Libnucnet__Species__getName( species.getNucnetSpecies() ) <<
              "   " <<
              d_abund * Libnucnet__Species__getA( species.getNucnetSpecies() ) <<
              "   " <<
              d_change * Libnucnet__Species__getA( species.getNucnetSpecies() ) <<
              "   " <<
              fabs( d_change / d_abund ) <<
              std::endl;
    
           }
    
         }
    
         std::cout << " 1 - Xsum = " <<
           1 -
           Libnucnet__Zone__computeAMoment(
             zones[i].getNucnetZone(), 1
           ) << std::endl;
    
         std::cout << std::endl;
    
      }

    }

  private:
    double d_y_min_print;

    int
    printAbundance(
      Libnucnet__Species *p_species,
      Libnucnet__Zone *p_zone
    )
    {
    
      double d_abund;
    
      if( 
          ( d_abund =
    	 Libnucnet__Zone__getSpeciesAbundance( p_zone, p_species )
          ) > d_y_min_print
      )
      {
        std::cout <<
          boost::format( "%5lu%5lu%16.6e%16.6e\n" ) %
            (unsigned long) Libnucnet__Species__getZ( p_species ) %
            (unsigned long) Libnucnet__Species__getA( p_species ) %
            d_abund %
            Libnucnet__Zone__getSpeciesAbundanceChange(
              p_zone, p_species
            );
      }
    
      return 1;
    
    }

};

////////////////////////////////////////////////////////////////////////////////
///
/// \class step_printer_options
///
/// \brief A class to implement the common step printout options.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// step_printer_options()
//##############################################################################

class step_printer_options
{

  public:
    step_printer_options(){}

/// Routine to return the example string.
///
/// \return The example string.

    std::string
    getExample()
    {
      return "";
    }

/// Routine to update the options descriptions.
///
/// \return On successful return, the options descriptions have been updated.

    void get( po::options_description& options_desc )
    {

      try
      {

        options_desc.add_options()

        ( S_Y_MIN_PRINT, po::value<double>()->default_value( 1.e-25, "1.e-25" ),
          "Minimum abundance to printout." )

        ; 

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace base

} // namespace wn_user

#endif // WN_STEP_PRINTER_BASE_HPP
