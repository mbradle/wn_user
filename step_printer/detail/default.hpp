////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_STEP_PRINTER_DETAIL_HPP
#define WN_STEP_PRINTER_DETAIL_HPP

#include "step_printer/base/step_printer.hpp"

#define S_PRINT_STEPS    "print_steps"
#define S_PRINTOUT       "printout"

namespace wn_user
{

namespace detail
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class step_printer
/// 
/// \brief A class to implement the step printout.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// step_printer().
//##############################################################################

class step_printer : base::step_printer
{

  public:
/// The constructor.
///
/// \param v_map The options variable map.

    step_printer( v_map_t& v_map ) : base::step_printer( v_map )
    {
      iCount = 0;
      iPrintSteps = v_map[S_PRINT_STEPS].as<size_t>();
      bPrintSteps = v_map[S_PRINTOUT].as<bool>();
    }

/// Routine to print out a time step for a set of zones.
///
/// \param zones The zones.

    void operator()( std::vector<nnt::Zone>& zones )
    {
      if(
        iCount++ % iPrintSteps == 0 ||
        zones[0].getProperty<double>( nnt::s_TIME ) >=
          zones[0].getProperty<double>( nnt::s_T_END )
      )
      {
        print( zones );
      }
    }

/// Routine to print out the time step for a zone.
///
/// \param zone The zone.

    void operator()( nnt::Zone& zone )
    {
      if( bPrintSteps )
      {
        if(
           (
             iCount++ % iPrintSteps == 0 ||
             zone.getProperty<double>( nnt::s_TIME ) >=
               zone.getProperty<double>( nnt::s_T_END )
           )
        )
        {
          print( zone );
        }
      }
    }

  private:
    size_t iPrintSteps, iCount;
    bool bPrintSteps;

};

////////////////////////////////////////////////////////////////////////////////
///
/// \class step_printer_options
/// 
/// \brief A class to implement options for the step printout.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// step_printer_options()
//##############################################################################

class step_printer_options : public base::step_printer_options
{

  public:
    step_printer_options() : base::step_printer_options() {}

/// Routine to return an example string.
///
/// \return The example string.

    std::string
    getExample()
    {
      return base::step_printer_options::getExample();
    }

/// Routine to update the options descriptions.
///
/// \param options_desc The options descriptions.
/// \return On successful return, the options descriptions have been updated.

    void get( po::options_description& options_desc )
    {

      try
      {

        options_desc.add_options()

        ( S_PRINT_STEPS, po::value<size_t>()->default_value( 20, "20" ),
          "Frequency of output to screen." )

        ( S_PRINTOUT, po::value<bool>()->default_value( true, "true" ),
          "Print to screen." )

        ; 

        base::step_printer_options::get( options_desc );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_STEP_PRINTER_DETAIL_HPP
