////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////


#ifndef WN_STEP_PRINTER_HPP
#define WN_STEP_PRINTER_HPP

namespace wn_user
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class step_printer
///
/// \brief A class to govern printout of time steps.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// step_printer().
//##############################################################################

class step_printer : public detail::step_printer
{
  public:
    step_printer( v_map_t& v_map ) : detail::step_printer( v_map ) {}

};

////////////////////////////////////////////////////////////////////////////////
///
/// \class step_printer_options
///
/// \brief A class to handle options of printout of time steps.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// step_printer_options()
//##############################################################################

class step_printer_options : public detail::step_printer_options
{

  public:
    step_printer_options() : detail::step_printer_options() {}

/// Routine to update the options map for step printer options.
///
/// \param o_map The options map.

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description step_printer_desc("\nStep printer options");

        detail::step_printer_options::get( step_printer_desc );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "step_printer", 
            options_struct( step_printer_desc )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace wn_user

#endif // WN_STEP_PRINTER_HPP
