// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file screening.hpp
//! \brief A file to define nse correction routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef NNP_NSE_CORRECTOR_HPP
#define NNP_NSE_CORRECTOR_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// nse_corrector().
//##############################################################################

class nse_corrector : public detail::nse_corrector
{
  public:
    nse_corrector() : detail::nse_corrector() {}
    nse_corrector( v_map_t& v_map ) : detail::nse_corrector( v_map ){}

    void operator()( nnt::Zone& zone )
    {
      setNseCorrector( zone );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
#ifndef NO_OPENMP
      #pragma omp parallel for schedule( dynamic, 1 )
#endif
      for( size_t i = 0; i < zones.size(); i++ )
      {
        setNseCorrector( zones[i] );
      }
    }

};

//##############################################################################
// nse_corrector_options().
//##############################################################################

class nse_corrector_options : public detail::nse_corrector_options
{

  public:
    nse_corrector_options() : detail::nse_corrector_options() {}

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description nse_corrector("\nNSE Corrector Options");

        getDetailOptions( nse_corrector );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "nse_corrector",
            options_struct( nse_corrector )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }
};

}  // namespace wn_user

#endif  // NNP_NSE_CORRECTOR_HPP
