////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file thermo_base.hpp
//! \brief A file to define base hydro routines.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.

#include "my_global_types.h"
#include "user/flow_utilities.h"
#include "user/thermo.h"

#ifndef NNP_THERMO_BASE_HPP
#define NNP_THERMO_BASE_HPP

/**
 * @brief A namespace for user-defined rate functions.
 */
namespace wn_user
{

namespace base
{

class compute_mukT
{

  public:
    compute_mukT( nnt::Zone& _zone, nnt::Species& _species ) :
      zone( _zone ), species( _species ) {}

    double operator()( double t9 ) const
    {

      Libnucnet__Zone * p_zone = zone.getNucnetZone();
      Libnucnet__Species * p_species = species.getNucnetSpecies();

      double d_abund =
        Libnucnet__Zone__getSpeciesAbundance( p_zone, p_species );

      if( d_abund > 1.e-100 )
      {
        return
          log(
            d_abund
            /
            Libnucnet__Species__computeQuantumAbundance(
              p_species,
              t9,
              zone.getProperty<double>( nnt::s_RHO )
            )
          );
      }
      else
      {
        return 0;
      }
    }

  private:
    nnt::Zone& zone;
    nnt::Species& species;

};

class compute_energy_generation_rate_by_abundance_change
{

  public:

    compute_energy_generation_rate_by_abundance_change(){}

    double
    operator()( nnt::Zone& zone, gsl_vector * p_old, double dt )
    {

      double eps = 0;

      double t9 = zone.getProperty<double>( nnt::s_T9 );

      Libnucnet__Nuc * p_nuc =
        Libnucnet__Net__getNuc(
          Libnucnet__Zone__getNet( zone.getNucnetZone() )
        );

      double electron_part_MeV =
        nnt::compute_kT_in_MeV( t9 ) * t9 * GSL_CONST_NUM_GIGA
        *
        user::compute_thermo_quantity(
          zone,
          nnt::s_T_DERIVATIVE_CHEMICAL_POTENTIAL_KT,
          nnt::s_ELECTRON
        );

      BOOST_FOREACH( nnt::Species species, nnt::make_species_list( p_nuc ) )
      {

        compute_mukT my_func( zone, species );

        Libnucnet__Species * p_species = species.getNucnetSpecies();

        species_part_MeV =
          t9 * nnt::compute_derivative( my_func, t9 );

        species_part_MeV *= nnt::compute_kT_in_MeV( t9 );

        double dydt =
          (
            Libnucnet__Zone__getSpeciesAbundance(
              zone.getNucnetZone(), p_species 
            )
            -
            gsl_vector_get( p_old, Libnucnet__Species__getIndex( p_species ) )
          ) / dt;

        eps +=
          (
            -Libnucnet__Species__getMassExcess( p_species )
            +
            electron_part_MeV * Libnucnet__Species__getZ( p_species )
            +
            species_part_MeV
          ) * dydt * GSL_CONST_CGSM_ELECTRON_VOLT * GSL_CONST_NUM_MEGA;

        }

        return eps;

      }
      
  private:
    double species_part_MeV;

};

class thermo
{

  public:

    thermo(){}

    static double computePressure(
      nnt::Zone& zone
    )
    {
      return
        user::compute_thermo_quantity(
          zone,
          nnt::s_PRESSURE,
          nnt::s_TOTAL
        );
    }

    static double computeEntropy(
      nnt::Zone& zone
    )
    {
      return
        user::compute_thermo_quantity(
          zone,
          nnt::s_ENTROPY_PER_NUCLEON,
          nnt::s_TOTAL
        );
    }

    double
    computeEntropyGenerationRate(
      nnt::Zone& zone,
      Libnucnet__NetView * p_view
    )
    {
      return user::compute_entropy_generation_rate( zone, p_view );
    }

    Libnucnet__NetView *
    getLossView(
      Libnucnet__NetView * p_view
    )
    {

      Libnucnet__NetView * p_new = Libnucnet__NetView__copy( p_view );

      BOOST_FOREACH(
        nnt::Reaction reaction,
        nnt::make_reaction_list(
          Libnucnet__Net__getReac( Libnucnet__NetView__getNet( p_view ) )
        )
      )
      {
        if( !Libnucnet__Reaction__isWeak( reaction.getNucnetReaction() ) )
        {
          Libnucnet__NetView__removeReaction(
            p_new,
            reaction.getNucnetReaction()
          );
        }
      }

      return p_new;
    }
          
    double
    computeEntropyLossRate(
      nnt::Zone& zone,
      Libnucnet__NetView * p_view
    )
    {

      Libnucnet__NetView * p_new = getLossView( p_view );

      double d_result = 0.6 * computeEntropyGenerationRate( zone, p_new );

      Libnucnet__NetView__free( p_new );

      return d_result;

    }

    double computeEnergyGenerationRate(
      nnt::Zone& zone,
      Libnucnet__NetView * p_view
    )
    {
      return
        user::compute_energy_generation_rate_per_nucleon( zone, p_view );
    }

    double computeEnergyGenerationRateByAbundanceChange(
      nnt::Zone& zone,
      gsl_vector * p_old,
      double dt
    )
    {
      compute_energy_generation_rate_by_abundance_change my_eps;
      return my_eps( zone, p_old, dt );
    }

    double computeEnergyLossRate(
      nnt::Zone& zone,
      Libnucnet__NetView * p_view
    )
    {

      Libnucnet__NetView * p_new = getLossView( p_view );

      double d_result =
        0.6 *
        computeEnergyGenerationRate( zone, p_new );

      Libnucnet__NetView__free( p_new );

      return d_result;

    }
      
    double computeSpecificHeatPerNucleon(
      nnt::Zone& zone
    )
    {
      return
        user::compute_thermo_quantity(
          zone,
          nnt::s_SPECIFIC_HEAT_PER_NUCLEON, 
          nnt::s_TOTAL
        ) * GSL_CONST_CGSM_BOLTZMANN;
    }

    double computeDPDT(
      nnt::Zone& zone
    )
    {
      return 
        user::compute_thermo_quantity( zone, nnt::s_DPDT, nnt::s_TOTAL );
    }

};

}  // namespace detail

}  // namespace wn_user

#endif // NNP_THERMO_BASE
