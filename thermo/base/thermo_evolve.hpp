////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file thermo_evolve.hpp
//! \brief A file to define base hydro routines.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.

#include "my_global_types.h"
#include "nnt/math.h"
#include <boost/assign.hpp>

#ifndef NNP_THERMO_EVOLVE_HPP
#define NNP_THERMO_EVOLVE_HPP

#define S_THERMO_EVOLVE    "thermo_evolve"
#define S_ROOT_OBSERVE     "root_observe"
#define S_ROOT_FACTOR      "root_factor"
#define S_ROOT_FACTOR_LOW  "root_factor_low"
#define S_ROOT_FACTOR_T9   "root_factor_t9"
#define S_TOLERANCE        "root_tolerance"

/**
 * @brief A namespace for user-defined rate functions.
 */
namespace wn_user
{

namespace base
{

void
check_tolerance( const double& value, const double& min, const double& max )
{
  if( value < min || value > max )
  {
    std::cerr << "Root tolerance range: [" << min << ", " << max << "]\n";
    exit( EXIT_FAILURE ); 
  }
}


class thermo_evolve : virtual public thermo, public network_evolver
{

  public:

    thermo_evolve(){}
    thermo_evolve( v_map_t& v_map ) : thermo(), my_evolver( v_map )
    {
      d_root_factor = v_map[S_ROOT_FACTOR].as<double>();
      d_root_factor_low = v_map[S_ROOT_FACTOR_LOW].as<double>();
      d_root_factor_t9 = v_map[S_ROOT_FACTOR_T9].as<double>();
      b_root_observe = v_map[S_ROOT_OBSERVE].as<bool>();
      b_thermo_evolve = v_map[S_THERMO_EVOLVE].as<bool>();
      d_tolerance =
        (
          1. - log( v_map[S_TOLERANCE].as<double>() ) / log( 2. )
        ) / std::numeric_limits<double>::digits;
    }

    static double
    computeEntropyRoot(
      double d_x,
      wn_user::network_evolver& my_evolver,
      nnt::Zone& zone,
      Libnucnet__NetView * p_view,
      bool b_observe,
      bool b_evolve
    )
    {

      double d_result;

      gsl_vector * p_abundances, * p_abundance_changes;

      zone.updateProperty(
        nnt::s_T9,
        d_x
      );

      p_abundances = Libnucnet__Zone__getAbundances( zone.getNucnetZone() );

      p_abundance_changes =
        Libnucnet__Zone__getAbundanceChanges(
          zone.getNucnetZone()
        );

      if( b_evolve )
      {
        my_evolver( zone );
      }

      d_result =
        computeEntropy( zone )
        -      
        zone.getProperty<double>( nnt::s_ENTROPY_PER_NUCLEON );

      Libnucnet__Zone__updateAbundances(
        zone.getNucnetZone(),
        p_abundances
      );

      Libnucnet__Zone__updateAbundanceChanges(
        zone.getNucnetZone(),
        p_abundance_changes
      );

      gsl_vector_free( p_abundances );
      gsl_vector_free( p_abundance_changes );

      if( b_observe )
      {
        std::cout << boost::format( "T9: %10.4e  Root: %10.4e\n" ) %
                       d_x % d_result;
      }

      return d_result;

    }
    
    double computeT9FromEntropy(
      nnt::Zone& zone,
      Libnucnet__NetView * p_view,
      double d_t9_guess
    )
    {

      double d_root_f = d_root_factor;
      if( zone.getProperty<double>( nnt::s_T9 ) < d_root_factor_t9 )
      {
        d_root_f = d_root_factor_low;
      }

      return
        nnt::compute_1d_root(
          boost::bind(
            computeEntropyRoot,
            _1,
            my_evolver,
            boost::ref( zone ),
            p_view,
            b_root_observe,
            b_thermo_evolve
          ),
          d_t9_guess,
          d_root_f,
          d_tolerance
        );

    }

  private:
    wn_user::network_evolver my_evolver;
    double d_root_factor;
    double d_root_factor_low;
    double d_root_factor_t9;
    double d_tolerance;
    bool b_root_observe, b_thermo_evolve;

};

//##############################################################################
// get_thermo_evolve_options().
//##############################################################################

class thermo_evolve_options
{

  public:
    thermo_evolve_options(){}

    void
    getOptions( po::options_description& thermo )
    {

      try
      {

        thermo.add_options()

          (
            S_TOLERANCE,
            po::value<double>()->default_value( 1.e-8, "1.e-8" )->notifier(
              boost::bind( &check_tolerance, _1, 1.e-16, 0.1 )
            ),
            "Root tolerance"
          )

          ( S_ROOT_FACTOR, po::value<double>()->default_value( 1.001, "1.001" ),
            "Root expansion factor"
          )

          ( S_ROOT_FACTOR_LOW,
            po::value<double>()->default_value( 1.01, "1.01" ),
            "Low T Root expansion factor"
          )

          ( S_ROOT_FACTOR_T9, po::value<double>()->default_value( 0.5, "0.5" ),
            "T9 to switch to low T root expansion factor"
          )

          ( S_ROOT_OBSERVE, po::value<bool>()->default_value( false, "false" ),
            "Observe root"
          )

          ( S_THERMO_EVOLVE, po::value<bool>()->default_value( false, "false" ),
            "Evolve network during entropy iterations"
          )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

    std::string
    getExample()
    {
      return
        "--" + std::string( S_ROOT_FACTOR ) + " 1.001";
    }

};

}  // namespace base

}  // namespace wn_user

#endif // NNP_THERMO_EVOLVE_HPP

