// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file neutrino_collection.hpp
//! \brief A file to define neutrino routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef NNP_NEUTRINO_COLLECTION_HPP
#define NNP_NEUTRINO_COLLECTION_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// neutrino_collection().
//##############################################################################

class neutrino_collection : public detail::neutrino_collection
{
  public:
    neutrino_collection() : detail::neutrino_collection() {}
    neutrino_collection( v_map_t& v_map ) :
      detail::neutrino_collection( v_map ){}

};

//##############################################################################
// neutrino_collection_options().
//##############################################################################

class neutrino_collection_options : public detail::neutrino_collection_options
{

  public:
    neutrino_collection_options() : detail::neutrino_collection_options() {}

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description neutrino_collection("\nNeutrino Options");

        detail::neutrino_collection_options::getOptions( neutrino_collection );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "neutrino_collection",
            options_struct( neutrino_collection )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }
};

}  // namespace wn_user

#endif  // NNP_NEUTRINO_COLLECTION_HPP
