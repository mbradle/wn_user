// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define default neutrino routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/assign.hpp>
#include <boost/math/special_functions/zeta.hpp>

#include "my_global_types.h"
#include "nnt/iter.h"

#include "neutrino_collection/base/neutrino_collection.hpp"

#ifndef NNP_NEUTRINO_COLLECTION_DETAIL_HPP
#define NNP_NEUTRINO_COLLECTION_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define  S_NU_E_LUM_0         "lum_nu_e_0"
#define  S_NU_E_T             "T_nu_e"
#define  S_NU_E_TAU           "tau_nu_e"
#define  S_NU_E_BAR_LUM_0     "lum_nu_e_bar_0"
#define  S_NU_E_BAR_T         "T_nu_e_bar"
#define  S_NU_E_BAR_TAU       "tau_nu_e_bar"

#define  S_NU_MU_LUM_0        "lum_nu_mu_0"
#define  S_NU_MU_T            "T_nu_mu"
#define  S_NU_MU_TAU          "tau_nu_mu"
#define  S_NU_MU_BAR_LUM_0    "lum_nu_mu_bar_0"
#define  S_NU_MU_BAR_T        "T_nu_mu_bar"
#define  S_NU_MU_BAR_TAU      "tau_nu_mu_bar"

#define  S_NU_TAU_LUM_0       "lum_nu_tau_0"
#define  S_NU_TAU_T           "T_nu_tau"
#define  S_NU_TAU_TAU         "tau_nu_tau"
#define  S_NU_TAU_BAR_LUM_0   "lum_nu_tau_bar_0"
#define  S_NU_TAU_BAR_T       "T_nu_tau_bar"
#define  S_NU_TAU_BAR_TAU     "tau_nu_tau_bar"

//##############################################################################
// neutrino_collection().
//##############################################################################

class neutrino_collection : public base::neutrino_collection
{
  public:
    neutrino_collection() : base::neutrino_collection(){}
    neutrino_collection( v_map_t& v_map ) : base::neutrino_collection()
    {

      getNeutrino(ELECTRON_NEUTRINO).updateProperty(
          nnt::s_NU_LUM_0, v_map[S_NU_E_LUM_0].as<double>()
      );
      getNeutrino(ELECTRON_NEUTRINO).updateProperty(
          nnt::s_NU_T, v_map[S_NU_E_T].as<double>()
      );
      getNeutrino(ELECTRON_NEUTRINO).updateProperty(
          nnt::s_NU_TAU_T, v_map[S_NU_E_TAU].as<double>()
      );

      getNeutrino(ELECTRON_ANTINEUTRINO).updateProperty(
          nnt::s_NU_LUM_0, v_map[S_NU_E_BAR_LUM_0].as<double>()
      );
      getNeutrino(ELECTRON_ANTINEUTRINO).updateProperty(
          nnt::s_NU_T, v_map[S_NU_E_BAR_T].as<double>()
      );
      getNeutrino(ELECTRON_ANTINEUTRINO).updateProperty(
          nnt::s_NU_TAU_T, v_map[S_NU_E_BAR_TAU].as<double>()
      );

      getNeutrino(MU_NEUTRINO).updateProperty(
          nnt::s_NU_LUM_0, v_map[S_NU_MU_LUM_0].as<double>()
      );
      getNeutrino(MU_NEUTRINO).updateProperty(
          nnt::s_NU_T, v_map[S_NU_MU_T].as<double>()
      );
      getNeutrino(MU_NEUTRINO).updateProperty(
          nnt::s_NU_TAU_T, v_map[S_NU_MU_TAU].as<double>()
      );

      getNeutrino(MU_ANTINEUTRINO).updateProperty(
          nnt::s_NU_LUM_0, v_map[S_NU_MU_BAR_LUM_0].as<double>()
      );
      getNeutrino(MU_ANTINEUTRINO).updateProperty(
          nnt::s_NU_T, v_map[S_NU_MU_BAR_T].as<double>()
      );
      getNeutrino(MU_ANTINEUTRINO).updateProperty(
          nnt::s_NU_TAU_T, v_map[S_NU_MU_BAR_TAU].as<double>()
      );

      getNeutrino(TAU_NEUTRINO).updateProperty(
          nnt::s_NU_LUM_0, v_map[S_NU_TAU_LUM_0].as<double>()
      );
      getNeutrino(TAU_NEUTRINO).updateProperty(
          nnt::s_NU_T, v_map[S_NU_TAU_T].as<double>()
      );
      getNeutrino(TAU_NEUTRINO).updateProperty(
          nnt::s_NU_TAU_T, v_map[S_NU_TAU_TAU].as<double>()
      );

      getNeutrino(TAU_ANTINEUTRINO).updateProperty(
          nnt::s_NU_LUM_0, v_map[S_NU_TAU_BAR_LUM_0].as<double>()
      );
      getNeutrino(TAU_ANTINEUTRINO).updateProperty(
          nnt::s_NU_T, v_map[S_NU_TAU_BAR_T].as<double>()
      );
      getNeutrino(TAU_ANTINEUTRINO).updateProperty(
          nnt::s_NU_TAU_T, v_map[S_NU_TAU_BAR_TAU].as<double>()
      );

      BOOST_FOREACH( std::string s, getNeutrinosVector() )
      {
        getNeutrino(s).updateProperty(
          nnt::s_NU_E_AV,
          3.5 * ( boost::math::zeta( 4. ) / boost::math::zeta( 3. ) ) *
          getNeutrino(s).getProperty<double>( nnt::s_NU_T )
        );
      }

    }

    double getAverageEnergyInMeV( std::string s_neutrino, nnt::Zone& zone )
    { 
      return
        getNeutrino( s_neutrino ).getProperty<double>( nnt::s_NU_E_AV );
    }

    double getAverageEnergyInErgs( std::string s_neutrino, nnt::Zone& zone )
    { 
      return
        getNeutrino( s_neutrino ).getProperty<double>( nnt::s_NU_E_AV ) *
          GSL_CONST_NUM_MEGA * GSL_CONST_CGSM_ELECTRON_VOLT;
    }

    double getTemperature( std::string s_neutrino, nnt::Zone& zone )
    { 
      return getNeutrino( s_neutrino ).getProperty<double>( nnt::s_NU_T );
    }

    double
    computeNeutrinoFlux( std::string s_neutrino, nnt::Zone& zone )
    {

      return
        getNeutrino( s_neutrino ).getProperty<double>( nnt::s_NU_LUM_0 ) *
        exp(
          -zone.getProperty<double>( nnt::s_TIME ) /
          getNeutrino( s_neutrino ).getProperty<double>( nnt::s_NU_TAU_T )
        )
        /
        (
          4. * M_PI *
          gsl_pow_2( zone.getProperty<double>( nnt::s_RADIUS ) ) *
          ( getAverageEnergyInErgs( s_neutrino, zone ) )
        );
    }

};

//##############################################################################
// neutrino_collection_options().
//##############################################################################

class neutrino_collection_options
{

  public:
    neutrino_collection_options() {}

    void
    getOptions( po::options_description& neutrino_collection )
    {

      try
      {

        po::options_description electron( "Electron neutrinos" );

        electron.add_options()

//      Electron neutrinos

        (
          S_NU_E_LUM_0,
          po::value<double>()->default_value( 1.67e52, "1.67e52" ),
          "Initial electron-type neutrino luminosity (ergs/s)"
        )

        (
          S_NU_E_TAU,
          po::value<double>()->default_value( 3., "3." ),
          "Electron-type neutrino luminosity e-folding time (s)"
        )

        (
          S_NU_E_T,
          po::value<double>()->default_value( 4., "4." ),
          "Electron-type neutrino temperature (MeV)"
        )

        ;

//      Electron anti-neutrinos

        po::options_description electron_bar( "Electron anti-neutrinos" );

        electron_bar.add_options()

        (
          S_NU_E_BAR_LUM_0,
          po::value<double>()->default_value( 1.67e52, "1.67e52" ),
          "Initial electron-type anti-neutrino luminosity (ergs/s)"
        )

        (
          S_NU_E_BAR_TAU,
          po::value<double>()->default_value( 3., "3." ),
          "Electron-type anti-neutrino luminosity e-folding time (s)"
        )

        (
          S_NU_E_BAR_T,
          po::value<double>()->default_value( 4., "4." ),
          "Electron-type anti-neutrino temperature (MeV)"
        )

        ;

//      Mu neutrinos

        po::options_description mu( "Muon neutrinos" );

        mu.add_options()

        (
          S_NU_MU_LUM_0,
          po::value<double>()->default_value( 1.67e52, "1.67e52" ),
          "Initial mu-type neutrino luminosity (ergs/s)"
        )

        (
          S_NU_MU_TAU,
          po::value<double>()->default_value( 3., "3." ),
          "Mu-type neutrino luminosity e-folding time (s)"
        )

        (
          S_NU_MU_T,
          po::value<double>()->default_value( 6., "6." ),
          "Mu-type neutrino temperature (MeV)"
        )

        ;

//      Mu anti-neutrinos

        po::options_description mu_bar( "Muon anti-neutrinos" );

        mu_bar.add_options()

        (
          S_NU_MU_BAR_LUM_0,
          po::value<double>()->default_value( 1.67e52, "1.67e52" ),
          "Initial mu-type anti-neutrino luminosity (ergs/s)"
        )

        (
          S_NU_MU_BAR_TAU,
          po::value<double>()->default_value( 3., "3." ),
          "Mu-type anti-neutrino luminosity e-folding time (s)"
        )

        (
          S_NU_MU_BAR_T,
          po::value<double>()->default_value( 6., "6." ),
          "Mu-type anti-neutrino temperature (MeV)"
        )

        ;

//      Tau neutrinos

        po::options_description tau( "Tauon neutrinos" );

        tau.add_options()

        (
          S_NU_TAU_LUM_0,
          po::value<double>()->default_value( 1.67e52, "1.67e52" ),
          "Initial tau-type neutrino luminosity (ergs/s)"
        )

        (
          S_NU_TAU_TAU,
          po::value<double>()->default_value( 3., "3." ),
          "Tau-type neutrino luminosity e-folding time (s)"
        )

        (
          S_NU_TAU_T,
          po::value<double>()->default_value( 6., "6." ),
          "Tau-type neutrino temperature (MeV)"
        )

        ;

//      Tau anti-neutrinos

        po::options_description tau_bar( "Tauon anti-neutrinos" );

        tau_bar.add_options()

        (
          S_NU_TAU_BAR_LUM_0,
          po::value<double>()->default_value( 1.67e52, "1.67e52" ),
          "Initial tau-type anti-neutrino luminosity (ergs/s)"
        )

        (
          S_NU_TAU_BAR_TAU,
          po::value<double>()->default_value( 3., "3." ),
          "Tau-type anti-neutrino luminosity e-folding time (s)"
        )

        (
          S_NU_TAU_BAR_T,
          po::value<double>()->default_value( 6., "6." ),
          "Tau-type anti-neutrino temperature (MeV)"
        )

        ;

        neutrino_collection.add( electron ).add( electron_bar ).add( mu ).add( mu_bar ).add( tau ).add( tau_bar );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }
};

}  // namespace detail

}  // namespace wn_user

#endif  // NNP_NEUTRINO_COLLECTION_DETAIL_HPP
