////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file rate_modifier.hpp
//! \brief A file to define rate modification routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/algorithm/string.hpp>

#include "my_global_types.h"
#include "nnt/iter.h"
#include "user/thermo.h"

#ifndef NNP_RATE_MODIFIER_DETAIL_HPP
#define NNP_RATE_MODIFIER_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// modify_beta_decay().
//##############################################################################

void
modify_beta_decay(
  nnt::Zone& zone,
  Libnucnet__Reaction * p_reaction,
  double& d_forward,
  double& d_reverse
)
{

  double d_QkT =
    Libnucnet__Net__computeReactionQValue(
      Libnucnet__Zone__getNet( zone.getNucnetZone() ),
      p_reaction
    ) / nnt::compute_kT_in_MeV( zone.getProperty<double>( nnt::s_T9 ) );

  double d_factor =
    1. / ( exp( zone.getProperty<double>( nnt::s_MUEKT ) - d_QkT ) + 1. );

  d_forward *= d_factor;
  d_reverse *= d_factor;

}

//##############################################################################
// modify_beta_decay_rates().
//##############################################################################

void
modify_beta_decay_rates( nnt::Zone& zone )
{

  zone.updateProperty(
    nnt::s_MUEKT,
    user::compute_electron_chemical_potential_kT( zone )
  );

  Libnucnet__NetView * p_evolve_view = zone.getNetView( EVOLUTION_NETWORK );

  std::vector<Libnucnet__Reaction *> v_reactions;

  BOOST_FOREACH(
    nnt::Reaction reaction,
    nnt::make_reaction_list(
      Libnucnet__Net__getReac( Libnucnet__NetView__getNet( p_evolve_view ) )
    )
  )
  {
    BOOST_FOREACH(
      nnt::ReactionElement product,
      nnt::make_reaction_product_list( reaction.getNucnetReaction() )
    )
    {
      if(
        std::string(
          Libnucnet__Reaction__Element__getName(
            product.getNucnetReactionElement()
          ) 
        ) == "electron"
      )
      {
        v_reactions.push_back( reaction.getNucnetReaction() );
        break;
      }
    }
  }

  BOOST_FOREACH(
    Libnucnet__Reaction * p_reaction,
    v_reactions
  )
  {

    double d_forward, d_reverse;

    Libnucnet__Zone__getRatesForReaction(
      zone.getNucnetZone(),
      p_reaction,
      &d_forward,
      &d_reverse
    );

    modify_beta_decay(
      zone,
      p_reaction,
      d_forward,
      d_reverse
    );

    Libnucnet__Zone__updateRatesForReaction(
      zone.getNucnetZone(),
      p_reaction,
      d_forward,
      d_reverse
    );

  }

}
 
//##############################################################################
// rate_modifier().
//##############################################################################

class rate_modifier
{

  public:
    rate_modifier(){}
    rate_modifier( v_map_t& v_map ){ b_set = false; }

    void operator()( nnt::Zone& zone )
    {
      if( !b_set )
      {
        zone.updateFunction(
          nnt::s_RATES_MODIFICATION_FUNCTION,
          static_cast<
            boost::function<
              void(
                nnt::Zone&,
                Libnucnet__Reaction *,
                double&,
                double&
              )
            >
          >( modify_beta_decay )
        );
        zone.updateFunction(
          nnt::s_RATES_MODIFICATION_FUNCTION,
          static_cast<boost::function<void()> >(
            boost::bind( modify_beta_decay_rates, boost::ref( zone ) )
          )
        );
        b_set = true;
      }
    }

  private:
    bool b_set;

};

//##############################################################################
// rate_modifier_options().
//##############################################################################

class rate_modifier_options
{

  public:
    rate_modifier_options(){}

    void
    getOptions( po::options_description& rate_modifier ){}

};

}  // namespace detail

}  // namespace wn_user

#endif // NNP_RATE_MODIFIER_DETAIL_HPP
