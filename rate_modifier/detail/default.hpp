////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file rate_modifier.hpp
//! \brief A file to define rate modification routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/algorithm/string.hpp>

#include "my_global_types.h"
#include "nnt/iter.h"
#include "user/rate_modifiers.h"

#ifndef NNP_RATE_MODIFIER_DETAIL_HPP
#define NNP_RATE_MODIFIER_DETAIL_HPP

#define S_RATE_MOD      "rate_mod"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

typedef boost::tuple<std::string, std::string, double> rate_mod_tuple_t;
typedef std::vector<rate_mod_tuple_t> v_rate_mod_t;

//##############################################################################
// rate_modifier().
//##############################################################################

class rate_modifier
{

  public:
    rate_modifier(){}
    rate_modifier( v_map_t& v_map ) : my_program_options()
    {

      b_set = false;

      if( v_map.count( S_RATE_MOD ) )
      {

        BOOST_FOREACH(
          const std::vector<std::vector<std::string> >::value_type& v_m,
          my_program_options.composeOptionVectorOfVectors(
            v_map, S_RATE_MOD, 3
          )
        )
        {

          v_rate_mod.push_back(
            rate_mod_tuple_t(
              v_m[0], v_m[1], boost::lexical_cast<double>( v_m[2] )
            ) 
          );

        }
      }

    }

    void operator()( nnt::Zone& zone )
    {
      if( !b_set )
      {
        setZoneRateModifier( zone );
        b_set = true;
        printModifiedRates( zone );
      }
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      if( !b_set )
      {
        for( size_t i = 0; i < zones.size(); i++ )
        {
          setZoneRateModifier( zones[i] );
        }
        b_set = true;
        printModifiedRates( zones[0] );
      }
    }

  private:
    v_rate_mod_t v_rate_mod;
    bool b_set;
    program_options my_program_options;

    void setZoneRateModifier( nnt::Zone& zone )
    {
      for( size_t i = 0; i < v_rate_mod.size(); i++ )
      {

        std::string s_i = boost::lexical_cast<std::string>( i );

        zone.updateProperty(
          nnt::s_NUC_XPATH,
          nnt::s_RATE_MODIFICATION_VIEW,
          s_i,
          v_rate_mod[i].get<0>()
        );

        zone.updateProperty(
          nnt::s_REAC_XPATH,
          nnt::s_RATE_MODIFICATION_VIEW,
          s_i,
          v_rate_mod[i].get<1>()
        );

        zone.updateProperty(
          nnt::s_FACTOR,
          nnt::s_RATE_MODIFICATION_VIEW,
          s_i,
          v_rate_mod[i].get<2>()
        );

      }

    }

    void printModifiedRates( nnt::Zone& zone )
    {
      if( v_rate_mod.size() != 0 )
      {
        user::print_modified_reactions( zone );
      }
    }

};

//##############################################################################
// rate_modifier_options().
//##############################################################################

class rate_modifier_options
{

  public:
    rate_modifier_options(){}

    void
    getOptions( po::options_description& rate_modifier )
    {

      try
      {

        rate_modifier.add_options()

          (
            S_RATE_MOD,
            po::value<std::vector<std::string> >()->multitoken(),
            "Rate modifier triplet (enter as {nuc_xpath; reac_xpath; factor})"
          )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

}  // namespace detail

}  // namespace wn_user

#endif // NNP_RATE_MODIFIER_DETAIL_HPP
