////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define flow routines.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.

#include <boost/bind/bind.hpp>

#include "flow_computer/base/flow_computer.hpp"

#ifndef NNP_FLOW_COMPUTER_DETAIL_HPP
#define NNP_FLOW_COMPUTER_DETAIL_HPP

/**
 * @brief A namespace for user-defined flow functions.
 */
namespace wn_user
{

namespace detail
{

#define S_FLOW_THRESHOLD     "flow_threshold"

class flow_computer : public base::flow_computer
{

  public:

    flow_computer() : base::flow_computer(){}
    flow_computer( v_map_t& v_map ) : base::flow_computer( v_map )
    {
      d_flow_threshold = v_map[S_FLOW_THRESHOLD].as<double>();
    }

    std::map<std::string, std::pair<double, double> >
    computeFlowMap( nnt::Zone& zone )
    {

      std::map<std::string, std::pair<double, double> > result;

      computeRates( zone );

      Libnucnet__NetView * p_view =
        Libnucnet__Zone__getEvolutionNetView( zone.getNucnetZone() );

      BOOST_FOREACH(
        nnt::Reaction reaction, 
        nnt::make_reaction_list(
          Libnucnet__Net__getReac(
            Libnucnet__NetView__getNet( p_view )
          )
        )
      )
      {

        double d_f, d_r, d_forward_rate, d_reverse_rate;

        Libnucnet__Reaction * p_reaction = reaction.getNucnetReaction();

        Libnucnet__Zone__getRatesForReaction(
          zone.getNucnetZone(),
          p_reaction,
          &d_forward_rate,
          &d_reverse_rate
        );

        nnt::reaction_element_list_t reactant_list =
          nnt::make_reaction_nuclide_reactant_list( p_reaction );

        d_f =
          d_forward_rate
          *
          computeReactionAbundanceProduct( zone, reactant_list ) 
          /
          Libnucnet__Reaction__getDuplicateReactantFactor( p_reaction );

        nnt::reaction_element_list_t product_list =
          nnt::make_reaction_nuclide_product_list( p_reaction );

        d_r =
          d_reverse_rate
          *
          computeReactionAbundanceProduct( zone, product_list ) 
          /
          Libnucnet__Reaction__getDuplicateProductFactor( p_reaction );

        if( b_threshold_net )
        {
          if( fabs( d_f - d_r ) >= d_flow_threshold )
          {
            result[Libnucnet__Reaction__getString( p_reaction )] =
              std::make_pair( d_f, d_r );
          }
        }
        else
        {
          if( d_f >= d_flow_threshold || d_r >= d_flow_threshold )
          {
            result[Libnucnet__Reaction__getString( p_reaction )] =
              std::make_pair( d_f, d_r );
          }
        }

      }

      return result;

    }

  private:
    rate_computer my_rate_computer;
    double d_flow_threshold;
    bool b_threshold_net;

};

//##############################################################################
// flow_computer_options().
//##############################################################################

class flow_computer_options
{

  public:
    flow_computer_options(){}

    void
    getDetailOptions( po::options_description& flow_computer )
    {

      try
      {

        flow_computer.add_options()

          (
            S_FLOW_THRESHOLD,
            po::value<double>()->default_value( 1.e-25, "1.e-25" ),
            "Threshold for cut-off of small flows."
          )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};


}  // namespace detail

}  // namespace wn_user

#endif // NNP_FLOW_COMPUTER_DETAIL_HPP
