////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file flow_computer.hpp
//! \brief A file to define flow_computation routines.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.

#ifndef NNP_FLOW_COMPUTER_BASE_HPP
#define NNP_FLOW_COMPUTER_BASE_HPP

#include "my_global_types.h"
#include "nnt/iter.h"

/**
 * @brief A namespace for user-defined rate functions.
 */
namespace wn_user
{

namespace base
{

class flow_computer
{

  public:

    flow_computer(){}
    flow_computer( v_map_t& v_map ) : my_rate_computer( v_map ){}

    void
    computeRates( nnt::Zone& zone )
    {
      my_rate_computer.computeRates( zone );
    }

    double
    computeReactionAbundanceProduct(
      nnt::Zone& zone,
      nnt::reaction_element_list_t& element_list,
      Libnucnet__Species * p_species_to_exclude
    )
    {

      double d_result = 1;
      Libnucnet__Species * p_exclude = p_species_to_exclude;

      BOOST_FOREACH( nnt::ReactionElement element, element_list )
      {

        Libnucnet__Species * p_species =
          Libnucnet__Nuc__getSpeciesByName(
            Libnucnet__Net__getNuc(
              Libnucnet__Zone__getNet( zone.getNucnetZone() )
            ),
            Libnucnet__Reaction__Element__getName(
              element.getNucnetReactionElement()
            )
          );

        if( p_species != p_exclude )
        {
          d_result *=
            Libnucnet__Zone__getSpeciesAbundance(
              zone.getNucnetZone(), p_species
            );
        }
        else
        {
          p_exclude = NULL;   // To exclude species only once.
        }

      }

      return d_result;

    }

    double
    computeReactionAbundanceProduct(
      nnt::Zone& zone,
      nnt::reaction_element_list_t& element_list
    )
    {
      return computeReactionAbundanceProduct( zone, element_list, NULL );
    }

    double
    computeRhoProduct( double dRho, nnt::reaction_element_list_t& element_list )
    {
      return pow( dRho, (double) element_list.size() - 1. );
    }

  private:
    wn_user::rate_computer my_rate_computer;

};

}  // base

}  // namespace wn_user

#endif // NNP_FLOW_COMPUTER_BASE_HPP
