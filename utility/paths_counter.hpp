////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022 Clemson University.
//
// This file was originally written by Bradley S. Meyer and Norberto J. Davila.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#ifndef WN_PATHS_COUNTER_HPP
#define WN_PATHS_COUNTER_HPP

#include <exception>

#include "utility/adjacency_matrix_maker.hpp"

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class paths_counter
///
/// \brief A class to count paths between vertics in a graph.
///
////////////////////////////////////////////////////////////////////////////////

template
<
  typename Graph,
  typename Vertex = typename boost::graph_traits<Graph>::vertex_descriptor
>
class paths_counter
{
  public:
    paths_counter(){}

/// The paths counter.
/// \param start The start vertex for the path.
/// \param end The end vertex for the path.
/// \param g The graph.
/// \param N The maximum number of arcs in a path (default N = 1000).
/// \param b_show_progress Show the progress (default false).
/// \return A vector of double elements.  The index with element n
///         gives the number of paths with n arcs between the start and
///         end vertex.

    std::vector<double>
    operator()( Vertex from, Vertex to, Graph const& g, size_t N = 1000,
                bool b_show_progress = false )
    {
      typedef
        typename boost::property_map<Graph, boost::vertex_index_t>::const_type
          IndexMap;

      IndexMap im = boost::get( boost::vertex_index, g );

      typedef typename boost::property_traits<IndexMap>::value_type index;

      index i_start = im[from];
      index i_end = im[to];

      std::vector<double> result;

      result.push_back( 0 );

      WnMatrix * p_work;
      adjacency_matrix_maker<Graph> my_adj_maker;
      WnMatrix * p_adjacency_matrix = my_adj_maker( g );

      p_work = WnMatrix__getCopy( p_adjacency_matrix ); 

      result.push_back(
        WnMatrix__getElement( p_work, i_start + 1, i_end + 1 )
      );

      for( size_t n = 2; n < N; n++ )
      {
        WnMatrix * p_tmp =
          WnMatrix__computeMatrixTimesMatrix( p_adjacency_matrix, p_work );

        double d_value = WnMatrix__getElement( p_work, i_start + 1, i_end + 1 );

        if( b_show_progress )
        {
          std::cout << n << "  " << d_value << std::endl;
        }

        result.push_back( d_value );

        WnMatrix__free( p_work );
        p_work = p_tmp; 
      }

      WnMatrix__free( p_work );
      WnMatrix__free( p_adjacency_matrix );

      return result;

    }

};


}  // namespace utility

}  // namespace wn_user

#endif // WN_PATHS_COUNTER_HPP
