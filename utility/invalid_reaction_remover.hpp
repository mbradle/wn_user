////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#ifndef WN_INVALID_REACTION_REMOVER_HPP
#define WN_INVALID_REACTION_REMOVER_HPP

#include <Libnucnet.h>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class invalid_reaction_remover
///
/// \brief A class to remove invalid reactions from a network.
///
/// Invalid reactions are ones that do not conserve charge, baryon number, 
/// or lepton number or for which not all reactants or products are in
/// the nuclide collection for the network.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// invalid_reaction_remover().
//##############################################################################

class invalid_reaction_remover
{

  public:
    invalid_reaction_remover(){}

/// The reaction remover.
/// \param p_net The network.
/// \return On successful return, the invalid reactions have been removed
///         from p_net.

    void operator()( Libnucnet__Net * p_net )
    {
      nnt::reaction_list_t reactions =
        nnt::make_reaction_list(
          Libnucnet__Net__getReac( p_net )
        );
      for(
        nnt::reaction_list_t::iterator it = reactions.begin();
        it != reactions.end();
        it++
      )
      {
        if(
          !Libnucnet__Net__isValidReaction(
             p_net,
             it->getNucnetReaction()
          )
        )
        {
          Libnucnet__Reac__removeReaction(
            Libnucnet__Net__getReac( p_net ),
            it->getNucnetReaction()
          );
        }
      }
    }

};

}  // namespace utility

}  // namespace wn_user

#endif // WN_INVALID_REACTION_REMOVER_HPP
