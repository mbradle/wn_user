////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020-2021 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#ifndef WN_DAG_PATH_HPP
#define WN_DAG_PATH_HPP

#include <limits>
#include <exception>

#include <boost/graph/tiernan_all_cycles.hpp>
#include <boost/graph/depth_first_search.hpp>
#include <boost/graph/filtered_graph.hpp>
#include <boost/graph/iteration_macros.hpp>
#include <boost/numeric/odeint.hpp>
#include <boost/math/special_functions/gamma.hpp>

#include "math/bell.hpp"
#include "math/set_utilities.hpp"

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class dag_checker
///
/// \brief A class to check that a graph is acyclic.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// dag_checker().
//##############################################################################

template <class Graph>
class dag_checker
{
  public:
    dag_checker(){}

/// The acyclic graph checker.
/// \return A bool indicating whether the underlying graph is acyclic (True)
//          or not (False).

    bool
    operator()( const Graph& g )
    {
      bool is_acyclic = true;
      cycle_detector vis( is_acyclic );
      boost::depth_first_search( g, boost::visitor( vis ) );
      return is_acyclic;
    }

  private:

    struct cycle_detector : public boost::dfs_visitor<>
    {
      cycle_detector( bool& is_acyclic ) : m_is_acyclic( is_acyclic ) {}

      template <class Edge>
      void back_edge( const Edge, const Graph& ) { m_is_acyclic = false; }
      protected:
        bool& m_is_acyclic;
    };

};

////////////////////////////////////////////////////////////////////////////////
///
/// \class dag_path_weights_retriever
///
/// \brief A class to retrieve the weights along a path.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// dag_path_weights_retriever().
//##############################################################################

template<
  typename Graph,
  typename Edge,
  typename WeightMap = 
    typename boost::property_map<Graph, boost::edge_weight_t>::type,
  typename Weight =
    typename boost::property_traits<WeightMap>::value_type
>
class dag_path_weights_retriever
{
  public:

/// The constructor.
    dag_path_weights_retriever(){}

/// The retriever.
/// \param g The graph.
/// \param v_p A vector containing the edges along the input path.
/// \return A pair containing 1) a vector of the weights along the path
///         from the start vertex to end vertex and 2) a vector of the sum
///         of weights of out edges from each vertex in the path.

    std::pair<std::vector<Weight>, std::vector<Weight> >
    operator()( Graph& g, std::vector<Edge>& v_p )
    {
      WeightMap wm = boost::get(boost::edge_weight, g);
      std::vector<Weight> v_wp, v_w;

      for( size_t i = 0; i < v_p.size(); i++ )
      {
        v_wp.push_back( wm[v_p[i]] );
        Weight sum = 0;
        BGL_FORALL_OUTEDGES_T( boost::source( v_p[i], g ), e, g, Graph )
        {
          sum += wm[e];
        }
        v_w.push_back( sum );
      }

      Weight sum = 0;
      BGL_FORALL_OUTEDGES_T(
        boost::target( v_p[v_p.size()-1], g ), e, g, Graph
      )
      {
        sum += wm[e];
      }
      v_w.push_back( sum );

      return std::make_pair( v_wp, v_w );
    }

};

template <typename T>
struct my_eqs
{
  std::vector<T> m_wp, m_w;

  my_eqs( std::vector<T>& v_wp, std::vector<T>& v_w ) :
     m_wp( v_wp ), m_w( v_w ) {}

  void operator()( const std::vector<T>& g, std::vector<T>& dgdt, const T t )
  {
    dgdt[0] = -m_w[0] * g[0];
    for( size_t i = 1; i < g.size(); i++ )
    {
      dgdt[i] = m_wp[i-1] * g[i-1] - m_w[i] * g[i];
    }
  }
};

template<typename Weight>
class my_type
{
  public:
    my_type( Weight& _w, size_t _i ) : w( _w ), i( _i ) {}

    Weight get_weight() const
    {
      return w;
    }

    size_t get_index() const
    {
      return i;
    }

    bool operator<( const my_type& rhs ) const
    {
      return this->w < rhs.w;
    }

  private:
    Weight w;
    size_t i;
    
};

//##############################################################################
// dag_path_storer().
//##############################################################################

template
<
  typename Graph,
  typename Edge,
  typename WeightMap = 
    typename boost::property_map<Graph, boost::edge_weight_t>::const_type,
  typename Weight =
    typename boost::property_traits<WeightMap>::value_type
>
class dag_path_storer
{
  public:
    dag_path_storer( Graph& g, std::vector<Edge>& v_p,
                     Weight cut = std::numeric_limits<Weight>::infinity() )
    {
      boost::tie( v_wp, v_w ) = my_retriever( g, v_p );
      last_alpha = v_w[v_w.size()-1];
      path_factor = computePathFactor();
      Weight sum = 0;
      for( size_t i = 0; i < v_w.size() - 1; i++ )
      {
        sum += log( v_w[i] );
      }
      a_g = exp( sum / ( v_w.size() - 1 ) );
      a_a = std::accumulate( v_w.begin(), v_w.end(), (Weight) 0 ) / v_w.size();
      a_max = *std::max_element( v_w.begin(), v_w.end() );
      if( cut < std::numeric_limits<Weight>::infinity() )
      {
        setSteadyFlow( cut );
      }
    }

/// Method to return path weights.
/// \return The vector of values.

    std::vector<Weight>
    getPathWeights()
    {
      return v_wp;
    }

/// Method to return full weights along path.
/// \return The vector of values.

    std::vector<Weight>
    getPathFullWeights()
    {
      return v_w;
    }

/// Method to return maximum full alpha along the path.
/// \return The value.

    Weight
    getAlphaMax()
    {
      return a_max;
    }

/// Method to return the geometric mean of the particular path alphas.
/// \return The value.

    Weight
    getAlpha() { return a_g; }

/// Method to return the arithmetic mean of the full alphas along the path.
/// \return The value.

    Weight
    getMean() { return a_a; }

/// Method to return the prefactor for the path.
/// \return The value.

/// Method to return the path factor.
/// \return The value.

    Weight
    getPathFactor() { return path_factor; }
      
/// Method to return the moments of a path.
/// \param M The highest number moment to retrieve.
/// \return The vector of values of length M+1.  If any input alpha is zero, all
///         moment elements are set to zero.

    std::vector<Weight>
    getMoments( size_t M )
    {
      if( std::count(v_w.begin(), v_w.end(), 0 ) )
      {
        return std::vector<Weight>( M+1, 0 );
      }

      std::vector<Weight> v_i;
      for( size_t i = 0; i < v_w.size(); i++ )
      {
        v_i.push_back( 1. / v_w[i] );
      }
      wn_user::math::multi_set<Weight> ms;
      std::vector<Weight> result = ms.compute_sums( M, v_i );
      for( size_t i = 0; i < result.size(); i++ )
      {
        result[i] *= exp( boost::math::lgamma( i + 1 ) ) * path_factor / last_alpha;
      }
      return result;
    }

/// Method to return the central moments of a path.
/// \param M The highest number moment to retrieve.
/// \return The vector of values of length M+1.  If any input alpha is zero, all
///         moment elements are set to zero.

    std::vector<Weight>
    getCentralMoments( size_t M )
    {
      if( std::count(v_w.begin(), v_w.end(), 0 ) )
      {
        return std::vector<Weight>( M+1, 0 );
      }

      std::vector<Weight> v_i, result;
      for( size_t i = 0; i < v_w.size(); i++ )
      {
        v_i.push_back( 1. / v_w[i] );
      }
      wn_user::math::multi_set<Weight> ms;
      std::vector<Weight> v_ms = ms.compute_sums( M, v_i );
      wn_user::math::power_set<Weight> ps;
      std::vector<Weight> v_ps = ps.compute_sums( M, v_i );
      for( size_t n = 0; n <= M; n++ )
      {
        Weight sum = 0;
        for( size_t q = 0; q <= n; q++ )
        {
          sum += 
              exp(
                boost::math::lgamma( float( n + 1 ) ) -
                boost::math::lgamma( float( q + 1 ) )
              ) *
              pow( -v_ps[1], q ) * v_ms[n-q]; 
        }
        result.push_back( sum * path_factor / last_alpha );
      }
      return result;
        
    }

  private:
    std::vector<Weight> v_wp, v_w;
    Weight a_a, a_g, a_max, path_factor, last_alpha;
    dag_path_weights_retriever<Graph, Edge> my_retriever;

    Weight
    computePathFactor()
    {
      Weight result = 1;
      for( size_t i = 0; i < v_wp.size(); i++ )
      {
        result *= (v_wp[i] / v_w[i]);
      }
      return result;
    }
      
    void
    setSteadyFlow( Weight cut )
    {

      Weight w_N = v_w[v_w.size()-1];
      Weight a_min = *std::min_element( v_w.begin(), v_w.end() );

      std::vector<Weight> v_y;
      for( size_t i = 0; i < v_w.size() - 1; i++ )
      {
        if( v_w[i] < a_min * cut )
        {
          v_y.push_back( v_w[i] );
        }
      }

      a_g = 0;
      if( v_y.size() > 0 )
      {
        for( size_t i = 0; i < v_y.size(); i++ )
        {
          a_g += log( v_y[i] );
        }
        a_g = exp( a_g / v_y.size() );
      }

      v_w = v_y;

      if( w_N < a_min * cut )
      {
        v_w.push_back( w_N );
      }
      else
      {
        a_g =
          exp(
            ( v_w.size() / ( v_w.size() - 1. ) ) * log( a_g ) -
            ( 1. / ( v_w.size() - 1. ) ) * log( w_N )
          );
      }

      a_a = std::accumulate( v_w.begin(), v_w.end(), (Weight) 0 ) / v_w.size();
      a_max = *std::max_element( v_w.begin(), v_w.end() );

    }

};

////////////////////////////////////////////////////////////////////////////////
///
/// \class dag_path_evolver
///
/// \brief A class to evolve the probability of the end vertex of a path
///        by integration.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// dag_path_evolver().
//##############################################################################

template
<
  typename Graph,
  typename Edge,
  typename WeightMap = 
    typename boost::property_map<Graph, boost::edge_weight_t>::const_type,
  typename Weight =
    typename boost::property_traits<WeightMap>::value_type
>
class dag_path_evolver : public dag_path_storer<Graph, Edge>
{
  public:

/// The constructor.
/// \param g The graph.
/// \param v_p A vector containing the edges along the input path.

    dag_path_evolver( Graph& g, std::vector<Edge>& v_p ) :
      dag_path_storer<Graph, Edge>( g, v_p ){}

/// The evolver.
/// \param tau The evolution parameter.
/// \return The evolved probability of the end vertex at the input evolution
///         parameter.  The type is the same as the weight type of the graph
///         edges.

    Weight operator()( Weight& tau )
    {

      std::vector<Weight> v_wp = (*this).getPathWeights();
      std::vector<Weight> v_w = (*this).getPathFullWeights();
      std::vector<Weight> g;

      g.push_back( 1. );

      for( size_t i = 1; i < v_w.size(); i++ )
      {
        g.push_back( 0. );
      }

      my_eqs<Weight> f(v_wp, v_w);

      boost::numeric::odeint::integrate( f, g , 0.0 , tau, 1.e-4 );        
      return g[g.size()-1];

    }

};

////////////////////////////////////////////////////////////////////////////////
///
/// \class dag_path_expansion_evolver
///
/// \brief A class to evolve the probability of the end vertex of a path
///        by use of the exact expansion solution.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// dag_path_expansion_evolver().
//##############################################################################


template
<
  typename Graph,
  typename Edge,
  typename WeightMap =
    typename boost::property_map<Graph, boost::edge_weight_t>::const_type,
  typename Weight =
    typename boost::property_traits<WeightMap>::value_type
>
class dag_path_expansion_evolver : public dag_path_storer<Graph, Edge>
{
  public:

/// The constructor.
/// \param g The graph.
/// \param v_p A vector containing the edges along the input path.
/// \param M The number of terms to include in the expansion.
/// \param cut The cutoff parameter for steady flow.

    dag_path_expansion_evolver( Graph& g, std::vector<Edge>& v_p,
                           size_t M = 100,
                           Weight cut = std::numeric_limits<Weight>::infinity() ) :
                           dag_path_storer<Graph, Edge>( g, v_p, cut )
    {
      v_w = (*this).getPathFullWeights();
      Weight a_max = (*this).getAlphaMax();
      v_b = ms.compute_normalized_sums( M, v_w );
      v_w_scaled = scale_vector( v_w, a_max );
      v_b_scaled = ms.compute_normalized_sums( M, v_w_scaled );
    }

/// The evolver.
/// \param tau The evolution parameter.
/// \return The evolved probability of the end vertex at the input evolution
///         parameter.  The type is the same as the weight type of the graph
///         edges.

    Weight operator()( Weight& tau )
    {
      Weight tmp, result = 0, a_max = (*this).getAlphaMax(),
             a_g = (*this).getAlpha(), path_factor = (*this).getPathFactor();;
      if( tau == 0 ) { return 0; }

      if( v_w_scaled.size() == 1 )
      {
        return path_factor * exp( -a_max * tau );
      }

      for( size_t i =  0; i < v_b.size(); i++ )
      {
        tmp = v_b_scaled[i] *
              exp( -a_max * tau + i * log( a_max * tau ) -
              boost::math::lgamma( i + 1 ) );
        result += tmp;
        if( abs( tmp / result ) < 1.e-16 ) {break;}
      }
      return path_factor * exp( log( result ) +
               ( v_w_scaled.size() - 1 ) * log( a_g * tau ) -
               boost::math::lgamma( v_w_scaled.size() ) );
    }

/// Method to return the Bell polynomial x's for the path.
/// \return A vector of the path x values.

    std::vector<Weight>
    getX() { return bell.invert( v_b ); }

/// Method to return the Bell polynomial x's for the path as scaled to
/// the maximum path alpha.
/// \return A vector of the path x values.

    std::vector<Weight>
    getScaledX() { return bell.invert( v_b_scaled ); }

/// Method to return the Bell polynomials for the path.
/// \return A vector of the values.

    std::vector<Weight>
    getB()
    {
      return v_b;
    }

/// Method to return the Bell polynomials for the path for the scaled alphas.
/// \return A vector of the values.

    std::vector<Weight>
    getScaledB()
    {
      return v_b_scaled;
    }

  private:
    std::vector<Weight> v_w, v_w_scaled, v_b, v_b_scaled;
    wn_user::math::multi_set<Weight> ms;
    wn_user::math::bell<Weight> bell;

    std::vector<Weight>
    scale_vector( std::vector<Weight>& v, Weight a_max )
    {
      std::vector<Weight> result;
      for( size_t i = 0; i < v.size(); i++ )
      {
        result.push_back( 1. - v[i] / a_max );
      }
      return result;
    }

};

}  // namespace utility

}  // namespace wn_user

#endif // WN_DAG_PATH_HPP
