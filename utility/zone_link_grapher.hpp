////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#ifndef WN_ZONE_LINK_GRAPHER_HPP
#define WN_ZONE_LINK_GRAPHER_HPP

#include <boost/format.hpp>
#include <boost/graph/graphviz.hpp>

#include <Libnucnet.h>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class zone_link_grapher
///
/// \brief A class to write out zone links as a graphviz file.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// zone_link_grapher().
//##############################################################################

class zone_link_grapher
{

  public:
    zone_link_grapher(){}

/// The grapher
/// \param g The input graph.
/// \param s_file The output graphviz file.

    template <typename Graph>
    void
    operator()( Graph& g, std::string s_file )
    {
      std::ofstream my_file( s_file.c_str() );

      boost::write_graphviz(
        my_file,
        g,
        vertex_writer<Graph>( g ),
        edge_writer<Graph>( g )
      );
      my_file.close();
    }

  private:

    template <typename Graph>
    struct
    vertex_writer
    {
      vertex_writer( Graph& g_ ) :          g( g_ ) {};

      template <class Vertex>
      void operator()( std::ostream& out, Vertex v )
      {
        out <<
          boost::format( "[label=\"(%s, %s, %s)\" ]" ) %
            Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 1 ) %
            Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 2 ) %
            Libnucnet__Zone__getLabel( g[v].getNucnetZone(), 3 );
      }

      Graph& g;

    };

    template <typename Graph>
    struct
    edge_writer
    {
       edge_writer( Graph& g_ ) :           g( g_ ) {};

       template <class Edge>
       void operator()( std::ostream& out, Edge e )
       {
         out << boost::format( "[label=\"%.2e\" ]" ) % g[e].getWeight();
       }

       Graph& g;
    };

};

}  // namespace utility

}  // namespace wn_user

#endif // WN_ZONE_LINK_GRAPHER_HPP
