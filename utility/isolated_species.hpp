////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file isolated_species_finder.hpp
//! \brief A class to find isolated species.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_ISOLATED_SPECIES_HPP
#define WN_ISOLATED_SPECIES_HPP

#include <Libnucnet.h>
#include <nnt/auxiliary.h>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class isolated_species
///
/// \brief A class for isolated species within a nuclear view.  These are species
///        that have no reaction in or out according to the reaction view.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// isolated_species().
//##############################################################################

class isolated_species
{

  public:

  isolated_species(){}

/// The finder.
/// \param p_net The network.
/// \param s_nuc_view The nuclear view.
/// \param s_reac_view The reaction view.
/// \return A set containing the isolated species.

  std::set<std::string>
  find(
    Libnucnet__Net * p_net,
    std::string s_nuc_xpath,
    std::string s_reac_xpath
  )
  {

    Libnucnet__NetView * p_view;
    std::set<std::string> species_set;
    nnt::reaction_element_list_t element_list;

    p_view =
      Libnucnet__NetView__new(
        p_net,
        s_nuc_xpath.c_str(),
        s_reac_xpath.c_str()
      );

    Libnucnet__Nuc__setSpeciesCompareFunction(
      Libnucnet__Net__getNuc( p_net ),
      (Libnucnet__Species__compare_function) nnt::species_sort_function
    );

    Libnucnet__Nuc__sortSpecies(
      Libnucnet__Net__getNuc( p_net )
    );

    nnt::species_list_t species_list =
      nnt::make_species_list( Libnucnet__Net__getNuc( p_net ) );

    BOOST_FOREACH( nnt::Species species, species_list )
    {

      species_set.insert(
        Libnucnet__Species__getName( species.getNucnetSpecies() )
      );

    }

    nnt::reaction_list_t reaction_list =
      nnt::make_reaction_list(
        Libnucnet__Net__getReac( Libnucnet__NetView__getNet( p_view ) )
      );

    BOOST_FOREACH( nnt::Reaction reaction, reaction_list )
    {

      element_list =
        nnt::make_reaction_nuclide_reactant_list( reaction.getNucnetReaction() );

      BOOST_FOREACH( nnt::ReactionElement element, element_list )
      {

        if(
          species_set.find(
            Libnucnet__Reaction__Element__getName(
              element.getNucnetReactionElement()
            )
          ) != species_set.end()
        )
        {
          species_set.erase(
            species_set.find(
              Libnucnet__Reaction__Element__getName(
                element.getNucnetReactionElement()
              )
            )
          );
        }

      }

      element_list =
        nnt::make_reaction_nuclide_product_list( reaction.getNucnetReaction() );

      BOOST_FOREACH( nnt::ReactionElement element, element_list )
      {

        if(
          species_set.find(
            Libnucnet__Reaction__Element__getName(
              element.getNucnetReactionElement()
            )
          ) != species_set.end()
        )
        {
          species_set.erase(
            species_set.find(
              Libnucnet__Reaction__Element__getName(
                element.getNucnetReactionElement()
              )
            )
          );
        }

      }

    }

    Libnucnet__NetView__free( p_view );
  
    return species_set;
      
  }

/// The remover.
/// \param p_net The network.
/// \param s_nuc_view The nuclear view.
/// \param s_reac_view The reaction view.
/// \return On successful return, the isolated species according to the network view
///         have been removed.

  void
  remove(
    Libnucnet__Net * p_net,
    std::string s_nuc_xpath,
    std::string s_reac_xpath
  )
  {
    Libnucnet__Nuc * p_nuc = Libnucnet__Net__getNuc( p_net );
    std::cout << "Removing isolated species:";
    BOOST_FOREACH(
      std::string s,
      this->find( p_net, s_nuc_xpath, s_reac_xpath )
    )
    {
      std::cout << " " << s;
      Libnucnet__Nuc__removeSpecies(
        p_nuc,
        Libnucnet__Nuc__getSpeciesByName( p_nuc, s.c_str() )
      );
    }
    std::cout << std::endl;
  }

};
  
}  // namespace utility

}  // namespace wn_user

#endif // WN_ISOLATED_SPECIES_HPP
