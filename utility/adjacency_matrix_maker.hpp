////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022 Clemson University.
//
// This file was originally written by Bradley S. Meyer and Norberto J. Davila.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#ifndef WN_ADJACENCY_MATRIX_MAKER_HPP
#define WN_ADJACENCY_MATRIX_MAKER_HPP

#include <exception>

#include <WnMatrix.h>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class adjacency_matrix_maker
///
/// \brief A class to create an adjacency matrix for a graph.
///
////////////////////////////////////////////////////////////////////////////////

template
<
  typename Graph,
  typename Vertex = typename boost::graph_traits<Graph>::vertex_descriptor
>
class adjacency_matrix_maker
{
  public:
    adjacency_matrix_maker(){}

/// The adjacency matrix creator.
/// \param g The graph.
/// \return An adjacency matrix in WnMatrix form.

    WnMatrix *
    operator()( Graph const& g )
    {

      typedef
        typename boost::property_map<Graph, boost::vertex_index_t>::const_type
          IndexMap;

      IndexMap im = boost::get( boost::vertex_index, g );

      typedef typename boost::property_traits<IndexMap>::value_type index;

      WnMatrix * p_adjacency_matrix =
        WnMatrix__new( boost::num_vertices( g ), boost::num_vertices( g ) );
      
      BGL_FORALL_VERTICES_T( v, g, Graph )
      {
        index i = im[v];
        BGL_FORALL_OUTEDGES_T( v, e, g, Graph )
        {
          index j = im[boost::target( e, g )];
          WnMatrix__updateElement(
            p_adjacency_matrix,
            (size_t) i + 1,
            (size_t) j + 1,
            WnMatrix__getElement( p_adjacency_matrix, i + 1, j + 1 ) + 1
          );
        }
      }

      return p_adjacency_matrix;
          
    }

};

}  // namespace utility

}  // namespace wn_user

#endif // WN_ADJACENCY_MATRIX_MAKER_HPP
