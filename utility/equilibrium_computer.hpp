////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \brief A file to define equilibrium routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#ifndef WN_EQUILIBRIUM_COMPUTER_HPP
#define WN_EQUILIBRIUM_COMPUTER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class equilibrium_computer
///
/// \brief A class to compute equilibria.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// equilibrium_computer().
//##############################################################################

class equilibrium_computer
{

  public:
    equilibrium_computer( Libnucnet__Nuc * p_nuc )
    {
      set( p_nuc );
    }

    equilibrium_computer( Libnucnet__Net * p_net )
    {
      set( Libnucnet__Net__getNuc( p_net ) );
    }

    equilibrium_computer( nnt::Zone& zone )
    {
      set(
        Libnucnet__Net__getNuc( Libnucnet__Zone__getNet( zone.getNucnetZone() ) )
      );
    }

    ~equilibrium_computer(){ Libnuceq__free( pEquil ); }

    void
    set( Libnucnet__Nuc * p_nuc )
    {
      if( pEquil )
      {
        Libnuceq__free( pEquil );
      }
      pEquil = Libnuceq__new( p_nuc );
    }

    void
    setYe( double d_ye )
    {
      Libnuceq__setYe( pEquil, d_ye );
    }

    void
    clearYe()
    {
      Libnuceq__clearYe( pEquil );
    }

    void
    updateCluster( std::string s_xpath, double d_y )
    {
      Libnuceq__Cluster * p_cluster =
        Libnuceq__getCluster( pEquil, s_xpath.c_str() );
      if( !p_cluster )
      {
        p_cluster = Libnuceq__newCluster( pEquil, s_xpath.c_str() );
      }
      Libnuceq__Cluster__updateConstraint( p_cluster, d_y );
    }

    void
    removeCluster( std::string s_xpath )
    {
      Libnuceq__Cluster * p_cluster =
        Libnuceq__getCluster( pEquil, s_xpath.c_str() );
      if( p_cluster )
      {
        Libnuceq__removeCluster( pEquil, p_cluster );
      }
    }

    void
    compute( double d_t9, double d_rho )
    {
      Libnuceq__computeEquilibrium( pEquil, d_t9, d_rho );
    }

    Libnuceq *
    getEquilibrium()
    {
      return pEquil;
    }

    void
    setZoneAbundancesToEquilibrium( nnt::Zone& zone )
    {
      gsl_vector * p_abundances = Libnuceq__getAbundances( pEquil );
      Libnucnet__Zone__updateAbundances( zone.getNucnetZone(), p_abundances );
      gsl_vector_free( p_abundances );
    }

  private:
    Libnuceq * pEquil = NULL;

};
     
} // namespace utility

} // namespace wn_user 

#endif // WN_EQUILIBRIUM_COMPUTER_HPP
