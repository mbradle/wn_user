////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020-2021 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#ifndef WN_GRAPH_RATES_HPP
#define WN_GRAPH_RATES_HPP

#include <boost/graph/iteration_macros.hpp>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class graph_rates
///
/// \brief A class create a rate matrix from a directed graph.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// graph_rates().
//##############################################################################

template<class Graph>
class graph_rates
{
  public:

    typedef
      typename boost::property_map<Graph, boost::vertex_index_t>::type IndexMap;
    typedef
      typename boost::property_traits<IndexMap>::value_type Index;
    typedef
      typename boost::property_map<Graph, boost::edge_weight_t>::type WeightMap;
    typedef
      typename boost::property_traits<WeightMap>::value_type Weight;

    graph_rates(){}

/// The graph rates matrix retriever.
/// \param g The graph.
/// \return A map giving the rate matrix from the directed graph in
///         coordinate matrix form.

    std::map<std::pair<Index, Index>, Weight>
    operator()( Graph& g )
    {
      typedef std::pair<Index, Index> mat_index;
      typename boost::graph_traits<Graph>::edge_descriptor e;
      IndexMap im = boost::get(boost::vertex_index, g);
      WeightMap wm = boost::get(boost::edge_weight, g);

      std::map<mat_index, Weight> result;

      BGL_FORALL_VERTICES_T( v, g, Graph )
      {
        Weight sum = 0;
        Index i_col = im[v];
        BGL_FORALL_OUTEDGES_T( v, e, g, Graph )
        {
          Index i_row = im[boost::target(e, g)];
          mat_index target_index = std::make_pair( i_row, i_col );
          if( result.find( target_index ) == result.end() )
          {
            result[target_index] = wm[e];
          }
          else
          {
            result[target_index] += wm[e];
          }
          sum -= wm[e];
        }
        mat_index source_index = std::make_pair( i_col, i_col );
        result[source_index] = sum;
      }

      return result;

    }

};

}  // namespace utility

}  // namespace wn_user

#endif // WN_GRAPH_RATES_HPP
