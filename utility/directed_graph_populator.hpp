////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#ifndef WN_DIRECTED_GRAPH_POPULATOR_HPP
#define WN_DIRECTED_GRAPH_POPULATOR_HPP

#include <boost/graph/iteration_macros.hpp>
#include <boost/tuple/tuple.hpp>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class directed_graph_populator
///
/// \brief A class to populate a directed_graph from the input graph.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// directed_graph_populator().
//##############################################################################

class directed_graph_populator
{
  public:

    directed_graph_populator(){}

/// The constructor.
/// \param g The input graph.
/// \param g_d The empty directed graph to be populated.

    template <class Graph, class Vertex,
              class Directed_Graph, class Directed_Vertex>
    std::pair<std::map<Vertex, Directed_Vertex>,
                 std::map<Directed_Vertex, Vertex> >
    populateDirectedGraph( Graph& g, Directed_Graph& g_d )
    {
      if( boost::num_vertices( g_d ) != 0 || boost::num_edges( g_d ) != 0 )
      {
        std::cerr << "Input directed graph is not empty." << std::endl;
        exit( EXIT_FAILURE );
      }
      std::map<Vertex, Directed_Vertex> map1;
      std::map<Directed_Vertex, Vertex> map2;
      BGL_FORALL_VERTICES_T( v, g, Graph )
      {
        Directed_Vertex u = boost::add_vertex( g_d );
        map1[v] = u;
        map2[u] = v;
      }
      BGL_FORALL_EDGES_T( e, g, Graph )
      {
        boost::add_edge(
          map1[boost::source( e, g )],
          map1[boost::target( e, g )],
          g_d
        );
      }
      return std::make_pair( map1, map2 );
    }

};

}  // namespace utility

}  // namespace wn_user

#endif // WN_DIRECTED_GRAPH_POPULATOR_HPP
