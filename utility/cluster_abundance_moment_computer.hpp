////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#ifndef WN_CLUSTER_ABUNDANCE_MOMENT_COMPUTER_HPP
#define WN_CLUSTER_ABUNDANCE_MOMENT_COMPUTER_HPP

#include <Libnucnet.h>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class cluster_abundance_moment_computer
///
/// \brief A class to compute the abundance moment for a cluster in a zone.
///
/// The abundance moment is defined as \f$\sum_{i\in C} N_i^d
/// Y_i\f$, where \f$C\f$ is the cluster, \f$i\f$ is a species in
/// the cluster, \f$N_i\f$ is the appropriate nucleon number
/// (atomic number, neutron number, or mass number) for the species,
/// \f$d\f$ is the
/// moment exponent, and \f$Y_i\f$ is the abundance of the species.        
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// cluster_abundance_moment_computer().
//##############################################################################

class cluster_abundance_moment_computer
{

  public:
    cluster_abundance_moment_computer(){}

/// The abundance moment computer.
/// \param zone The zone for which one seeks the abundance moment.
/// \param s_nuc_xpath The XPath expression defining the cluster.
/// \param s_nucleon The nucleon for the abundance moment (must be
///        "z", the atomic number, "n", the neutron number, or "a",
///        the mass number).
/// \param d_moment The moment power.
/// \return The abundance moment.

    template<class T = double>
    T
    operator()(
      nnt::Zone& zone, std::string s_nuc_xpath, std::string s_nucleon,
      T d_moment
    )
    {
      T result = 0;

      assert( s_nucleon == "z" || s_nucleon == "n" || s_nucleon == "a" );

      p_view = zone.getNetView( s_nuc_xpath.c_str(), "" );

      BOOST_FOREACH(
        nnt::Species species,
        nnt::make_species_list(
          Libnucnet__Net__getNuc(
            Libnucnet__NetView__getNet( p_view )
          )
        )
      )
      {
        if( s_nucleon == "z" )
        {
          d_nucleon = Libnucnet__Species__getZ( species.getNucnetSpecies() );
        }
        else if( s_nucleon == "n" )
        {
          d_nucleon =
            Libnucnet__Species__getA( species.getNucnetSpecies() )
            -
            Libnucnet__Species__getA( species.getNucnetSpecies() );
        }
        else
        {
          d_nucleon = Libnucnet__Species__getA( species.getNucnetSpecies() );
        }
        result +=
          pow( d_nucleon, d_moment ) *
          Libnucnet__Zone__getSpeciesAbundance(
            zone.getNucnetZone(),
            species.getNucnetSpecies()
          );
      }
      return result;
    }

  private:
    double d_nucleon;
    Libnucnet__NetView * p_view;

};

}  // namespace utility

}  // namespace wn_user

#endif // WN_CLUSTER_ABUNDANCE_MOMENT_COMPUTER_HPP
