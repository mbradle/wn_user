////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#ifndef WN_ZONE_COPIER_HPP
#define WN_ZONE_COPIER_HPP

#include <Libnucnet.h>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class zone_copier
///
/// \brief A class to copy a zone with a possibly different underlying network.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// zone_copier().
//##############################################################################

class zone_copier
{

  public:
    zone_copier(){}

/// The constructor.
/// \param p_net The network for the target zone.

    zone_copier( Libnucnet__Net * p_net ) : pNet( p_net ){}

/// The copier.
/// \param p_zone The source zone to be copied.
/// \param s_labels A vector of up to three labels providing the new labels
///                 for the copied zone (optional).  If not provided,
///                 the copied zone will have the same labels as the source
///                 zone.
/// \return The copied zone.

    Libnucnet__Zone *
    operator()(
      Libnucnet__Zone * p_zone,
      std::vector<std::string> s_labels
    )
    {

      assert( s_labels.size() <= 3 );

      gsl_vector * p_vector;

      std::vector<std::string> s_new_labels( 3, "0" );

      for( size_t i = 0; i < s_labels.size(); i++ )
      {
        s_new_labels[i] = s_labels[i];
      }

      Libnucnet__Zone *
        p_new_zone =
          Libnucnet__Zone__new(
            pNet,
            s_new_labels[0].c_str(),
            s_new_labels[1].c_str(),
            s_new_labels[2].c_str()
          );
   
      Libnucnet__Zone__iterateOptionalProperties(
        p_zone,
        NULL,
        NULL,
        NULL,
        (Libnucnet__Zone__optional_property_iterate_function) copyProperties,
        p_new_zone
      );
    
      p_vector = Libnucnet__Zone__getAbundances( p_zone );
      Libnucnet__Zone__updateAbundances( p_new_zone, p_vector );
      gsl_vector_free( p_vector );
    
      p_vector = Libnucnet__Zone__getAbundanceChanges( p_zone );
      Libnucnet__Zone__updateAbundanceChanges( p_new_zone, p_vector );
      gsl_vector_free( p_vector );

      return p_new_zone;

    }

    /// @cond

    Libnucnet__Zone *
    operator()( Libnucnet__Zone * p_zone )
    {
      std::vector<std::string> s_labels;
      s_labels.push_back( Libnucnet__Zone__getLabel( p_zone, 1 ) );
      s_labels.push_back( Libnucnet__Zone__getLabel( p_zone, 2 ) );
      s_labels.push_back( Libnucnet__Zone__getLabel( p_zone, 3 ) );
      return (*this)( p_zone, s_labels );
    }

    /// @endcond

  private:
    Libnucnet__Net * pNet;

    static void
    copyProperties(
      const char * s_name,
      const char * s_tag1,
      const char * s_tag2,
      const char * s_value,
      Libnucnet__Zone * p_zone
    )
    {
    
      Libnucnet__Zone__updateProperty(
        p_zone,
        s_name,
        s_tag1,
        s_tag2,
        s_value
      );
    
    } 

};

}  // namespace utility

}  // namespace wn_user

#endif // WN_ZONE_COPIER_HPP
