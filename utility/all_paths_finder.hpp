////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2022 Clemson University.
//
// This file was originally written by Bradley S. Meyer and Norberto J. Davila.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#ifndef WN_ALL_PATHS_FINDER_HPP
#define WN_ALL_PATHS_FINDER_HPP

#include <exception>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class dag_paths_finder
///
/// \brief A class to discover all paths between two vertices in an input
///        graph.
///
////////////////////////////////////////////////////////////////////////////////

template <typename Graph, typename Vertex, typename Edge>
class all_paths_finder
{
  public:
    all_paths_finder(){}

/// The path finder.
/// \param v_start The start vertex for the path.
/// \param v_end The end vertex for the path.
/// \param g The graph.
/// \return A vector of the paths.  Each path is represented as a vector
///         of edges giving the path.

    std::vector<std::vector<Edge> >
    operator()( Vertex v_start, Vertex v_end, Graph const& g )
    {
      i_path = 0;
      std::vector<std::vector<Edge> > v_p;
      std::vector<Vertex> state;
      std::vector<Edge> edge_state;
      PathStorer storer{ g, v_p };
      helper( v_start, v_end, g, state, edge_state, storer );
      return v_p;
    }

    void
    set_show_progress()
    {
      show_progress = true;
    }

  private:
    bool show_progress = false;
    size_t i_path = 0; 
    template <typename Report>
    void helper(
      Vertex v_start, Vertex v_end, Graph const& g, std::vector<Vertex>& path,
      std::vector<Edge>& edge_path, Report const& callback
    )
    {
      path.push_back(v_start);

      if( v_start == v_end )
      {
        if( show_progress )
        {
          std::cout << "Found path " << ++i_path << std::endl;
        }
        callback( edge_path );
      }
      else
      {
        BGL_FORALL_OUTEDGES_T( v_start, e, g, Graph )
        {
          typename Graph::vertex_descriptor v = target( e, g );
          if( path.end() == std::find( path.begin(), path.end(), v ) )
          {
            edge_path.push_back( e );
            helper( v, v_end, g, path, edge_path, callback );
          }
        }
      }

      path.pop_back();
      edge_path.pop_back();
  }

  struct PathStorer
  {
    Graph const& g;
    std::vector<std::vector<Edge> >& v_p;

    void operator()( std::vector<Edge> const& edge_path ) const
    {
      v_p.push_back( edge_path );
    }
  };

};


}  // namespace utility

}  // namespace wn_user

#endif // WN_ALL_PATHS_FINDER_HPP
