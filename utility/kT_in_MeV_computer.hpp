////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

#ifndef WN_KT_IN_MEV_COMPUTER_HPP
#define WN_KT_IN_MEV_COMPUTER_HPP

#include <Libnucnet.h>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class kT_in_MeV_computer
///
/// \brief A class to convert the temperature into kT in MeV
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// kT_in_MeV_computer().
//##############################################################################

class kT_in_MeV_computer
{

  public:
    kT_in_MeV_computer() {}

/// The computer.
/// \param T9 The temperature in 10^9 K
/// \return kT in MeV

    template<class T = double>
    T
    operator()( T T9 )
    {
      T result =
        T9 *
          GSL_CONST_CGSM_BOLTZMANN *
          GSL_CONST_NUM_KILO /
          GSL_CONST_CGSM_ELECTRON_VOLT;

      return result;
    }

};

}  // namespace utility

}  // namespace wn_user

#endif // WN_KT_IN_MEV_COMPUTER_HPP
