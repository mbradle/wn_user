////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2020 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file stable_species.hpp
//! \brief A class to copy a nuclear network.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_NUCNET_COPIER_HPP
#define WN_NUCNET_COPIER_HPP

#include <Libnucnet.h>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class nucnet_copier
///
/// \brief A class to copy a nucnet structure.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// nucnet_copier().
//##############################################################################

class nucnet_copier
{

  public:
    nucnet_copier(){}

/// The copier.
/// \param p_nucnet The nucnet to be copied.
/// \return The copy.

    Libnucnet *
    operator()( Libnucnet * p_nucnet )
    {

      Libnucnet *p_new_nucnet;

      p_new_nucnet =  Libnucnet__new();

      Libnucnet__Nuc__iterateSpecies(
        Libnucnet__Net__getNuc( Libnucnet__getNet( p_nucnet ) ),
        (Libnucnet__Species__iterateFunction) insert_species_in_nuc,
        p_new_nucnet
      );

      Libnucnet__Reac__iterateReactions(
        Libnucnet__Net__getReac( Libnucnet__getNet( p_nucnet ) ),
        (Libnucnet__Reaction__iterateFunction) insert_reaction_in_reac,
        p_new_nucnet
      );

      return p_new_nucnet;

    }

  private:

    static int
    insert_species_in_nuc(
      Libnucnet__Species *p_species,
      Libnucnet *p_nucnet
    )
    {
      Libnucnet__Nuc__addSpecies(
        Libnucnet__Net__getNuc( Libnucnet__getNet( p_nucnet ) ),
        Libnucnet__Species__copy( p_species )
      );

      return 1;
    }

    static int
    insert_reaction_in_reac(
      Libnucnet__Reaction *p_reaction,
      Libnucnet *p_nucnet
    )
    {
      if(
         Libnucnet__Net__isValidReaction(
           Libnucnet__getNet( p_nucnet ),
           p_reaction
         )
      )
      {
        Libnucnet__Reac__addReaction(
          Libnucnet__Net__getReac( Libnucnet__getNet( p_nucnet ) ),
          Libnucnet__Reaction__copy( p_reaction )
        );
      }

      return 1;
    }

};

}  // namespace utility

}  // namespace wn_user

#endif // WN_NUCNET_COPIER_HPP
