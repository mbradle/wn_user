////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file stable_species.hpp
//! \brief A class to return strings of stable species.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_STABLE_SPECIES_HPP
#define WN_STABLE_SPECIES_HPP

#include <vector>
#include <set>

namespace wn_user
{

namespace utility
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class stable_species
///
/// \brief A class to list stable species.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// stable_species().
//##############################################################################

class stable_species
{

  public:
    stable_species(){}

/// Routine to return the stable species as a vector of strings.
/// \return The vector.

    std::vector<std::string>
    getVector()
    {

      const char *s_stables[] =
        {
          "h1","h2",
          "he3","he4",
          "li6","li7",
          "be9",
          "b10","b11",
          "c12","c13",
          "n14","n15",
          "o16","o17","o18",
          "f19",
          "ne20","ne21","ne22",
          "na23",
          "mg24","mg25","mg26",
          "al27",
          "si28","si29","si30",
          "p31",
          "s32","s33","s34","s36",
          "cl35","cl37",
          "ar36","ar38","ar40",
          "k39","k40","k41",
          "ca40","ca42","ca43","ca44","ca46","ca48",
          "sc45",
          "ti46","ti47","ti48","ti49","ti50",
          "v50","v51",
          "cr50","cr52","cr53","cr54",
          "mn55",
          "fe54","fe56","fe57","fe58",
          "co59",
          "ni58","ni60","ni61","ni62","ni64",
          "cu63","cu65",
          "zn64","zn66","zn67","zn68","zn70",
          "ga69","ga71",
          "ge70","ge72","ge73","ge74","ge76",
          "as75",
          "se74","se76","se77","se78","se80","se82",
          "br79","br81",
          "kr78","kr80","kr82","kr83","kr84","kr86",
          "rb85","rb87",
          "sr84","sr86","sr87","sr88",
          "y89",
          "zr90","zr91","zr92","zr94","zr96",
          "nb93",
          "mo92","mo94","mo95","mo96","mo97","mo98","mo100",
          "ru96","ru98","ru99","ru100","ru101","ru102","ru104",
          "rh103",
          "pd102","pd104","pd105","pd106","pd108","pd110",
          "ag107","ag109",
          "cd106","cd108","cd110","cd111","cd112","cd113","cd114","cd116",
          "in113","in115",
          "sn112","sn114","sn115","sn116","sn117","sn118","sn119","sn120",
             "sn122", "sn124",
          "sb121","sb123",
          "te120","te122","te123","te124","te125","te126","te128","te130",
          "i127",
          "xe124","xe126","xe128","xe129","xe130","xe131","xe132","xe134",
             "xe136",
          "cs133",
          "ba130","ba132","ba134","ba135","ba136","ba137","ba138",
          "la138","la139",
          "ce136","ce138","ce140","ce142",
          "pr141",
          "nd142","nd143","nd144","nd145","nd146","nd148","nd150",
          "sm144","sm147","sm148","sm149","sm150","sm152","sm154",
          "eu151","eu153",
          "gd152","gd154","gd155","gd156","gd157","gd158","gd160",
          "tb159",
          "dy156","dy158","dy160","dy161","dy162","dy163","dy164",
          "ho165",
          "er162","er164","er166","er167","er168","er170",
          "tm169",
          "yb168","yb170","yb171","yb172","yb173","yb174","yb176",
          "lu175","lu176",
          "hf174","hf176","hf177","hf178","hf179","hf180",
          "ta180","ta181",
          "w180","w182","w183","w184","w186",
          "re185","re187",
          "os184","os186","os187","os188","os189","os190","os192",
          "ir191","ir193",
          "pt190","pt192","pt194","pt195","pt196","pt198",
          "au197",
          "hg196","hg198","hg200","hg201","hg202","hg204",
          "tl203","tl205",
          "pb204","pb206","pb207","pb208",
          "bi209",
          "th232",
          "u235","u238"
        };

      size_t i, i_species;
      std::vector<std::string> stable_species;

      i_species = sizeof( s_stables ) / sizeof( s_stables[0] );

      for( i = 0; i < i_species; i++ )
            stable_species.push_back( s_stables[i] );

      return stable_species;

    }

/// Routine to return the stable species as a set of strings.
/// \return The set.

    std::set<std::string>
    getSet()
    {
      std::set<std::string> my_set;
      BOOST_FOREACH( std::string s, getVector() )
      {
        my_set.insert( s );
      }
      return my_set;
    }

};

}  // namespace utility

}  // namespace wn_user

#endif // WN_STABLE_SPECIES_HPP
