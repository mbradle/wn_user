////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file exponential_rho.hpp
//! \brief A file to define useful hydro helper routines.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.

#include "my_global_types.h"

#include "hydro/single_zone/base/hydro.hpp"
#include "thermo/base/thermo_evolve.hpp"

#ifndef NNP_HYDRO_DETAIL_HPP
#define NNP_HYDRO_DETAIL_HPP

#define HYDRO_EVOLVE

/**
 * @brief A namespace for user-defined rate functions.
 */
namespace wn_user
{

namespace detail
{

class hydro : public thermo, public base::thermo_evolve
{

  public:

    hydro(){}
    hydro( v_map_t& v_map ) :
      thermo(), base::thermo_evolve( v_map )
    {
      d_t9_0 = v_map[nnt::s_T9_0].as<double>();
      d_rho_0 = v_map[nnt::s_RHO_0].as<double>();
      d_tau = v_map[nnt::s_TAU].as<double>();
    }

    state_type
    initializeX( nnt::Zone& zone )
    {
      state_type x;
      x.push_back( 1 );
      x.push_back( x[0] / ( 3. * d_tau ) );
      x.push_back( 0 );
      return x;
    }

    void
    setT9andRho( state_type& x, nnt::Zone& zone )
    {
      zone.updateProperty( nnt::s_T9, d_t9_0 );
      zone.updateProperty( nnt::s_RHO, computeRho( x, 0., zone ) );
    }

    void
    initialize( state_type& x, nnt::Zone& zone )
    {
      x[2] = computeEntropy( zone );
    }

    double computeRho( const state_type& x, double d_t, nnt::Zone& zone )
    {
      return d_rho_0 / gsl_pow_3( x[0] );
    }

    double computeDlnRhoDt(
      const state_type& x, const double time, nnt::Zone& zone
    )
    {
      return -3. * x[1] / x[0];
    }

    double
    computeAcceleration(
      const state_type& x, const double time, nnt::Zone& zone
    )
    {
      return x[1] / ( 3. * d_tau );
    }

    double
    computeHeatingRatePerNucleon(
      const state_type& x,
      const double time,
      nnt::Zone& zone
    )
    { return 0.; }

    void
    updateOtherZoneProperties(const state_type& x, nnt::Zone& zone ) { }

  private:
    double d_tau, d_t9_0, d_rho_0;

};

//##############################################################################
// hydro_options().
//##############################################################################

class hydro_options : public base::hydro_options,
	              public base::thermo_evolve_options
{

  public:
    hydro_options() : base::hydro_options(), base::thermo_evolve_options() {}

    void
    getOptions( po::options_description& hydro )
    {

      try
      {

        hydro.add_options()

          ( nnt::s_T9_0, po::value<double>()->default_value( 10., "10." ),
            "Initial T (in 10^9 K)"
          )

          ( nnt::s_RHO_0, po::value<double>()->default_value( 1.e8, "1.e8" ),
            "Initial density (in g/cc)"
          )

          ( nnt::s_TAU, po::value<double>()->default_value( 0.1, "0.1" ),
            "Expansion timescale (s)"
          )

        ;

	base::hydro_options::getOptions( hydro );
        base::thermo_evolve_options::getOptions( hydro );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

    std::string
    getExample()
    {

      return
        "--" + std::string( nnt::s_T9_0 ) + " 10 " +
        "--" + std::string( nnt::s_TAU ) + " 0.1 " +
        base::thermo_evolve_options::getExample();

    }

};

}  // namespace detail

}  // namespace wn_user

#endif // NNP_HYDRO_DETAIL_HPP
