////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file step_time_t9_rho.hpp
//! \brief A file to define hydro routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "math/linear_interpolator.hpp"
#include "hydro/single_zone/base/hydro.hpp"

#ifndef NNP_HYDRO_DETAIL_HPP
#define NNP_HYDRO_DETAIL_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define S_INTERP_FILE          "interp_file"

//##############################################################################
// hydro().
//##############################################################################

class hydro : public base::hydro
{

  public:
    hydro() : base::hydro() {}
    hydro( v_map_t& v_map ) : base::hydro( v_map ), t9_interp(), lrho_interp()
    {
      std::vector<double> time, t9, log10_rho;
      if( v_map.count( S_INTERP_FILE ) == 0 )
      {
        std::cerr << "No file provided for trajectory." << std::endl;
        exit( EXIT_FAILURE );
      }
      std::string s_file = v_map[S_INTERP_FILE].as<std::string>();
      std::ifstream my_file;
      double d_x0, d_x1, d_x2, d_x3;

      my_file.open( s_file.c_str() );

      if( !my_file.is_open() || my_file.bad() )
      {
        std::cerr << "Couldn't open file " << s_file << "!" << std::endl;
        exit( EXIT_FAILURE );
      }

      while( my_file >> d_x0 >> d_x1 >> d_x2 >> d_x3 )
      {
        time.push_back( d_x1 );
        log10_rho.push_back( log10( d_x2 ) );
        t9.push_back( d_x3 / 1.e9 );
      }

      my_file.close();

      t9_interp.set( time, t9 );
      lrho_interp.set( time, log10_rho );
    }

    void
    initialize( nnt::Zone& zone )
    {
      (*this)( zone );
      if( initializeEquilibrium() ) { setAbundancesToEquilibrium( zone ); }
    }

    void
    operator()( nnt::Zone& zone )
    {
      zone.updateProperty(
        nnt::s_T9, 
        t9_interp( zone.getProperty<double>( nnt::s_TIME ) )
      );
      zone.updateProperty(
        nnt::s_RHO, 
        pow(
          10.,
          lrho_interp( zone.getProperty<double>( nnt::s_TIME ) )
        )
      );
    }

    void
    adjustTimeStep( nnt::Zone& zone ) {}

  private:
    wn_user::math::linear_interpolator<> t9_interp, lrho_interp;

};
 
//##############################################################################
// hydro_options().
//##############################################################################

class hydro_options : public base::hydro_options
{

  public:
    hydro_options() : base::hydro_options(){}

    std::string
    getExample()
    {
      return
        "--" + std::string( S_INTERP_FILE ) + " my_file.txt " +
        base::hydro_options::getExample();
    }

    void
    getOptions( po::options_description& hydro )
    {

      try
      {

        base::hydro_options::getOptions( hydro );

        hydro.add_options()

          ( S_INTERP_FILE, po::value<std::string>()->required(),
            "Interpolation file" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_HYDRO_DETAIL_HPP
