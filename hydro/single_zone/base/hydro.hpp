////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file hydro.hpp
//! \brief A file to define base hydro routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#include "nnt/iter.h"

#ifndef NNP_HYDRO_BASE_HPP
#define NNP_HYDRO_BASE_HPP

#define S_NSE              "nse"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

//##############################################################################
// hydro().
//##############################################################################

class hydro
{

  public:
    hydro(){}
    hydro( v_map_t& v_map )
    {
      b_nse = v_map[S_NSE].as<bool>();
    }

    void setAbundancesToEquilibrium( nnt::Zone& zone )
    {
      zone.updateProperty(
        nnt::s_YE,
        user::compute_cluster_abundance_moment( zone, "", "z", 1 )
      );
      nnt::set_zone_abundances_to_equilibrium( zone );
    }

    bool initializeEquilibrium(){ return b_nse; }

    void
    adjustTimeStep( nnt::Zone& zone ) {}

  private:
    bool b_nse;

};
 
//##############################################################################
// hydro_options().
//##############################################################################

class hydro_options
{

  public:
    hydro_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    getOptions( po::options_description& hydro )
    {

      try
      {

        hydro.add_options()

          ( S_NSE,
            po::value<bool>()->default_value( false, "false" ),
            "Set initial abundances to NSE"
          )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace base

} // namespace wn_user

#endif // NNP_HYDRO_BASE_HPP
