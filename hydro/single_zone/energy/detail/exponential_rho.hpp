////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file exponential_rho.hpp
//! \brief A file to define useful hydro helper routines.
//!
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.

#include "my_global_types.h"

#ifndef NNP_HYDRO_DETAIL_HPP
#define NNP_HYDRO_DETAIL_HPP

#define HYDRO_EVOLVE

/**
 * @brief A namespace for user-defined rate functions.
 */
namespace wn_user
{

class hydro_detail: public thermo
{

  public:

    hydro_detail(){}
    hydro_detail( v_map_t& v_map ) : thermo()
    {
      d_t9_0 = v_map[nnt::s_T9_0].as<double>();
      d_rho_0 = v_map[nnt::s_RHO_0].as<double>();
      d_tau = v_map[nnt::s_TAU].as<double>();
    }

    state_type
    set( nnt::Zone& zone )
    {
      state_type x, dxdt;
      x.push_back( 1 );
      x.push_back( x[0] / ( 3. * d_tau ) );
      x.push_back( d_t9_0 );
      return x;
    }

    double
    computeHeatingRate(
      const state_type& x,
      double d_t,
      nnt::Zone& zone,
      Libnucnet__NetView * p_view
    )
    {
      return -computeEnergyLossRate( zone, p_view );
    }

    double computeRho( const state_type& x, double d_t, nnt::Zone& zone )
    {
      return d_rho_0 / gsl_pow_3( x[0] );
    }

    double computeDlnRhoDt(
      const state_type& x, const double time, nnt::Zone& zone
    )
    {
      return -3. * x[1] / x[0];
    }

    void initialize( state_type& x, nnt::Zone& zone ) { }

    double
    computeAcceleration(
      const state_type& x, const double time, nnt::Zone& zone
    )
    {
      return x[1] / ( 3. * d_tau );
    }

    void
    updateOtherZoneProperties(const state_type& x, nnt::Zone& zone ) { }

  private:
    double d_tau, d_t9_0, d_rho_0;

};

//##############################################################################
// hydro_detail_options().
//##############################################################################

class hydro_detail_options
{

  public:
    hydro_detail_options(){}

    void
    getDetailOptions( po::options_description& hydro )
    {

      try
      {

        hydro.add_options()

          ( nnt::s_T9_0, po::value<double>()->default_value( 10., "10." ),
            "Initial T (in 10^9 K)"
          )

          ( nnt::s_RHO_0, po::value<double>()->default_value( 1.e8, "1.e8" ),
            "Initial density (in g/cc)"
          )

          ( nnt::s_TAU, po::value<double>()->default_value( 0.1, "0.1" ),
            "Expansion timescale (s)"
          )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

    std::string
    getExample()
    {

      return
        "--" + std::string( nnt::s_T9_0 ) + " 10 " +
        "--" + std::string( nnt::s_RHO_0 ) + " 1.e8 " +
        "--" + std::string( nnt::s_TAU ) + " 0.1 ";

    }

};

}  // namespace wn_user

#endif // NNP_HYDRO_DETAIL_HPP
