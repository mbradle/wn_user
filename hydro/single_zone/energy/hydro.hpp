////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file hydro.hpp
//! \brief A file to define hydro routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <limits>
#include <boost/bind/bind.hpp>
#include <boost/numeric/odeint.hpp>

#include "my_global_types.h"

#include "nnt/iter.h"
#include "hydro/single_zone/base/hydro_base.hpp"
#include "hydro/single_zone/base/hydro_stepper.hpp"

#ifndef NNP_HYDRO_HPP
#define NNP_HYDRO_HPP

#define S_OBSERVE          "observe"
#define S_EG_NUC_XPATH     "eg_nuc_xpath"
#define S_EG_REAC_XPATH    "eg_reac_xpath"
#define S_X_FAC            "x_factor"
#define S_X_LIM            "x_lim"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

void
observe(
  nnt::Zone& zone,
  const state_type& x,
  const state_type& dxdt,
  const double d_t
)
{

  double d_dt =
    d_t - zone.getProperty<double>( nnt::s_TIME );

  std::cout <<
    boost::format( "t = %.5e dt = %.5e\n" ) %
    d_t %
    d_dt;

  std::cout <<
    boost::format( "x = {%.5e, %.5e, %.5e}\n" ) %
    x[0] %
    x[1] %
    x[2]; 

  std::cout <<
    boost::format( "dxdt = {%.5e, %.5e, %.5e}\n\n" ) %
    dxdt[0] %
    dxdt[1] %
    dxdt[2];

}

struct
energy_rhs
{

  nnt::Zone& zone;
  network_evolver& my_evolver;
  hydro_detail& my_hydro;
  Libnucnet__NetView * p_view;
  bool b_observe;

  energy_rhs(
    nnt::Zone& _zone,
    network_evolver& _my_evolver,
    hydro_detail& _my_hydro,
    Libnucnet__NetView * _p_view,
    bool _b_observe
  ) : zone( _zone ), my_evolver( _my_evolver ), my_hydro( _my_hydro ),
      p_view( _p_view ), b_observe( _b_observe ){}

  void
  operator()( const state_type &x, state_type &dxdt, const double d_t ) const
  {

    double d_dt, d_dt_save, d_eps;
    gsl_vector * p_abundances, * p_abundance_changes;

    p_abundances = Libnucnet__Zone__getAbundances( zone.getNucnetZone() );

    p_abundance_changes =
      Libnucnet__Zone__getAbundanceChanges( zone.getNucnetZone() );

    d_dt_save = zone.getProperty<double>( nnt::s_DTIME );

    d_dt = d_t - zone.getProperty<double>( nnt::s_TIME );

    zone.updateProperty(
      nnt::s_DTIME,
      d_dt
    );

    zone.updateProperty(
      nnt::s_T9,
      x[2]
    );

    zone.updateProperty(
      nnt::s_RHO,
      my_hydro.computeRho( x, d_t, zone )
    );

    dxdt[0] = x[1];

    dxdt[0] = x[1];

    dxdt[1] = my_hydro.computeAcceleration( x, d_t, zone );

    if( d_dt > 0 )
    {
      my_evolver( zone );
      d_eps =
        my_hydro.computeEnergyGenerationRateByAbundanceChange(
          zone, p_abundances, d_dt
        );
    }
    else
    {
      d_eps = my_hydro.computeEnergyGenerationRate( zone, p_view );
    }

    dxdt[2] =
      ( 
        1. / 
        (
          my_hydro.computeSpecificHeatPerNucleon( zone )
          *
          GSL_CONST_NUM_GIGA
        ) 
      )
      *
      (
        x[2] *
           GSL_CONST_NUM_GIGA
           * my_hydro.computeDPDT( zone )
           * my_hydro.computeDlnRhoDt( x, d_t, zone )
        / 
        (
          zone.getProperty<double>( nnt::s_RHO ) * GSL_CONST_NUM_AVOGADRO
        )
        + d_eps
        + my_hydro.computeHeatingRate( x, d_t, zone, p_view )
      );

    if( b_observe )
    {
      observe( zone, x, dxdt, d_t );
    }

    Libnucnet__Zone__updateAbundances( zone.getNucnetZone(), p_abundances );
  
    Libnucnet__Zone__updateAbundanceChanges(
      zone.getNucnetZone(),
      p_abundance_changes
    );

    zone.updateProperty( nnt::s_DTIME, d_dt_save );

    gsl_vector_free( p_abundances );
    gsl_vector_free( p_abundance_changes );

  }

};

//##############################################################################
// do_step().
//##############################################################################

struct do_step : public boost::static_visitor<>
{
  energy_rhs rhs;
  state_type& x;
  double d_t, d_dt;
  do_step( energy_rhs& _rhs, state_type& _x, double _d_t, double _d_dt ) :
    rhs( _rhs ), x( _x ), d_t( _d_t ), d_dt( _d_dt ) {}

  template <typename T>
  void operator()(T& t) const { t.do_step( rhs, x, d_t, d_dt ); }
};

//##############################################################################
// hydro().
//##############################################################################

class hydro : public hydro_base, public hydro_stepper
{

  public:
    hydro( v_map_t& v_map, network_evolver _my_evolver ) :
      hydro_base( v_map ), hydro_stepper( v_map ), my_evolver( _my_evolver ),
      my_hydro( v_map )
    {
      if( v_map.count( S_EG_NUC_XPATH ) )
      {
        s_eg_nuc_xpath = v_map[S_EG_NUC_XPATH].as<std::string>();
      }
      if( v_map.count( S_EG_REAC_XPATH ) )
      {
        s_eg_reac_xpath = v_map[S_EG_REAC_XPATH].as<std::string>();
      }
      d_x_fac = v_map[S_X_FAC].as<double>();
      d_x_lim = v_map[S_X_LIM].as<double>();
      b_observe = v_map[S_OBSERVE].as<bool>();
      stepper = getStepper();
    }

    void
    initialize( nnt::Zone& zone )
    {
      x = my_hydro.set( zone );
      zone.updateProperty( nnt::s_T9, x[2] );
      zone.updateProperty( nnt::s_RHO, my_hydro.computeRho( x, 0., zone ) );
      if( initializeEquilibrium() ) { setAbundancesToEquilibrium( zone ); }
      my_hydro.initialize( x, zone );
      std::copy( x.begin(), x.end(), std::back_inserter( x0 ) );
    }

    Libnucnet__NetView *
    getEGNetView( nnt::Zone& zone )
    {
      if( s_eg_nuc_xpath.empty() && s_eg_reac_xpath.empty() )
      {
        return zone.getNetView( EVOLUTION_NETWORK );
      }
      else
      {
        if( s_eg_nuc_xpath.empty() )
        {
          return zone.getNetView( "", s_eg_reac_xpath.c_str() );
        }
        if( s_eg_reac_xpath.empty() )
        {
          return zone.getNetView( s_eg_nuc_xpath.c_str(), "" );
        }
        else
        {
          return
            zone.getNetView(
              s_eg_nuc_xpath.c_str(), s_eg_reac_xpath.c_str()
            );
        }
      }
    }

    void
    operator()( nnt::Zone& zone )
    {

      Libnucnet__NetView * p_view;

      double d_t_s = zone.getProperty<double>( nnt::s_TIME );
      double d_dt = zone.getProperty<double>( nnt::s_DTIME );
      double d_t = d_t_s - d_dt;
      zone.updateProperty( nnt::s_TIME, d_t );

      double d_dtm = checkTimeStep( zone, d_t );
      if( d_dtm < d_dt )
      {
        d_dt = d_dtm;
        d_t_s = d_t + d_dt;
      }

      std::copy( x.begin(), x.end(), x0.begin() );

      p_view = getEGNetView( zone );

      energy_rhs my_rhs( zone, my_evolver, my_hydro, p_view, b_observe );

      boost::apply_visitor( do_step( my_rhs, x, d_t, d_dt ), stepper );

      if( b_observe )
      {
        std::cout <<
          boost::format(
            "t = %g\nx = {%g, %g, %g}\n\n" 
          ) % d_t_s % x[0] % x[1] % x[2];
        std::cout << boost::format( "-----------\n\n" );
      }

      zone.updateProperty( nnt::s_RHO, my_hydro.computeRho( x, d_t_s, zone ) );
      zone.updateProperty( nnt::s_T9, x[2] );
      zone.updateProperty( nnt::s_TIME, d_t_s );
      my_hydro.updateOtherZoneProperties( x, zone );

    }

    double
    checkTimeStep( nnt::Zone& zone, const double d_t )
    {
      state_type dxdt(3);
      double d_dt = std::numeric_limits<double>::infinity();
      energy_rhs my_rhs(
        zone,
        my_evolver,
        my_hydro,
        getEGNetView( zone ),
        false
      );
      my_rhs( x, dxdt, d_t );
      for( size_t i = 0; i < x.size(); i++ )
      {
        if( fabs( x[i] ) > d_x_lim && fabs( dxdt[i] ) > 0 )
        {
          double d_dtm = d_x_fac * fabs( x[i] ) / fabs( dxdt[i] );
          if( d_dtm < d_dt ) { d_dt = d_dtm; }
        }
      }
      return d_dt;
    }

    void
    adjustTimeStep( nnt::Zone& zone )
    {
      double d_t = zone.getProperty<double>( nnt::s_TIME );
      double d_dt = zone.getProperty<double>( nnt::s_DTIME );
      double d_dtm = checkTimeStep( zone, d_t );
      zone.updateProperty( nnt::s_DTIME, GSL_MIN( d_dt, d_dtm ) );
    }

  private:
    network_evolver my_evolver;
    hydro_detail my_hydro;
    bool b_observe;
    std::string s_eg_nuc_xpath, s_eg_reac_xpath;
    Stepper stepper;
    state_type x, x0;
    double d_x_fac, d_x_lim;

};
 
//##############################################################################
// hydro_options().
//##############################################################################

class hydro_options :
  public hydro_detail_options, public hydro_base_options,
  public hydro_stepper_options
{

  public:
    hydro_options() :
      hydro_detail_options(), hydro_base_options(), hydro_stepper_options() {}

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description hydro("\nHydro options");
        hydro.add_options()

          ( S_EG_NUC_XPATH,
            po::value<std::string>(),
            "Energy generation nuclear XPath (default: the evolution nuclides)"
          )

          ( S_EG_REAC_XPATH,
            po::value<std::string>(),
            "Energy generation nuclear XPath (default: the evolution reactions)"
          )

          ( S_OBSERVE, po::value<bool>()->default_value( false, "false" ),
            "Observe hydro steps"
          )

          ( S_X_FAC, po::value<double>()->default_value( 0.1, "0.1" ),
            "Hydro timescale factor"
          )

          ( S_X_LIM, po::value<double>()->default_value( 1.e-10, "1.e-10" ),
            "Minimum value for time step adjustment"
          )

        ;

        getBaseOptions( hydro );
        getDetailOptions( hydro );
        getStepperOptions( hydro );

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "hydro",
            options_struct( hydro, getExample() )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }
};

} // namespace wn_user

#endif // NNP_HYDRO_HPP
