////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define hydro routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"

#include "hydro/single_zone/base/hydro.hpp"

#ifndef NNP_HYDRO_DETAIL_HPP
#define NNP_HYDRO_DETAIL_HPP

#define D_MIN_RHO    1.e-30
#define D_MIN_T9     1.e-10

#define S_RHO_FAC    "rho_dev_fac"
#define S_OSC_TIME   "osc_time"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// hydro().
//##############################################################################

class hydro : public base::hydro
{

  public:
    hydro( v_map_t& v_map ) : base::hydro( v_map )
    {
      sTau = v_map[S_OSC_TIME].as<std::string>();
      dT9_0 = v_map[nnt::s_T9_0].as<double>();
      dRho_0 = v_map[nnt::s_RHO_0].as<double>();
      dFac = v_map[S_RHO_FAC].as<double>();
      if( sTau != "inf" )
      {
        dOmega = 2. * M_PI / boost::lexical_cast<double>( sTau );
      }
    }

    void
    initialize( nnt::Zone& zone )
    {
      zone.updateProperty( nnt::s_RHO, dRho_0 );
      zone.updateProperty( nnt::s_T9, dT9_0 );
      if( initializeEquilibrium() ) { setAbundancesToEquilibrium( zone ); }
    }

    void
    operator()( nnt::Zone& zone )
    {
      if( sTau != "inf" )
      {
        double time = zone.getProperty<double>( nnt::s_TIME );
        double rho = dRho_0 * (1. - dFac * sin( dOmega * time ) );
        zone.updateProperty(
          nnt::s_RHO,
          rho
        );
        zone.updateProperty(
          nnt::s_T9,
          dT9_0 * pow(rho / dRho_0, 1./3.)
        );
      }
      else
      {
        zone.updateProperty( nnt::s_RHO, dRho_0 );
        zone.updateProperty( nnt::s_T9, dT9_0 );
      }
    }

  private:
    std::string sTau;
    double dRho_0, dT9_0, dFac, dOmega;

};
 
//##############################################################################
// hydro_options().
//##############################################################################

class hydro_options : public base::hydro_options
{

  public:
    hydro_options() : base::hydro_options() {}

    std::string
    getExample()
    {

      return
        "--" + std::string( nnt::s_T9_0 ) + " 10 " +
        "--" + std::string( nnt::s_RHO_0 ) + " 1.e9 " +
        "--" + std::string( S_OSC_TIME ) + " 1 ";

    }

    void
    getOptions( po::options_description& hydro )
    {

      try
      {

        base::hydro_options::getOptions( hydro );

        hydro.add_options()

          ( nnt::s_T9_0, po::value<double>()->default_value( 10., "10." ),
            "Initial temperature (in 10^9 K)" )

          ( nnt::s_RHO_0, po::value<double>()->default_value( 1.e8, "1.e8" ),
            "Initial density (in g/cc)" )

          ( S_OSC_TIME, po::value<std::string>()->default_value( "inf" ),
            "Oscillation timescale (s)" )

          ( S_RHO_FAC, po::value<double>()->default_value( 0.1, "0.1" ),
            "Factor oscillation away from mean density" )

        ;
        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_HYDRO_DETAIL_HPP
