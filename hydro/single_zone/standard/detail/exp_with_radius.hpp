////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define hydro routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/iter.h"

#include "hydro/single_zone/base/hydro.hpp"

#ifndef NNP_HYDRO_DETAIL_HPP
#define NNP_HYDRO_DETAIL_HPP

#define D_MIN_RHO    1.e-30
#define D_MIN_T9     1.e-10

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// hydro().
//##############################################################################

class hydro
{

  public:
    hydro( v_map_t& v_map )
    {
      sTau = v_map[nnt::s_TAU].as<std::string>();
      dT9_0 = v_map[nnt::s_T9_0].as<double>();
      dRho_0 = v_map[nnt::s_RHO_0].as<double>();
      dR_0 = v_map[nnt::s_RADIUS_0].as<double>();
      if( sTau != "inf" )
      {
        dTau = boost::lexical_cast<double>( sTau );
      }
    }

    void
    initialize( nnt::Zone& zone )
    {
      zone.updateProperty( nnt::s_RHO, dRho_0 );
      zone.updateProperty( nnt::s_RADIUS, dR_0 );
      zone.updateProperty( nnt::s_T9, dT9_0 );
    }

    void
    operator()( nnt::Zone& zone )
    {
      if( sTau != "inf" )
      {
        double time = zone.getProperty<double>( nnt::s_TIME );
        zone.updateProperty(
          nnt::s_RHO,
          GSL_MAX( dRho_0 * exp( -time / dTau ), D_MIN_RHO )
        );
        zone.updateProperty(
          nnt::s_RADIUS,
          dR_0 * exp( time / dTau )
        );
        zone.updateProperty(
          nnt::s_T9,
          GSL_MAX( dT9_0 * exp( -time / ( 3. * dTau ) ), D_MIN_T9 )
        );
      }
      else
      {
        zone.updateProperty( nnt::s_RHO, dRho_0 );
        zone.updateProperty( nnt::s_T9, dT9_0 );
        zone.updateProperty( nnt::s_RADIUS, dR_0 );
      }
    }

  private:
    std::string sTau;
    double dRho_0, dR_0, dT9_0, dTau;

};
 
//##############################################################################
// hydro_options().
//##############################################################################

class hydro_options
{

  public:
    hydro_options() {}

    std::string
    getExample()
    {

      return
        "--" + std::string( nnt::s_T9_0 ) + " 10 " +
        "--" + std::string( nnt::s_RHO_0 ) + " 1.e9 " +
        "--" + std::string( nnt::s_RADIUS_0 ) + " 1.e7 " +
        "--" + std::string( nnt::s_TAU ) + " 0.1 ";

    }

    void
    getOptions( po::options_description& hydro )
    {

      try
      {

        hydro.add_options()

          ( nnt::s_T9_0, po::value<double>()->default_value( 10., "10." ),
            "Initial temperature (in 10^9 K)" )

          ( nnt::s_RHO_0, po::value<double>()->default_value( 1.e8, "1.e8" ),
            "Initial density (in g/cc)" )

          ( nnt::s_RADIUS_0, po::value<double>()->default_value( 1.e6, "1.e6" ),
            "Initial radius (in cm)" )

          ( nnt::s_TAU, po::value<std::string>()->default_value( "inf" ),
            "Density e-folding timescale (s)" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_HYDRO_DETAIL_HPP
