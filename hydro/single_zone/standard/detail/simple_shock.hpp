////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Norberto J. Davila and Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Simple shock hydro.
////////////////////////////////////////////////////////////////////////////////

#include "math/linear_interpolator.hpp"

#include "my_global_types.h"
#include "nnt/iter.h"

#ifndef NNP_HYDRO_DETAIL_HPP
#define NNP_HYDRO_DETAIL_HPP

/**
 *  * @brief A namespace for user-defined helper functions.
 *   */
namespace wn_user
{

namespace detail
{

//#############################################################################
// Defines.
//##############################################################################

#define  S_DELTA_MAX          "delta_max"
#define  S_SHOCK_TIME         "shock time"
#define  S_SHOCK_SPEED        "shock_speed"
#define  S_TIME_CHANGE_DELTA  "time_change_delta"
#define  S_VEL_ALPHA          "velocity_alpha"

//##############################################################################
// hydro().
//##############################################################################

class hydro
{

  public:
    hydro(){}
    hydro( v_map_t& v_map )
    {
      d_velocity_alpha = v_map[S_VEL_ALPHA].as<double>();
      d_delta_max = v_map[S_DELTA_MAX].as<double>();
      d_time_change_delta = v_map[S_TIME_CHANGE_DELTA].as<double>();
    }

    void
    initialize( std::vector<nnt::Zone>& zones )
    {
      vShockTime.push_back( 0. );
      vT9.push_back( zones[0].getProperty<double>( nnt::s_T9 ) );
      vRho.push_back( zones[0].getProperty<double>( nnt::s_RHO ) );
      BOOST_FOREACH( nnt::Zone& zone, zones )
      {
        vShockTime.push_back( zone.getProperty<double>( S_SHOCK_TIME ) );
        vT9.push_back( zone.getProperty<double>( nnt::s_T9 ) );
        vRho.push_back( zone.getProperty<double>( nnt::s_RHO ) );
      }
      BOOST_FOREACH( nnt::Zone& zone, zones )
      {
        wn_user::math::linear_interpolator<> t9_interp;
        t9_interp.set( vShockTime, getZoneVector( zone, nnt::s_T9_0 ) );
        t9_interp_map[zone] = t9_interp;
      }
      BOOST_FOREACH( nnt::Zone& zone, zones )
      {
        wn_user::math::linear_interpolator<> rho_interp;
        rho_interp.set( vShockTime, getZoneVector( zone, nnt::s_RHO_0 ) );
        rho_interp_map[zone] = rho_interp;
      }
    }

    std::vector<double>
    getZoneVector( nnt::Zone& zone, std::string s )
    {
      double t_shock = zone.getProperty<double>( S_SHOCK_TIME );
      std::vector<double> v;
      for( size_t i = 0; i < vShockTime.size(); i++ )
      {
        if( vShockTime[i] < t_shock )
        {
          v.push_back( zone.getProperty<double>( s ) );
        }
        else
        {
          if( s == nnt::s_T9_0 )
          {
            v.push_back( vT9[i] );
          }
          else
          {
            v.push_back( vRho[i] );
          }
        }
      }
      return v;
    }

    std::pair<double, double>
    operator()( nnt::Zone& zone, double t )
    {
      if( t >= zone.getProperty<double>( S_SHOCK_TIME ) )
      {
        zone.updateProperty(
          nnt::s_RADIUS,
          zone.getProperty<double>( nnt::s_RADIUS )
          +
          d_velocity_alpha * 
            zone.getProperty<double>( S_SHOCK_SPEED ) *
            ( t - zone.getProperty<double>( S_SHOCK_TIME ) )
        );
      }
      return
        std::make_pair( t9_interp_map[zone]( t ), rho_interp_map[zone]( t ) );
    }

    void
    adjustTimeStep( nnt::Zone& zone )
    {
      double t = zone.getProperty<double>( nnt::s_TIME );
      double dt = zone.getProperty<double>( nnt::s_DTIME );
      double t9_old, rho_old;
      boost::tie( t9_old, rho_old ) = (*this)( zone, t );
      if( t - zone.getProperty<double>( S_SHOCK_TIME ) < d_time_change_delta )
      {
        while( dt > 1.e-100 )
        {
          double t9, rho;
          boost::tie( t9, rho ) = (*this)( zone, t + dt );
          double delta_t9 = fabs( t9 - t9_old ) / t9_old;
          double delta_rho = fabs( rho - rho_old ) / rho_old;
          if( delta_t9 > d_delta_max || delta_rho > d_delta_max )
          {
            dt /= 2;
          }
          else
          {
            break;
          }
        }
        zone.updateProperty( nnt::s_DTIME, dt );
      }
    }
         
  private:
    std::vector<double> vShockTime, vT9, vRho;
    std::map<nnt::Zone, wn_user::math::linear_interpolator<> >
      t9_interp_map, rho_interp_map;
    double d_velocity_alpha, d_delta_max, d_time_change_delta;

};
  
//##############################################################################
// hydro_options().
//##############################################################################

class hydro_options
{

  public:
    hydro_options() {}

    std::string
    getExample()
    {

      return
        "--" + std::string( S_VEL_ALPHA ) + " 0.5 ";

    }

    void
    getOptions( po::options_description& hydro )
    {

      try
      {

        hydro.add_options()

          ( S_VEL_ALPHA, po::value<double>()->default_value( 0., "0." ),
            "Post-shock velocity factor (relative to shock speed)." )

          ( S_DELTA_MAX, po::value<double>()->default_value( 0.01, "0.01" ),
            "Maximum quantity change in step." )

          ( S_TIME_CHANGE_DELTA,
            po::value<double>()->default_value( 3., "3." ),
            "Time past shock to stop checking for t9/rho changes." )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif // NNP_HYDRO_DETAIL_HPP
