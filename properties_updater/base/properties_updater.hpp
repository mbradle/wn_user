////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef WN_PROPERTIES_UPDATER_BASE_HPP
#define WN_PROPERTIES_UPDATER_BASE_HPP

namespace wn_user
{

namespace base
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class properties_updater
///
/// \brief Essential class inherited by all property updaters.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// properties_updater().
//##############################################################################

class properties_updater
{

  public:
    virtual ~properties_updater(){}
    virtual void initialize( nnt::Zone& zone ){}
    virtual void initialize( std::vector<nnt::Zone>& zones ){}
    virtual void operator()( nnt::Zone& zone ){}
    virtual void operator()( std::vector<nnt::Zone>& zones ){}

};

////////////////////////////////////////////////////////////////////////////////
///
/// \class options
///
/// \brief Essential class inherited by all option classes for
///        property updaters.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// options().
//##############################################################################

class options
{

  public:
    virtual ~options(){}
    virtual std::string getExample(){ return ""; }
    virtual void operator()( po::options_description& options_desc ){}

};

} // namespace base

} // namespace wn_user

#endif // WN_PROPERTIES_UPDATER_BASE_HPP
