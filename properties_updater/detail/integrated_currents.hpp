////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include "my_global_types.h"
#include "nnt/auxiliary.h"
#include "properties_updater/base/properties_updater.hpp"

#ifndef WN_INTEGRATED_CURRENTS_UPDATER_HPP
#define WN_INTEGRATED_CURRENTS_UPDATER_HPP

#define S_CURRENTS_NUC_XPATH            "currents_nuc_xpath"
#define S_CURRENTS_REAC_XPATH           "currents_reac_xpath"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// integrated_currents_updater().
//##############################################################################

class integrated_currents_updater : public base::properties_updater
{

  public:
    integrated_currents_updater( v_map_t& v_map ) :
      base::properties_updater(), my_program_options()
    {

      m_v_map = v_map;
      if( v_map.count( S_CURRENTS_NUC_XPATH ) )
      {
        s_currents_nuc_xpath =
          my_program_options.composeOptionStringFromVector(
            v_map, S_CURRENTS_NUC_XPATH
          );
      }
      else
      {
        s_currents_nuc_xpath = "";
      }

      if( v_map.count( S_CURRENTS_REAC_XPATH ) )
      {
        s_currents_nuc_xpath =
          my_program_options.composeOptionStringFromVector(
            v_map, S_CURRENTS_REAC_XPATH
          );
      }
      else
      {
        s_currents_nuc_xpath = "";
      }

      s_currents_reac_xpath =
        my_program_options.composeOptionStringFromVector( 
          v_map, S_CURRENTS_REAC_XPATH
        );

      if( v_map.count( nnt::s_CURRENTS_T_BEGIN ) )
      {
        d_t_begin = v_map[nnt::s_CURRENTS_T_BEGIN].as<double>();
      }
      else
      {
        d_t_begin = -std::numeric_limits<double>::infinity();
      }

      if( v_map.count( nnt::s_CURRENTS_T_END ) )
      {
        d_t_end = v_map[nnt::s_CURRENTS_T_END].as<double>();
      }
      else
      {
        d_t_end = std::numeric_limits<double>::infinity();
      }

    }

    nnt::species_list_t
    get_species_list( nnt::Zone& zone )
    {
      return
        nnt::make_species_list(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          )
        );
    }

    nnt::reaction_list_t
    get_reaction_list( nnt::Zone& zone )
    {
      return
        nnt::make_reaction_list(
          Libnucnet__Net__getReac(
            Libnucnet__NetView__getNet(
              zone.getNetView(
                s_currents_nuc_xpath.c_str(),
                s_currents_reac_xpath.c_str()
              )
            )
          )
        );
    }

    void initialize( nnt::Zone& zone )
    {

      zone.updateProperty( nnt::s_CURRENTS_T_BEGIN, d_t_begin );
      zone.updateProperty( nnt::s_CURRENTS_T_END, d_t_end );

      BOOST_FOREACH( nnt::Species species, get_species_list( zone ) )
      {
        double d_abund =
          Libnucnet__Zone__getSpeciesAbundance(
            zone.getNucnetZone(),
            species.getNucnetSpecies()
          );
        if( d_abund != 0 )
        {
          zone.updateProperty(
            nnt::s_INITIAL_ABUNDANCE,
            Libnucnet__Species__getName( species.getNucnetSpecies() ),
            Libnucnet__Zone__getSpeciesAbundance(
              zone.getNucnetZone(),
              species.getNucnetSpecies()
            )
          );
        }
      }

    }

    void initialize( std::vector<nnt::Zone>& zones )
    {
      for( size_t i = 0; i < zones.size(); i++ )
      {
        initialize( zones[i] );
      }
    }

    void operator()( nnt::Zone& zone )
    {

      if( zone.getProperty<double>( nnt::s_TIME ) < d_t_begin )
      {
        updateAbundances( zone, nnt::s_INITIAL_ABUNDANCE );
      }

      if(
        zone.getProperty<double>( nnt::s_TIME ) >= d_t_begin &&
        zone.getProperty<double>( nnt::s_TIME ) <= d_t_end
      )
      {
        updateCurrents( zone );
        updateAbundances( zone, nnt::s_FINAL_ABUNDANCE );
      }

    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      for( size_t i = 0; i < zones.size(); i++ )
      {
        (*this)( zones[i] );
      }
    }

  private:
    program_options my_program_options;
    std::string s_currents_nuc_xpath, s_currents_reac_xpath;
    double d_t_begin, d_t_end;
    v_map_t m_v_map;

    void
    updateCurrents( nnt::Zone& zone )
    {

      double d_dt = zone.getProperty<double>( nnt::s_DTIME );

      wn_user::flow_computer my_flow_computer( m_v_map );

/*
      my_flow_computer.setRates(
        Libnucnet__Zone__getNet( zone.getNucnetZone() )
      );
*/     

      wn_user::flow_map_t flow_map = my_flow_computer( zone );

      wn_user::flow_map_t::iterator it;

      for( it = flow_map.begin(); it != flow_map.end(); it++ )
      {

        double d_current = ( (it->second).first - (it->second).second ) * d_dt;

        if( d_current != 0 )
        {
          if( zone.hasProperty( nnt::s_FLOW_CURRENT, it->first.c_str() ) )
          {
            zone.updateProperty(
              nnt::s_FLOW_CURRENT, 
              it->first.c_str(),
              zone.getProperty<double>(
                nnt::s_FLOW_CURRENT, it->first.c_str()
              ) + d_current
            );
          }
          else
          {
            zone.updateProperty(
              nnt::s_FLOW_CURRENT, it->first.c_str(), d_current
            );
          } 
        }
      }
    }

    void
    updateAbundances( nnt::Zone& zone, std::string s_abundance_type )
    {
      BOOST_FOREACH( nnt::Species species, get_species_list( zone ) )
      {
        double d_abund =
          Libnucnet__Zone__getSpeciesAbundance(
            zone.getNucnetZone(),
            species.getNucnetSpecies()
          );
        if( d_abund != 0 )
        {
          zone.updateProperty(
            s_abundance_type,
            Libnucnet__Species__getName( species.getNucnetSpecies() ),
            Libnucnet__Zone__getSpeciesAbundance(
              zone.getNucnetZone(),
              species.getNucnetSpecies()
            )
          );
        }
      }
    }

};
    
//##############################################################################
// integrated_currents_options().
//##############################################################################

class integrated_currents_options : public base::options
{

  public:
    integrated_currents_options() : base::options(){}

    void operator()( po::options_description& properties_updater )
    {

      try
      {

        properties_updater.add_options()

        // Option for value of XPath to select nuclei
        (
          S_CURRENTS_NUC_XPATH,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          "XPath to select nuclei for currents (default: all nuclides)"
        )
    
        // Option for value of XPath to select reactions
        (
          S_CURRENTS_REAC_XPATH,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          "XPath to select reactions for currents (default: all reactions)"
        )
    
        // Option for time range of adding up currents
        (
          nnt::s_CURRENTS_T_BEGIN,
          po::value<double>(),
          "Starting time to record currents (default: start of calculation)"
        )
    
        // Option for time range of adding up currents
        (
          nnt::s_CURRENTS_T_END,
          po::value<double>(),
          "Ending time to record currents (default: end of calculation)"
        )
    
        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_INTEGRATED_CURRENTS_UPDATER_HPP
