////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include "my_global_types.h"
#include "nnt/auxiliary.h"
#include "equilibrium/ng_equilibrium.hpp"

#include "properties_updater/base/properties_updater.hpp"

#ifndef WN_NG_EQUIL_UPDATER_HPP
#define WN_NG_EQUIL_UPDATER_HPP

#define S_MUN_EQ            "mun_eq"
#define S_MUNKT_EQ          "munkT_eq"
#define S_NG_EQ             "Y_ng_eq"
#define S_ZEQ_XPATH         "z_eq_xpath"
#define S_ZEQ_ABUND_CUTOFF  "z_eq_y_cutoff"
#define S_ZEQ_T9_CUTOFF     "z_eq_t9_cutoff"

namespace wn_user
{

namespace detail
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class ng_equil_updater
///
/// \brief A class to update the (n,g)-(g,n) equilibrium for chosen species
///        and zones.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// ng_equil_updater().
//##############################################################################

class ng_equil_updater : public base::properties_updater
{

  public:

/// The constructor.
/// \param v_map The options variable map.

    ng_equil_updater( v_map_t& v_map ) :
      base::properties_updater(), my_program_options()
    {
      if( v_map.count( S_ZEQ_XPATH ) )
      {
        s_zeq_xpath =
          my_program_options.composeOptionStringFromVector(
            v_map, S_ZEQ_XPATH
          );
        d_y_cutoff = v_map[S_ZEQ_ABUND_CUTOFF].as<double>();
        d_t9_cutoff = v_map[S_ZEQ_T9_CUTOFF].as<double>();
      }
    }

/// The updater for a zone.
/// \param zone The zone.
/// \return On successful return, the (n,g)-(g,n) equilibrium abundances
///         for species in the zone have been updated.

    void operator()( nnt::Zone& zone )
    {
      updateNGEquilibrium( zone );
    }

/// The updater for zones.
/// \param zone The zones.
/// \return On successful return, the (n,g)-(g,n) equilibrium abundances
///         for species in the zones have been updated.

    void operator()( std::vector<nnt::Zone>& zones )
    {
      zones[0].getNetView( s_zeq_xpath.c_str(), "" );
#ifndef NO_OPENMP
      #pragma omp parallel for schedule( dynamic, 1 )
#endif
      for( size_t i = 1; i < zones.size(); i++ )
      {
        Libnucnet__Zone__copy_net_views(
          zones[i].getNucnetZone(),
          zones[0].getNucnetZone()
        );
      }
#ifndef NO_OPENMP
      #pragma omp parallel for schedule( dynamic, 1 )
#endif
      for( size_t i = 0; i < zones.size(); i++ )
      {
        updateNGEquilibrium( zones[i] );
      }
    }

  private:
    program_options my_program_options;
    double d_t9_cutoff, d_y_cutoff;
    std::string s_zeq_xpath;

    void
    updateNGEquilibrium( nnt::Zone& zone )
    {
      if(
        zone.getProperty<double>( nnt::s_TIME ) <=
        zone.getProperty<double>( nnt::s_T_END )
      )
      {
        if( !s_zeq_xpath.empty() )
        {
          if( zone.getProperty<double>( nnt::s_T9 ) > d_t9_cutoff )
          {
            computeAndStoreNgEquilibrium( zone, s_zeq_xpath );
          }
        }
      }
    }

    void
    computeAndStoreNgEquilibrium( nnt::Zone& zone, std::string s_zeq_xpath )
    {
      ng_equilibrium ng( zone );

      double d_guess = ng.computeNeutronChemicalPotentialKT( zone );

      wn_user::math::bracketer_by_expansion<double> br( ng );
      double x1 = GSL_MIN( 0.9 * d_guess, 1.1 * d_guess );
      double x2 = GSL_MAX( 0.9 * d_guess, 1.1 * d_guess );
      std::pair<double, double> p = br( x1, x2 );

      std::pair<double, double> result =
        boost::math::tools::bisect(
          ng,
          p.first,
          p.second,
          boost::math::tools::eps_tolerance<double>( )
        );
      double x = ( result.first + result.second ) / 2;

      zone.updateProperty( S_MUNKT_EQ, x );
      zone.updateProperty(
        S_MUN_EQ,
        x * nnt::compute_kT_in_MeV( zone.getProperty<double>( nnt::s_T9 ) )
      );

      boost::format fmt( "%d" );

      ng( x );

      BOOST_FOREACH(
        nnt::Species species,
        nnt::make_species_list(
          Libnucnet__Net__getNuc(
            Libnucnet__NetView__getNet(
              zone.getNetView( s_zeq_xpath.c_str(), "" )
            )
          )
        )
      )
      {
        double d_abund = ng.computeSpeciesAbundance( species, x );
        if( d_abund > d_y_cutoff )
        {
          zone.updateProperty(
            S_NG_EQ,
            Libnucnet__Species__getName( species.getNucnetSpecies() ),
            d_abund
          );
        }
      }
    }

};
    
////////////////////////////////////////////////////////////////////////////////
///
/// \class ng_equil_options
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// ng_equil_options().
//##############################################################################

class ng_equil_options : public base::options
{

  public:
    ng_equil_options() : base::options(){}

/// \example_string

    std::string
    getExample()
    {
      return "";
    }

/// \options_description

    void
    operator()( po::options_description& options_desc )
    {

      try
      {

        options_desc.add_options()

        // Option for (n,g)-(g,n) XPath
        (
          S_ZEQ_XPATH,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          "XPath to select z's to record for (n,g)-(g,n) equilibrium"
        )

        // Option for (n,g)-(g,n) abundance cutoff
        (
          S_ZEQ_ABUND_CUTOFF,
          po::value<double>()->default_value( 1.e-25, "1.e-25" ),
          "Abundance below which to not record (n,g)-(g,n) equilibrium Y"
        )

        // Option for (n,g)-(g,n) temperature cutoff
        (
          S_ZEQ_T9_CUTOFF,
          po::value<double>()->default_value( 1.e-6, "1.e-6" ),
          "T9 below which to stop computing (n,g)-(g,n) equilibrium"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_NG_EQUIL_UPDATER_HPP
