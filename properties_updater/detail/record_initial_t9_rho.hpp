////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2021 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "nnt/auxiliary.h"

#include "properties_updater/base/properties_updater.hpp"

#ifndef WN_RECORD_INITIAL_T9_RHO_UPDATER_HPP
#define WN_RECORD_INITIAL_T9_RHO_UPDATER_HPP

namespace wn_user
{

namespace detail
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class record_initial_t9_rho_updater
///
/// \brief A class to update each zone with initial t9 and rho.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// record_initial_t9_rho_updater().
//##############################################################################

class record_initial_t9_rho_updater : public base::properties_updater
{

  public:
    record_initial_t9_rho_updater( v_map_t& v_map ) : base::properties_updater()
    { }

    void initialize( nnt::Zone& zone )
    {
      zone.updateProperty( nnt::s_T9_0, zone.getProperty<double>( nnt::s_T9 ) );
      zone.updateProperty( nnt::s_RHO_0, zone.getProperty<double>( nnt::s_RHO ) );
    }

};
    
////////////////////////////////////////////////////////////////////////////////
///
/// \class record_initial_t9_rho_options
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// record_initial_t9_rho_options().
//##############################################################################

class record_initial_t9_rho_options : public base::options
{

  public:
    record_initial_t9_rho_options() : base::options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    operator()( po::options_description& options_desc )
    {

      try
      {

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_RECORD_INITIAL_T9_RHO_UPDATER_HPP
