////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "properties_updater/base/properties_updater.hpp"
#include "utility/kT_in_MeV_computer.hpp"

#ifndef WN_CLASSICAL_CHEMICAL_POTENTIAL_UPDATER_HPP
#define WN_CLASSICAL_CHEMICAL_POTENTIAL_UPDATER_HPP

#define S_MU                "mu"
#define S_MU_KT             "mu_kT"

#define S_MU_SPECIES        "mu_species"
#define S_REL_KT            "mu_rel_kT"

namespace wn_user
{

namespace detail
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class classical_chemical_potential_updater
///
/// \brief A class to update classical chemical potentials for species.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// classical_chemical_potential_updater().
//##############################################################################

class classical_chemical_potential_updater : public base::properties_updater
{

  public:

/// The constructor.
/// \param v_map The options variable map.

    classical_chemical_potential_updater( v_map_t& v_map ) :
      base::properties_updater(), my_program_options(), kT_MeV()
    {
      if( v_map.count( S_MU_SPECIES ) )
      {
        BOOST_FOREACH(
          const std::vector<std::vector<std::string> >::value_type& v_m,
          my_program_options.composeOptionVectorOfVectors(
            v_map, S_MU_SPECIES, ",", "", 1
          )
        )
        {
          v_s.push_back( v_m[0] );
        }
      }
      b_mukT = v_map[S_REL_KT].as<bool>();
    }

/// The updater for a zone.
/// \param zone The zone.
/// \return On successful return, the chemical potentials for the chosen
///         species in the zone have been updated.

    void operator()( nnt::Zone& zone )
    {
       updateClassicalChemicalPotential( zone );
    }

/// The updater for zones.
/// \param zone The zones.
/// \return On successful return, the chemical potentials for the chosen
///         species in the zones have been updated.

    void operator()( std::vector<nnt::Zone>& zones )
    {
       for( size_t i = 0; i < zones.size(); i++ )
       {
         updateClassicalChemicalPotential( zones[i] );
       }
    }

  private:
    program_options my_program_options;
    wn_user::utility::kT_in_MeV_computer kT_MeV;
    std::vector<std::string> v_s;
    bool b_mukT;

    void updateClassicalChemicalPotential( nnt::Zone& zone )
    {
      BOOST_FOREACH( std::string s, v_s )
      {
        Libnucnet__Species * p_species =
          Libnucnet__Nuc__getSpeciesByName(
            Libnucnet__Net__getNuc(
              Libnucnet__Zone__getNet( zone.getNucnetZone() )
            ),
            s.c_str()
          );
        if( !p_species )
        {
          std::cerr << s <<
            " is an invalid species for chemical potential." << std::endl;
          exit( EXIT_FAILURE );
        }
        double d_y =
          Libnucnet__Zone__getSpeciesAbundance(
            zone.getNucnetZone(),
            p_species
          );
        double d_y_q =
          Libnucnet__Species__computeQuantumAbundance(
            p_species,
            zone.getProperty<double>( nnt::s_T9 ),
            zone.getProperty<double>( nnt::s_RHO )
          );
        double mu_kT = log( d_y / d_y_q );
        if( b_mukT )
        {
          zone.updateProperty( S_MU_KT, s.c_str(), mu_kT );
        }
        else
        {
          zone.updateProperty(
            S_MU,
            s.c_str(),
            mu_kT * kT_MeV( zone.getProperty<double>( nnt::s_T9 ) )
          );
        }
      }
    }

};
    
////////////////////////////////////////////////////////////////////////////////
///
/// \class classical_chemical_potential_options
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// classical_chemical_potential_options().
//##############################################################################

class classical_chemical_potential_options : public base::options
{

  public:
    classical_chemical_potential_options() : base::options() {}

/// \example_string

    std::string
    getExample()
    {
      return
        "--" + std::string( S_MU_SPECIES ) + " \"h1, n\" ";
    }

/// \options_description

    void operator()( po::options_description& options_desc )
    {

      try
      {

        options_desc.add_options()

        // Option for chemical potential
        (
          S_MU_SPECIES,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          "Comma-delimited list of species whose chemical potentials are to be recorded"
        )

        // Option for dimensions of mu
        (
          S_REL_KT,
          po::value<bool>()->default_value( false, "false" ),
          "Record mu/kT instead of mu"
        )
        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_CLASSICAL_CHEMICAL_POTENTIAL_UPDATER_HPP
