////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include "my_global_types.h"
#include "nnt/auxiliary.h"

#include "properties_updater/base/properties_updater.hpp"

#ifndef WN_ABUNDANCE_CHANGE_UPDATER_HPP
#define WN_ABUNDANCE_CHANGE_UPDATER_HPP

#define S_DY              "dy"
#define S_DY_CUTOFF       "dy_cutoff"

namespace wn_user
{

namespace detail
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class abundance_change_updater
///
/// \brief A class to record abundance changes.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// abundance_change_updater().
//##############################################################################

class abundance_change_updater : public base::properties_updater
{

  public:
    abundance_change_updater( v_map_t& v_map ) : base::properties_updater()
    {
      d_cutoff = v_map[S_DY_CUTOFF].as<double>();
    }

    void operator()( nnt::Zone& zone )
    {
      updateAbundanceChanges( zone );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      for( size_t i = 0; i < zones.size(); i++ )
      {
        (*this)( zones[i] );
      }
    }

  private:
    double d_cutoff;

    void
    updateAbundanceChanges( nnt::Zone& zone )
    {
      BOOST_FOREACH(
        nnt::Species species,
        nnt::make_species_list(
           Libnucnet__Net__getNuc(
             Libnucnet__Zone__getNet( zone.getNucnetZone() )
           )
        )
      )
      {
        double dy = 
          Libnucnet__Zone__getSpeciesAbundanceChange(
            zone.getNucnetZone(),
            species.getNucnetSpecies()
          );
        if( fabs(dy) > d_cutoff )
        {
          zone.updateProperty(
            S_DY,
            Libnucnet__Species__getName( species.getNucnetSpecies() ),
            dy
          );
        }
      }
    }

};
    
////////////////////////////////////////////////////////////////////////////////
///
/// \class abundance_change_options
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// abundance_change_options().
//##############################################################################

class abundance_change_options : public base::options
{

  public:
    abundance_change_options() : base::options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    operator()( po::options_description& options_desc )
    {

      try
      {

        options_desc.add_options()

        // Option for species timescale
        (
          S_DY_CUTOFF,
          po::value<double>()->default_value( 1.e-25, "1.e-25" ),
          "Cutoff magnitude for recording abundance change"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_ABUNDANCE_CHANGE_UPDATER_HPP
