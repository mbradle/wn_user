////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"

#include "properties_updater/base/properties_updater.hpp"

#ifndef WN_METALLICITY_UPDATER_HPP
#define WN_METALLICITY_UPDATER_HPP

#define S_METALLICITY    "metallicity"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// metallicity_updater().
//##############################################################################

class metallicity_updater : public base::properties_updater
{

  public:
    metallicity_updater( ) : base::properties_updater(){}

    void
    updateMetallicity( nnt::Zone& zone )
    {
      zone.updateProperty(
        S_METALLICITY,
        user::compute_cluster_abundance_moment( zone, "[z > 2]", "a", 1 )
      );
    }

    void initialize( nnt::Zone& zone )
    {
      zone.updateProperty( S_METALLICITY, 0. );
    }

    void initialize( std::vector<nnt::Zone>& zones )
    {
#ifndef NO_OPENMP
  #pragma omp parallel for schedule( dynamic, 1 )
#endif
      for( size_t i = 0; i < zones.size(); i++ )
      {
        initialize( zones[i] );
      }
    }

    void operator()( nnt::Zone& zone )
    {
      updateMetallicity( zone );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
#ifndef NO_OPENMP
  #pragma omp parallel for schedule( dynamic, 1 )
#endif
      for( size_t i = 0; i < zones.size(); i++ )
      {
        updateMetallicity( zones[i] );
      }
    }
    
};
    
//##############################################################################
// metallicity_options().
//##############################################################################

class metallicity_options : public base::options
{

  public:
    metallicity_options() : base::options(){}

    void operator()( po::options_description& properties_updater ){}
};

} // namespace detail

} // namespace wn_user

#endif // WN_METALLICITY_UPDATER_HPP
