////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include "my_global_types.h"
#include "nnt/auxiliary.h"

#include "properties_updater/base/properties_updater.hpp"

#ifndef WN_NEUTRINOS_UPDATER_HPP
#define WN_NEUTRINOS_UPDATER_HPP

namespace wn_user
{

namespace detail
{

//##############################################################################
// neutrinos_updater().
//##############################################################################

class neutrinos_updater : public base::properties_updater
{

  public:
    neutrinos_updater( v_map_t& v_map ) : base::properties_updater(),
      my_neutrinos( v_map )
    { }

    void updateNeutrinoFluxesAndTemperatures( nnt::Zone& zone )
    {
      BOOST_FOREACH( std::string s, my_neutrinos.getNeutrinosVector() )
      {
        zone.updateProperty(
          nnt::s_NU_FLUX,
          s,
          my_neutrinos.computeNeutrinoFlux( s, zone )
        );
        zone.updateProperty(
          nnt::s_NU_T,
          s,
          my_neutrinos.getTemperature( s, zone )
        );
      }
    }


    void operator()( nnt::Zone& zone )
    {
      updateNeutrinoFluxesAndTemperatures( zone );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      for( size_t i = 0; i < zones.size(); i++ )
      {
        updateNeutrinoFluxesAndTemperatures( zones[i] );
      }
    }
    
  private:
    neutrino_collection my_neutrinos;
    
};
    
} // namespace detail

} // namespace wn_user

#endif // WN_NEUTRINOS_UPDATER_HPP
