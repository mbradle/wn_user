////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include "my_global_types.h"
#include "nnt/auxiliary.h"

#include "properties_updater/base/properties_updater.hpp"

#ifndef WN_MOMENT_ABUNDANCE_UPDATER_HPP
#define WN_MOMENT_ABUNDANCE_UPDATER_HPP

#define S_ABUNDANCE    "abundance"
#define S_EXPONENT     "exponent"
#define S_MOMENT       "moment_abund"
#define S_NUCLEON      "nucleon"
#define S_XPATH        "xpath"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// moment_abundance_updater().
//##############################################################################

class moment_abundance_updater : public base::properties_updater
{

  public:
    moment_abundance_updater( v_map_t& v_map ) :
      base::properties_updater(), my_program_options()
    {
      if( v_map.count( S_MOMENT ) )
      {
        v_moment =
          my_program_options.composeOptionVectorOfVectors( v_map, S_MOMENT, 3 );
      }
    }

    void
    updateClusterAbundances( nnt::Zone& zone )
    {
      for( size_t i = 0; i < v_moment.size(); i++ )
      {
        zone.updateProperty(
          S_MOMENT,
          boost::lexical_cast<std::string>( i ).c_str(),
          S_XPATH,
          v_moment[i][0].c_str()
        );
        zone.updateProperty(
          S_MOMENT,
          boost::lexical_cast<std::string>( i ).c_str(),
          S_NUCLEON,
          v_moment[i][1].c_str()
        );
        zone.updateProperty(
          S_MOMENT,
          boost::lexical_cast<std::string>( i ).c_str(),
          S_EXPONENT,
          v_moment[i][2].c_str()
        );
        zone.updateProperty(
          S_MOMENT,
          boost::lexical_cast<std::string>( i ).c_str(),
          S_ABUNDANCE,
          user::compute_cluster_abundance_moment(
            zone,
            v_moment[i][0].c_str(),
            v_moment[i][1].c_str(),
            boost::lexical_cast<double>( v_moment[i][2].c_str() )
          )
        );
      }
    }

    void operator()( nnt::Zone& zone )
    {
      updateClusterAbundances( zone );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      for( size_t i = 0; i < zones.size(); i++ )
      {
        updateClusterAbundances( zones[i] );
      }
    }

  private:
    program_options my_program_options;
    std::vector<std::vector<std::string> > v_moment;

};
    
//##############################################################################
// moment_abundance_options().
//##############################################################################

class moment_abundance_options : public base::options
{

  public:
    moment_abundance_options() : base::options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    operator()( po::options_description& properties_updater )
    {

      try
      {

        properties_updater.add_options()

        // Option for species timescale
        (
          S_MOMENT,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          "Moments whose abundances are to be recorded (Enter as triplet {XPath; nucleon; exponent})"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_MOMENT_ABUNDANCE_UPDATER_HPP
