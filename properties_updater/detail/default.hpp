////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include "my_global_types.h"
#include "nnt/auxiliary.h"

#include "properties_updater/base/properties_updater.hpp"

#ifndef WN_DEFAULT_PROPERTIES_UPDATER_HPP
#define WN_DEFAULT_PROPERTIES_UPDATER_HPP

namespace wn_user
{

namespace detail
{

////////////////////////////////////////////////////////////////////////////////
///
/// \class default_updater
///
/// \brief The default properties updater implementation class.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// default_updater().
//##############################################################################

class default_updater : public base::properties_updater
{

  public:
    default_updater( v_map_t& v_map ) :
      base::properties_updater() {}

};
    
////////////////////////////////////////////////////////////////////////////////
///
/// \class default_options
///
/// \brief The default properties updater detail options.
///
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// default_options().
//##############################################################################

class default_options :
  public base::options
{

  public:
    default_options() : base::options() {}

};

} // namespace detail

} // namespace wn_user

#endif // WN_DEFAULT_PROPERTIES_UPDATER_HPP
