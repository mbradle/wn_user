////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default.hpp
//! \brief A file to define properties updater routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/tokenizer.hpp>
#include <boost/algorithm/string.hpp>
#include "my_global_types.h"
#include "nnt/auxiliary.h"

#include "properties_updater/base/properties_updater.hpp"

#ifndef WN_EXPOSURE_UPDATER_HPP
#define WN_EXPOSURE_UPDATER_HPP

#define S_EXPOSURE_SPECIES        "exposure_species"

namespace wn_user
{

namespace detail
{

//##############################################################################
// exposure_updater().
//##############################################################################

class exposure_updater : public base::properties_updater
{

  public:

/// The constructor.
/// \param_v_map

    exposure_updater( v_map_t& v_map ) :
      base::properties_updater(), my_program_options()
    {
      if( v_map.count( S_EXPOSURE_SPECIES ) )
      {
        BOOST_FOREACH(
          const std::vector<std::vector<std::string> >::value_type& v_m,
          my_program_options.composeOptionVectorOfVectors(
            v_map, S_EXPOSURE_SPECIES, ",", "", 1
          )
        )
        {
          v_exposure_s.push_back( v_m[0] );
        }
      }
    }

/// \prop_updater_init_zone

    void initialize( nnt::Zone& zone )
    {
      BOOST_FOREACH( std::string s, v_exposure_s )
      {
        zone.updateProperty( nnt::s_EXPOSURE, s, 0. );
      }
    }

/// \prop_updater_init_zones

    void initialize( std::vector<nnt::Zone>& zones )
    {
      for( size_t i = 0; i < zones.size(); i++ )
      {
        initialize( zones[i] );
      }
    }

/// The exposure updater for a zone.
/// \param zone The zone.
/// \return On successful return, the exposures for the chosen species for
///         the zone have been updated.

    void operator()( nnt::Zone& zone )
    {

      BOOST_FOREACH( std::string s, v_exposure_s )
      {
        updateExposure( s, zone );
      }

    }

/// The exposure updater for zones.
/// \param zone The zones.
/// \return On successful return, the exposures for the chosen species for
///         the zones have been updated.

    void operator()( std::vector<nnt::Zone>& zones )
    {
      for( size_t i = 0; i < zones.size(); i++ )
      {
        BOOST_FOREACH( std::string s, v_exposure_s )
        {
          updateExposure( s, zones[i] );
        }
      }
    }
    
  private:
    program_options my_program_options;
    std::vector<std::string> v_exposure_s;
    bool b_set;
    
    double compute_vThermal( Libnucnet__Species * p_species, double d_t9 )
    {

      if( !p_species )
      {
        std::cerr << "No such species." << std::endl;
        exit( EXIT_FAILURE );
      }

      return
        sqrt(
          2. * GSL_CONST_CGSM_BOLTZMANN * d_t9 *GSL_CONST_NUM_GIGA /
          ( 
            GSL_CONST_CGSM_UNIFIED_ATOMIC_MASS *
              Libnucnet__Species__getA( p_species )
            +
            Libnucnet__Species__getMassExcess( p_species ) *
              GSL_CONST_CGSM_ELECTRON_VOLT *
              GSL_CONST_NUM_MEGA /
              gsl_pow_2( GSL_CONST_CGSM_SPEED_OF_LIGHT )
          )
        );

    }

    void
    updateExposure(
      std::string s_species,
      nnt::Zone& zone
    )
    {

      Libnucnet__Species * p_species =
        Libnucnet__Nuc__getSpeciesByName(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          ),
          s_species.c_str()
        );

      if( !p_species ) return;

      zone.updateProperty(
        nnt::s_EXPOSURE,
        s_species,
        zone.getProperty<double>( nnt::s_EXPOSURE, s_species )
        +
        compute_vThermal(
          p_species,
          zone.getProperty<double>( nnt::s_T9 )
        ) *
        Libnucnet__Zone__getSpeciesAbundance(
          zone.getNucnetZone(),
          p_species
        ) *
        zone.getProperty<double>( nnt::s_RHO ) *
        GSL_CONST_NUM_AVOGADRO *
        zone.getProperty<double>( nnt::s_DTIME ) *
        GSL_CONST_CGSM_BARN *
        GSL_CONST_NUM_MILLI
      );
    }

};
    
//##############################################################################
// exposure_options().
//##############################################################################

class exposure_options : public base::options
{

  public:
    exposure_options() : base::options(){}

    void operator()( po::options_description& options_desc )
    {

      try
      {

        options_desc.add_options()

        // Option for species timescale
        (
          S_EXPOSURE_SPECIES,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          "Comma-delimited list of species whose exposures are to be recorded"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif // WN_EXPOSURE_UPDATER_HPP
