//////////////////////////////////////////////////////////////////////////////
//  
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to compute links in multi-zone chemical evolution model.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include <boost/assign.hpp>
#include <boost/tuple/tuple.hpp>
#include <boost/tuple/tuple_comparison.hpp>
#include <boost/optional.hpp>

#include "user/multi_zone_utilities.h"
#include "utility/zone_copier.hpp"

#include <boost/graph/iteration_macros.hpp>

//##############################################################################
// Check if already included.
//##############################################################################

#ifndef WN_GCE_DETAIL_HPP
#define WN_GCE_DETAIL_HPP

namespace wn_user
{

namespace detail
{

#define S_N_X_ZONES   "number_x_zones"
#define S_N_Y_ZONES   "number_y_zones"
#define S_N_Z_ZONES   "number_z_zones"

#define S_HALO        "halo"
#define S_HALO_ZONE   "halo_zone"
#define S_HOT_ZONES   "hot_zones"

#define S_INITIAL_TOTAL_MASS  "initial_total_mass"

typedef boost::tuple<std::string, std::string, std::string> zone_tuple_t;

typedef std::vector<zone_tuple_t> zone_tuple_vector_t;

//##############################################################################
// gce().
//##############################################################################

class gce
{

  public:
    gce() {}
    gce( v_map_t& v_map, Libnucnet * p_nucnet )
    {
      pNucnet = p_nucnet;

      b_halo = v_map[S_HALO_ZONE].as<bool>();

      b_hot = v_map[S_HOT_ZONES].as<bool>();

      d_total_mass = v_map[S_INITIAL_TOTAL_MASS].as<double>();

      n_grid = v_map[S_N_X_ZONES].as<size_t>() *
               v_map[S_N_Y_ZONES].as<size_t>() *
               v_map[S_N_Z_ZONES].as<size_t>();

      for( size_t i = 0; i < v_map[S_N_X_ZONES].as<size_t>(); i++ )
      {
        for( size_t j = 0; j < v_map[S_N_Y_ZONES].as<size_t>(); j++ )
        {
          for( size_t k = 0; k < v_map[S_N_Z_ZONES].as<size_t>(); k++ )
          {
            zone_tuple_vector.push_back
            (
              boost::make_tuple(
                boost::lexical_cast<std::string>( i ).c_str(),
                boost::lexical_cast<std::string>( j ).c_str(),
                boost::lexical_cast<std::string>( k ).c_str()
              )
            );
            if( b_hot )
            {
              zone_tuple_vector.push_back
              (
                boost::make_tuple(
                  boost::lexical_cast<std::string>( i ).c_str(),
                  boost::lexical_cast<std::string>( j ).c_str(),
                  (boost::lexical_cast<std::string>( k ) + "h").c_str()
                )
              );
            }
           }
         }
      }

      if( b_halo )
      {
        zone_tuple_vector.push_back
        (
          boost::make_tuple( S_HALO, "0", "0" )
        );
      }
    }


    bool
    isHotZone( Libnucnet__Zone * p_zone )
    {
      std::string s3 = Libnucnet__Zone__getLabel( p_zone, 3 );
      return ( s3.find( "h" ) != std::string::npos );
    }

    bool
    isHaloZone( Libnucnet__Zone * p_zone )
    {
      std::string s1 = Libnucnet__Zone__getLabel( p_zone, 1 );
      return( s1.find( "halo" ) != std::string::npos );
    }

    std::vector<nnt::Zone> getStarFormingZones()
    {

      std::vector<nnt::Zone> zones;

      BOOST_FOREACH(
        nnt::Zone zone, nnt::make_zone_list( pNucnet )
      )
      {
        if(
          !(*this).isHotZone( zone.getNucnetZone() ) &&
          !(*this).isHaloZone( zone.getNucnetZone() )
        )
        {
          zones.push_back( zone );
        }
      }

      return zones;

    }

    std::vector<nnt::Zone>
    createNewZones( nnt::Zone base_zone )
    {
      std::vector<nnt::Zone> zones;

      base_zone.updateProperty( nnt::s_ZONE_MASS, 0. );
      base_zone.updateProperty( nnt::s_RHO, 1. );
      base_zone.updateProperty( nnt::s_T9, 0.1 );

      utility::zone_copier
        zc( Libnucnet__Zone__getNet( base_zone.getNucnetZone() ) );

      BOOST_FOREACH( zone_tuple_t& t, zone_tuple_vector )
      {
        std::vector<std::string> v_labels;
        v_labels.push_back( t.get<0>() );
        v_labels.push_back( t.get<1>() );
        v_labels.push_back( t.get<2>() );
        nnt::Zone new_zone;
        new_zone.setNucnetZone( zc( base_zone.getNucnetZone(), v_labels ) );
        zones.push_back( new_zone );
      }

      Libnucnet__removeZone( pNucnet, base_zone.getNucnetZone() );

      BOOST_FOREACH( nnt::Zone& zone, zones )
      {
        Libnucnet__addZone( pNucnet, zone.getNucnetZone() );
      }
    
      if( b_halo )
      {
        BOOST_FOREACH( nnt::Zone& zone, zones )
        {
          if( isHaloZone( zone.getNucnetZone() ) )
          {
            zone.updateProperty( nnt::s_ZONE_MASS, d_total_mass );
          }
        }
      }
      else
      {
        BOOST_FOREACH( nnt::Zone& zone, zones )
        {
          if( !isHotZone( zone.getNucnetZone() ) )
          {
            zone.updateProperty( nnt::s_ZONE_MASS, d_total_mass / n_grid );
          }
        }
      }
      return zones;

    }

    void
    setZoneRateDataUpdateFunction( nnt::Zone& zone )
    {
      user::set_rate_data_update_function( zone );
    }

    nnt::Zone
    getInjectaZone( const StarSystem& star_system )
    {
      nnt::Zone zone;
      Star star = star_system.getTopStar();
      std::string s3 = star.getProperty<std::string>( nnt::s_GRID_X );

      Libnucnet__Zone * p_zone =
	Libnucnet__getZoneByLabels(
          pNucnet,
          star.getProperty<std::string>( nnt::s_GRID_X ).c_str(),
          star.getProperty<std::string>( nnt::s_GRID_Y ).c_str(),
          (star.getProperty<std::string>( nnt::s_GRID_Z ) + 'h').c_str()
       );

      if( p_zone )
      {
        zone.setNucnetZone( p_zone );
      }
      else
      {
        p_zone =
  	  Libnucnet__getZoneByLabels(
            pNucnet,
            star.getProperty<std::string>( nnt::s_GRID_X ).c_str(),
            star.getProperty<std::string>( nnt::s_GRID_Y ).c_str(),
            star.getProperty<std::string>( nnt::s_GRID_Z ).c_str()
         );

        zone.setNucnetZone( p_zone );
      }
      return zone;
    }

    Libnucnet * getNucnet(){ return pNucnet; }

  private:
    Libnucnet * pNucnet;
    bool b_halo, b_hot;
    size_t n_grid;
    double d_total_mass;
    zone_tuple_vector_t zone_tuple_vector;

};

//##############################################################################
// gce_options().
//##############################################################################

class gce_options
{

  public:
    gce_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    get( po::options_description& gce )
    {

      try
      {

        gce.add_options()

        // Option for number of x zones
        (  S_N_X_ZONES, po::value<size_t>()->default_value( 10 ),
           "Number of x zones" )

        // Option for number of y zones
        (  S_N_Y_ZONES, po::value<size_t>()->default_value( 1 ),
           "Number of y zones" )

        // Option for number of z zones
        (  S_N_Z_ZONES, po::value<size_t>()->default_value( 1 ),
           "Number of z zones" )

        // Option for halo zone
        (  S_HALO_ZONE, po::value<bool>()->default_value( true, "true" ),
           "Include halo zone" )

        // Option for hot zones
        (  S_HOT_ZONES, po::value<bool>()->default_value( false, "false" ),
           "Include hot zones" )

        // Option for total mass
        (  S_INITIAL_TOTAL_MASS,
	   po::value<double>()->default_value( 1.e7, "1.e7" ),
           "Initial total mass" )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

} // namespace detail

} // namespace wn_user

#endif  // WN_GCE_DETAIL_HPP

