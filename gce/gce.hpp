////////////////////////////////////////////////////////////////////////////////
// 
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Utility routines for multi-zone chemical evolution code.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include "my_global_types.h"

#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/poisson_distribution.hpp>
#include <boost/random/uniform_01.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include <boost/assign/list_of.hpp>

#ifndef WN_GCE_HPP
#define WN_GCE_HPP

//##############################################################################
// Defines.
//##############################################################################

#define   S_STAR_FORM_TIME    "star_form_time"
#define   S_SCHMIDT_EXPONENT  "schmidt_exponent"
#define   S_RANDOM_SEED       "random_seed"

//##############################################################################
// Types.
//##############################################################################

namespace ac = boost::accumulators;

typedef
  ac::accumulator_set<
    double,
    ac::stats<
      ac::tag::mean(ac::immediate),
      ac::tag::sum,
      ac::tag::moment<2>
    >
  > accum_t;

typedef std::map<double, accum_t> star_map_t;
  
namespace wn_user
{

//##############################################################################
// gce().
//##############################################################################

class gce : public detail::gce
{
  public:
    gce() : detail::gce() {}
    gce(
      v_map_t& v_map,
      Libnucnet * p_nucnet
    ) : detail::gce( v_map, p_nucnet )
    {
      dStarFormTime = v_map[S_STAR_FORM_TIME].as<double>() * D_YEAR;
      dSchmidtExponent = v_map[S_SCHMIDT_EXPONENT].as<double>();

      pImf = new imf( v_map );

      pMassTracker = new mass_tracker( v_map, p_nucnet );

      pStarTracker = new star_tracker( v_map, p_nucnet );

      pYields = new yields( v_map );

      pRemnants = new remnants( v_map );;

      if( v_map.count( S_RANDOM_SEED ) )
        gen.seed( v_map[S_RANDOM_SEED].as<unsigned int>() );
      else
        gen.seed( static_cast<unsigned int>( std::time( 0 ) ) );
    }

    void setYields( std::vector<nnt::Zone>& zones )
    {
      pYields->setYields( getNucnet() );
      pYields->createHistoryNucnet( getNucnet() );
      pYields->setHistoryZones( zones );
    }

    boost::tuple<double,double,double>
    compute3dSubgridLocation( )
    {
      std::vector<double> vec;

      for( int i = 0; i < 3; i++ )
      {
        boost::random::uniform_01<> xdistFactor;

        boost::variate_generator<
          boost::mt19937&, boost::random::uniform_01<>
        > xvtFactor( gen, xdistFactor );

        vec.push_back( xvtFactor() );
      }

      return boost::make_tuple( vec[0], vec[1], vec[2] );
    }

    boost::tuple<int, int, int>
    getZoneLabelsAsIntegerTuple( nnt::Zone& zone )
    {
      Libnucnet__Zone * p_zone = zone.getNucnetZone();

      return
        boost::make_tuple(
          boost::lexical_cast<int>( Libnucnet__Zone__getLabel( p_zone, 1 ) ),
          boost::lexical_cast<int>( Libnucnet__Zone__getLabel( p_zone, 2 ) ),
          boost::lexical_cast<int>( Libnucnet__Zone__getLabel( p_zone, 3 ) )
        );
    }
  
    void
    insertNewStarInZone(
      nnt::Zone& cloud_zone,
      Star& star
    )
    {

      star.updateProperty(
        S_METALLICITY,
        cloud_zone.getProperty<double>( S_METALLICITY )
      );

      double d_new_cloud_mass =
        cloud_zone.getProperty<double>( nnt::s_ZONE_MASS )
        -
        star.getProperty<int>( nnt::s_NUMBER ) *
          star.getProperty( S_CURRENT_MASS );

      if( d_new_cloud_mass < 0 )
      {
        std::cerr << "Star mass greater than cloud mass." << std::endl;
        exit( EXIT_FAILURE );
      }

      cloud_zone.updateProperty(
        nnt::s_ZONE_MASS,
        d_new_cloud_mass
      );

    }

    int
    computeNumberInMean( double mean )
    {
      boost::random::poisson_distribution<> pdist( mean );

      boost::variate_generator<
        boost::mt19937&,
        boost::random::poisson_distribution<int>
      > pvt( gen, pdist );

      return pvt();
    }

    void updateStars(
      star_system_heap_t& star_system_heap,
      double d_t1,
      double d_t2,
      double d_tend
    )
    {

      boost::random::uniform_01<> mdist;

      boost::variate_generator<
        boost::mt19937&, boost::random::uniform_01<>
      > mvt( gen, mdist );

      boost::random::uniform_real_distribution<> tdist( d_t1, d_t2 );

      boost::variate_generator<
        boost::mt19937&, boost::random::uniform_real_distribution<>
      > rvt( gen, tdist );

      StarSystemGenerator ss_gen;

      std::vector<nnt::Zone> star_forming_zones;

      std::vector<size_t> n_stars, zone_id;

      std::vector<double> mass_bins =
        boost::assign::list_of(0.001)(0.4)(0.5)(0.6)(0.7);

      // Create stars.

      BOOST_FOREACH(
        nnt::Zone zone,
        getStarFormingZones()
      )
      {
        star_forming_zones.push_back( zone );

        double mean =
          ( d_t2 - d_t1 ) *
          ( ( 1. / dStarFormTime ) *
            pow(
              zone.getProperty<double>( nnt::s_ZONE_MASS ) / 1.e6,
	      dSchmidtExponent
            )
	  );

        zone_id.push_back( pYields->getHistoryZoneId( zone ) );

        n_stars.push_back( computeNumberInMean( mean ) );
      }

      std::vector<size_t> v_id( n_stars.size() );
      std::vector<star_system_heap_t> v_heap( n_stars.size() );

      std::vector<std::vector<nnt::Zone> > v_stars( n_stars.size() );

      v_id[0] = i_Id;

      for( size_t i = 1; i < n_stars.size(); i++ )
      {
        v_id[i] = v_id[i-1] + n_stars[i-1];
      }

    // Loop on zones to add stars.
    
/*
    #ifndef NO_OPENMP
      #pragma omp parallel for schedule( dynamic, 1 )
    #endif
*/
      for( size_t i = 0; i < n_stars.size(); i++ )
      {
    
        double d_available_mass =
          star_forming_zones[i].getProperty<double>( nnt::s_ZONE_MASS );
          
        star_map_t star_map;
    
        gsl_vector * p_abunds =
          Libnucnet__Zone__getAbundances( star_forming_zones[i].getNucnetZone() );
    
        size_t j = n_stars[i];
    
        while( j > 0 )
        {
    
          std::vector<double> v_star_masses;
    
          for( size_t k = 0; k < ss_gen( gen ); k++ )
          {
            v_star_masses.push_back( pImf->getNewStarMass( mvt() ) );
          }
    
          double d_system_mass =
            std::accumulate( v_star_masses.begin(), v_star_masses.end(), 0. );
    
          double d_max_mass =
            *std::max_element( v_star_masses.begin(), v_star_masses.end() );

          if( d_available_mass > d_system_mass && j >= v_star_masses.size() )
          {
            d_available_mass -= d_system_mass;
    
            j -= v_star_masses.size();

	    // Add stars not in bins.
    
            if( d_max_mass > mass_bins[mass_bins.size() - 1] )
            {
    
              StarSystem star_system;
    
              boost::tuple<double,double,double> t =
                compute3dSubgridLocation( );
    
              BOOST_FOREACH( double mass, v_star_masses )
              {
                Star star( mass, d_t1 );

		star.updateProperty( nnt::s_NUMBER, 1 );
		star.updateProperty( nnt::s_STAR_ID, v_id[i]++ );
		star.updateProperty( nnt::s_ORIGINAL_ZONE_ID, zone_id[i] );
		star.setGridLocation(
                    getZoneLabelsAsIntegerTuple( star_forming_zones[i] )
		);
		star.setSubGridLocation( t );
    
                insertNewStarInZone(
                  star_forming_zones[i],
                  star
                );
    
                star_system.addStar( star );
    
              }

	      pMassTracker->addStarSystem( star_system, d_tend );
    
              if( star_system.getTopStar().getProperty( S_END_TIME ) <= d_tend )
              {
                v_heap[i].push( star_system );
              }
              else
              {
                while( star_system.getNumberOfStars() > 0 )
                {
	          pStarTracker->addStar( star_system.getTopStar(), p_abunds );
                  star_system.popTopStar();
                }
              }
            }
            else
            {
              BOOST_FOREACH( double mass, v_star_masses )
              {

                size_t i_mass =
                  std::lower_bound(
                    mass_bins.begin(), mass_bins.end(), mass
                )
                -
                mass_bins.begin();
    
                double f_mass = mass_bins[i_mass - 1];
                star_map_t::iterator it = star_map.find( f_mass );
                if( it != star_map.end() )
                {
                  it->second( mass );
                }
                else
                {
                  star_map[f_mass]( mass );
                }
              }
            }
          }
        }

        // Add stars in bins.
	
        for(
          star_map_t::iterator it = star_map.begin();
          it != star_map.end();
          it++
        )
        {
    
          StarSystem star_system;
    
          boost::tuple<double,double,double> t =
            compute3dSubgridLocation( );
        
	  Star star( ac::mean( it->second ), d_t1 );

	  int i_number = ac::count( it->second );

	  star.updateProperty( nnt::s_NUMBER, i_number );
          star.updateProperty( nnt::s_STAR_ID, v_id[i]++ );
          star.updateProperty( nnt::s_ORIGINAL_ZONE_ID, zone_id[i] );
          star.setGridLocation(
              getZoneLabelsAsIntegerTuple( star_forming_zones[i] )
          );
          star.setSubGridLocation( t );
    
          if( star.getProperty( S_END_TIME ) <= d_tend )
          {
            pStarTracker->addStar( star, p_abunds );
	    star_system.addStar( star );
            v_heap[i].push( star_system );
          }
          else
          {
	    star_system.addStar( star );
    	    pStarTracker->addStar( star, p_abunds );
          }

          insertNewStarInZone(
            star_forming_zones[i],
            star
          );
    
          pMassTracker->addStarSystem( star_system, d_tend );
    
        }
    
        gsl_vector_free( p_abunds );
    
      }  // End loop on zones.
    
      for( size_t i = 0; i < v_heap.size(); i++ )
      {
        star_system_heap.merge( v_heap[i] );
      }
    
      i_Id = v_id[v_id.size()-1];
    
    }

    void
    addStellarDebrisAndUpdateSystem(
      nnt::Zone& zone,
      StarSystem& star_system,
      double d_time
    )
    {
    
      Star star = star_system.getTopStar();
    
      gsl_vector * p_new;
    
      double ejected_mass, remnant_mass;
    
      //========================================================================
      // Get injecta zone masses.
      //========================================================================
    
      gsl_vector * p_old =
        Libnucnet__Zone__getMassFractions( zone.getNucnetZone() );
    
      gsl_vector_scale(
        p_old,
        zone.getProperty<double>( nnt::s_ZONE_MASS )
      );
    
      //========================================================================
      // Get yields.
      //========================================================================
    
      boost::tie( p_new, ejected_mass, remnant_mass ) =
        pYields->getYieldData( star_system );

      double d_yield_mass = star.getProperty( nnt::s_NUMBER )  * ejected_mass;

      double d_remnant_mass = star.getProperty( nnt::s_NUMBER )  * remnant_mass;
    
      if( star.getStatus() == "white dwarf" )
      {
	std::cout << ejected_mass << std::endl;
      }

      gsl_vector_scale(
        p_new,
        d_yield_mass
      );
    
      //========================================================================
      // Add stellar debris to zone.
      //========================================================================
    
      gsl_vector_add( p_old, p_new );
    
      double d_zone_mass =
        zone.getProperty<double>( nnt::s_ZONE_MASS )
        +
        d_yield_mass
        -
        d_remnant_mass;
    
      gsl_vector_scale(
        p_old,
        1. / d_zone_mass
      );
    
      Libnucnet__Zone__updateMassFractions( zone.getNucnetZone(), p_old );
    
      // Add star's mass to zone.
    
      zone.updateProperty(
        nnt::s_ZONE_MASS,
        d_zone_mass
      ); 
    
      //========================================================================
      // Update.
      //========================================================================
    
      if( zone.hasProperty( "total remnant mass" ) )
      {
        zone.updateProperty(
          "total remnant mass",
          zone.getProperty<double>( "total remnant mass" ) + d_remnant_mass
        );
      }
      else
      {
        zone.updateProperty( "total remnant mass", d_remnant_mass );
      }

      pRemnants->updateStellarSystem(
        star_system, remnant_mass, gen, d_time
      );
    
      gsl_vector_free( p_old );
      gsl_vector_free( p_new );
    
    }

    void addEjecta(
      star_system_heap_t& star_system_heap,
      double d_time,
      double d_tend
    )
    {

      typedef std::map<nnt::Zone, star_system_heap_t > my_map_t;
      my_map_t my_map;
      std::vector<boost::tuple<nnt::Zone, star_system_heap_t> > v;

      while(
        star_system_heap.size() != 0 &&
        d_time > (star_system_heap.top()).getTopStar().getProperty( S_END_TIME )
      )
      {
        StarSystem star_system = star_system_heap.top();
        star_system_heap.pop();
        nnt::Zone zone = getInjectaZone( star_system );
        my_map[zone].push( star_system );
	pMassTracker->subtractStarSystem( star_system, d_tend );
      }

      BOOST_FOREACH( my_map_t::value_type& p, my_map )
      {
        v.push_back( boost::make_tuple( p.first, p.second ) );
      }

    #ifndef NO_OPENMP
      #pragma omp parallel for schedule( dynamic, 1 )
    #endif
      for( size_t i = 0; i < v.size(); i++ )
      {
        while(
          !(v[i].get<1>()).empty() &&
          d_time > ((v[i].get<1>()).top()).getTopStar().getProperty( S_END_TIME )
        )
        {
          StarSystem star_system = (v[i].get<1>()).top();
          (v[i].get<1>()).pop();
          addStellarDebrisAndUpdateSystem( v[i].get<0>(), star_system, d_time );
          pMassTracker->addStarSystem( star_system, d_tend );
          pRemnants->updateStellarSystemHeap(
	    v[i].get<1>(), star_system, d_tend
	  );
        }
      }
      pMassTracker->addDataToFile( d_time );
    }

  private:
    imf * pImf;
    mass_tracker * pMassTracker;
    star_tracker * pStarTracker;
    yields * pYields;
    remnants * pRemnants;
    double dStarFormTime, dSchmidtExponent;
    size_t i_Id = 0;
    boost::mt19937 gen;

};

//##############################################################################
// gce_options().
//##############################################################################

class gce_options : public detail::gce_options
{

  public:
    gce_options() : detail::gce_options() {}

    std::string
    getExample()
    {
      return "";
    }

    void
    get( options_map& o_map )
    {

      try
      {

       po::options_description gce( "\nGCE Options" );

       gce.add_options()

       // Option for star formation timescale
       ( S_STAR_FORM_TIME,
	 po::value<double>()->default_value( 1500, "1500" ),
         "Star formation timescale" )

       // Option for Schmidt exponent
       ( S_SCHMIDT_EXPONENT, po::value<double>()->default_value( 1, "1" ),
         "Schmidt exponent" )

       // Option for random seed
       ( S_RANDOM_SEED, po::value<unsigned int>(),
         "Random seed" )

       ;

       detail::gce_options::get( gce );

       o_map.insert(
         std::make_pair<std::string, options_struct>(
           "gce",
           options_struct( gce, getExample() )
         )
       );

      // Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // wn_user

#endif  // WN_GCE_HPP
