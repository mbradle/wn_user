////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file rate_registerer.hpp
//! \brief A file to define rate registration routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <vector>
#include <boost/function.hpp>
#include <boost/ptr_container/ptr_vector.hpp>

#include "my_global_types.h"

#ifndef WN_RATE_REGISTERER_HPP
#define WN_RATE_REGISTERER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

//##############################################################################
// rate_registerer().
//##############################################################################

class rate_registerer
{

  public:
    rate_registerer(){}
    rate_registerer( v_map_t& v_map )
    {

#ifdef WN_DEFAULT_REGISTERER_HPP
      f_vec.push_back( new detail::default_registerer( v_map ) );
#endif

#ifdef WN_STARLIB_REGISTERER_HPP
      f_vec.push_back( new detail::starlib_registerer( v_map ) );
#endif

#ifdef WN_CONSTANT_SIGMA_REGISTERER_HPP
      f_vec.push_back( new detail::constant_sigma_registerer( v_map ) );
#endif

#ifdef WN_NEUTRINO_REGISTERER_HPP
      f_vec.push_back( new detail::neutrino_registerer( v_map ) );
#endif

#ifdef WN_TWO_D_WEAK_REGISTERER_HPP
      f_vec.push_back( new detail::two_d_weak_registerer( v_map ) );
#endif

#ifdef WN_TWO_D_WEAK_LOG10_FT_REGISTERER_HPP
      f_vec.push_back( new detail::two_d_weak_log10_ft_registerer( v_map ) );
#endif

#ifdef WN_AA522A25_REGISTERER_HPP
      f_vec.push_back( new detail::aa522a25_registerer( v_map ) );
#endif
    }

    void
    operator()( Libnucnet__Net * p_net )
    {
      BOOST_FOREACH( base::rate_registerer& f, f_vec )
      {
        f( p_net );
      }
    }

    void
    updateRateData( nnt::Zone& zone )
    {
      BOOST_FOREACH( base::rate_registerer& f, f_vec )
      {
        f.data_updater( zone );
      }
    }

    void
    setZoneDataUpdater( nnt::Zone& zone )
    {
      zone.updateFunction(
        nnt::s_RATE_DATA_UPDATE_FUNCTION,
        static_cast<boost::function<void()> >(
          boost::bind(
            &rate_registerer::updateRateData,
            this,
            boost::ref( zone )
          )
        )
      );
    }

    void
    setZoneDataUpdater( std::vector<nnt::Zone>& zones )
    {
      for( size_t i = 0; i < zones.size(); i++ )
      {
        setZoneDataUpdater( zones[i] );
      }
    }

  private:
    boost::ptr_vector<base::rate_registerer> f_vec;

};

//##############################################################################
// rate_registerer_options().
//##############################################################################

class rate_registerer_options
{

  public:
    rate_registerer_options(){}

    void
    get( options_map& o_map )
    {

      try
      {

        po::options_description rate_registerer("\nRates registerer options");
        rate_registerer.add_options()

        ;

#ifdef WN_DEFAULT_REGISTERER_HPP
        o_vec.push_back( new detail::default_registerer_options() );
#endif

#ifdef WN_AA522A25_REGISTERER_HPP
        o_vec.push_back( new detail::aa522a25_registerer_options() );
#endif

        BOOST_FOREACH( base::rate_registerer_options& f, o_vec )
        {
          f( rate_registerer );
        } 

        o_map.insert(
          std::make_pair<std::string, options_struct>(
            "rate_registerer", 
            options_struct( rate_registerer )
          )
        );

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

  private:
    boost::ptr_vector<base::rate_registerer_options> o_vec;

};

}  // namespace wn_user

#endif // WN_RATE_REGISTERER_HPP
