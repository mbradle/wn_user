////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file two_d_weak_log10_ft.hpp
//! \brief A file to define rate registration routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/algorithm/string.hpp>

#include "my_global_types.h"
#include "nnt/iter.h"
#include "nnt/two_d_weak_rates.h"
#include "rate_registerer/base/rate_registerer_base.hpp"

#ifndef WN_TWO_D_WEAK_LOG10_FT_REGISTERER_HPP
#define WN_TWO_D_WEAK_LOG10_FT_REGISTERER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

typedef struct {
  Libnucnet__Net *pNet;
  double dElectronMass;
  double dRhoe;
  double dEtaF;
  double dMuNuekT;
} log10_ft_work;

//##############################################################################
// compute_two_d_weak_rate_from_log10_ft().
//##############################################################################

double
compute_two_d_weak_rate_from_log10_ft(
  Libnucnet__Reaction * p_reaction,
  double d_t9,
  log10_ft_work * p_log10_ft_work
)
{

  std::pair<double,double> weak_pair;

  if( !p_log10_ft_work )
  {
    std::cerr << "Data not set in two-d weak log10 ft rate function."
              << std::endl;
    exit( EXIT_FAILURE );
  }

  nnt::TwoDWeakQuantity w_log10_ft( p_reaction, nnt::s_LOG10_FT );

  weak_pair = w_log10_ft.computeValue( d_t9, p_log10_ft_work->dRhoe );

  return
    M_LN2 *
    nnt::ffnIV__compute_Ie(
      p_reaction,
      p_log10_ft_work->pNet,
      p_log10_ft_work->dElectronMass,
      d_t9, 
      p_log10_ft_work->dEtaF,
      p_log10_ft_work->dMuNuekT
    ) /
    pow(
      10.,
      weak_pair.first
    );

}

//##############################################################################
// two_d_weak_log10_ft_registerer().
//##############################################################################

class two_d_weak_log10_ft_registerer : public rate_registerer_base
{

  public:
    two_d_weak_log10_ft_registerer( v_map_t& v_map ) :
      rate_registerer_base() {}

    void register_rates( Libnucnet__Reac * p_reac )
    {

      Libnucnet__Reac__registerUserRateFunction(
        p_reac,
        nnt::s_TWO_D_WEAK_RATES_LOG10_FT,
        (Libnucnet__Reaction__userRateFunction)
          compute_two_d_weak_rate_from_log10_ft
      );

      Libnucnet__Reac__setUserRateFunctionDataDeallocator(
        p_reac,
        nnt::s_TWO_D_WEAK_RATES_LOG10_FT,
        (Libnucnet__Reaction__user_rate_function_data_deallocator) free
      );

    }

    void operator()( nnt::Zone& zone )
    {
      register_rates(
        Libnucnet__Net__getReac(
          Libnucnet__Zone__getNet( zone.getNucnetZone() )
        )
      );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      register_rates(
        Libnucnet__Net__getReac(
          Libnucnet__Zone__getNet( zones[0].getNucnetZone() )
        )
      );
    }

    void data_updater( nnt::Zone& zone )
    {

      log10_ft_work *p_log10_ft_work;

      Libstatmech__Fermion * p_electron;
      double d_mue_kT, d_eta_F, d_mu_nue_kT;
  
      p_electron =
        Libstatmech__Fermion__new(
          nnt::s_ELECTRON,
          nnt::d_ELECTRON_MASS_IN_MEV,
          2,
          -1
        );

      d_mue_kT =
        user::compute_thermo_quantity(
          zone,
          nnt::s_CHEMICAL_POTENTIAL_KT,
          nnt::s_ELECTRON
        );

      d_mu_nue_kT =
        user::compute_thermo_quantity(
          zone,
          nnt::s_CHEMICAL_POTENTIAL_KT,
          nnt::s_NEUTRINO_E
        );

      d_eta_F =
        d_mue_kT
        +
        Libstatmech__Fermion__getRestMass( p_electron ) /
        nnt::compute_kT_in_MeV( zone.getProperty<double>( nnt::s_T9 ) );

      p_log10_ft_work = ( log10_ft_work * ) malloc( sizeof( log10_ft_work ) );

      if( !p_log10_ft_work )
      {
        std::cerr << "Couldn't allocate memory for log10_ft_work structure.\n";
        exit( EXIT_FAILURE );
      }

      p_log10_ft_work->pNet = Libnucnet__Zone__getNet( zone.getNucnetZone() );
      p_log10_ft_work->dRhoe = 
        zone.getProperty<double>( nnt::s_RHO )
        * Libnucnet__Zone__computeZMoment( zone.getNucnetZone(), 1 );

      p_log10_ft_work->dElectronMass =
        Libstatmech__Fermion__getRestMass( p_electron );

      p_log10_ft_work->dEtaF = d_eta_F;

      p_log10_ft_work->dMuNuekT = d_mu_nue_kT;

      Libnucnet__Zone__updateDataForUserRateFunction(
        zone.getNucnetZone(),
        nnt::s_TWO_D_WEAK_RATES_LOG10_FT,
        p_log10_ft_work
      );

      Libstatmech__Fermion__free( p_electron );

    }

};

}  // namespace detail

}  // namespace wn_user

#endif // WN_TWO_D_WEAK_LOG10_FT_REGISTERER_HPP
