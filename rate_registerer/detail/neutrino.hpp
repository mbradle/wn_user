////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Norberto J. Davila and Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file neutrino.hpp
//! \brief A file to define rate registration routines for neutrinos.
//!
////////////////////////////////////////////////////////////////////////////////

#include <math.h>

#include "my_global_types.h"

#include "rate_registerer/base/rate_registerer.hpp"
#include "rate_registerer/base/rate_registerer_properties_getter.hpp"
#include "math/linear_interpolator.hpp"

#ifndef WN_NEUTRINO_REGISTERER_HPP
#define WN_NEUTRINO_REGISTERER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define S_DELTA                  "Delta"
#define S_NU_NUCL                "neutrino-nucleus"
#define S_NU_N_CAPTURE           "capture on free neutron"
#define S_NU_P_CAPTURE           "capture on free proton"
#define S_PREFACTOR              "rate prefactor"

#define S_LOG_10_XSEC            "log_10_xsec"

typedef std::map<std::string, wn_user::math::linear_interpolator<> > nu_nucl_interp_map_t;

typedef std::map<std::string, double> nu_prop_map_t;

typedef struct
{
  nu_nucl_interp_map_t * pInterpMap;
  nnt::Zone * pZone;
} nu_nucl_data_t;

//##############################################################################
// neutrino_registerer().
//##############################################################################

class neutrino_registerer : public base::rate_registerer,
  public base::rate_registerer_properties_getter
{

  public:
    neutrino_registerer( v_map_t& v_map ) : base::rate_registerer(),
      base::rate_registerer_properties_getter(), my_neutrinos( v_map ){}

    void register_rates( Libnucnet__Reac * p_reac )
    {

      Libnucnet__Reac__registerUserRateFunction(
        p_reac,
        S_NU_N_CAPTURE,
        (Libnucnet__Reaction__userRateFunction) nu_capture_function
      );

      Libnucnet__Reac__registerUserRateFunction(
        p_reac,
        S_NU_P_CAPTURE,
        (Libnucnet__Reaction__userRateFunction) nu_capture_function
      );

      Libnucnet__Reac__registerUserRateFunction(
        p_reac,
        S_NU_NUCL,
        (Libnucnet__Reaction__userRateFunction) neutrino_nucleus_function
      );

      Libnucnet__Reac__setUserRateFunctionDataDeallocator(
        p_reac,
        S_NU_NUCL,
        (Libnucnet__Reaction__user_rate_function_data_deallocator) free
      );

      set_neutrino_nucleus_data( p_reac );
      
    }

    static double
    nu_capture_function(
      Libnucnet__Reaction *p_reaction,
      double d_t9,
      void * p_data
    )
    {

      if( !p_data )
      {
        std::cerr << "nu_capture_function requires extra data."
                  << std::endl;
        exit( EXIT_FAILURE );
      }

      nnt::Zone zone = *( nnt::Zone * ) p_data;

      double d_prefactor, d_Delta, d_Delta_part;
      const char *s_tmp;
      std::string s_neutrino;

      double dQWFactor =
        4. * M_PI * 1.e-39 *
        GSL_CONST_NUM_MEGA * GSL_CONST_CGSM_ELECTRON_VOLT;

      if( d_t9 <= 0 )
      {
        std::cerr << "Must have t9 > 0 for this function.\n";
        exit( EXIT_FAILURE );
      }

      //======================================================================
      // Rate parameters.
      //======================================================================

      if(
        ( s_tmp =
            Libnucnet__Reaction__getUserRateFunctionProperty(
              p_reaction,
              S_PREFACTOR,
              NULL,
              NULL
            )
        )
      )
      {
        d_prefactor = atof( s_tmp );
      }
      else
      {
        std::cerr << " prefactor not provided.\n";
        exit( EXIT_FAILURE );
      }

      if(
        ( s_tmp =
            Libnucnet__Reaction__getUserRateFunctionProperty(
              p_reaction,
              S_DELTA,
              NULL,
              NULL
            )
        )
      )
      {
        d_Delta = atof( s_tmp );
      }
      else
      {
        std::cerr << " prefactor not provided.\n";
        exit( EXIT_FAILURE );
      }

      //======================================================================
      // Get neutrino.
      //======================================================================

      if(
        std::string( Libnucnet__Reaction__getRateFunctionKey( p_reaction ) )
        ==
        S_NU_N_CAPTURE
      )
      {
        s_neutrino = ELECTRON_NEUTRINO;
        d_Delta_part = 2. * d_Delta;
      }
      else if(
        std::string( Libnucnet__Reaction__getRateFunctionKey( p_reaction ) )
        ==
        S_NU_P_CAPTURE
      )
      {
        s_neutrino = ELECTRON_ANTINEUTRINO;
        d_Delta_part = - 2. * d_Delta;
      }
      else
      {
        std::cerr << "Invalid key for routine." << std::endl;
      }

      //======================================================================
      // Compute rate.
      //======================================================================

      double d_Enu_MeV = zone.getProperty<double>( nnt::s_NU_E_AV, s_neutrino );

      return
        d_prefactor * dQWFactor *
          zone.getProperty<double>( nnt::s_NU_FLUX, s_neutrino ) *
          (
            d_Enu_MeV + d_Delta_part + 1.2 * gsl_pow_2( d_Delta ) / d_Enu_MeV
          );

    }

    static double
    neutrino_nucleus_function(
      Libnucnet__Reaction *p_reaction,
      double d_t9,
      void *p_data
    )
    {

      if( !p_data )
      {
        std::cerr << "neutrino_nucleus_function requires extra data."
                  << std::endl;
        exit( EXIT_FAILURE );
      }

      nu_nucl_data_t  my_data = *( nu_nucl_data_t * ) p_data;

      std::string s_neutrino;

      if( d_t9 <= 0 )
      {
        std::cerr << "Must have t9 > 0 for this function.\n";
        exit( EXIT_FAILURE );
      }

      BOOST_FOREACH(
        nnt::ReactionElement element,
        nnt::make_reaction_reactant_list( p_reaction )
      )
      {
        if(
          !Libnucnet__Reaction__Element__isNuclide(
            element.getNucnetReactionElement()
          )
        )
        {
          s_neutrino =
            std::string(
              Libnucnet__Reaction__Element__getName(
                element.getNucnetReactionElement()
              )
            );
        }
      }

      return
        my_data.pZone->getProperty<double>( nnt::s_NU_FLUX, s_neutrino ) *
        pow(
          10.,
          (*my_data.pInterpMap)[
            Libnucnet__Reaction__getString( p_reaction )
          ]( my_data.pZone->getProperty<double>( nnt::s_NU_T, s_neutrino ) )
        );

    }

    void data_updater( nnt::Zone& zone )
    {

      nu_nucl_data_t * p_nu_nucl_data =
        (nu_nucl_data_t *) malloc( sizeof( nu_nucl_data_t ) );

      BOOST_FOREACH( std::string s_neutrino, my_neutrinos.getNeutrinosVector() )
      {
        zone.updateProperty(
          nnt::s_NU_FLUX,
          s_neutrino,
          my_neutrinos.computeNeutrinoFlux( s_neutrino, zone )
        );
        zone.updateProperty(
          nnt::s_NU_T,
          s_neutrino,
          my_neutrinos.getTemperature( s_neutrino, zone )
        );
        zone.updateProperty(
          nnt::s_NU_E_AV,
          s_neutrino,
          my_neutrinos.getAverageEnergyInMeV( s_neutrino, zone )
        );
      }

      p_nu_nucl_data->pInterpMap = &nu_nucl_interp_map;
      p_nu_nucl_data->pZone = &zone;

      Libnucnet__Zone__updateDataForUserRateFunction(
        zone.getNucnetZone(),
        S_NU_NUCL,
        p_nu_nucl_data
      );

      Libnucnet__Zone__updateDataForUserRateFunction(
        zone.getNucnetZone(),
        S_NU_N_CAPTURE,
        &zone
      );

      Libnucnet__Zone__updateDataForUserRateFunction(
        zone.getNucnetZone(),
        S_NU_P_CAPTURE,
        &zone
      );

    }

    void operator()( Libnucnet__Net * p_net )
    {
      register_rates( Libnucnet__Net__getReac( p_net ) );
    }

  private:
    neutrino_collection my_neutrinos;
    nu_nucl_interp_map_t nu_nucl_interp_map;

    void
    set_neutrino_nucleus_data( Libnucnet__Reac * p_reac )
    {

      BOOST_FOREACH( nnt::Reaction reaction, nnt::make_reaction_list( p_reac ) )
      {
        if(
          strcmp(
            Libnucnet__Reaction__getRateFunctionKey(
              reaction.getNucnetReaction() 
            ),
            S_NU_NUCL
          ) == 0
        )
        {
          std::vector<double> v_T =
            getPropertyVector<double>(
              reaction.getNucnetReaction(), nnt::s_NU_T
            );

          std::vector<double> v_L =
            getPropertyVector<double>(
              reaction.getNucnetReaction(), S_LOG_10_XSEC
            );

          wn_user::math::linear_interpolator<> my_interp;

          my_interp.set( v_T, v_L );

          nu_nucl_interp_map.insert(
            std::make_pair(
              Libnucnet__Reaction__getString( reaction.getNucnetReaction() ),
              my_interp
            )
          );
        }
      }
    }

};

}  // namespace detail

}  // namespace wn_user

#endif // WN_NEUTRINO_REGISTERER_HPP
