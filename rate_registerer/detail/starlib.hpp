////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file starlib.hpp
//! \brief A file to define rate registration routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/lexical_cast.hpp>

#include "my_global_types.h"
#include "nnt/math.h"

#include "rate_registerer/base/rate_registerer_base.hpp"

#ifndef WN_STARLIB_REGISTERER_HPP
#define WN_STARLIB_REGISTERER_HPP

#define S_STARLIB_WEAK   "starlib weak"
#define S_STARLIB        "starlib" 
#define S_RATE           "rate"
#define S_SINGLE_RATE    "single_rate"

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

typedef std::map<std::string, double> map_type;

typedef struct
{
  map_type rate;
  map_type t9;
} starlib_data;

typedef
std::map<std::string, linear_interpolater<> > interp_map_t;

static interp_map_t interp_map;

//##############################################################################
// starlib_registerer().
//##############################################################################

class starlib_registerer : public rate_registerer_base
{

  public:
    starlib_registerer( v_map_t& v_map ) : rate_registerer_base() {}

    static void
    get_starlib_data(
      const char *s_name,
      const char *s_tag1,
      const char *s_tag2,
      const char *s_property,
      starlib_data *p_data
    )
    {

      if( !s_name || !s_tag1 || s_tag2 )
      {
        std::cerr << "Invalid input to get_starlib_data\n";
        exit( EXIT_FAILURE );
      }

      if( std::string( s_name ) == "t9" )
      {
        (*p_data).t9[std::string( s_tag1 )] =
          boost::lexical_cast<double>( s_property );
      }

      if( std::string( s_name ) == S_RATE )
      {
        (*p_data).rate[std::string( s_tag1 )] =
          boost::lexical_cast<double>( s_property );
      }

    }

    static void
    assignInterpolationData( Libnucnet__Reaction * p_reaction )
    {

      starlib_data my_data;

      Libnucnet__Reaction__iterateUserRateFunctionProperties(
        p_reaction,
        NULL,
        NULL,
        NULL,
        (Libnucnet__Reaction__user_rate_property_iterate_function)
          get_starlib_data,
        &my_data
      );

      std::vector<double> v_log10_t9( my_data.t9.size() );
      std::vector<double> v_log10_rate( my_data.rate.size() );

      BOOST_FOREACH( const map_type::value_type& t, my_data.t9 )
      {
        v_log10_t9[boost::lexical_cast<size_t>( t.first )] =
          log10( t.second );
      }

      BOOST_FOREACH( const map_type::value_type& t, my_data.rate )
      {
        v_log10_rate[boost::lexical_cast<size_t>( t.first )] =
          log10( t.second + 1.0e-300);
      }

      interp_map.insert(
        std::make_pair(
          Libnucnet__Reaction__getString( p_reaction ),
          linear_interpolater<>( v_log10_t9, v_log10_rate )
        )
      );

    }

    static double
    computeRate(
      Libnucnet__Reaction *p_reaction,
      double d_t9,
      void *p_data
    )
    {

      if( p_data )
      {
        fprintf( stderr, "No extra data to this function.\n" );
        exit( EXIT_FAILURE );
      }

      interp_map_t::iterator it =
        interp_map.find( Libnucnet__Reaction__getString( p_reaction ) );

      if( it == interp_map.end() )
      {
        assignInterpolationData( p_reaction );
      }

      return pow( 10., it->second( log10( d_t9 ) ) );

    }

    static void
    get_starlib_weak_rate(
      const char *s_name,
      const char *s_tag1,
      const char *s_tag2,
      const char *s_property,
      double *p_rate
    )
    {

      if( !s_name || s_tag1 || s_tag2 )
      {
        std::cerr << "Invalid input to get_starlib_weak_rate\n";
        exit( EXIT_FAILURE );
      }

      if( std::string( s_name ) == S_SINGLE_RATE )
      {
        (*p_rate) = boost::lexical_cast<double>( s_property );
      }

    }

    void register_rates( nnt::Zone& zone )
    {

      Libnucnet__Reac * p_reac =
        Libnucnet__Net__getReac(
          Libnucnet__Zone__getNet( zone.getNucnetZone() )
        );

      Libnucnet__Reac__registerUserRateFunction(
        p_reac,
        S_STARLIB_WEAK,
        (Libnucnet__Reaction__userRateFunction) compute_starlib_weak_rate
      );

      Libnucnet__Reac__registerUserRateFunction(
        p_reac,
        S_STARLIB,
        (Libnucnet__Reaction__userRateFunction) computeRate
      );

    }

    static double
    compute_starlib_weak_rate(
      Libnucnet__Reaction *p_reaction,
      double d_t9,
      void *p_data
    )
    {

      double d_rate;

      if( p_data )
      {
        fprintf( stderr, "No extra data to this function.\n" );
        exit( EXIT_FAILURE );
      }

      Libnucnet__Reaction__iterateUserRateFunctionProperties(
        p_reaction,
        S_SINGLE_RATE,
        NULL,
        NULL,
        (Libnucnet__Reaction__user_rate_property_iterate_function)
          get_starlib_weak_rate,
        &d_rate
      );

      return d_rate;

    }

    void operator()( nnt::Zone& zone )
    {
      register_rates( zone );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      register_rates( zones[0] );
    }

};

}  // namespace detail

}  // namespace wn_user

#endif // WN_STARLIB_REGISTERER_HPP
