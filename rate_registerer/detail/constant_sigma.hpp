////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2017 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file constant_sigma.hpp
//! \brief A file to define registration routines for constant sigma rates.
//!
////////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <map>
#include <string>

#include <boost/lexical_cast.hpp>

#include "my_global_types.h"
#include "nnt/iter.h"
#include "nnt/math.h"
#include "user/network_utilities.h"

#include "rate_registerer/base/rate_registerer.hpp"

#ifndef WN_CONSTANT_SIGMA_REGISTERER_HPP
#define WN_CONSTANT_SIGMA_REGISTERER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// compute_constant_sigma_rate()
//##############################################################################

double
compute_constant_sigma_rate(
  Libnucnet__Reaction *p_reaction,
  double d_t9,
  nnt::Zone& zone
)
{

  double d_sigma =
    boost::lexical_cast<double>(
      Libnucnet__Reaction__getUserRateFunctionProperty(
        p_reaction,
        nnt::s_SIGMA,
        NULL,
        NULL
      )
    );

  return
    GSL_CONST_NUM_AVOGADRO * 
      d_sigma * GSL_CONST_CGSM_BARN * GSL_CONST_NUM_MILLI *
      user::compute_v_Thermal(
        Libnucnet__Nuc__getSpeciesByName(
          Libnucnet__Net__getNuc(
            Libnucnet__Zone__getNet( zone.getNucnetZone() )
          ),
          "n"
        ),
        zone.getProperty<double>( nnt::s_T9 )
      );

}

//##############################################################################
// constant_sigma_registerer().
//##############################################################################

class constant_sigma_registerer : public base::rate_registerer
{

  public:
    constant_sigma_registerer( v_map_t& v_map ) : base::rate_registerer(){}

    void operator()( Libnucnet__Net * p_net )
    {

      Libnucnet__Reac * p_reac = Libnucnet__Net__getReac( p_net );

      Libnucnet__Reac__registerUserRateFunction(
        p_reac,
        nnt::s_CONSTANT_SIGMA,
        (Libnucnet__Reaction__userRateFunction) compute_constant_sigma_rate
      );

    }

    void data_updater( nnt::Zone& zone )
    {

      Libnucnet__Zone__updateDataForUserRateFunction(
        zone.getNucnetZone(),
        nnt::s_CONSTANT_SIGMA,
        &zone
      );

    }

};

//##############################################################################
// constant_sigma_registerer_options().
//##############################################################################

class constant_sigma_registerer_options : public base::rate_registerer_options
{

  public:
    constant_sigma_registerer_options() : base::rate_registerer_options(){}

};

}  // namespace detail

}  // namespace wn_user

#endif // WN_CONSTANT_SIGMA_REGISTERER_HPP
