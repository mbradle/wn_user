////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file rate_registerer.hpp
//! \brief A file to define rate registration routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/algorithm/string.hpp>

#include "my_global_types.h"
#include "nnt/iter.h"
#include "nnt/two_d_weak_rates.h"

#include "rate_registerer/base/rate_registerer_base.hpp"

#ifndef WN_TWO_D_WEAK_REGISTERER_HPP
#define WN_TWO_D_WEAK_REGISTERER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// two_d_weak_registerer().
//##############################################################################

class two_d_weak_registerer : public rate_registerer_base
{

  public:
    two_d_weak_registerer( v_map_t& v_map ) : rate_registerer_base(){}

    void register_rates( Libnucnet__Reac * p_reac )
    {
      Libnucnet__Reac__registerUserRateFunction(
        p_reac,
        nnt::s_TWO_D_WEAK_RATES,
        (Libnucnet__Reaction__userRateFunction) compute_two_d_weak_rate
      );

      Libnucnet__Reac__setUserRateFunctionDataDeallocator(
        p_reac,
        nnt::s_TWO_D_WEAK_RATES,
        (Libnucnet__Reaction__user_rate_function_data_deallocator) free
      );

    }

    static
    double
    compute_two_d_weak_rate(
      Libnucnet__Reaction * p_reaction,
      double d_t9,
      double *p_rhoe
    )
    {

      double d_result;

      if( !p_rhoe )
      {
        std::cerr << "Rhoe not set in two-d weak rate function." << std::endl;
        exit( EXIT_FAILURE );
      }

      nnt::TwoDWeakQuantity w_rate( p_reaction, nnt::s_LOG10_RATE );

      d_result = w_rate.computeValue( d_t9, *p_rhoe ).first;

      if( d_result < -50. )
        d_result = 0.;
      else
        d_result = pow( 10., d_result );

      return d_result;

    }

    void operator()( Libnucnet__Net * p_net )
    {
      register_rates( Libnucnet__Net__getReac( p_net ) );
    }

    void operator()( nnt::Zone& zone )
    {
      register_rates(
        Libnucnet__Net__getReac(
          Libnucnet__Zone__getNet( zone.getNucnetZone() )
        )
      );
    }

    void operator()( std::vector<nnt::Zone>& zones )
    {
      register_rates(
        Libnucnet__Net__getReac(
          Libnucnet__Zone__getNet( zones[0].getNucnetZone() )
        )
      );
    }

    void data_updater( nnt::Zone& zone )
    {

      double *p_rhoe;
  
      p_rhoe = ( double * ) malloc( sizeof( double ) );

      *p_rhoe = zone.getProperty<double>( nnt::s_RHO )
                * Libnucnet__Zone__computeZMoment( zone.getNucnetZone(), 1 ),

      Libnucnet__Zone__updateDataForUserRateFunction(
        zone.getNucnetZone(),
        nnt::s_TWO_D_WEAK_RATES,
        p_rhoe
      );

    }

};

}  // namespace detail

}  // namespace wn_user

#endif // WN_TWO_D_WEAK_REGISTERER_HPP
