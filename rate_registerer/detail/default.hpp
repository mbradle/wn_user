////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file default_registerer.hpp
//! \brief A file to define rate registration routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include "my_global_types.h"
#include "rate_registerer/base/rate_registerer.hpp"

#ifndef WN_DEFAULT_REGISTERER_HPP
#define WN_DEFAULT_REGISTERER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

//##############################################################################
// default_registerer().
//##############################################################################

class default_registerer : public base::rate_registerer
{

  public:
    default_registerer( v_map_t& v_map ) : base::rate_registerer() {}

};

//##############################################################################
// default_registerer_options().
//##############################################################################

class default_registerer_options : public base::rate_registerer_options
{

  public:
    default_registerer_options() : base::rate_registerer_options(){}

};

}  // namespace detail

}  // namespace wn_user

#endif // WN_DEFAULT_REGISTERER_HPP
