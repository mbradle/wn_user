////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2018 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
//////////////////////////////////////////////////////////////////////////////*/

////////////////////////////////////////////////////////////////////////////////
//!
//! \file two_d_weak_log10_ft.hpp
//! \brief A file to define rate registration routines.
//!
////////////////////////////////////////////////////////////////////////////////

#include <boost/assign.hpp>

#include "my_global_types.h"
#include "nnt/iter.h"

#include "rate_registerer/base/rate_registerer.hpp"

#ifndef WN_AA522A25_REGISTERER_HPP
#define WN_AA522A25_REGISTERER_HPP

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace detail
{

#define S_AA522A25                     "aa522a25"
#define S_AA522A25_B_FACTOR            "aa522a25 B factor"

#define S_AA522A25_NUC_XPATH           "aa522a25_nuc_xpath"
#define S_AA522A25_UPDATE_NET          "aa522a25_update_net"

const double d_AA522A25_WEAK_K    = 6144;         /* Hardy & Towner 2009 */
const size_t i_AA522A25_WORKSPACE = 1000;
const double d_AA522A25_EPSABS    = 1.e-12;
const double d_AA522A25_EPSREL    = 1.e-12;
const size_t i_AA522A25_LIMIT     = 1000;
const double d_AA522A25_EC_CUTOFF = 1.e-100;
const double d_AA522A25_EC_LIMIT  = 1.01;

typedef struct {
  Libnucnet__Net *pNet;
  double dElectronMass;
  double dRhoe;
  double dEtaF;
  double dMuNuekT;
} aa522a25_work;

typedef struct
{
  double dEtaF;
  double dMunuekT;
  double dQ;
  double dkT;
  int iNeutrinoExponent;
} aa522a25_function_data;

//##############################################################################
// aa522a25_update_reaction().
//##############################################################################

void
aa522a25_update_reaction(
  Libnucnet__Net * p_net,
  Libnucnet__Species * p_reactant,
  Libnucnet__Species * p_product,
  std::vector<std::string>& v_other_reactants,
  std::vector<std::string>& v_other_products
)
{

  Libnucnet__Reaction * p_reaction;
  std::string s_b;

  //============================================================================
  // Set aa522a25 B factor.
  //============================================================================

  if( Libnucnet__Species__getA( p_reactant ) == 1 )
    s_b = "5.76";
  else
    s_b = "4.6";

  //============================================================================
  // Reaction data.
  //============================================================================

  p_reaction = Libnucnet__Reaction__new();

  Libnucnet__Reaction__updateSource(
    p_reaction,
    "aa522a25"
  );

  Libnucnet__Reaction__addReactant(
    p_reaction,
    Libnucnet__Species__getName( p_reactant )
  );

  Libnucnet__Reaction__addProduct(
    p_reaction,
    Libnucnet__Species__getName( p_product )
  );

  BOOST_FOREACH( std::string s, v_other_reactants )
  {
    Libnucnet__Reaction__addReactant(
      p_reaction,
      s.c_str()
    );
  }

  BOOST_FOREACH( std::string s, v_other_products )
  {
    Libnucnet__Reaction__addProduct(
      p_reaction,
      s.c_str()
    );
  }

  Libnucnet__Reaction__setUserRateFunctionKey(
    p_reaction,
    S_AA522A25
  );

  Libnucnet__Reac__removeReaction(
    Libnucnet__Net__getReac( p_net ),
    p_reaction
  );

  Libnucnet__Reaction__updateUserRateFunctionProperty(
    p_reaction,
    S_AA522A25_B_FACTOR,
    NULL,
    NULL,
    s_b.c_str()
  );

  Libnucnet__Reac__addReaction(
    Libnucnet__Net__getReac( p_net ),
    p_reaction
  );

}

//##############################################################################
// aa522a25_update_reactions().
//##############################################################################

void
aa522a25_update_reactions(
  Libnucnet__Species *p_species,
  Libnucnet__Net *p_net
)
{

  Libnucnet__Species *p_product;

  //============================================================================
  // Check that electron capture or beta+ can happen.
  //============================================================================

  if(
    Libnucnet__Species__getZ( p_species ) > 1
    ||
    (
      Libnucnet__Species__getZ( p_species ) == 1 && 
      Libnucnet__Species__getA( p_species ) < 2
    )
  )
  {

    p_product =
      Libnucnet__Nuc__getSpeciesByZA(
        Libnucnet__Net__getNuc( p_net ),
        Libnucnet__Species__getZ( p_species ) - 1,
        Libnucnet__Species__getA( p_species ),
        NULL
      );

    //--------------------------------------------------------------------------
    // Electron capture.
    //--------------------------------------------------------------------------

    if( p_product )
    {

      std::vector<std::string> v_r =
        boost::assign::list_of(nnt::s_ELECTRON);
      
      std::vector<std::string> v_p =
        boost::assign::list_of(nnt::s_NEUTRINO_E);

      aa522a25_update_reaction( p_net, p_species, p_product, v_r, v_p );
      
    }

    //--------------------------------------------------------------------------
    // Beta plus.
    //--------------------------------------------------------------------------

    if( p_product )
    {
      std::vector<std::string> v_r;
      
      std::vector<std::string> v_p =
        boost::assign::list_of(nnt::s_POSITRON)(nnt::s_NEUTRINO_E);

      aa522a25_update_reaction( p_net, p_species, p_product, v_r, v_p );
    }

  }
 
  //============================================================================
  // Beta minus and positron capture.
  //============================================================================

  p_product =
    Libnucnet__Nuc__getSpeciesByZA(
      Libnucnet__Net__getNuc( p_net ),
      Libnucnet__Species__getZ( p_species ) + 1,
      Libnucnet__Species__getA( p_species ),
      NULL
    );

  if( p_product )
  {

    //--------------------------------------------------------------------------
    // Beta minus.
    //--------------------------------------------------------------------------

    {
      std::vector<std::string> v_r;
      
      std::vector<std::string> v_p =
        boost::assign::list_of(nnt::s_ELECTRON)(nnt::s_ANTI_NEUTRINO_E);

      aa522a25_update_reaction( p_net, p_species, p_product, v_r, v_p );
    }

    //--------------------------------------------------------------------------
    // Positron capture.
    //--------------------------------------------------------------------------

    {
      std::vector<std::string> v_r =
        boost::assign::list_of(nnt::s_POSITRON);
      
      std::vector<std::string> v_p =
        boost::assign::list_of(nnt::s_ANTI_NEUTRINO_E);

      aa522a25_update_reaction( p_net, p_species, p_product, v_r, v_p );
    }

  }

}

//##############################################################################
// aa522a25_update_net().
//##############################################################################

void
aa522a25_update_net(
  Libnucnet__Net *p_net,
  const char * s_nuc_xpath
)
{

  Libnucnet__NucView * p_nuc_view =
    Libnucnet__NucView__new(
      Libnucnet__Net__getNuc( p_net ),
      s_nuc_xpath
    );

  BOOST_FOREACH(
    nnt::Species species,
    nnt::make_species_list(
      Libnucnet__NucView__getNuc( p_nuc_view )
    )
  )
  {
     aa522a25_update_reactions( species.getNucnetSpecies(), p_net );
  }

  Libnucnet__NucView__free( p_nuc_view );

}

//##############################################################################
// aa522a25__electron_capture_integrand().
//##############################################################################

double
aa522a25__electron_capture_integrand(
  double d_E,
  void *p_data
)
{

  double d_result;

  aa522a25_function_data *p_function_data = ( aa522a25_function_data * ) p_data;

  d_result =
    pow( d_E - p_function_data->dQ, p_function_data->iNeutrinoExponent );

  d_result *=
    gsl_pow_2( d_E ) *
    nnt::compute_fermi_dirac_factor(
      d_E / p_function_data->dkT,
      p_function_data->dEtaF
    );

  if( p_function_data->dMunuekT == GSL_NEGINF )
    return d_result;
  else
    return
      d_result *
      nnt::compute_one_minus_fermi_dirac_factor(
        d_E / p_function_data->dkT - p_function_data->dQ / p_function_data->dkT,
        p_function_data->dMunuekT
      );

}

//##############################################################################
// aa522a25__beta_plus_integrand().
//##############################################################################

double
aa522a25__beta_plus_integrand(
  double d_E,
  void *p_data
)
{

  double d_result;

  aa522a25_function_data *p_function_data = ( aa522a25_function_data * ) p_data;

  d_result =
    pow( -p_function_data->dQ - d_E, p_function_data->iNeutrinoExponent );

  d_result *=
    gsl_pow_2( d_E ) *
    nnt::compute_one_minus_fermi_dirac_factor(
      d_E / p_function_data->dkT,
      -p_function_data->dEtaF
    );

  if( p_function_data->dMunuekT == GSL_NEGINF )
    return d_result;
  else
    return
      d_result *
      nnt::compute_one_minus_fermi_dirac_factor(
        -p_function_data->dQ / p_function_data->dkT -
           d_E / p_function_data->dkT,
        p_function_data->dMunuekT
      );

}

//##############################################################################
// aa522a25__positron_capture_integrand().
//##############################################################################

double
aa522a25__positron_capture_integrand(
  double d_E,
  void *p_data
)
{

  double d_result;

  aa522a25_function_data *p_function_data = ( aa522a25_function_data * ) p_data;

  d_result =
    pow( d_E + p_function_data->dQ, p_function_data->iNeutrinoExponent );

  d_result *=
    gsl_pow_2( d_E ) *
    nnt::compute_fermi_dirac_factor(
      d_E / p_function_data->dkT,
      -p_function_data->dEtaF
    );

  if( p_function_data->dMunuekT == GSL_NEGINF )
    return d_result;
  else
    return
      d_result *
      nnt::compute_one_minus_fermi_dirac_factor(
        d_E / p_function_data->dkT + p_function_data->dQ / p_function_data->dkT,
        -p_function_data->dMunuekT
      );

}

//##############################################################################
// aa522a25__beta_minus_integrand().
//##############################################################################

double
aa522a25__beta_minus_integrand(
  double d_E,
  void *p_data
)
{

  double d_result;

  aa522a25_function_data *p_function_data = ( aa522a25_function_data * ) p_data;

  d_result =
    pow( p_function_data->dQ - d_E, p_function_data->iNeutrinoExponent );

  d_result *=
    gsl_pow_2( d_E ) *
    nnt::compute_one_minus_fermi_dirac_factor(
      d_E / p_function_data->dkT,
      p_function_data->dEtaF
    );

  if( p_function_data->dMunuekT == GSL_NEGINF )
    return d_result;
  else
    return
      d_result *
      nnt::compute_one_minus_fermi_dirac_factor(
        p_function_data->dQ / p_function_data->dkT - d_E / p_function_data->dkT,
        -p_function_data->dMunuekT
      );

}

//##############################################################################
// compute_aa522a25_integral().
//##############################################################################

double
compute_aa522a25_integral(
  Libnucnet__Reaction * p_reaction,
  Libnucnet__Net * p_net,
  double d_electron_mass,
  double d_t9,
  double d_eta_F,
  double d_mu_nue_kT,
  int i_neutrino_exponent
)
{

  double d_result = 0, d_error, d_limit;
  gsl_integration_workspace * p_work_space =
    gsl_integration_workspace_alloc( (size_t) i_AA522A25_WORKSPACE );
  gsl_function F;
  aa522a25_function_data function_data;

  function_data.dkT = nnt::compute_kT_in_MeV( d_t9 );

  function_data.dEtaF = d_eta_F;

  function_data.dMunuekT = d_mu_nue_kT;

  function_data.iNeutrinoExponent = i_neutrino_exponent;

  function_data.dQ =
    nnt::compute_reaction_nuclear_Qvalue(
      p_net,
      p_reaction,
      d_electron_mass
    );

  F.params = &function_data;

  if( nnt::is_electron_capture_reaction( p_reaction ) )
  {

    F.function = &aa522a25__electron_capture_integrand;

    function_data.dQ *= -1;

    d_limit =
      GSL_MAX_DBL(
        function_data.dQ,
        d_electron_mass
      );

    if( ( d_limit / function_data.dkT - d_eta_F ) < 4 )
    {
      gsl_integration_qagiu(
        &F,
        d_limit,
        d_AA522A25_EPSABS,
        d_AA522A25_EPSREL,
        (size_t) i_AA522A25_LIMIT,
        p_work_space,
        &d_result,
        &d_error
      );
    }
    else
    {
      gsl_integration_qags(
        &F,
        d_limit,
        d_limit + 24. * function_data.dkT,
        d_AA522A25_EPSABS,
        d_AA522A25_EPSREL,
        (size_t) i_AA522A25_LIMIT,
        p_work_space,
        &d_result,
        &d_error
      );
    }

  }
  else if( nnt::is_beta_plus_reaction( p_reaction ) )
  {

    F.function = &aa522a25__beta_plus_integrand;

    function_data.dQ *= -1;

    if( 
      d_electron_mass > -function_data.dQ
    )
      d_result = 0.;
    else
      gsl_integration_qags(
        &F,
        d_electron_mass,
        -function_data.dQ,
        d_AA522A25_EPSABS,
        d_AA522A25_EPSREL,
        (size_t) i_AA522A25_LIMIT,
        p_work_space,
        &d_result,
        &d_error
      );

  }
  else if( nnt::is_positron_capture_reaction( p_reaction ) )
  {

    F.function = &aa522a25__positron_capture_integrand;

    gsl_integration_qagiu(
      &F,
      GSL_MAX_DBL( -function_data.dQ, d_electron_mass ),
      d_AA522A25_EPSABS,
      d_AA522A25_EPSREL,
      (size_t) i_AA522A25_LIMIT,
      p_work_space,
      &d_result,
      &d_error
    );

  }
  else if( nnt::is_beta_minus_reaction( p_reaction ) )
  {

    F.function = &aa522a25__beta_minus_integrand;

    if( 
      d_electron_mass >
      function_data.dQ
    )
      d_result = 0.;
    else
      gsl_integration_qags(
        &F,
        d_electron_mass,
        function_data.dQ,
        d_AA522A25_EPSABS,
        d_AA522A25_EPSREL,
        (size_t) i_AA522A25_LIMIT,
        p_work_space,
        &d_result,
        &d_error
      );

  }
  else
  {
    std::cerr <<
      "No such rate for AA522A25 reaction rate function." << std::endl;
    exit( EXIT_FAILURE );
  }

  gsl_integration_workspace_free( p_work_space );

  return d_result;

}

//##############################################################################
// compute_aa522a25_rate().
//##############################################################################

double
compute_aa522a25_rate(
  Libnucnet__Reaction * p_reaction,
  double d_t9,
  aa522a25_work * p_aa522a25_work
)
{

  double d_result =
    compute_aa522a25_integral(
      p_reaction,
      p_aa522a25_work->pNet,
      p_aa522a25_work->dElectronMass,
      d_t9,
      p_aa522a25_work->dEtaF,
      p_aa522a25_work->dMuNuekT,
      2
    );

  if(
      !Libnucnet__Reaction__getUserRateFunctionProperty(
        p_reaction,
        S_AA522A25_B_FACTOR,
        NULL,
        NULL
      )
  )
  {
    std::cerr <<
      "No such user rate property." << std::endl;
    exit( EXIT_FAILURE );
  }

  return
    d_result *
    atof(
      Libnucnet__Reaction__getUserRateFunctionProperty(
        p_reaction,
        S_AA522A25_B_FACTOR,
        NULL,
        NULL
      )
    ) *
    M_LN2 /
    (
      gsl_pow_5( p_aa522a25_work->dElectronMass )
      *
      d_AA522A25_WEAK_K
    );

}

//##############################################################################
// aa522a25_registerer().
//##############################################################################

class aa522a25_registerer : public base::rate_registerer
{

  public:
    aa522a25_registerer( v_map_t& v_map ) : base::rate_registerer()
    {
      b_update = v_map[S_AA522A25_UPDATE_NET].as<bool>();

      s_aa522a25_nuc_xpath =
        my_program_options.composeOptionStringFromVector( 
          v_map, S_AA522A25_NUC_XPATH
        );
    }

    void register_rates( Libnucnet__Net * p_net )
    {

      if( b_update )
      {
        aa522a25_update_net( p_net, s_aa522a25_nuc_xpath.c_str() );
      }

      Libnucnet__Reac__registerUserRateFunction(
        Libnucnet__Net__getReac( p_net ),
        S_AA522A25,
        (Libnucnet__Reaction__userRateFunction)
          compute_aa522a25_rate
      );

      Libnucnet__Reac__setUserRateFunctionDataDeallocator(
        Libnucnet__Net__getReac( p_net ),
        S_AA522A25,
        (Libnucnet__Reaction__user_rate_function_data_deallocator) free
      );

    }

    void operator()( Libnucnet__Net * p_net )
    {
      register_rates( p_net );
    }

    void data_updater( nnt::Zone& zone )
    {

      aa522a25_work *p_aa522a25_work;

      Libstatmech__Fermion * p_electron;
      double d_mue_kT, d_eta_F, d_mu_nue_kT;

      p_electron =
        Libstatmech__Fermion__new(
          nnt::s_ELECTRON,
          nnt::d_ELECTRON_MASS_IN_MEV,
          2,
          -1
        );

      d_mue_kT =
        user::compute_thermo_quantity(
          zone,
          nnt::s_CHEMICAL_POTENTIAL_KT,
          nnt::s_ELECTRON
        );

      d_mu_nue_kT =
        user::compute_thermo_quantity(
          zone,
          nnt::s_CHEMICAL_POTENTIAL_KT,
          nnt::s_NEUTRINO_E
        ) ;

      d_eta_F =
        d_mue_kT
        +
        Libstatmech__Fermion__getRestMass( p_electron ) /
        nnt::compute_kT_in_MeV( zone.getProperty<double>( nnt::s_T9 ) );

      p_aa522a25_work = ( aa522a25_work * ) malloc( sizeof( aa522a25_work ) );

      if( !p_aa522a25_work )
      {
        std::cerr << "Couldn't allocate memory for aa522a25_work structure.\n";
        exit( EXIT_FAILURE );
      }

      p_aa522a25_work->pNet = Libnucnet__Zone__getNet( zone.getNucnetZone() );
      p_aa522a25_work->dRhoe = 
        zone.getProperty<double>( nnt::s_RHO )
        * Libnucnet__Zone__computeZMoment( zone.getNucnetZone(), 1 );

      p_aa522a25_work->dElectronMass =
        Libstatmech__Fermion__getRestMass( p_electron );

      p_aa522a25_work->dEtaF = d_eta_F;

      p_aa522a25_work->dMuNuekT = d_mu_nue_kT;

      Libnucnet__Zone__updateDataForUserRateFunction(
        zone.getNucnetZone(),
        S_AA522A25,
        p_aa522a25_work
      );

      Libstatmech__Fermion__free( p_electron );

    }

  private:
    program_options my_program_options;
    bool b_update;
    std::string s_aa522a25_nuc_xpath;

};

//##############################################################################
// aa522a25_registerer_options().
//##############################################################################

class aa522a25_registerer_options : public base::rate_registerer_options
{

  public:
    aa522a25_registerer_options() : base::rate_registerer_options(){}

    std::string
    getExample()
    {
      return "";
    }

    void
    operator()( po::options_description& rate_registerer )
    {

      try
      {

        rate_registerer.add_options()

        (
          S_AA522A25_UPDATE_NET,
          po::value<bool>()->default_value( false, "false" ),
          "Update net with aa522a25 data"
        )

        // Option for value of XPath to select nuclei
        (
          S_AA522A25_NUC_XPATH,
          po::value<std::vector<std::string> >()->multitoken()->composing(),
          "Nuclear XPath for aa522a25 net update (default: all species)"
        )

        ;

// Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }

    }

};

}  // namespace detail

}  // namespace wn_user

#endif // WN_AA522A25_REGISTERER_HPP
