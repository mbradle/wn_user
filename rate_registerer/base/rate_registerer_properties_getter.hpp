////////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2019 Clemson University.
//
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this software; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
// USA
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//!
//! \file rate_registerer_properties_getter.hpp
//! \brief A file to define rate registration.
//!
////////////////////////////////////////////////////////////////////////////////

#ifndef NNP_RATE_REGISTERER_PROPERTIES_GETTER_BASE_HPP
#define NNP_RATE_REGISTERER_PROPERTIES_GETTER_BASE_HPP

#include <limits>

/**
 * @brief A namespace for user-defined helper functions.
 */
namespace wn_user
{

namespace base
{

void
get_property_map(
  const char * s_name,
  const char * s_tag1,
  const char * s_tag2,
  const char * s_value,
  std::map<std::string, std::string> * p_map
)
{ 
  (*p_map)[s_tag1] = std::string( s_value );
}

//##############################################################################
// rate_registerer_properties_getter().
//##############################################################################

class rate_registerer_properties_getter
{

  public:
    rate_registerer_properties_getter(){}

  template <class T>
  std::vector<T>
  getPropertyVector(
    Libnucnet__Reaction * p_reaction,
    const char * s_property
  )
  {

    typedef std::map<std::string, std::string> prop_map_t;
    prop_map_t my_prop_map;

    Libnucnet__Reaction__iterateUserRateFunctionProperties(
      p_reaction,
      s_property,
      NULL,
      NULL,
      (Libnucnet__Reaction__user_rate_property_iterate_function)
        get_property_map,
      &my_prop_map
    );

    std::vector<T> v( my_prop_map.size() );

    BOOST_FOREACH( prop_map_t::value_type &t, my_prop_map )
    {
      if( t.second == "inf" )
      {
        v[boost::lexical_cast<size_t>( t.first )] =
          std::numeric_limits<T>::infinity();
      }
      else if( t.second == "-inf" )
      {
        v[boost::lexical_cast<size_t>( t.first )] =
          -std::numeric_limits<T>::infinity();
      }
      else
      {
        v[boost::lexical_cast<size_t>( t.first )] =
          boost::lexical_cast<T>( t.second );
      }
    }

    return v;
  }

};

} // namespace base

} // namespace wn_user

#endif // NNP_RATE_REGISTERER_PROPERTIES_GETTER_BASE_HPP
