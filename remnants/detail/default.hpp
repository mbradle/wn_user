////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header for standard remnants code.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#include "my_global_types.h"
#include <boost/random/uniform_01.hpp>

#include "nnt/iter.h"

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef WN_REMNANTS_DETAIL_HPP
#define WN_REMNANTS_DETAIL_HPP

//##############################################################################
// Strings.
//##############################################################################

#define S_IA_FRACTION               "Ia_fraction"
#define S_IA_TIME                   "Ia_time"
#define S_NRLEE_FRACTION            "nrlee_fraction"
#define S_NS_MERGER_TIME            "ns_merger_time"
#define S_NS_MERGER_FRACTION        "ns_merger_fraction"
#define S_IA_FILE                   "Ia_file"
#define S_NS_MERGER_FILE            "ns_merger_file"

namespace wn_user
{

namespace detail
{

class remnants
{

  public:
    remnants(){}
    remnants( v_map_t& v_map )
    {
      d_Ia_fraction = v_map[S_IA_FRACTION].as<double>();
      d_nrlee_fraction = v_map[S_NRLEE_FRACTION].as<double>();
      d_Ia_time = v_map[S_IA_TIME].as<double>() * D_YEAR;
      d_NS_merger_time = v_map[S_NS_MERGER_TIME].as<double>() * D_YEAR;
      d_NS_merger_fraction = v_map[S_NS_MERGER_FRACTION].as<double>();
    
      if( v_map.count(S_IA_FILE) )
      {
        s_Ia_file = v_map[S_IA_FILE].as<std::string>();
    
        std::ofstream my_file;
        my_file.open( s_Ia_file.c_str() );
        my_file.close();
      }
    
      if( v_map.count(S_NS_MERGER_FILE) )
      {
        s_NS_merger_file = v_map[S_NS_MERGER_FILE].as<std::string>();
    
        std::ofstream my_file;
        my_file.open( s_NS_merger_file.c_str() );
        my_file.close();
      }
    }

    std::string
    getNewStatus( wn_user::Star& star, double remnant_mass, boost::mt19937& gen )
    {

      std::string status;
      if( star.getProperty( S_CURRENT_MASS ) < 10 )
      {
	boost::random::uniform_01<double> dist;
        if( dist(gen) > d_nrlee_fraction )
        {
          status = S_WHITE_DWARF;
        }
        else
        {
          status = S_C_WHITE_DWARF;
        }
      }
      else
      {
        if( remnant_mass < 2 )
          status = S_NEUTRON_STAR;
        else
          status = S_BLACK_HOLE;
      }
      return status;
    }

    void
    updateStarInSingleStarSystem(
      StarSystem& star_system,
      wn_user::Star& star,
      double remnant_mass,
      boost::mt19937& gen
    )
    {
      star.updateStatus( getNewStatus( star, remnant_mass, gen ) );
      star.updateProperty( S_END_TIME, GSL_POSINF );
      star.updateProperty( S_CURRENT_MASS, remnant_mass ); 
    
      star_system.popTopStar();
      star_system.addStar( star );
    }

    void
    updateStarInMultiStarSystem(
      StarSystem& star_system,
      std::vector<wn_user::Star>& v,
      double remnant_mass,
      boost::mt19937& gen
    )
    {
    
      boost::uniform_01<double> dist;
    
      std::string status = getNewStatus( v[0], remnant_mass, gen );
      if( status == S_WHITE_DWARF )
      {
        if( v[1].getStatus() == S_STAR )
        {  
          v[0].updateStatus( status );
          if( dist( gen ) < d_Ia_fraction )
          {
            v[0].updateProperty( S_END_TIME, d_Ia_time );
          }
          else
          {
            v[0].updateProperty( S_END_TIME, GSL_POSINF );
          }
          v[0].updateProperty( S_CURRENT_MASS, remnant_mass ); 
    
          star_system.popTopStar();
          star_system.addStar( v[0] );
        }
        else
        {
          v[0].updateStatus( status );
          v[0].updateProperty( S_CURRENT_MASS, remnant_mass );
          BOOST_FOREACH( wn_user::Star s, v )
          {
            star_system.popTopStar();
          }
          BOOST_FOREACH( wn_user::Star s, v )
          {
            s.updateProperty( S_END_TIME, GSL_POSINF );
            star_system.addStar( s );
          }
        }
      }  
      else if( status == S_C_WHITE_DWARF )
      {
        if( v[1].getStatus() == S_STAR )
        {  
          v[0].updateStatus( status );
          if( dist( gen ) < d_Ia_fraction )
          {
            v[0].updateProperty( S_END_TIME, d_Ia_time );
          }
          else
          {
            v[0].updateProperty( S_END_TIME, GSL_POSINF );
          }
          v[0].updateProperty( S_CURRENT_MASS, remnant_mass ); 
    
          star_system.popTopStar();
          star_system.addStar( v[0] );
        }
      }
      else
      {
        if( v[1].getStatus() == S_STAR )
        {  
          v[0].updateStatus( status );
          v[0].updateProperty( S_END_TIME, GSL_POSINF );
          v[0].updateProperty( S_CURRENT_MASS, remnant_mass ); 
    
          star_system.popTopStar();
          star_system.addStar( v[0] );
        }
        else if(
          status == S_NEUTRON_STAR && v[1].getStatus() == S_NEUTRON_STAR
        )
        {
          double d_merge_time = GSL_POSINF;
          v[0].updateStatus( status );
          v[0].updateProperty( S_CURRENT_MASS, remnant_mass );
          if( dist( gen ) < d_NS_merger_fraction )
          {
            d_merge_time = d_NS_merger_time;
          }
          for( size_t i = 0; i < 2; i++ )
          {
            star_system.popTopStar();
          }
          for( size_t i = 0; i < 2; i++ )
          {
            v[i].updateProperty( S_END_TIME, d_merge_time );
            star_system.addStar( v[i] );
          }
        }
        else
        {
          v[0].updateStatus( status );
          v[0].updateProperty( S_CURRENT_MASS, remnant_mass );
          BOOST_FOREACH( wn_user::Star s, v )
          {
            star_system.popTopStar();
          }
          BOOST_FOREACH( wn_user::Star s, v )
          {
            s.updateProperty( S_END_TIME, GSL_POSINF );
            star_system.addStar( s );
          }
        }
      }
    }

    void
    updateSystemForTypeIaSupernova(StarSystem& star_system, double time )
    {
    
      if( !s_Ia_file.empty() )
      {
    
        wn_user::Star s = star_system.getTopStar();
    
        std::ofstream my_file;
    
        my_file.open( s_Ia_file.c_str(), std::ios::app );
    
        my_file << time << "  " <<
          s.getProperty( S_CURRENT_MASS ) << "  " <<
	  s.getProperty( nnt::s_GRID_X ) << "  " <<
	  s.getProperty( nnt::s_GRID_Y ) << "  " <<
          s.getProperty( nnt::s_GRID_Z ) << std::endl;
    
        my_file.close();
    
      }
    
      star_system.popTopStar();
    
    }

    void
    updateSystemForNeutronStarMerger(
      StarSystem& star_system,
      std::vector<wn_user::Star>& v,
      double remnant_mass,
      double time
    )
    {
    
      if( !s_NS_merger_file.empty() )
      {
    
        std::ofstream my_file;
    
        my_file.open( s_NS_merger_file.c_str(), std::ios::app );
    
        my_file << time << "  " <<
          v[0].getProperty( S_ORIGINAL_MASS ) << "  " <<
          v[1].getProperty( S_ORIGINAL_MASS ) << "  " <<
          v[0].getProperty( S_CURRENT_MASS )  << "  " <<
	  v[1].getProperty( S_CURRENT_MASS )  << "  " <<
          v[0].getProperty<int>( nnt::s_GRID_X ) << "  " <<
          v[0].getProperty<int>( nnt::s_GRID_Y ) << "  " <<
          v[0].getProperty<int>( nnt::s_GRID_Z ) << std::endl;
    
        my_file.close();
    
      }
    
      star_system.popTopStar();
    
      wn_user::Star star = star_system.getTopStar();
      star_system.popTopStar();
      star.updateStatus( S_BLACK_HOLE );
      star.updateProperty( S_CURRENT_MASS, remnant_mass );
      star.updateProperty( S_END_TIME, GSL_POSINF );
    
      star_system.addStar( star );
      
    }

    void
    updateStellarSystem(
      StarSystem& star_system,
      double remnant_mass,
      boost::mt19937 gen,
      double time
    )
    {
    
      std::vector<wn_user::Star> v = star_system.getStarVector();
    
      if( v.size() == 1 )
      {
        updateStarInSingleStarSystem( star_system, v[0], remnant_mass, gen );
      }
      else
      {
        if( v[0].getStatus() == S_STAR )
        {
          updateStarInMultiStarSystem( star_system, v, remnant_mass, gen );
        }
        else if( v[0].getStatus() == S_WHITE_DWARF )
        {
          updateSystemForTypeIaSupernova( star_system, time );
        }
        else if( v[0].getStatus() == S_C_WHITE_DWARF )
        {
          updateSystemForTypeIaSupernova( star_system, time );
        }
        else if( v[0].getStatus() == S_NEUTRON_STAR )
        {
          updateSystemForNeutronStarMerger(
            star_system, v, remnant_mass, time
          );
        }
      }
    
    }

    void
    updateStellarSystemHeap(
      star_system_heap_t& heap,
      StarSystem& star_system,
      double t_end
    )
    {
    
      if( star_system.getTopStar().getProperty( S_END_TIME )< t_end )
      {
        heap.push( star_system );
      }
    
    }

  private:
    double d_Ia_fraction, d_Ia_time, d_nrlee_fraction;
    double d_NS_merger_fraction, d_NS_merger_time;
    std::string s_Ia_file, s_NS_merger_file;

};

//##############################################################################
// remnants_options().
//##############################################################################

class remnants_options
{

  public:
    remnants_options() {}

    std::string
    getExample()
    { return ""; }

    void
    get( po::options_description& remnants )
    {
    
      try
      {
    
        remnants.add_options()
    
        // Option for Ia fraction
        ( S_IA_FRACTION, po::value<double>()->default_value( 0.05, "0.05" ),
          "Fraction of star/white dwarf systems that will become Ias" )
        
        // Option for NRLEE fraction
        ( S_NRLEE_FRACTION, po::value<double>()->default_value( 0.05, "0.05" ),
          "Fraction of Ias that are NRLEEs" )
        
        // Option for Ia timescale
        ( S_IA_TIME, po::value<double>()->default_value( 1.e8, "1.e8" ),
          "Timescale (years) for Ia explosion on star/white dwarf system" )
        
        // Option for Ia timescale
        ( S_IA_FILE, po::value<std::string>(),
          "Name of text file to record Ia events (default: not set)" )
        
        // Option for NS-NS merger timescale
        (
          S_NS_MERGER_FRACTION,
          po::value<double>()->default_value( 0.01, "0.01" ),
          "Fraction of neutron star/neutron star systems that will merge"
        )
        
        (
          S_NS_MERGER_TIME,
          po::value<double>()->default_value( 2.e8, "2.e8" ),
          "Timescale (years) for neutron star/neutron star merger"
        )
        
        // Option for Ia timescale
        ( S_NS_MERGER_FILE, po::value<std::string>(),
          "Name of text file to record ns-ns merger events (default: not set)" )

        ;
        
      // Add checks on input.
      
      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // namespace detail

} // namespace wn_user

#endif  // WN_REMNANTS_DETAIL_HPP
