////////////////////////////////////////////////////////////////////////////////
//  
// This file was originally written by Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
////////////////////////////////////////////////////////////////////////////////
 
////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Header for standard remnants code.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Check if already defined.
//##############################################################################

#ifndef WN_REMNANTS_HPP
#define WN_REMNANTS_HPP

namespace wn_user
{

//##############################################################################
// remnants().
//##############################################################################

class remnants : public detail::remnants
{

  public:
    remnants() : detail::remnants() {}
    remnants( v_map_t& v_map ) : detail::remnants( v_map ) {}

};

//##############################################################################
// remnants_options().
//##############################################################################

class remnants_options : public detail::remnants_options
{

  public:
    remnants_options() : detail::remnants_options(){}

    std::string
    getExample()
    {
      return detail::remnants_options::getExample();
    }

    void
    get( options_map& o_map )
    {

      try
      {

       po::options_description remnants( "\nRemnants Options" );

       detail::remnants_options::get( remnants );

       o_map.insert(
         std::make_pair<std::string, options_struct>(
           "remnants",
           options_struct( remnants, getExample() )
         )
       );

      // Add checks on input.

      }
      catch( std::exception& e )
      {
        std::cerr << "Error: " << e.what() << "\n";
        exit( EXIT_FAILURE );
      }
      catch(...)
      {
        std::cerr << "Exception of unknown type!\n";
        exit( EXIT_FAILURE );
      }
    }

};

} // wn_user

#endif  // WN_REMNANTS_HPP
